let mix = require('laravel-mix')

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.sass('resources/assets/sass/frontend/app.scss', 'public/css/frontend.css')
  .sass('resources/assets/sass/backend/app.scss', 'public/css/backend.css')
  .babel(
  [
    'resources/assets/js/frontend/indicatorRecords.js',
  ],
    'public/js/indicatorRecords.js'
  )
  .js([
    'resources/assets/js/frontend/app.js',
    'resources/assets/js/plugins.js',
    'node_modules/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js',
    'node_modules/bootstrap-datepicker/dist/locales/bootstrap-datepicker.pt-BR.min.js',
    'resources/assets/js/frontend/ceap.js'
  ], 'public/js/frontend.js')
  .js([
    'resources/assets/js/backend/before.js',
    'resources/assets/js/backend/app.js',
    'resources/assets/js/backend/after.js'
  ], 'public/js/backend.js')
  .copy('resources/lang/pt-BR/datatable.json', 'public/datatable/pt-BR.json')
  .copy('resources/lang/en/datatable.json', 'public/datatable/en.json')
  .copy('resources/lang/es/datatable.json', 'public/datatable/es.json')

let WebpackRTLPlugin = require('webpack-rtl-plugin')
let WebpackShellPlugin = require('webpack-shell-plugin')
mix.webpackConfig({
  plugins: [
    new WebpackRTLPlugin('/css/[name].rtl.css'),
    new WebpackShellPlugin({onBuildStart: ['php artisan lang:js --quiet'], onBuildEnd: []})
  ],
  node: {
    fs: 'empty'
  }
})

if (mix.inProduction()) {
  mix.version()
} else {
  mix
    .browserSync({
      proxy: 'ceap.test',
      browser: 'google chrome'
    })
    .options({
      uglify: {
        compress: false
      }
    })
  if (process.env.npm_lifecycle_event !== 'hot') {
    mix.version()
  }
}
