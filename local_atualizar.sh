#!/bin/bash

composer install
npm install

composer dump-autoload
composer run-script dev-update

php artisan clear-compiled
php artisan cache:clear
php artisan config:clear
php artisan debugbar:clear
php artisan route:clear
php artisan view:clear

npm run development
