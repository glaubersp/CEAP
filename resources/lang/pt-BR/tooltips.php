<?php

return [
    'actions'    =>
        [
            'goal'            => 'Escolha um objetivo para a ação a ser criada',
            'indicator'       => 'O indicador já é selecionado assim que você escolhe um objetivo',
            'name'            => 'Digite um nome para a ação',
            'details'         => 'Detalhe a ação com o maior número de fatos',
            'users'           => 'Escolha os responsáveis pela ação',
            'startDate'       => 'Escolha a data de ínicio prevista para a ação',
            'endDate'         => 'Escolha a data de término previsto para a ação',
            'evidences'       =>
                [
                    'title'    => 'Digite um nome para a evidência',
                    'details'  => 'Detalhe as evidências coletadas',
                    'fileName' => 'Anexe arquivos relacionados a essa evidência',
                ],
            'evaluations'     =>
                [
                    'evaluation'    => 'Escolha se a ação foi implementada ou não',
                    'result'        => 'Avalie com um determinado número de estrelas (1 - Ruim ou 5 - Ótimo)',
                    'justification' => 'Descreva sucintamente como foi executada a ação',
                ],
            'evaluation_term' => 'Selecione o período para essa ação',
        ],
    'agreements' =>
        [
            'title'              => 'Digite um nome para o combinado',
            'details'            => 'Detalhe o combinado',
            'topicTags'          => 'Selecione os assuntos relacionados a esse combinado',
            'roles'              => 'Escolha os envolvidos pelo combinado',
            'actions'            => 'Escolha os envolvidos pelo combinado',
            'evaluationCycle'    => 'Selecione o período da avaliação do combinado',
            'monitoringStrategy' => 'Selecione a forma de monitoramento do combinado',
        ],
    'goals'      =>
        [
            'indicator'     => 'Escolha um indicador para o objetivo a ser criado',
            'description'   => 'Descrição do indicador selecionado acima',
            'name'          => 'Digite um nome para o objetivo',
            'justification' => 'Descreva sucintamente o problema',
        ],
    'indicators' =>
        [
            'value' => 'Digite o valor do indicador',
            'group' => 'Selecione o grupo',
            'modal' =>
                [
                    'group'     =>
                        [
                            'name'        => 'Digite um nome para o grupo de indicadores',
                            'description' => 'Adicione uma breve descrição do indicador',
                            'type'        => 'Selecione o tipo do grupo do indicador',
                            'cycle'       => 'Selecione o período da avaliação do indicador',
                            'startDate'   => 'Escolha a data de ínicio do acompanhamento do indicador',
                        ],
                    'indicator' =>
                        [
                            'name'        => 'Digite um nome para o indicador',
                            'description' => 'Adicione uma breve descrição do indicador',
                        ],
                    'ceap'      =>
                        [
                            'startDate' => 'Escolha a data de ínicio do acompanhamento do indicador',
                            'cycle'     => 'Selecione o ciclo escolar',
                        ],
                    'rooms'     =>
                        [
                            'year'      => 'Selecione o ano letivo',
                            'startDate' => 'Escolha a data de ínicio do acompanhamento do indicador',
                            'indicator' => 'Escolha o indicador',
                        ],
                    'school'    =>
                        [
                            'startDate' => 'Escolha a data de ínicio do acompanhamento do indicador',
                        ],
                ],
        ],
];
