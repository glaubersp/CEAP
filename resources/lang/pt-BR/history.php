<?php

return [
    'backend' =>
        [
            'none'            => 'Não há histórico recente.',
            'none_for_type'   => 'Não há histórico para este tipo.',
            'none_for_entity' => 'Não há histórico para este(a) :entity.',
            'recent_history'  => 'Histórico Recente',
            'roles'           =>
                [
                    'created' => 'papel criado',
                    'deleted' => 'papel apagado',
                    'updated' => 'papel atualizado',
                ],
            'users'           =>
                [
                    'changed_password'    => 'senha alterada para o usuário',
                    'confirmed'           => 'usuário confirmado',
                    'created'             => 'usuário criado',
                    'deactivated'         => 'usuário desativado',
                    'deleted'             => 'usuário apagado',
                    'deleted_social'      => 'deleted social account',
                    'permanently_deleted' => 'usuário apagado permanentemente',
                    'updated'             => 'usuário atualizado',
                    'unconfirmed'         => 'usurio pendente',
                    'reactivated'         => 'usuário reativado',
                    'restored'            => 'usuário restaurado',
                ],
        ],
];
