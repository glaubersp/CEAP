<?php

return [
    'app'                    =>
        [
            'name'        => 'Sistema de Acompanhamento de Ações Escolares',
            'description' => 'Melhorando as condições de Ensino, Aprendizagem e Participação na Escola',
            'about'       => 'Sobre o projeto',
        ],
    'login'                  =>
        [
            'name'            => 'Entrar',
            'email'           => 'E-mail',
            'password'        => 'Senha',
            'remember'        => 'Lembrar-me',
            'forgot_password' => 'Esqueceu Sua Senha?',
            'user'            => 'Usuário',
        ],
    'button'                 =>
        [
            'login'                     => 'Entrar',
            'show'                      => 'Visualizar',
            'evidences'                 => 'Evidências',
            'add'                       => 'Adicionar',
            'create'                    => 'Criar',
            'edit'                      => 'Editar',
            'cancel'                    => 'Cancelar',
            'save'                      => 'Salvar',
            'save_action_add_evidences' => 'Salvar e Coletar Evidência',
            'collect'                   => 'Coletar',
            'save_goal_add_action'      => 'Salvar e Adicionar Ação',
            'add_action'                => 'Adicionar Ação',
            'delete'                    => 'Apagar',
            'plan'                      => 'Avaliar',
            'create_graph'              => 'Gerar Gráfico',
            'define_goals'              => 'Adicionar Objetivo',
            'add_register_value'        => 'Adicionar Registro',
            'update_register_value'     => 'Atualizar  Registro',
            'register_history'          => 'Histórico de Registros',
            'print'                     => 'Imprimir',
            'back'                      => 'Voltar',
            'trash'                     => 'Arquivar',
            'restore'                   => 'Restaurar',
        ],
    'menu'                   =>
        [
            'home'       => 'Painel Geral',
            'help'       => 'Ajuda',
            'planning'   =>
                [
                    'title'     => 'Adicionar',
                    'action'    => 'Ação',
                    'goal'      => 'Objetivo',
                    'indicator' => 'Indicador',
                ],
            'monitoring' =>
                [
                    'title'            => 'Acompanhar',
                    'monitoring_chart' => 'Painel Geral',
                    'actions'          => 'Ações',
                    'goals'            => 'Objetivos',
                    'indicators'       => 'Indicadores',
                ],
            'agreements' =>
                [
                    'title'  => 'Combinados',
                    'create' => 'Criar',
                    'list'   => 'Listar',
                ],
            'management' =>
                [
                    'title' => 'Gerenciar',
                    'soon'  => 'Em breve',
                ],
        ],
    'home'                   =>
        [
            'indicators' => 'Indicadores Cadastrados',
            'goals'      => 'Objetivos Cadastrados',
            'actions'    => 'Ações Cadastradas',
            'agreements' => 'Combinados Cadastrados',
        ],
    'table'                  =>
        [
            'end_date'          => 'Expira',
            'updated_at'        => 'Atualização',
            'priority'          => 'Prioridade',
            'interactions'      => 'Interações',
            'goal'              => 'Objetivo',
            'indicator'         => 'Indicador',
            'description'       => 'Descrição',
            'print_at'          => 'Impresso em ',
            'user'              => 'Usuário',
            'year'              => 'Ano Letivo',
            'grade'             => 'Série',
            'room'              => 'Sala',
            'indicator_value'   => 'Valor do Indicador',
            'trashed_registers' => 'Incluir registros arquivados',
        ],
    'actions'                =>
        [
            'create'          =>
                [
                    'panel_title' => 'Adicionar nova Ação',
                ],
            'edit'            =>
                [
                    'panel_title' => 'Editar Ação',
                ],
            'show'            =>
                [
                    'panel_title'         => 'Exibir Ação',
                    'related_actions'     => 'Ações Vinculadas:',
                    'no_evaluation'       => 'Essa ação ainda não foi avaliada. Para avaliar, volte para \'Acompanhar Ações\' e aperte o botão \'Avaliar\'.',
                    'no_evidence'         => 'Essa ação ainda não possui evidência. Para adicionar evidências, clique no botão abaixo para ir para \'Coletar Evidências\'.',
                    'no_evaluation_label' => 'Sem Avaliação',
                    'no_evidence_label'   => 'Sem Evidências',
                ],
            'index'           =>
                [
                    'panel_title' => 'Acompanhar Ações',
                ],
            'goal'            =>
                [
                    'label'       => 'Objetivo:',
                    'placeholder' => 'Selecione o objetivo',
                ],
            'indicator'       =>
                [
                    'label'       => 'Indicador:',
                    'placeholder' => 'Selecione o indicador',
                ],
            'name'            =>
                [
                    'label'       => 'Ação:',
                    'placeholder' => 'Digite um nome para a ação',
                ],
            'details'         =>
                [
                    'label'       => 'Detalhamento:',
                    'placeholder' => 'Detalhe a ação',
                ],
            'users'           =>
                [
                    'label'       => 'Responsáveis:',
                    'placeholder' => 'Selecione o(s) responsável(is)',
                ],
            'start_date'      =>
                [
                    'label'       => 'Início Previsto:',
                    'placeholder' => 'DD/MM/AAAA',
                ],
            'end_date'        =>
                [
                    'label'       => 'Término Previsto:',
                    'placeholder' => 'DD/MM/AAAA',
                ],
            'evidences'       =>
                [
                    'panel_title' => 'Coletar Evidências',
                    'evidence'    => 'Evidências',
                    'file'        => 'Nome do Arquivo',
                    'modal'       =>
                        [
                            'title'               => 'Título:',
                            'placeholder_title'   => 'Digite um título para a evidência',
                            'details'             => 'Detalhamento:',
                            'placeholder_details' => 'Detalhe a evidência coletada',
                            'attach'              => 'Anexar arquivo:',
                        ],
                    'show'        =>
                        [
                            'title' => 'Evidência da Ação: ',
                            'files' => 'Arquivos:',
                        ],
                    'edit'        =>
                        [
                            'title' => 'Editar Evidência',
                        ],
                ],
            'evaluations'     =>
                [
                    'panel_title'                      => 'Avaliar Ações',
                    'evaluate'                         => 'Avaliação',
                    'star'                             => 'Estrela',
                    'stars'                            => 'Estrelas',
                    'modal'                            =>
                        [
                            'title'              => 'Avaliar Ação',
                            'evaluation'         => 'Implementação:',
                            'evaluation_options' =>
                                [
                                    'Unset'                 => 'Não avaliado',
                                    'Not implemented'       => 'Não implementado',
                                    'Partially implemented' => 'Implementado parcialmente',
                                    'Fully implemented'     => 'Implementado totalmente',
                                ],
                            'result'             => 'Resultado:',
                            'choose_star'        => 'Avalie escolhendo de 1 a 5 estrelas',
                            'stars_rating'       =>
                                [
                                    '5stars' => '5',
                                    '4stars' => '4',
                                    '3stars' => '3',
                                    '2stars' => '2',
                                    '1star'  => '1',
                                ],
                            'justification'      => 'Justificativa:',
                            'date'               => 'Data',
                            'evaluation_option'  => 'Selecione a implementação',
                            'duplicate_title'    => 'Duplicar Ação?',
                            'duplicate_text'     => 'Duplicar essa ação para o próximo período?',
                        ],
                    'save_evaluation'                  => 'Avaliação salva com sucesso',
                    'save_evaluation_duplicate_action' => 'Avaliação salva e ação ":name" foi duplicada com sucesso',
                    'save_evaluation_duplicate_error'  => 'Avaliação salva com sucesso, mas a ação ":name" não pode ser duplicada.',
                ],
            'action_priority' =>
                [
                    'Unset'  => 'Não definida',
                    'Low'    => 'Baixa',
                    'Medium' => 'Média',
                    'High'   => 'Alta',
                ],
            'modal'           =>
                [
                    'delete' =>
                        [
                            'title'   => 'Apagar Ação',
                            'message' => 'Deseja apagar a ação?',
                            'success' => 'Sucesso ao apagar :item.',
                            'text'    => 'Todas as informações relacionadas a essa ação serão removidas.',
                        ],
                    'trash'  =>
                        [
                            'title'   => 'Arquivar Ação',
                            'message' => 'Deseja arquivar a ação?',
                            'success' => 'Sucesso ao arquivar :item.',
                            'text'    => 'A ação será arquivada e você pode recuperá-la em "Acompanhar Ações" marcando a opção "Incluir registros arquivados" e apertando em "Restaurar".',
                        ],
                ],
            'restore'         =>
                [
                    'text' => 'Sucesso ao restaurar :item.',
                ],
            'evaluation_term' =>
                [
                    'label'       => 'Período:',
                    'placeholder' => 'Selecione o período',
                    'all_terms'   => 'Todos os períodos',
                    'semester'    => 'Semestral',
                    'bimester'    => 'Bimestral',
                    'periodicity' => 'Periodicidade',
                ],
        ],
    'agreements'             =>
        [
            'create'              =>
                [
                    'panel_title' => 'Criar Combinado',
                ],
            'edit'                =>
                [
                    'panel_title' => 'Editar Combinado',
                ],
            'show'                =>
                [
                    'panel_title' => 'Exibir Combinado',
                ],
            'index'               =>
                [
                    'panel_title' => 'Listar Combinados',
                ],
            'title'               =>
                [
                    'label'       => 'Título:',
                    'placeholder' => 'Digite um título para o combinado',
                ],
            'details'             =>
                [
                    'label'       => 'Detalhamento:',
                    'placeholder' => 'Digite o detalhamento do combinado',
                ],
            'topic_tags'          =>
                [
                    'label'       => 'Assuntos Relacionados:',
                    'placeholder' => 'Selecione os assuntos relacionados',
                ],
            'roles'               =>
                [
                    'label'       => 'Envolvidos:',
                    'placeholder' => 'Selecione o(s) envolvido(s)',
                ],
            'actions'             =>
                [
                    'label'       => 'Ações Relacionadas:',
                    'placeholder' => 'Selecione as ações relacionadas',
                ],
            'execution_cycle'     =>
                [
                    'label'       => 'Periodicidade da Execução:',
                    'placeholder' => 'Selecione o ciclo de execução',
                ],
            'evaluation_strategy' =>
                [
                    'label'       => 'Forma de Monitoramento:',
                    'placeholder' => 'Selecione a forma de monitoramento',
                ],
            'modal'               =>
                [
                    'delete' =>
                        [
                            'title'   => 'Apagar Combinado',
                            'message' => 'Deseja apagar o combinado?',
                            'text'    => 'Todas as informações relacionadas a esse combinado serão removidas.',
                        ],
                    'trash'  =>
                        [
                            'title'   => 'Arquivar Combinado',
                            'message' => 'Deseja arquivar esse combinado?',
                            'success' => 'Sucesso ao arquivar :item.',
                            'text'    => 'Todas as informações relacionadas a esse combinado serão arquivadas.',
                        ],
                ],
            'cycles'              =>
                [
                    'monthly'    => 'Mensal',
                    'bimestrial' => 'Bimestral',
                    'semestrial' => 'Semestral',
                    'annual'     => 'Anual',
                    'biannual'   => 'Bianual',
                ],
        ],
    'goals'                  =>
        [
            'create'                =>
                [
                    'panel_title' => 'Adicionar novo Objetivo',
                ],
            'edit'                  =>
                [
                    'panel_title' => 'Editar Objetivo',
                ],
            'show'                  =>
                [
                    'panel_title' => 'Exibir Objetivo',
                ],
            'index'                 =>
                [
                    'panel_title' => 'Acompanhar Objetivos',
                ],
            'indicator'             =>
                [
                    'label'       => 'Indicador:',
                    'placeholder' => 'Selecione o indicador',
                ],
            'indicator_description' =>
                [
                    'label'       => 'Descrição do Indicador:',
                    'placeholder' => 'Selecione o indicador acima',
                ],
            'name'                  =>
                [
                    'label'       => 'Nome:',
                    'placeholder' => 'Digite um nome para o objetivo',
                ],
            'justification'         =>
                [
                    'label'       => 'Justificativa:',
                    'placeholder' => 'Descreva sucintamente o problema',
                ],
            'restore'         =>
                [
                    'text' => 'Sucesso ao restaurar :item.',
                ],
            'modal'                 =>
                [
                    'delete' =>
                        [
                            'title'   => 'Apagar Objetivo',
                            'message' => 'Deseja apagar o objetivo?',
                            'success' => 'Sucesso ao apagar :item.',
                            'text'    => 'Todas as informações relacionadas a esse objetivo serão removidas.',
                        ],
                    'trash'  =>
                        [
                            'title'   => 'Arquivar Objetivo',
                            'message' => 'Deseja arquivar esse objetivo?',
                            'success' => 'Sucesso ao arquivar :item.',
                            'text'    => 'Todas as informações relacionadas a esse objetivo serão arquivadas.',
                        ],
                ],
        ],
    'indicators'             =>
        [
            'create'            =>
                [
                    'panel_title' => 'Adicionar novo Indicador',
                ],
            'edit'              =>
                [
                    'panel_title' => 'Editar Indicador',
                ],
            'show'              =>
                [
                    'panel_title' => 'Exibir Indicador',
                ],
            'index'             =>
                [
                    'panel_title' => 'Acompanhar Indicadores',
                ],
            'value'             => 'Valor do Indicador:',
            'import'            =>
                [
                    'label'  => 'Importar série de dados:',
                    'button' => 'Procurar',
                ],
            'group'             =>
                [
                    'panel_title' => 'Grupo de Indicadores',
                    'name'        => 'Nome do Grupo:',
                    'placeholder' => 'Selecione o grupo',
                    'description' => 'Descrição do Grupo:',
                    'indicators'  => 'Indicadores do Grupo',
                    'cycle'       => 'Ciclo:',
                    'type'        =>
                        [
                            'label'       => 'Tipo:',
                            'placeholder' => 'Selecione o tipo',
                            'school'      => 'Escola',
                            'rooms'       => 'Salas',
                            'ceap'        => 'CEAP',
                            'schools'     => 'Escola',
                        ],
                    'start_date'  => 'Início do Acompanhamento:',
                ],
            'records'           =>
                [
                    'create'             =>
                        [
                            'panel_title' => 'Registrar novo valor para Indicador',
                        ],
                    'edit'               =>
                        [
                            'panel_title' => 'Editar Histórico do Indicador',
                        ],
                    'index'              =>
                        [
                            'panel_title' => 'Histórico do Indicador',
                        ],
                    'date'               =>
                        [
                            'label'       => 'Data do Relatório:',
                            'placeholder' => 'Selecione a data do relatório',
                        ],
                    'recordDate'         =>
                        [
                            'label'       => 'Data do Registro:',
                            'placeholder' => 'Selecione a data do registro',
                        ],
                    'value'              =>
                        [
                            'label' => 'Valor do Indicador:',
                        ],
                    'year'               =>
                        [
                            'label'       => 'Ano Letivo:',
                            'placeholder' => 'Selecione o ano letivo',
                        ],
                    'grade'              =>
                        [
                            'label'       => 'Série:',
                            'placeholder' => 'Filtre pela série',
                        ],
                    'room'               =>
                        [
                            'label'       => 'Sala:',
                            'placeholder' => 'Filtre pela sala',
                        ],
                    'school_grade_group' =>
                        [
                            'label'             => 'Período Escolar',
                            'elementary_school' => 'Anos Iniciais',
                            'middle_school'     => 'Anos Finais',
                            'high_school'       => 'Ensino Médio',
                        ],
                ],
            'modal'             =>
                [
                    'create_register' => 'Criar Registros',
                    'update_register' => 'Atualizar Registros',
                    'create'          => 'Criar Grupo',
                    'edit'            => 'Editar Grupo',
                    'ceap'            =>
                        [
                            'start_date'         => 'Data do Registro:',
                            'school_grade_group' =>
                                [
                                    'label'       => 'Ciclos Escolares:',
                                    'placeholder' => 'Selecione o ciclo escolar',
                                ],
                        ],
                    'indicator'       =>
                        [
                            'create' => 'Adicionar Indicador',
                            'edit'   => 'Editar Indicador',
                            'delete' =>
                                [
                                    'title'   => 'Apagar Indicador',
                                    'message' => 'Deseja apagar esse indicador?',
                                    'text'    => 'Todas as informações relacionadas a esse indicador serão removidas.',
                                ],
                        ],
                    'group'           =>
                        [
                            'name'        =>
                                [
                                    'label'       => 'Nome do Grupo:',
                                    'placeholder' => 'Digite um nome para o grupo',
                                ],
                            'description' =>
                                [
                                    'label'       => 'Descrição do Grupo:',
                                    'placeholder' => 'Adicione uma breve descrição do grupo.',
                                ],
                        ],
                    'delete'          =>
                        [
                            'title'   => 'Apagar Grupo',
                            'message' => 'Deseja apagar o grupo?',
                        ],
                    'name'            =>
                        [
                            'label'       => 'Nome:',
                            'placeholder' => 'Digite um nome para o indicador',
                        ],
                    'description'     =>
                        [
                            'label'       => 'Descrição do Indicador:',
                            'placeholder' => 'Adicione uma breve descrição do indicador.',
                        ],
                    'cycle'           =>
                        [
                            'label'       => 'Ciclo:',
                            'placeholder' => 'Selecione o ciclo de avaliação',
                        ],
                    'start_date'      =>
                        [
                            'label'       => 'Início do Acompanhamento:',
                            'placeholder' => 'DD/MM/AAAA',
                        ],
                ],
            'chart'             =>
                [
                    'title'    => 'Gráfico de Indicador(es)',
                    'no_chart' => 'Nenhum gráfico disponível para esse período',
                ],
            'trashed_registers' =>
                [
                    'label' => 'Incluir registros arquivados',
                ],
        ],
    'frontend'               =>
        [
            'ceap' =>
                [
                    'indicators' =>
                        [
                            'index' => 'Acompanhamento de Indicadores',
                            'chart' => 'Painel Geral',
                        ],
                    'actions'    =>
                        [
                            'index'       => 'Acompanhamento de Ações',
                            'evidences'   =>
                                [
                                    'index' => 'Evidências',
                                ],
                            'evaluations' =>
                                [
                                    'index' => 'Avaliações',
                                ],
                        ],
                    'goals'      =>
                        [
                            'index' => 'Acompanhamento de Objetivos',
                        ],
                    'agreements' =>
                        [
                            'index' => 'Lista de Combinados',
                        ],
                ],
        ],
    'createSuccessMessage'   => 'Sucesso ao criar :item ":name".',
    'editSuccessMessage'     => 'Sucesso ao editar :item ":name".',
    'registerSuccessMessage' => 'Sucesso ao atualizar :item ":name".',
    'error'                  =>
        [
            '404_title'     => 'Página não encontrada',
            '404_message_1' => 'Desculpe-nos, não pudemos encontar a página solicitada.',
            '404_message_2' => 'Por favor, tente novamente.',
        ],
    'permissions'            =>
        [
            'view-backend' => 'Visualizar administração',
            'manage-users' => 'Gerenciar usuários',
            'manage-roles' => 'Gerenciar papéis',
            'can-read'     => 'Visualizar itens',
            'can-create'   => 'Criar itens',
            'can-edit'     => 'Editar itens',
            'can-trash'    => 'Arquivar itens',
            'can-restore'  => 'Restaurar itens',
            'can-delete'   => 'Apagar itens',
            'manage-ceap'  => 'Gerenciar CEAP',
        ],
    'datatable'              =>
        [
            'filenames' =>
                [
                    'goals'       => 'Objetivos',
                    'indicators'  => 'Indicadores',
                    'actions'     => 'Ações',
                    'agreements'  => 'Combinados',
                    'evidences'   => 'Evidências',
                    'evaluations' => 'Avaliações',
                ],
        ],
    'date_format'            => 'DD/MM/AAAA',
    'backend'                =>
        [
            'agreement_role'      =>
                [
                    'store'   => 'O registro foi criado com sucesso!',
                    'update'  => 'O registro foi atualizado.',
                    'destroy' => 'O registro foi excluído.',
                ],
            'evaluation_term'     =>
                [
                    'store'   => 'O período foi criado com sucesso!',
                    'update'  => 'O período foi atualizado.',
                    'destroy' => 'O registro foi excluído.',
                    'title'   => 'Períodos de Avaliação',
                ],
            'monitoring_strategy' =>
                [
                    'store'   => 'O registro foi criado com sucesso!',
                    'update'  => 'O registro foi atualizado.',
                    'destroy' => 'O registro foi excluído.',
                ],
            'topic_tag'           =>
                [
                    'store'   => 'O registro foi criado com sucesso!',
                    'update'  => 'O registro foi atualizado.',
                    'destroy' => 'O registro foi excluído.',
                ],
            'user'                =>
                [
                    'more_options' => 'Mais opções',
                ],
        ],
];
