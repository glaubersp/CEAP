<?php

return [
    'general'  =>
        [
            'all'               => 'Todas',
            'yes'               => 'Sim',
            'no'                => 'Não',
            'custom'            => 'Personalizado',
            'actions'           => 'Ações',
            'active'            => 'Ativo',
            'buttons'           =>
                [
                    'save'   => 'Salvar',
                    'update' => 'Atualizar',
                ],
            'hide'              => 'Esconder',
            'inactive'          => 'Inativo',
            'none'              => 'Nenhum',
            'show'              => 'Mostrar',
            'toggle_navigation' => 'Mostrar / Esconder Navegação',
            'copyright'         => 'Copyleft',
        ],
    'backend'  =>
        [
            'access' =>
                [
                    'roles' =>
                        [
                            'create'     => 'Criar Papel',
                            'edit'       => 'Editar Papel',
                            'management' => 'Gerenciamento de Papéis',
                            'table'      =>
                                [
                                    'number_of_users' => 'Número de Usuários',
                                    'permissions'     => 'Permissões',
                                    'role'            => 'Papel',
                                    'sort'            => 'Ordenar',
                                    'total'           => '[0,1] papel no total|[2,*] papéis no total',
                                ],
                        ],
                    'users' =>
                        [
                            'active'              => 'Usuários Ativos',
                            'all_permissions'     => 'Todas as Permissões',
                            'change_password'     => 'Alterar Senha',
                            'change_password_for' => 'Alterar senha para :user',
                            'create'              => 'Criar Usuário',
                            'deactivated'         => 'Usuários Desativados',
                            'deleted'             => 'Usuários Excluídos',
                            'edit'                => 'Editar Usuário',
                            'management'          => 'Gerenciamento de Usuários',
                            'no_permissions'      => 'Sem permissões',
                            'no_roles'            => 'Sem papéis para definir.',
                            'permissions'         => 'Permissões',
                            'table'               =>
                                [
                                    'confirmed'         => 'Confirmado',
                                    'created'           => 'Criado',
                                    'email'             => 'E-mail',
                                    'id'                => 'ID',
                                    'last_updated'      => 'Última atualização',
                                    'name'              => 'Nome',
                                    'no_deactivated'    => 'Nenhum usuário desativado.',
                                    'no_deleted'        => 'Nenhum usuário excluído',
                                    'roles'             => 'Papéis',
                                    'social'            => 'Social',
                                    'total'             => '[0,1] usuário no total|[2,*] usuários no total',
                                    'first_name'        => 'Nome',
                                    'last_name'         => 'Sobrenome',
                                    'other_permissions' => 'Demais Permissões',
                                    'permissions'       => 'Permissões',
                                ],
                            'tabs'                =>
                                [
                                    'titles'  =>
                                        [
                                            'overview' => 'Visão Geral',
                                            'history'  => 'Histórico',
                                        ],
                                    'content' =>
                                        [
                                            'overview' =>
                                                [
                                                    'avatar'       => 'Avatar',
                                                    'confirmed'    => 'Confirmado',
                                                    'created_at'   => 'Criado em',
                                                    'deleted_at'   => 'Apagado em',
                                                    'email'        => 'E-mail',
                                                    'last_updated' => 'Última atualização',
                                                    'name'         => 'Nome',
                                                    'status'       => 'Estado',
                                                    'first_name'   => 'Nome',
                                                    'last_name'    => 'Sobrenome',
                                                ],
                                        ],
                                ],
                            'view'                => 'Visualizar Usuário',
                        ],
                ],
            'ceap' => [
                'monitoring_strategies' => [
                    'table' => [
                        'total'             => '[0,1] forma de monitoramento no total|[2,*] formas de monitoramento no total',
                    ]
                ],
                'agreement_role' => [
                    'table' => [
                        'total'             => '[0,1] envolvido no total|[2,*] envolvidos no total',
                    ]
                ],
                'evaluation_term' => [
                    'table' => [
                        'total'             => '[0,1] período de avaliação no total|[2,*] períodos de avaliação no total',
                    ]
                ],
                'topic_tag' => [
                    'table' => [
                        'total'             => '[0,1] assunto relacionado no total|[2,*] assuntos relacionados no total',
                    ]
                ],
            ],
        ],
    'frontend' =>
        [
            'auth'      =>
                [
                    'login_box_title'    => 'Entrar',
                    'login_button'       => 'Entrar',
                    'login_with'         => 'Entrar com :social_media',
                    'register_box_title' => 'Registrar',
                    'register_button'    => 'Registrar',
                    'remember_me'        => 'Lembrar-me',
                ],
            'contact'   =>
                [
                    'box_title' => 'Contato',
                    'button'    => 'Enviar',
                ],
            'passwords' =>
                [
                    'forgot_password'                 => 'Esqueceu Sua Senha?',
                    'reset_password_box_title'        => 'Redefinir Senha',
                    'reset_password_button'           => 'Redefinir Senha',
                    'send_password_reset_link_button' => 'Enviar link para redefinição de senha',
                    'expired_password_box_title'      => 'Sua senha expirou.',
                    'update_password_button'          => 'Atualizar Senha',
                ],
            'macros'    =>
                [
                    'country'        =>
                        [
                            'alpha'   => 'Códigos de País Alpha',
                            'alpha2'  => 'Códigos de País Alpha 2',
                            'alpha3'  => 'Códigos de País Alpha 3',
                            'numeric' => 'Códigos Numéricos País',
                        ],
                    'macro_examples' => 'Exemplo de Macros',
                    'state'          =>
                        [
                            'mexico' => 'Lista de Estados do México',
                            'us'     =>
                                [
                                    'us'       => 'Lista de estados dos EUA',
                                    'outlying' => 'Territórios Distantes EUA',
                                    'armed'    => 'Forças Armadas dos EUA',
                                ],
                        ],
                    'territories'    =>
                        [
                            'canada' => 'Província do Canadá e Lista de Territórios',
                        ],
                    'timezone'       => 'Fuso horário',
                ],
            'user'      =>
                [
                    'passwords' =>
                        [
                            'change' => 'Alterar Senha',
                        ],
                    'profile'   =>
                        [
                            'avatar'             => 'Avatar',
                            'created_at'         => 'Criado em',
                            'edit_information'   => 'Editar informações',
                            'email'              => 'E-mail',
                            'last_updated'       => 'Última atualização',
                            'name'               => 'Nome',
                            'update_information' => 'Atualizar informação',
                            'first_name'         => 'Nome',
                            'last_name'          => 'Sobrenome',
                        ],
                ],
        ],
];
