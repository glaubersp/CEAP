<?php

return [
    'backend'         =>
        [
            'access'     =>
                [
                    'title' => 'Usuários',
                    'roles' =>
                        [
                            'all'        => 'Todos os Papéis',
                            'create'     => 'Criar Papel',
                            'edit'       => 'Editar Papel',
                            'management' => 'Gerenciamento de Papéis',
                            'main'       => 'Papéis',
                        ],
                    'users' =>
                        [
                            'all'             => 'Todos os Usuários',
                            'change-password' => 'Alterar Senha',
                            'create'          => 'Criar Usuário',
                            'deactivated'     => 'Usuários Desativados',
                            'deleted'         => 'Usuários Excluídos',
                            'edit'            => 'Editar Usuário',
                            'main'            => 'Usuários',
                            'view'            => 'Visualizar Usuário',
                        ],
                ],
            'log-viewer' =>
                [
                    'main'      => 'Logs',
                    'dashboard' => 'Painel de Controle',
                    'logs'      => 'Logs',
                ],
            'sidebar'    =>
                [
                    'dashboard' => 'Painel Geral',
                    'general'   => 'Geral',
                    'system'    => 'Sistema',
                ],
        ],
    'language-picker' =>
        [
            'language' => 'Idioma',
            'langs'    =>
                [
                    'ar'    => 'Árabe (Arabic)',
                    'da'    => 'Dinamarquês (Danish)',
                    'de'    => 'Alemão (German)',
                    'en'    => 'Inglês (English)',
                    'es'    => 'Espanhol (Spanish)',
                    'fr'    => 'Francês (French)',
                    'id'    => 'indonésio (Indonesian)',
                    'it'    => 'Italiano (Italian)',
                    'ja'    => '(Japanese)',
                    'nl'    => 'Holandês (Dutch)',
                    'pt-BR' => 'Português do Brasil (Brazilian Portuguese)',
                    'ru'    => 'Russo (Russian)',
                    'sv'    => 'Sueco (Swedish)',
                    'th'    => '(Thai)',
                    'zh'    => '(Chinese Simplified)',
                    'zh-TW' => '(Chinese Traditional)',
                    'tr'    => '(Turkish)',
                ],
        ],
];
