<?php

return [
    'general'  =>
        [
            'home'   => 'Início',
            'logout' => 'Sair',
        ],
    'frontend' =>
        [
            'contact'   => 'Contato',
            'dashboard' => 'Painel Geral',
            'login'     => 'Entrar',
            'macros'    => 'Macros',
            'register'  => 'Registrar',
            'user'      =>
                [
                    'account'          => 'Minha Conta',
                    'administration'   => 'Administração',
                    'change_password'  => 'Alterar Senha',
                    'my_information'   => 'Minhas Informações',
                    'profile'          => 'Perfil',
                    'password_expired' => 'Senha Expirada',
                ],
        ],
];
