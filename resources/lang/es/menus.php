<?php

return [
    'backend'         =>
        [
            'access'     =>
                [
                    'title' => 'Administración de acceso',
                    'roles' =>
                        [
                            'all'        => 'Todos los Roles',
                            'create'     => 'Nuevo Rol',
                            'edit'       => 'Modificar Rol',
                            'management' => 'Administración de Roles',
                            'main'       => 'Roles',
                        ],
                    'users' =>
                        [
                            'all'             => 'Todos los Usuarios',
                            'change-password' => 'Cambiar la contraseña',
                            'create'          => 'Nuevo Usuario',
                            'deactivated'     => 'Usuarios Desactivados',
                            'deleted'         => 'Usuarios Eliminados',
                            'edit'            => 'Modificar Usuario',
                            'main'            => 'Usuario',
                            'view'            => 'Ver Usuario',
                        ],
                ],
            'log-viewer' =>
                [
                    'main'      => 'Gestór de Logs',
                    'dashboard' => 'Principal',
                    'logs'      => 'Logs',
                ],
            'sidebar'    =>
                [
                    'dashboard' => 'Principal',
                    'general'   => 'General',
                    'system'    => 'Sistema',
                ],
        ],
    'language-picker' =>
        [
            'language' => 'Idioma',
            'langs'    =>
                [
                    'ar'    => 'العربية (Arabic)',
                    'zh'    => '(Chinese Simplified)',
                    'zh-TW' => '(Chinese Traditional)',
                    'da'    => 'Danés (Danish)',
                    'de'    => 'Alemán (German)',
                    'el'    => '(Greek)',
                    'en'    => 'Inglés (English)',
                    'es'    => 'Español (Spanish)',
                    'fr'    => 'Francés (French)',
                    'id'    => 'Indonesio (Indonesian)',
                    'it'    => 'Italiano (Italian)',
                    'ja'    => '(Japanese)',
                    'nl'    => 'Holandés (Dutch)',
                    'pt-BR' => 'Portugués Brasileño',
                    'ru'    => 'Russian (Russian)',
                    'sv'    => 'Sueco (Swedish)',
                    'th'    => '(Thai)',
                    'tr'    => '(Turkish)',
                ],
        ],
];
