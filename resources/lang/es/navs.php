<?php

return [
    'general'  =>
        [
            'home'   => 'Inicio',
            'logout' => 'Cerrar Sessión',
        ],
    'frontend' =>
        [
            'contact'   => 'Contact',
            'dashboard' => 'Principal',
            'login'     => 'Iniciar Sessión',
            'macros'    => 'Macros',
            'register'  => 'Registrarse',
            'user'      =>
                [
                    'account'         => 'Mi Cuenta',
                    'administration'  => 'Administración',
                    'change_password' => 'Cambiar la contraseña',
                    'my_information'  => 'Mi Cuenta',
                    'profile'         => 'Perfil',
                ],
        ],
];
