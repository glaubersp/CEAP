<?php

return [
    'backend'         =>
        [
            'access'     =>
                [
                    'title' => 'Access Management',
                    'roles' =>
                        [
                            'all'        => 'All Roles',
                            'create'     => 'Create Role',
                            'edit'       => 'Edit Role',
                            'management' => 'Role Management',
                            'main'       => 'Roles',
                        ],
                    'users' =>
                        [
                            'all'             => 'All Users',
                            'change-password' => 'Change Password',
                            'create'          => 'Create User',
                            'deactivated'     => 'Deactivated Users',
                            'deleted'         => 'Deleted Users',
                            'edit'            => 'Edit User',
                            'main'            => 'Users',
                            'view'            => 'View User',
                        ],
                ],
            'log-viewer' =>
                [
                    'main'      => 'Log Viewer',
                    'dashboard' => 'Dashboard',
                    'logs'      => 'Logs',
                ],
            'sidebar'    =>
                [
                    'dashboard' => 'Dashboard',
                    'general'   => 'General',
                    'system'    => 'System',
                ],
        ],
    'language-picker' =>
        [
            'language' => 'Language',
            'langs'    =>
                [
                    'ar'    => 'Arabic',
                    'zh'    => 'Chinese Simplified',
                    'zh-TW' => 'Chinese Traditional',
                    'da'    => 'Danish',
                    'de'    => 'German',
                    'el'    => 'Greek',
                    'en'    => 'English',
                    'es'    => 'Spanish',
                    'fr'    => 'French',
                    'id'    => 'Indonesian',
                    'it'    => 'Italian',
                    'ja'    => 'Japanese',
                    'nl'    => 'Dutch',
                    'pt-BR' => 'Brazilian Portuguese',
                    'ru'    => 'Russian',
                    'sv'    => 'Swedish',
                    'th'    => 'Thai',
                    'tr'    => 'Turkish',
                ],
        ],
];
