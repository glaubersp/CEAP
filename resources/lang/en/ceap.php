<?php

return [
    'app'                    =>
        [
            'name'        => 'CEAP System',
            'description' => 'Improving conditions for Teaching, Learning and Participation',
            'about'       => 'About the project',
        ],
    'login'                  =>
        [
            'name'            => 'Login',
            'email'           => 'E-mail',
            'password'        => 'Password',
            'remember'        => 'Remind me',
            'forgot_password' => 'Forgot your password?',
            'user'            => 'User',
        ],
    'button'                 =>
        [
            'login'                     => 'Login',
            'show'                      => 'Show',
            'evidences'                 => 'Evidence',
            'add'                       => 'Add',
            'create'                    => 'Create',
            'edit'                      => 'Edit',
            'cancel'                    => 'Cancel',
            'save'                      => 'Save',
            'save_action_add_evidences' => 'Save and Collect Evidence',
            'collect'                   => 'Collect',
            'save_goal_add_action'      => 'Save and Plan Action',
            'add_action'                => 'Add Action',
            'delete'                    => 'Delete',
            'plan'                      => 'Plan',
            'create_graph'              => 'View chart',
            'define_goals'              => 'Define Objective',
            'add_register_value'        => 'Add Register',
            'update_register_value'     => 'Update indicator',
            'register_history'          => 'Historical Records',
            'print'                     => 'Print',
            'back'                      => 'Go back',
            'restore'                   => 'Restore',
            'trash'                     => 'Trash',
        ],
    'menu'                   =>
        [
            'home'       => 'Overview',
            'help'       => 'Help',
            'planning'   =>
                [
                    'title'     => 'Plan',
                    'action'    => 'Action',
                    'goal'      => 'Objective',
                    'indicator' => 'Indicator',
                ],
            'monitoring' =>
                [
                    'title'            => 'Check',
                    'monitoring_chart' => 'Overview',
                    'actions'          => 'Actions',
                    'goals'            => 'Objectives',
                    'indicators'       => 'Indicators',
                ],
            'agreements' =>
                [
                    'title'  => 'Agreements',
                    'create' => 'Create',
                    'list'   => 'List',
                ],
            'management' =>
                [
                    'title' => 'Manage',
                    'soon'  => 'Soon',
                ],
        ],
    'home'                   =>
        [
            'indicators' => 'Indicators Registered',
            'goals'      => 'Goals Registered',
            'actions'    => 'Actions Registered',
            'agreements' => 'Agreements Registered',
            'title'      => 'Principle',
            'summary'    => 'Summary',
        ],
    'table'                  =>
        [
            'end_date'          => 'Expiration',
            'updated_at'        => 'Update',
            'priority'          => 'Priority',
            'interactions'      => 'Interactions',
            'goal'              => 'Objective',
            'indicator'         => 'Indicator',
            'description'       => 'Description',
            'print_at'          => 'Printed on ',
            'user'              => 'User',
            'year'              => 'Year',
            'grade'             => 'Grade',
            'room'              => 'Class',
            'indicator_value'   => 'Indicator value',
            'trashed_registers' => 'Include items in the trash',
        ],
    'actions'                =>
        [
            'create'          =>
                [
                    'panel_title' => 'Plan a new Action',
                ],
            'edit'            =>
                [
                    'panel_title' => 'Edit Action',
                ],
            'show'            =>
                [
                    'panel_title'         => 'Show Action',
                    'related_actions'     => 'Related Actions:',
                    'no_evaluation'       => 'This action has not been evaluated yet. To evaluate, go back to \'Check Actions\' and press the \'Plan\' button.',
                    'no_evidence'         => 'This action still has no evidence. To add evidence, press the button below to go to \'Collect Evidence\'.',
                    'no_evaluation_label' => 'No Evaluation',
                    'no_evidence_label'   => 'No Evidences',
                ],
            'index'           =>
                [
                    'panel_title' => 'Track Actions',
                ],
            'goal'            =>
                [
                    'label'       => 'Objective:',
                    'placeholder' => 'Select the objective',
                ],
            'indicator'       =>
                [
                    'label'       => 'Indicator:',
                    'placeholder' => 'Select indicator',
                ],
            'name'            =>
                [
                    'label'       => 'Action:',
                    'placeholder' => 'Type a name for the action',
                ],
            'details'         =>
                [
                    'label'       => 'Details:',
                    'placeholder' => 'Action details',
                ],
            'users'           =>
                [
                    'label'       => 'Responsible:',
                    'placeholder' => 'Select those responsible',
                ],
            'start_date'      =>
                [
                    'label'       => 'Estimated start:',
                    'placeholder' => 'DD/MM/YYYY',
                ],
            'end_date'        =>
                [
                    'label'       => 'Expected end date:',
                    'placeholder' => 'DD/MM/YYYY',
                ],
            'evidences'       =>
                [
                    'panel_title' => 'Collect Evidence',
                    'evidence'    => 'Evidence',
                    'file'        => 'File Name',
                    'modal'       =>
                        [
                            'title'               => 'Title:',
                            'placeholder_title'   => 'Write a title of the evidence',
                            'details'             => 'Details:',
                            'placeholder_details' => 'Write a description for the evidence',
                            'attach'              => 'Attach file:',
                        ],
                    'show'        =>
                        [
                            'title' => 'Action Evidence: ',
                            'files' => 'Files:',
                        ],
                    'edit'        =>
                        [
                            'title' => 'Edit Evidence',
                        ],
                ],
            'evaluations'     =>
                [
                    'panel_title' => 'Evaluate Actions',
                    'evaluate'    => 'Evaluations',
                    'star'        => 'Star',
                    'stars'       => 'Stars',
                    'modal'       =>
                        [
                            'title'              => 'Evaluate Action',
                            'evaluation'         => 'Implementation:',
                            'evaluation_options' =>
                                [
                                    'Unset'                 => 'Not evaluated',
                                    'Not implemented'       => 'Not implemented',
                                    'Partially implemented' => 'Implemented partially',
                                    'Fully implemented'     => 'Implemented fully',
                                ],
                            'result'             => 'Result:',
                            'choose_star'        => 'Choose stars to evaluate the action',
                            'stars_rating'       =>
                                [
                                    '5stars' => '5',
                                    '4stars' => '4',
                                    '3stars' => '3',
                                    '2stars' => '2',
                                    '1star'  => '1',
                                ],
                            'justification'      => 'Justification:',
                            'date'               => 'Date',
                            'evaluation_option'  => 'Select the implementation',
                            'duplicate_title'    => 'Duplicate Action?',
                            'duplicate_text'     => 'Duplicate this action for the next term?',
                        ],
                ],
            'action_priority' =>
                [
                    'Unset'  => 'Not defined',
                    'Low'    => 'Low',
                    'Medium' => 'Average',
                    'High'   => 'High',
                ],
            'modal'           =>
                [
                    'trash'  =>
                        [
                            'title'   => 'Move action to trash',
                            'message' => 'Do you really want to put this item in the trash?',
                            'success' => 'Successfully put :item in the trash',
                            'text'    => 'This action will be archived and you can retrieve it in "Track Actions" by checking the "Include trashed records" option and pressing "Restore" button.',
                        ],
                    'delete' =>
                        [
                            'title'   => 'Delete Action',
                            'message' => 'Do you really want to delete this action?',
                            'success' => 'Success on deleting :item',
                            'text'    => 'All information related to this action will be removed.',
                        ],
                ],
            'evaluation_term' =>
                [
                    'periodicity' => 'Periodicity',
                    'placeholder' => 'Select term',
                    'semester'    => 'Semester',
                    'all_terms'   => 'All terms',
                    'bimester'    => 'Bimester',
                    'label'       => 'Term:',
                ],
            'restore'         =>
                [
                    'text' => 'Success on restoring :item',
                ],
        ],
    'agreements'             =>
        [
            'create'              =>
                [
                    'panel_title' => 'Create Agreement',
                ],
            'edit'                =>
                [
                    'panel_title' => 'Edit Agreement',
                ],
            'show'                =>
                [
                    'panel_title' => 'Show Agreement',
                ],
            'index'               =>
                [
                    'panel_title' => 'List Agreements',
                ],
            'title'               =>
                [
                    'label'       => 'Title:',
                    'placeholder' => 'Type a title for the agreement',
                ],
            'details'             =>
                [
                    'label'       => 'Details:',
                    'placeholder' => 'Type details for the agreement',
                ],
            'topic_tags'          =>
                [
                    'label'       => 'Related Topics:',
                    'placeholder' => 'Select related topics',
                ],
            'roles'               =>
                [
                    'label'       => 'Members:',
                    'placeholder' => 'Select members',
                ],
            'actions'             =>
                [
                    'label'       => 'Related actions:',
                    'placeholder' => 'Select related actions',
                ],
            'execution_cycle'     =>
                [
                    'label'       => 'Frequency of action:',
                    'placeholder' => 'Select an action cycle',
                ],
            'evaluation_strategy' =>
                [
                    'label'       => 'Monitoring method:',
                    'placeholder' => 'Select a monitoring method',
                ],
            'modal'               =>
                [
                    'delete' =>
                        [
                            'message' => 'Do you really want to delete this agreement?',
                            'title'   => 'Delete Agreement',
                        ],
                ],
            'cycles'              =>
                [
                    'monthly'    => 'Monthly',
                    'bimestrial' => 'Bimestrial',
                    'semestrial' => 'Semestrial',
                    'annual'     => 'Annual',
                    'biannual'   => 'Biannual',
                ],
        ],
    'goals'                  =>
        [
            'create'                =>
                [
                    'panel_title' => 'Plan a new Objective',
                ],
            'edit'                  =>
                [
                    'panel_title' => 'Edit Objective',
                ],
            'show'                  =>
                [
                    'panel_title' => 'Show Objective',
                ],
            'index'                 =>
                [
                    'panel_title' => 'Check on Objectives',
                ],
            'indicator'             =>
                [
                    'label'       => 'Indicator:',
                    'placeholder' => 'Select indicator',
                ],
            'indicator_description' =>
                [
                    'label'       => 'Description of Indicator:',
                    'placeholder' => 'Select an indicator above',
                ],
            'name'                  =>
                [
                    'label'       => 'Name:',
                    'placeholder' => 'Name for objective',
                ],
            'justification'         =>
                [
                    'label'       => 'Explanation:',
                    'placeholder' => 'Describe the problem (short)',
                ],
            'modal'                 =>
                [
                    'trash'  =>
                        [
                            'title'   => 'Put objective in the trash',
                            'success' => 'Success in putting :item in the trash',
                            'message' => 'Do you really want to put this objective in the trash?',
                            'text'    => 'All information related to this goal will be archived.',
                        ],
                    'delete' =>
                        [
                            'title'   => 'Delete Objective',
                            'message' => 'Do you really want to delete the objective?',
                            'success' => 'Success in deleting :item',
                            'text'    => 'All information related to this goal will be removed.',
                        ],
                ],
        ],
    'indicators'             =>
        [
            'create'            =>
                [
                    'panel_title' => 'Plan a new Indicator',
                ],
            'edit'              =>
                [
                    'panel_title' => 'Edit Indicator',
                ],
            'show'              =>
                [
                    'panel_title' => 'Show Indicator',
                ],
            'index'             =>
                [
                    'panel_title' => 'Check on Indicators',
                ],
            'value'             => 'Indicator Value:',
            'import'            =>
                [
                    'label'  => 'Import data series:',
                    'button' => 'Search',
                ],
            'group'             =>
                [
                    'panel_title' => 'Indicator groups',
                    'name'        => 'Group Name:',
                    'placeholder' => 'Select the Indicator group',
                    'description' => 'Description of Group:',
                    'indicators'  => 'Group Indicators:',
                    'cycle'       => 'Cicle:',
                    'type'        =>
                        [
                            'label'       => 'Type:',
                            'placeholder' => 'Select type',
                            'school'      => 'School',
                            'rooms'       => 'Classes',
                            'ceap'        => 'CEAP',
                            'schools'     => 'Schools',
                        ],
                    'start_date'  => 'Start date:',
                ],
            'records'           =>
                [
                    'create'             =>
                        [
                            'panel_title' => 'Register new value for indicator',
                        ],
                    'edit'               =>
                        [
                            'panel_title' => 'Edit Indicator history',
                        ],
                    'index'              =>
                        [
                            'panel_title' => 'Indicator history',
                        ],
                    'date'               =>
                        [
                            'label'       => 'Report Date:',
                            'placeholder' => 'Select the report date',
                        ],
                    'recordDate'         =>
                        [
                            'label'       => 'Record date:',
                            'placeholder' => 'Select the record date',
                        ],
                    'value'              =>
                        [
                            'label' => 'Indicator value:',
                        ],
                    'year'               =>
                        [
                            'label'       => 'Year:',
                            'placeholder' => 'Select year',
                        ],
                    'grade'              =>
                        [
                            'label'       => 'Grade:',
                            'placeholder' => 'Filter by grade',
                        ],
                    'room'               =>
                        [
                            'label'       => 'Class:',
                            'placeholder' => 'Filter by class',
                        ],
                    'school_grade_group' =>
                        [
                            'label'             => 'School period',
                            'elementary_school' => 'Elementary school',
                            'middle_school'     => 'Middle school',
                            'high_school'       => 'High school',
                        ],
                ],
            'modal'             =>
                [
                    'create_register' => 'Criar Registros',
                    'update_register' => 'Update Records',
                    'create'          => 'Criar Grupo',
                    'edit'            => 'Edit Group',
                    'ceap'            =>
                        [
                            'start_date'         => 'Register Date:',
                            'school_grade_group' =>
                                [
                                    'label'       => 'School period:',
                                    'placeholder' => 'Select the school period',
                                ],
                        ],
                    'indicator'       =>
                        [
                            'create' => 'Add Indicator',
                            'edit'   => 'Edit Indicator',
                            'delete' =>
                                [
                                    'title'   => 'Delete Indicator',
                                    'message' => 'Are you sure you want to delete this indicator?',
                                    'text'    => 'All information related to this indicator will be removed.',
                                ],
                        ],
                    'group'           =>
                        [
                            'name'        =>
                                [
                                    'label'       => 'Group Name:',
                                    'placeholder' => 'Type a name for the Group',
                                ],
                            'description' =>
                                [
                                    'label'       => 'Description of Group:',
                                    'placeholder' => 'Add short Group description.',
                                ],
                        ],
                    'delete'          =>
                        [
                            'title'   => 'Delete Group',
                            'message' => 'Are you sure you want to delete this group?',
                        ],
                    'name'            =>
                        [
                            'label'       => 'Name:',
                            'placeholder' => 'Type a name for the Indicator',
                        ],
                    'description'     =>
                        [
                            'label'       => 'Description of indicator:',
                            'placeholder' => 'Add short Indicator description',
                        ],
                    'cycle'           =>
                        [
                            'label'       => 'Cicle:',
                            'placeholder' => 'Select the evaluation cicle',
                        ],
                    'start_date'      =>
                        [
                            'label'       => 'Start date:',
                            'placeholder' => 'DD/MM/YYYY',
                        ],
                ],
            'chart'             =>
                [
                    'title'    => 'Indicator chart(s)',
                    'no_chart' => 'No chart available',
                ],
            'trashed_registers' =>
                [
                    'label' => 'Include items in the trash',
                ],
        ],
    'frontend'               =>
        [
            'ceap' =>
                [
                    'indicators' =>
                        [
                            'index' => 'Indicators Follow-up',
                            'chart' => 'Overview',
                        ],
                    'actions'    =>
                        [
                            'index'       => 'Action Follow-up',
                            'evidences'   =>
                                [
                                    'index' => 'Evidence',
                                ],
                            'evaluations' =>
                                [
                                    'index' => 'Evaluations',
                                ],
                        ],
                    'goals'      =>
                        [
                            'index' => 'Objectives Follow-up',
                        ],
                    'agreements' =>
                        [
                            'index' => 'List Agreements',
                        ],
                ],
        ],
    'createSuccessMessage'   => 'Success in creating :item ":name".',
    'editSuccessMessage'     => 'Success in editing :item ":name".',
    'registerSuccessMessage' => 'Success in updating :item ":name".',
    'permissions'            =>
        [
            'view-backend' => 'View backend',
            'manage-users' => 'Manage users',
            'manage-roles' => 'Manage roles',
            'can-read'     => 'Read items',
            'can-create'   => 'Create items',
            'can-edit'     => 'Edit items',
            'can-trash'    => 'Trash items',
            'can-restore'  => 'Restore items',
            'can-delete'   => 'Delete items',
            'manage-ceap'  => 'Manage CEAP',
        ],
    'datatable'              =>
        [
            'filenames' =>
                [
                    'goals'       => 'Goals',
                    'indicators'  => 'Indicators',
                    'actions'     => 'Actions',
                    'agreements'  => 'Agreements',
                    'evidences'   => 'Evidence',
                    'evaluations' => 'Evaluations',
                ],
        ],
    'error'                  =>
        [
            '404_message_1' => 'Ops, we could not find the page you were looking for.',
            '404_message_2' => 'Please, try again soon.',
            '404_title'     => 'Page not found',
        ],
    'date_format'            => 'MM/DD/YYYY',
    'backend'                =>
        [
            'agreement_role'      =>
                [
                    'store'   => 'The record was successfully created!',
                    'update'  => 'The record was successfully updated.',
                    'destroy' => 'The record was successfully deleted.',
                ],
            'evaluation_term'     =>
                [
                    'store'   => 'The evaluation term was successfully created!',
                    'update'  => 'The evaluation term was successfully updated.',
                    'destroy' => 'The evaluation term was successfully deleted.',
                    'title'   => 'Evaluation Term',
                ],
            'monitoring_strategy' =>
                [
                    'store'   => 'The record was successfully created!',
                    'update'  => 'The record was successfully updated.',
                    'destroy' => 'The record was successfully deleted.',
                ],
            'topic_tag'           =>
                [
                    'store'   => 'The record was successfully created!',
                    'update'  => 'The record was successfully updated.',
                    'destroy' => 'The record was successfully deleted.',
                ],
            'user'                =>
                [
                    'more_options' => 'More options',
                ],
        ],
];
