<?php

return [
    'general'  =>
        [
            'all'               => 'All',
            'yes'               => 'Yes',
            'no'                => 'No',
            'custom'            => 'Custom',
            'actions'           => 'Actions',
            'active'            => 'Active',
            'buttons'           =>
                [
                    'save'   => 'Save',
                    'update' => 'Update',
                ],
            'hide'              => 'Hide',
            'inactive'          => 'Inactive',
            'none'              => 'None',
            'show'              => 'Show',
            'toggle_navigation' => 'Toggle Navigation',
            'copyright'         => 'Copyright',
        ],
    'backend'  =>
        [
            'access' =>
                [
                    'roles' =>
                        [
                            'create'     => 'Create Role',
                            'edit'       => 'Edit Role',
                            'management' => 'Role Management',
                            'table'      =>
                                [
                                    'number_of_users' => 'Number of Users',
                                    'permissions'     => 'Permissions',
                                    'role'            => 'Role',
                                    'sort'            => 'Sort',
                                    'total'           => 'role total|roles total',
                                ],
                        ],
                    'users' =>
                        [
                            'active'              => 'Active Users',
                            'all_permissions'     => 'All Permissions',
                            'change_password'     => 'Change Password',
                            'change_password_for' => 'Change Password for :user',
                            'create'              => 'Create User',
                            'deactivated'         => 'Deactivated Users',
                            'deleted'             => 'Deleted Users',
                            'edit'                => 'Edit User',
                            'management'          => 'User Management',
                            'no_permissions'      => 'No Permissions',
                            'no_roles'            => 'No Roles to set.',
                            'permissions'         => 'Permissions',
                            'table'               =>
                                [
                                    'confirmed'         => 'Confirmed',
                                    'created'           => 'Created',
                                    'email'             => 'E-mail',
                                    'id'                => 'ID',
                                    'last_updated'      => 'Last Updated',
                                    'name'              => 'Name',
                                    'first_name'        => 'First Name',
                                    'last_name'         => 'Last Name',
                                    'no_deactivated'    => 'No Deactivated Users',
                                    'no_deleted'        => 'No Deleted Users',
                                    'roles'             => 'Roles',
                                    'social'            => 'Social',
                                    'total'             => 'user total|users total',
                                    'other_permissions' => 'Other Permissions',
                                    'permissions'       => 'Permissions',
                                ],
                            'tabs'                =>
                                [
                                    'titles'  =>
                                        [
                                            'overview' => 'Overview',
                                            'history'  => 'History',
                                        ],
                                    'content' =>
                                        [
                                            'overview' =>
                                                [
                                                    'avatar'       => 'Avatar',
                                                    'confirmed'    => 'Confirmed',
                                                    'created_at'   => 'Created At',
                                                    'deleted_at'   => 'Deleted At',
                                                    'email'        => 'E-mail',
                                                    'last_updated' => 'Last Updated',
                                                    'name'         => 'Name',
                                                    'first_name'   => 'First Name',
                                                    'last_name'    => 'Last Name',
                                                    'status'       => 'Status',
                                                ],
                                        ],
                                ],
                            'view'                => 'View User',
                        ],
                ],
            'ceap' => [
                'monitoring_strategies' => [
                    'table' => [
                        'total'             => '[0,1] monitoring method total|[2,*] monitoring methods total',
                    ]
                ],
                'agreement_role' => [
                    'table' => [
                        'total'             => '[0,1] member total|[2,*] members total',
                    ]
                ],
                'evaluation_term' => [
                    'table' => [
                        'total'             => '[0,1] evaluation term total|[2,*] evaluation terms total',
                    ]
                ],
                'topic_tag' => [
                    'table' => [
                        'total'             => '[0,1] topic tag total|[2,*] topic tags total',
                    ]
                ],
            ],
        ],
    'frontend' =>
        [
            'auth'      =>
                [
                    'login_box_title'    => 'Login',
                    'login_button'       => 'Login',
                    'login_with'         => 'Login with :social_media',
                    'register_box_title' => 'Register',
                    'register_button'    => 'Register',
                    'remember_me'        => 'Remember Me',
                ],
            'contact'   =>
                [
                    'box_title' => 'Contact Us',
                    'button'    => 'Send Information',
                ],
            'passwords' =>
                [
                    'forgot_password'                 => 'Forgot Your Password?',
                    'reset_password_box_title'        => 'Reset Password',
                    'reset_password_button'           => 'Reset Password',
                    'send_password_reset_link_button' => 'Send Password Reset Link',
                    'expired_password_box_title'      => 'Your password has expired.',
                    'update_password_button'          => 'Update Password',
                ],
            'macros'    =>
                [
                    'country'        =>
                        [
                            'alpha'   => 'Country Alpha Codes',
                            'alpha2'  => 'Country Alpha 2 Codes',
                            'alpha3'  => 'Country Alpha 3 Codes',
                            'numeric' => 'Country Numeric Codes',
                        ],
                    'macro_examples' => 'Macro Examples',
                    'state'          =>
                        [
                            'mexico' => 'Mexico State List',
                            'us'     =>
                                [
                                    'us'       => 'US States',
                                    'outlying' => 'US Outlying Territories',
                                    'armed'    => 'US Armed Forces',
                                ],
                        ],
                    'territories'    =>
                        [
                            'canada' => 'Canada Province & Territories List',
                        ],
                    'timezone'       => 'Timezone',
                ],
            'user'      =>
                [
                    'passwords' =>
                        [
                            'change' => 'Change Password',
                        ],
                    'profile'   =>
                        [
                            'avatar'             => 'Avatar',
                            'created_at'         => 'Created At',
                            'edit_information'   => 'Edit Information',
                            'email'              => 'E-mail',
                            'last_updated'       => 'Last Updated',
                            'name'               => 'Name',
                            'first_name'         => 'First Name',
                            'last_name'          => 'Last Name',
                            'update_information' => 'Update Information',
                        ],
                ],
        ],
];
