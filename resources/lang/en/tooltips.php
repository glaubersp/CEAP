<?php

return [
    'actions'    =>
        [
            'details'         => 'Provide details of the action with relevant evidence',
            'endDate'         => 'Choose the end date for the action',
            'evaluations'     =>
                [
                    'evaluation'    => 'Choose if the action was implemented or not',
                    'justification' => 'Describe how the action was implemented',
                    'result'        => 'Evaluate the action (1 - Poor or 5 - Excellent)',
                ],
            'evidences'       =>
                [
                    'details'  => 'Detail the collected evidence',
                    'fileName' => 'Attach any files which contain relevant evidence',
                    'title'    => 'Type a name for the evidence',
                ],
            'goal'            => 'Chose an objective associated with the action',
            'indicator'       => 'The indicator is automatically selected when you choose the objective',
            'name'            => 'Choose a name for the action',
            'startDate'       => 'Select the start date for the action',
            'users'           => 'Select those responsible for the action',
            'evaluation_term' => 'Select the term for the action',
        ],
    'agreements' =>
        [
            'actions'            => 'Choose those impacted by the agreement',
            'details'            => 'Detail the agreement',
            'evaluationCycle'    => 'Select the period for the evaluation of the agreement',
            'monitoringStrategy' => 'Select the method that will be used to monitor the agreement',
            'roles'              => 'Select those involved in the agreement',
            'title'              => 'Type a name for the agreement',
            'topicTags'          => 'Select topics associated with this agreement',
        ],
    'goals'      =>
        [
            'description'   => 'Description of the indicator selected above',
            'indicator'     => 'Choose an indicator for the objective that will be created',
            'justification' => 'Describe the problem',
            'name'          => 'Type a name for the objective',
        ],
    'indicators' =>
        [
            'group' => 'Select a group',
            'modal' =>
                [
                    'ceap'      =>
                        [
                            'cycle'     => 'Select the cycle/timeframe',
                            'startDate' => 'Choose the start date for the evaluation of the indicator',
                        ],
                    'group'     =>
                        [
                            'cycle'       => 'Select the timeframe fo for the evaluation of the indicator',
                            'description' => 'Add a short description for the indicator',
                            'name'        => 'Type a name for the group of indicators',
                            'startDate'   => 'Choose a start date for the evaluation of the indicator',
                            'type'        => 'Select the indicator group type',
                        ],
                    'indicator' =>
                        [
                            'description' => 'Add a short description for the indicator',
                            'name'        => 'Type a name for the indicator',
                        ],
                    'rooms'     =>
                        [
                            'indicator' => 'Choose the indicator',
                            'startDate' => 'Choose the start date for the evaluation of the indicator',
                            'year'      => 'Select the school year',
                        ],
                    'school'    =>
                        [
                            'startDate' => 'Select the start date for the evaluation of the indicator',
                        ],
                ],
            'value' => 'Indicator value',
        ],
];
