@extends ('backend.layouts.app')

@section ('title', __('ceap.button.edit') . ' ' . removeCollon(__('ceap.agreements.evaluation_strategy.label')))

@section('content')
    {{ html()->modelForm($monitoringStrategy, 'PATCH', route('admin.ceap.monitoringStrategy.update', $monitoringStrategy->id))->class('form-horizontal')->open() }}
    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-sm-5 col-md-12">
                    <h4 class="card-title mb-0">
                        {{ removeCollon(__('ceap.agreements.evaluation_strategy.label')) }}
                        <small class="text-muted">{{ __('ceap.button.edit') }} {{ removeCollon(__('ceap.agreements.evaluation_strategy.label')) }}</small>
                    </h4>
                </div><!--col-->
            </div><!--row-->

            <hr />

            <div class="row mt-4 mb-4">
                <div class="col">
                    <div class="form-group row">
                        {{ html()->label(removeCollon(__('ceap.agreements.title.label')))
                            ->class('col-md-2 form-control-label')
                            ->for('title') }}

                        <div class="col-md-10">
                            {{ html()->text('title')
                                ->class('form-control')
                                ->placeholder(removeCollon(__('ceap.agreements.title.label')))
                                ->attribute('maxlength', 191)
                                ->value($monitoringStrategy->title)
                                ->required() }}
                        </div><!--col-->
                    </div><!--form-group-->
                </div>
            </div>
        </div><!--card-body-->

        <div class="card-footer clearfix">
            <div class="row">
                <div class="col">
                    {{ form_cancel(route('admin.ceap.monitoringStrategy.index'), __('buttons.general.cancel')) }}
                </div><!--col-->

                <div class="col text-right">
                    {{ form_submit(__('buttons.general.crud.update')) }}
                </div><!--col-->
            </div><!--row-->
        </div><!--card-footer-->
    </div><!--card-->
    {{ html()->closeModelForm() }}
@endsection