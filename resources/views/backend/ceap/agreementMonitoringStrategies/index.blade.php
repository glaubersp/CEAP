@extends ('backend.layouts.app')

@section ('title', removeCollon(__('ceap.agreements.evaluation_strategy.label')))

@section('content')
    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-sm-5">
                    <h4 class="card-title mb-0">
                        {{ removeCollon(__('ceap.agreements.evaluation_strategy.label')) }}
                    </h4>
                </div><!--col-->

                <div class="col-sm-7">
                    @include('backend.ceap.agreementMonitoringStrategies.includes.header-buttons')
                </div><!--col-->
            </div><!--row-->

            <div class="row mt-4">
                <div class="col">
                    <div class="table-responsive">
                        <table class="table">
                            <thead>
                            <tr>
                                <th>{{ removeCollon(__('ceap.agreements.title.label')) }}</th>
                                <th>{{ __('labels.general.actions') }}</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach ($monitoring_strategies as $monitoring_strategy)
                                <tr>
                                    <td>{{ $monitoring_strategy->title }}</td>
                                    <td>{!! $monitoring_strategy->action_buttons !!}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div><!--col-->
            </div><!--row-->
            <div class="row">
                <div class="col-7">
                    <div class="float-left">
                        {{$monitoring_strategy_count}} {{ trans_choice('labels.backend.ceap.monitoring_strategies.table.total', $monitoring_strategy_count) }}
                    </div>
                </div><!--col-->
            </div><!--row-->
        </div><!--card-body-->
    </div><!--card-->
@endsection

@push('after-scripts')
    <style>
        a:not([href]):not([tabindex]), a:not([href]):not([tabindex]):focus, a:not([href]):not([tabindex]):hover {
            color: white;
        }
    </style>
@endpush

