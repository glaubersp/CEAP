@extends ('backend.layouts.app')

@section ('title', __('ceap.button.edit') . ' ' . removeCollon(__('ceap.agreements.roles.label')))

@section('content')
    {{ html()->modelForm($agreementRole, 'PATCH', route('admin.ceap.agreementRole.update', $agreementRole->id))->class('form-horizontal')->open() }}
    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-sm-5 col-md-12">
                    <h4 class="card-title mb-0">
                        {{ removeCollon(__('ceap.agreements.roles.label')) }}
                        <small class="text-muted">{{ __('ceap.button.edit') }} {{ removeCollon(__('ceap.agreements.roles.label')) }}</small>
                    </h4>
                </div><!--col-->
            </div><!--row-->

            <hr />

            <div class="row mt-4 mb-4">
                <div class="col">
                    <div class="form-group row">
                        {{ html()->label(__('labels.backend.access.users.table.name'))
                            ->class('col-md-2 form-control-label')
                            ->for('agreement_roles_name') }}

                        <div class="col-md-10">
                            {{ html()->text('agreement_roles_name')
                                ->class('form-control')
                                ->placeholder(__('labels.backend.access.users.table.name'))
                                ->attribute('maxlength', 191)
                                ->value($agreementRole->name)
                                ->required() }}
                        </div><!--col-->
                    </div><!--form-group-->
                </div>
            </div>
        </div><!--card-body-->

        <div class="card-footer clearfix">
            <div class="row">
                <div class="col">
                    {{ form_cancel(route('admin.ceap.agreementRole.index'), __('buttons.general.cancel')) }}
                </div><!--col-->

                <div class="col text-right">
                    {{ form_submit(__('buttons.general.crud.update')) }}
                </div><!--col-->
            </div><!--row-->
        </div><!--card-footer-->
    </div><!--card-->
    {{ html()->closeModelForm() }}
@endsection