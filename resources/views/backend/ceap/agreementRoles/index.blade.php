@extends ('backend.layouts.app')

@section ('title', removeCollon(__('ceap.agreements.roles.label')))

@section('content')
    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-sm-5">
                    <h4 class="card-title mb-0">
                        {{ removeCollon(__('ceap.agreements.roles.label')) }}
                    </h4>
                </div><!--col-->

                <div class="col-sm-7">
                    @include('backend.ceap.agreementRoles.includes.header-buttons')
                </div><!--col-->
            </div><!--row-->

            <div class="row mt-4">
                <div class="col">
                    <div class="table-responsive">
                        <table class="table">
                            <thead>
                            <tr>
                                <th>{{ __('labels.backend.access.users.table.name') }}</th>
                                <th>{{ __('labels.general.actions') }}</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach ($agreement_roles as $agreement_role)
                                <tr>
                                    <td>{{ $agreement_role->name }}</td>
                                    <td>{!! $agreement_role->action_buttons !!}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div><!--col-->
            </div><!--row-->
            <div class="row">
                <div class="col-7">
                    <div class="float-left">
                        {{$agreement_roles_count}} {{ trans_choice('labels.backend.ceap.agreement_role.table.total', $agreement_roles_count) }}
                    </div>
                </div><!--col-->
            </div><!--row-->
        </div><!--card-body-->
    </div><!--card-->
@endsection

@push('after-scripts')
    <style>
        a:not([href]):not([tabindex]), a:not([href]):not([tabindex]):focus, a:not([href]):not([tabindex]):hover {
            color: white;
        }
    </style>
@endpush

