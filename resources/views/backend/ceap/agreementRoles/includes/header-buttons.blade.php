<div class="btn-toolbar float-right" role="toolbar" aria-label="Toolbar with button groups">
    <a href="{{ route('admin.ceap.agreementRole.create') }}" class="btn btn-success ml-1" data-toggle="tooltip" title="Criar Novo Registro"><i class="fa fa-plus-circle"></i> {{  __('ceap.menu.planning.title') }}</a>
</div><!--btn-toolbar-->