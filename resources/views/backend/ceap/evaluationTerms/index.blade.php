@extends ('backend.layouts.app')

@section ('title', __('ceap.backend.evaluation_term.title'))

@section('content')
    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-sm-5">
                    <h4 class="card-title mb-0">
                        {{ __('ceap.backend.evaluation_term.title') }}
                    </h4>
                </div><!--col-->

                <div class="col-sm-7">
                    @include('backend.ceap.evaluationTerms.includes.header-buttons')
                </div><!--col-->
            </div><!--row-->

            <div class="row mt-4">
                <div class="col">
                    <div class="table-responsive">
                        <table class="table">
                            <thead>
                            <tr>
                                <th>{{ removeCollon(__('ceap.indicators.records.year.label')) }}</th>
                                <th>Periodicidade</th>
                                <th>{{ __('labels.general.actions') }}</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach ($terms_select as $term)
                                <tr>
                                    <td>{{ $term->year }}</td>
                                    <td>{{ $term->name }}</td>
                                    <td>{!! $term->action_buttons !!}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div><!--col-->
            </div><!--row-->
            <div class="row">
                <div class="col-7">
                    <div class="float-left">
                        {{$terms_count}} {{ trans_choice('labels.backend.ceap.evaluation_term.table.total', $terms_count) }}
                    </div>
                </div><!--col-->
            </div><!--row-->
        </div><!--card-body-->
    </div><!--card-->
@endsection

@push('after-scripts')
    <style>
        a:not([href]):not([tabindex]), a:not([href]):not([tabindex]):focus, a:not([href]):not([tabindex]):hover {
            color: white;
        }
    </style>
@endpush

