@extends ('backend.layouts.app')

@section ('title', __('ceap.button.create') . ' '. __('ceap.backend.evaluation_term.title'))

@section('content')
    {{ html()->form('POST', route('admin.ceap.evaluationTerm.store'))->class('form-horizontal')->open() }}
    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-sm-5 col-md-12">
                    <h4 class="card-title mb-0">
                       {{ __('ceap.backend.evaluation_term.title') }}
                        <small class="text-muted">{{ __('ceap.button.create') }} {{ __('ceap.backend.evaluation_term.title')  }}</small>
                    </h4>
                </div><!--col-->
            </div><!--row-->

            <hr />

            <div class="row mt-4 mb-4">
                <div class="col">
                    <div class="form-group row">
                        {{ html()->label(removeCollon(__('ceap.indicators.records.year.label')))->class('col-md-2 form-control-label')->for('year') }}

                        <div class="col-md-10">
                            {{ Form::selectYear('year', 2015, 2025, date('Y'), ['class' => 'form-control']) }}
                        </div><!--col-->
                    </div><!--form-group-->
                    <div class="form-group row">
                        {{ html()->label(__('ceap.actions.evaluation_term.periodicity'))->class('col-md-2 form-control-label')->for('cycle') }}

                        <div class="col-md-10">
                            <select name="cycle" id="cycle" class="form-control">
                                <option value="" selected disabled="">{{ __('ceap.actions.evaluation_term.placeholder') }}</option>
                                <option value="2">{{ __('ceap.actions.evaluation_term.semester') }}</option>
                            </select>
                        </div><!--col-->
                    </div><!--form-group-->
                </div>
            </div>
        </div><!--card-body-->

        <div class="card-footer clearfix">
            <div class="row">
                <div class="col">
                    {{ form_cancel(route('admin.ceap.evaluationTerm.index'), __('buttons.general.cancel')) }}
                </div><!--col-->

                <div class="col text-right">
                    {{ form_submit(__('buttons.general.crud.create')) }}
                </div><!--col-->
            </div><!--row-->
        </div><!--card-footer-->
    </div><!--card-->
    {{ html()->form()->close() }}
@endsection