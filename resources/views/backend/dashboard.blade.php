@extends('backend.layouts.app')

@section('content')
    <div class="row">
        <div class="col">
            <div class="card">
                <div class="card-header">
                    <strong>{{ __('strings.backend.dashboard.welcome') }} {{ $logged_in_user->name }}!</strong>
                </div><!--card-header-->
                <div class="card-block">
                    <div class="row">
                        {{--Gerenciamento de Usuários--}}
                        <div class="col-sm-6 col-lg-3">
                            <div class="card text-white bg-secondary">
                                <div class="card-body pb-0">
                                    <h2 class="mb-0">{{$users}}</h2>
                                    <p>{{__('labels.backend.access.users.active')}}</p>
                                </div>
                                <a href="{{ route('admin.auth.user.index') }}" style="text-decoration: none;">
                                    <button type="button" class="btn btn-success btn-block">{{__('ceap.menu.management.title')}}</button>
                                </a>
                            </div>
                        </div>
                        <!--/.col-->

                        {{--Gerenciamento de Papéis--}}
                        <div class="col-sm-6 col-lg-3">
                            <div class="card text-white bg-secondary">
                                <div class="card-body pb-0">
                                    <h2 class="mb-0">{{$roles}}</h2>
                                    <p>{{__('labels.backend.access.users.table.roles')}}</p>
                                </div>
                                <a href="{{ route('admin.auth.role.index') }}" style="text-decoration: none;">
                                    <button type="button" class="btn btn-success btn-block">{{__('ceap.menu.management.title')}}</button>
                                </a>
                            </div>
                        </div>
                        <!--/.col-->

                        {{--Períodos de Avaliação--}}
                        <div class="col-sm-6 col-lg-3">
                            <div class="card text-white bg-secondary">
                                <div class="card-body pb-0">
                                    <h2 class="mb-0">{{$evaluation_terms}}</h2>
                                    <p>{{ __('ceap.backend.evaluation_term.title') }}</p>
                                </div>
                                <a href="{{ route('admin.ceap.evaluationTerm.index') }}" style="text-decoration: none;">
                                    <button type="button" class="btn btn-success btn-block">{{__('ceap.menu.management.title')}}</button>
                                </a>
                            </div>
                        </div>
                        <!--/.col-->

                        {{--Formas de Monitoramento--}}
                        <div class="col-sm-6 col-lg-3">
                            <div class="card text-white bg-secondary">
                                <div class="card-body pb-0">
                                    <h2 class="mb-0">{{$monitoring_strategy}}</h2>
                                    <p>{{ removeCollon(__('ceap.agreements.evaluation_strategy.label')) }}</p>
                                </div>
                                <a href="{{ route('admin.ceap.monitoringStrategy.index') }}" style="text-decoration: none;">
                                    <button type="button" class="btn btn-success btn-block">{{__('ceap.menu.management.title')}}</button>
                                </a>
                            </div>
                        </div>
                        <!--/.col-->

                        {{--Assuntos Relacionados--}}
                        <div class="col-sm-6 col-lg-3">
                            <div class="card text-white bg-secondary">
                                <div class="card-body pb-0">
                                    <h2 class="mb-0">{{$topic_tags}}</h2>
                                    <p>{{ removeCollon(__('ceap.agreements.topic_tags.label')) }}</p>
                                </div>
                                <a href="{{ route('admin.ceap.topicTag.index') }}" style="text-decoration: none;">
                                    <button type="button" class="btn btn-success btn-block">{{__('ceap.menu.management.title')}}</button>
                                </a>
                            </div>
                        </div>
                        <!--/.col-->

                        {{--Envolvidos--}}
                        <div class="col-sm-6 col-lg-3">
                            <div class="card text-white bg-secondary">
                                <div class="card-body pb-0">
                                    <h2 class="mb-0">{{$agreement_roles}}</h2>
                                    <p>{{ removeCollon(__('ceap.agreements.roles.label')) }}</p>
                                </div>
                                <a href="{{ route('admin.ceap.agreementRole.index') }}" style="text-decoration: none;">
                                    <button type="button" class="btn btn-success btn-block">{{__('ceap.menu.management.title')}}</button>
                                </a>
                            </div>
                        </div>
                        <!--/.col-->

                    </div>
                </div><!--card-block-->
            </div><!--card-->
        </div><!--col-->
    </div><!--row-->
@endsection