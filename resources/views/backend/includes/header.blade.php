<header class="app-header navbar">
    <button class="navbar-toggler mobile-sidebar-toggler d-lg-none mr-auto" type="button">☰</button>
    <a class="navbar-brand" href="{{ route('frontend.index') }}"></a>

    <ul class="nav navbar-nav ml-auto" style="margin-right: 15px;">
        <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle nav-link" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
                <span class="d-md-down-none">{{ $logged_in_user->full_name }}</span>
            </a>
            <div class="dropdown-menu dropdown-menu-right">
                <div class="dropdown-header text-center">
                    {{ trans('menus.language-picker.language') }}
                </div>
                @foreach (array_keys(config('locale.languages')) as $lang)
                    @if ($lang != app()->getLocale())
                        <a href="{{ url('/lang/'.$lang) }}"
                               class="dropdown-item">{{ __('menus.language-picker.langs.'.$lang) }}</a>
                    @endif
                @endforeach
                <div class="dropdown-header text-center">
                    {{ $logged_in_user->full_name }}
                </div>
                <a class="dropdown-item" href="{{ route('frontend.index') }}"><i class="icon-home"></i> {{ __('navs.frontend.dashboard') }}</a>
                <a class="dropdown-item" href="{{ route('frontend.auth.logout') }}"><i class="fa fa-lock"></i> {{ __('navs.general.logout') }}</a>
            </div>
        </li>
    </ul>
</header>