<div class="sidebar">
    <nav class="sidebar-nav">
        <ul class="nav">
            {{--GERAL--}}
            <li class="nav-title">
                {{ __('menus.backend.sidebar.general') }}
            </li>

            {{--Painel Geral--}}
            <li class="nav-item">
                <a class="nav-link {{ active_class(Active::checkUriPattern('admin/dashboard')) }}" href="{{ route('admin.dashboard') }}"><i class="icon-speedometer"></i> {{ __('menus.backend.sidebar.dashboard') }}</a>
            </li>

            {{--SISTEMA--}}
            <li class="nav-title">
                {{ __('menus.backend.sidebar.system') }}
            </li>

            @if ($logged_in_user->isAdmin())
                {{--Usuarios--}}
                <li class="nav-item nav-dropdown {{ active_class(Active::checkUriPattern('admin/auth*'), 'open') }}">
                    <a class="nav-link nav-dropdown-toggle" href="#">
                        <i class="icon-user"></i> {{ __('menus.backend.access.title') }}

                        @if ($pending_approval > 0)
                            <span class="badge badge-danger">{{ $pending_approval }}</span>
                        @endif
                    </a>

                    <ul class="nav-dropdown-items">
                        {{--Gerenciamento de Usuários--}}
                        <li class="nav-item">
                            <a class="nav-link {{ active_class(Active::checkUriPattern('admin/auth/user*')) }}" href="{{ route('admin.auth.user.index') }}">
                                {{ __('labels.backend.access.users.management') }}

                                @if ($pending_approval > 0)
                                    <span class="badge badge-danger">{{ $pending_approval }}</span>
                                @endif
                            </a>
                        </li>
                        {{--Gerenciamento de Papéis--}}
                        <li class="nav-item">
                            <a class="nav-link {{ active_class(Active::checkUriPattern('admin/auth/role*')) }}" href="{{ route('admin.auth.role.index') }}">
                                {{ __('labels.backend.access.roles.management') }}
                            </a>
                        </li>
                    </ul>
                </li>
            @endif
            {{--Fim do menu SISTEMA--}}

            {{--CEAP--}}
            @can('manage-ceap')
            <li class="nav-item nav-dropdown {{ active_class(Active::checkUriPattern('admin/ceap*'), 'open') }}">
                <a class="nav-link nav-dropdown-toggle" href="#">
                    <i class="icon-pin"></i> CEAP
                </a>

                {{--Períodos de Avaliação--}}
                <ul class="nav-dropdown-items">
                    <li class="nav-item">
                        <a class="nav-link {{ active_class(Active::checkUriPattern('admin/ceap/evaluationTerm*')) }}" href="{{ route('admin.ceap.evaluationTerm.index') }}">
                            {{ __('ceap.backend.evaluation_term.title') }}
                        </a>
                    </li>
                </ul>

                {{--Formas de Monitoramento--}}
                <ul class="nav-dropdown-items">
                    <li class="nav-item">
                        <a class="nav-link {{ active_class(Active::checkUriPattern('admin/ceap/monitoringStrategy*')) }}" href="{{ route('admin.ceap.monitoringStrategy.index') }}">
                            {{ removeCollon(__('ceap.agreements.evaluation_strategy.label')) }}
                        </a>
                    </li>
                </ul>

                {{--Assuntos Relacionados--}}
                <ul class="nav-dropdown-items">
                    <li class="nav-item">
                        <a class="nav-link {{ active_class(Active::checkUriPattern('admin/ceap/topicTag*')) }}" href="{{ route('admin.ceap.topicTag.index') }}">
                            {{ removeCollon(__('ceap.agreements.topic_tags.label')) }}
                        </a>
                    </li>
                </ul>

                {{--Envolvidos--}}
                <ul class="nav-dropdown-items">
                    <li class="nav-item">
                        <a class="nav-link {{ active_class(Active::checkUriPattern('admin/ceap/agreementRole*')) }}" href="{{ route('admin.ceap.agreementRole.index') }}">
                            {{ removeCollon(__('ceap.agreements.roles.label')) }}
                        </a>
                    </li>
                </ul>
            </li>
            @endcan
            {{--Fim do menu CEAP--}}

            {{--Logs--}}
            <li class="nav-item nav-dropdown {{ active_class(Active::checkUriPattern('admin/log-viewer*'), 'open') }}">
                <a class="nav-link nav-dropdown-toggle" href="#">
                    <i class="icon-list"></i> {{ __('menus.backend.log-viewer.main') }}
                </a>

                {{--Painel de Controle--}}
                <ul class="nav-dropdown-items">
                    <li class="nav-item">
                        <a class="nav-link {{ active_class(Active::checkUriPattern('admin/log-viewer')) }}" href="{{ route('log-viewer::dashboard') }}">
                            {{ __('menus.backend.log-viewer.dashboard') }}
                        </a>
                    </li>
                    {{--Logs--}}
                    <li class="nav-item">
                        <a class="nav-link {{ active_class(Active::checkUriPattern('admin/log-viewer/logs*')) }}" href="{{ route('log-viewer::logs.list') }}">
                            {{ __('menus.backend.log-viewer.logs') }}
                        </a>
                    </li>
                </ul>
            </li>
        </ul>
    </nav>
</div><!--sidebar-->