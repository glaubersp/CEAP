{!! Form::model($logged_in_user, [ 'method' =>'PATCH', 'route' => ['frontend.user.profile.update']]) !!}
{{--First Name--}}
<div class="form-group">
    {{ html()->label(__('validation.attributes.frontend.first_name'))->for('first_name') }}

    {{ html()->text('first_name', $logged_in_user->first_name)
        ->class('form-control')
        ->placeholder(__('validation.attributes.frontend.first_name'))
        ->attribute('maxlength', 191)
        ->required()
        ->autofocus()
    }}
</div><!--form-group-->

{{--Last Name--}}
<div class="form-group">
    {{ html()->label(__('validation.attributes.frontend.last_name'))->for('last_name') }}

    {{ html()->text('last_name', $logged_in_user->last_name)
        ->class('form-control')
        ->placeholder(__('validation.attributes.frontend.last_name'))
        ->attribute('maxlength', 191)
        ->required()
    }}
</div><!--form-group-->

@if ($logged_in_user->canChangeEmail())
    <div class="alert alert-info">
        <i class="fa fa-info-circle"></i> {{  __('strings.frontend.user.change_email_notice') }}
    </div>

    <div class="form-group">
        {{ html()->label(__('validation.attributes.frontend.email'))->for('email') }}

        {{ html()->email('email', $logged_in_user->email)
            ->class('form-control')
            ->placeholder(__('validation.attributes.frontend.email'))
            ->attribute('maxlength', 191)
            ->required()
        }}
    </div><!--form-group-->
@endif

{{--Fuso horário--}}
<div class="form-group">
    {{ html()->label(__('validation.attributes.frontend.timezone'))->for('timezone') }}

    <select name="timezone" id="timezone" class="form-control" required="required">
        @foreach (timezone_identifiers_list() as $timezone)
            <option value="{{ $timezone }}" {{ $timezone == $logged_in_user->timezone ? 'selected' : '' }} {{ $timezone == old('timezone') ? ' selected' : '' }}>{{ $timezone }}</option>
        @endforeach
    </select>
</div><!--form-group-->

<div class="form-group clearfix">
    {{ form_submit(__('labels.general.buttons.update')) }}
</div><!--form-group-->

{!! Form::close() !!}
