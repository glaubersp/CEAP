@extends('frontend.layouts.app')

@section('content')
    <div class="form-group col-md-12 col-sm-12">
        <div class="panel" style="border-color:#D8E6E7">
            <!-- Nav tabs -->
            <ul class="nav nav-tabs nav-justified" role="tablist">
                <li role="presentation" class="active">
                    <a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">
                        {{ __('navs.frontend.user.profile') }}
                    </a>
                </li>
                <li role="presentation">
                    <a href="#edit" aria-controls="edit" role="tab" data-toggle="tab">
                        {{ __('labels.frontend.user.profile.update_information') }}
                    </a>
                </li>
                <li role="presentation">
                    <a href="#password" aria-controls="password" role="tab" data-toggle="tab">
                        {{ __('navs.frontend.user.change_password') }}
                    </a>
                </li>
            </ul>

            <!-- Tab panes -->
            <div class="tab-content">
                <div role="tabpanel" class="tab-pane fade in active" id="profile">
                    <div class="panel-body">
                        @include('frontend.user.account.tabs.profile')
                    </div>
                </div>
                <div role="tabpanel" class="tab-pane fade" id="edit">
                    <div class="panel-body">
                        @include('frontend.user.account.tabs.edit')
                    </div>
                </div>
                <div role="tabpanel" class="tab-pane fade" id="password">
                    @if ($logged_in_user->canChangePassword())
                    <div class="panel-body">
                        @include('frontend.user.account.tabs.change-password')
                    </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
@endsection

@push('after-styles')
    <style>
        .form-group > div > label {
            font-weight: 500
        }
        /* bootstrap hack: fix content width inside hidden tabs */
        /* Source: http://bit.ly/2stcaoX */
        .tab-content > .tab-pane:not(.active),
        .pill-content > .pill-pane:not(.active) {
            display: block;
            height: 0;
            overflow-y: hidden;
        }

        .panel.panel-default {
            border-top: 0;
        }

        .nav-tabs > li > a, .nav-tabs > li > a:hover {
            text-decoration: none;
            background-color: #eee;
            border-top: 1px solid #ddd;
            border-right: 1px solid #ddd;
            border-left: 1px solid #ddd;
        }

        .panel-heading {
            display: flex;
            justify-content: space-between;
        }

        .btn-mini {
            padding-bottom: 0;
            padding-top: 0;
        }

        ul .active {
            font-weight: bold;
        }

        @media print {
            .highcharts-container {
                margin: auto;
                margin-top: 15px;
                margin-bottom: 15px;
            }
        }

    </style>
@endpush
