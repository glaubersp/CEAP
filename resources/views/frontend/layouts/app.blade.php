<!doctype html>

@langrtl
<html lang="{{ app()->getLocale() }}" dir="rtl">
@else
<html lang="{{ app()->getLocale() }}">
@endlangrtl

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    {{--<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">--}}
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>@yield('title', app_name())</title>

    <!-- Meta -->
    <link rel="shortcut icon" href="{{ asset('favicon.ico') }}">
    <meta name="description" content="@yield('meta_description', 'Laravel 5 Boilerplate')">
    <meta name="author" content="@yield('meta_author', 'Anthony Rappa')">
    @yield('meta')

<!-- Styles -->
    {{-- See https://laravel.com/docs/5.5/blade#stacks for usage --}}
    @stack('before-styles')

<!-- Check if the language is set to RTL, so apply the RTL layouts -->
    <!-- Otherwise apply the normal LTR layouts -->
    @langrtl
    {{ style(getRtlCss(mix('css/frontend.css'))) }}
    @else
        {{ style(mix('css/frontend.css')) }}
        @endlangrtl

    @stack('after-styles')

    <!-- Scripts -->
    <script>
      window.Laravel = <?php echo json_encode([
            'csrfToken' => csrf_token(),
        ]); ?>
    </script>
</head>
<body id="app-layout">

@include('includes.partials.logged-in-as')

@include('frontend.includes.navbar')

<div id='app'>
    <div class='container'>
        <div class='row'>
            @auth
                <div class="col-md-2 col-sm-3">
                    @include('frontend.includes.sidenav')
                </div><!--col-md-2 col-sm-3-->
            @endauth
            <div class="col-md-10 col-sm-9">
                @auth
                    {!! Breadcrumbs::render() !!}
                @endauth
                @include('includes.partials.messages')
                @yield('content')
            </div><!--col-md-10 col-sm-9-->
        </div><!--row-->
    </div><!-- container -->
</div><!--#app-->
<hr>
<footer>
    <p>Projeto CEAP - NIED/UNICAMP</p>
</footer>

<!-- Scripts -->
@include('includes.partials.scripts')
{{--    @include('includes.partials.ga')--}}

</body>
</html>
