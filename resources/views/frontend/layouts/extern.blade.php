<!doctype html>

@langrtl
<html lang="{{ app()->getLocale() }}" dir="rtl">
@else
    <html lang="{{ app()->getLocale() }}">
    @endlangrtl

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    {{--<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">--}}
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>@yield('title', app_name())</title>

    <!-- Meta -->
    <link rel="shortcut icon" href="{{ asset('favicon.ico') }}">
    <meta name="description" content="@yield('meta_description', 'Laravel 5 Boilerplate')">
    <meta name="author" content="@yield('meta_author', 'Anthony Rappa')">
    @yield('meta')

<!-- Styles -->
    @yield('before-styles')

<!-- Check if the language is set to RTL, so apply the RTL layouts -->
    <!-- Otherwise apply the normal LTR layouts -->
    @langrtl
    {{ style(getRtlCss(mix('css/frontend.css'))) }}
    @else
        {{ style(mix('css/frontend.css')) }}
        @endlangrtl

    {{--{{ Html::style(mix('css/ceap.css')) }}--}}

    @yield('after-styles')

    <!-- Scripts -->
    <script>
        window.Laravel = <?php echo json_encode([
            'csrfToken' => csrf_token(),
        ]); ?>
    </script>
</head>
<body id="app-layout">


<nav class="navbar navbar-default">
    <div class="container-fluid">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">

            <!-- Collapsed Hamburger -->
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                    data-target="#extern-navbar-collapse">
                <span class="sr-only">@lang('labels.general.toggle_navigation')</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>

        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="extern-navbar-collapse">
            <ul class="nav navbar-nav navbar-right">
                <li><a href="{{ route('frontend.about') }}">@lang('ceap.app.about')</a></li>
                <li><a href="{{ route('frontend.index') }}">@lang('ceap.button.login')</a></li>
            </ul>
        </div><!-- /.navbar-collapse -->
    </div><!-- /.container-fluid -->
</nav>

<div class='container'>
    <div class='row'>
        <div id='app'>
            <div class="col-md-12 col-sm-12">
                @yield('content')
            </div><!-- container -->
        </div><!--#app-->

        <!-- Scripts -->
@yield('before-scripts')
{!! script(mix('js/frontend.js')) !!}
@yield('after-scripts')

{{--@include('includes.partials.ga')--}}
</body>
</html>