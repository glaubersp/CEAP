@extends('frontend.layouts.app')

@section('content')
<div class="col-md-12 col-sm-6">
    <h1 class="text-center title-login"> {{ config('app.name') }}
        <br>
    </h1>
</div>
<div class="col-md-12 col-sm-6">
    <div class="hidden-xs text-center text-well-login well well-lg">
        <h2>@lang('ceap.ceap_title')</h2>
    </div>
    <div class="hidden-lg hidden-md hidden-sm well well-sm">
        <h4 class="text-center">@lang('ceap.ceap_title')</h4>
    </div>
</div>
@endsection
