<nav class="navbar navbar-default">
    <div class="container">
        <div class="navbar-header">

            <!-- Collapsed Hamburger -->
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                    data-target="#app-navbar-collapse">
                <span class="sr-only">@lang('labels.general.toggle_navigation')</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>

            <!-- Branding Image -->
            <a class="navbar-brand" href="{{ route('frontend.index') }}">
                <span>
                    <img alt="Logotipo" src="{{ url('/') }}/img/frontend/logo_escolas.png" class="logo-navbar">
                    <span class="hidden-xs">{{ app_name() }}</span>
                </span>
            </a>
        </div>

        <div class="collapse navbar-collapse" id="app-navbar-collapse">
            <!-- Right Side Of Navbar -->
            <ul class="nav navbar-nav navbar-right">

                @if(! $logged_in_user)
                    {{--<li>{{ link_to_route('frontend.auth.login', trans('navs.frontend.login')) }}</li>--}}
                    {{--<li>{{ link_to_route('frontend.auth.register', trans('navs.frontend.register')) }}</li>--}}
                @else
                    <!-- Painel Geral -->
                    <div class="hidden-lg hidden-md hidden-sm panel-group" style="margin-bottom:0px" id="accordion-menu"
                         role="tablist" aria-multiselectable="true">
                        <div class="panel home-color">
                            <div class="panel-heading" role="tab" id="heading-home"
                                 style="margin-bottom: -18px;padding-right: 0px;">
                                <h4 class="panel-title home-icon">
                                    <a href="{{ route('frontend.index') }}"
                                       style="display: block;margin-left: 23px;">{{ trans('navs.frontend.dashboard') }}</a>
                                    <br>
                                </h4>
                            </div>
                            <div id="collapse-home" class="panel-collapse collapse" role="tabpanel"
                                 aria-labelledby="heading-home"></div>
                        </div>
                        <!-- Adicionar -->
                        @can('can-create')
                        <div class="panel planning-color">
                            <div class="panel-heading" role="tab" id="heading-planning">
                                <h4 class="panel-title collapsed accordion-toggle" role="button" data-toggle="collapse"
                                    data-parent="#accordion-menu" href="#collapse-planning" aria-expanded="false"
                                    aria-controls="collapse-planning">
                                    <a style="margin-left: 10px;">{{ trans('ceap.menu.planning.title') }}</a>
                                </h4>
                            </div>
                            <div id="collapse-planning" class="panel-collapse collapse" role="tabpanel"
                                 aria-labelledby="heading-planning">
                                <div id="navbar-planning-hover">
                                    <!--Ação-->
                                    <a class="list-group-item planning-color-border"
                                       href="{{ route('frontend.ceap.actions.create') }}">
                                        {{ trans('ceap.menu.planning.action') }}</a>
                                    <!--Objetivo-->
                                    <a class="list-group-item planning-color-border"
                                       href="{{ route('frontend.ceap.goals.create') }}">
                                        {{ trans('ceap.menu.planning.goal') }}</a>
                                    <!--Indicador-->
                                    <a class="list-group-item planning-color-border"
                                       href="{{ route('frontend.ceap.indicators.create') }}">
                                        {{ trans('ceap.menu.planning.indicator') }}</a>
                                </div>
                            </div>
                        </div>
                        @endcan
                        <!-- Acompanhar -->
                        <div class="panel monitoring-color">
                            <div class="panel-heading" role="tab" id="heading-monitoring">
                                <h4 class="panel-title collapsed accordion-toggle" role="button" data-toggle="collapse"
                                    data-parent="#accordion-menu" href="#collapse-monitoring" aria-expanded="false"
                                    aria-controls="collapse-monitoring">
                                    <a style="margin-left: 10px;">{{ trans('ceap.menu.monitoring.title') }}</a>
                                </h4>
                            </div>
                            <div id="collapse-monitoring" class="panel-collapse collapse" role="tabpanel"
                                 aria-labelledby="heading-monitoring">
                                <div id="navbar-monitoring-hover">
                                    <!--Ações-->
                                    <a class="list-group-item monitoring-color-border"
                                       href="{{ route('frontend.ceap.actions.index') }}">
                                        {{ trans('ceap.menu.monitoring.actions') }}</a>
                                    <!--Objetivos-->
                                    <a class="list-group-item monitoring-color-border"
                                       href="{{ route('frontend.ceap.goals.index') }}">
                                        {{ trans('ceap.menu.monitoring.goals') }}</a>
                                    <!--Indicadores-->
                                    <a class="list-group-item monitoring-color-border"
                                       href="{{ route('frontend.ceap.indicators.index') }}">
                                        {{ trans('ceap.menu.monitoring.indicators') }}</a>
                                </div>
                            </div>
                        </div>
                        <!-- Combinados -->
                        <div class="panel agreements-color">
                            <div class="panel-heading" role="tab" id="heading-agreements">
                                <h4 class="panel-title collapsed accordion-toggle" role="button" data-toggle="collapse"
                                    data-parent="#accordion-menu" href="#collapse-agreements" aria-expanded="false"
                                    aria-controls="collapse-agreements">
                                    <a style="margin-left: 10px;">{{ trans('ceap.menu.agreements.title') }}</a>
                                </h4>
                            </div>
                            <div id="collapse-agreements" class="panel-collapse collapse" role="tabpanel"
                                 aria-labelledby="heading-agreements">
                                <div id="navbar-agreements-hover">
                                    <!--Criar-->
                                    @can('can-create')
                                    <a class="list-group-item agreements-color-border"
                                       href="{{ route('frontend.ceap.agreements.create') }}">
                                        {{ trans('ceap.menu.agreements.create') }}</a>
                                    @endcan
                                    <!--Listar-->
                                    <a class="list-group-item agreements-color-border"
                                       href="{{ route('frontend.ceap.agreements.index') }}">
                                        {{ trans('ceap.menu.agreements.list') }}</a>
                                </div>
                            </div>
                        </div>
                        <!-- Gerenciar -->
                        <div class="panel management-color">
                            <div class="panel-heading" role="tab" id="heading-management">
                                <h4 class="panel-title collapsed accordion-toggle" role="button" data-toggle="collapse"
                                    data-parent="#accordion-menu" href="#collapse-management" aria-expanded="false"
                                    aria-controls="collapse-management">
                                    <a style="margin-left: 10px;">{{trans('ceap.menu.management.title')}}</a>
                                </h4>
                            </div>
                            @can('view-backend')
                            <div id="collapse-management" class="panel-collapse collapse" role="tabpanel"
                                 aria-labelledby="heading-management">
                                <div id="navbar-management-hover">
                                    <a class="list-group-item management-color-border"
                                       href="{{ route('admin.dashboard') }}">
                                        {{ trans('navs.frontend.user.administration') }}</a>
                                </div>
                            </div>
                            @endcan
                        </div>
                    </div>

                    @include('includes.partials.lang')

                    <li class="hidden-sm hidden-md">
                        <a href="{{ route('frontend.ceap.help') }}"><i
                                class="fa fa-question-circle fa-fw"></i>@lang('ceap.menu.help')</a>
                    </li>

                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                            <i class="fa fa-user fa-fw"></i>
                            {{ $logged_in_user->name }}
                            <span class="caret"></span>
                        </a>

                        <ul class="dropdown-menu" role="menu">
                            @can('view-backend')
                            <li>{{ link_to_route('admin.dashboard', trans('navs.frontend.user.administration')) }}</li>
                            @endcan

                            <li>{{ link_to_route('frontend.user.account', trans('navs.frontend.user.account')) }}</li>
                            <li role="separator" class="divider"></li>

                            <li class="hidden-xs hidden-lg">
                                <a href="{{ route('frontend.ceap.help') }}">@lang('ceap.menu.help')</a>
                            </li>

                            <li>{{ link_to_route('frontend.auth.logout', trans('navs.general.logout')) }}</li>
                        </ul>
                    </li>

                @endif
            </ul>
        </div>
    </div>
</nav>