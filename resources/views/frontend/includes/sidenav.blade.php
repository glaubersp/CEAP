<ul class="hidden-xs list-group">
    <!-- Painel Geral -->
    <a class="list-group-item home-color" href="{{ url('/') }}">{{ trans('navs.frontend.dashboard') }}</a>
    <!-- Adicionar -->
    @can('can-create')
    <a class="list-group-item planning-color">{{ trans('ceap.menu.planning.title') }}</a>
    <div id="planning-hover">
        <!--Ação-->
        <a class="list-group-item planning-color-border btn-actions-create"
           href="{{ route('frontend.ceap.actions.create') }}">
            {{ trans('ceap.menu.planning.action') }}</a>
        <!--Objetivo-->
        <a class="list-group-item planning-color-border btn-goals-create"
           href="{{ route('frontend.ceap.goals.create') }}">
            {{ trans('ceap.menu.planning.goal') }}</a>
        <!--Indicador-->
        <a class="list-group-item planning-color-border btn-indicators-create"
           href="{{ route('frontend.ceap.indicators.create') }}">
            {{ trans('ceap.menu.planning.indicator') }}</a>
    </div>
    @endcan
    <!-- Acompanhar -->
    <a class="list-group-item monitoring-color">{{ trans('ceap.menu.monitoring.title') }}</a>
    <div id="monitoring-hover">
        <!--Ações-->
        <a class="list-group-item monitoring-color-border btn-monitoring-actions"
           href="{{ route('frontend.ceap.actions.index') }}">
            {{ trans('ceap.menu.monitoring.actions') }}</a>
        <!--Objetivos-->
        <a class="list-group-item monitoring-color-border btn-monitoring-goals"
           href="{{ route('frontend.ceap.goals.index') }}">
            {{ trans('ceap.menu.monitoring.goals') }}</a>
        <!--Indicadores-->
        <a class="list-group-item monitoring-color-border btn-monitoring-indicators"
           href="{{ route('frontend.ceap.indicators.index') }}">
            {{ trans('ceap.menu.monitoring.indicators') }}</a>
    </div>
    <!-- Combinados -->
    <a class="list-group-item agreements-color">{{ trans('ceap.menu.agreements.title') }}</a>
    <div id="agreements-hover">
        @can('can-create')
        <!--Criar-->
        <a class="list-group-item agreements-color-border btn-agreements-create"
           href="{{ route('frontend.ceap.agreements.create') }}">
            {{ trans('ceap.menu.agreements.create') }}</a>
        @endcan
        <!--Listar-->
        <a class="list-group-item agreements-color-border btn-agreements-index"
           href="{{ route('frontend.ceap.agreements.index') }}">
            {{ trans('ceap.menu.agreements.list') }}</a>
    </div>
    <!-- Gerenciar -->
    @can('view-backend')
    <a class="list-group-item management-color">{{trans('ceap.menu.management.title')}}</a>
    <div id="management-hover">
        <a class="list-group-item management-color-border"
           href="{{ route('admin.dashboard') }}">
            {{ trans('navs.frontend.user.administration') }}</a>
    </div>
    @endcan
</ul>
