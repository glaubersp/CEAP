@extends('frontend.layouts.app')

@section('content')

    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title">{{ trans('ceap.menu.help') }}</h3>
        </div>
        <div class="panel-body">
            <div class="panel" style="border-color:#F73545;">
                <div class="panel-heading planning-color" style="margin-top:0px;">
                    <h3 class="panel-title text-danger">Plan</h3>
                </div>
                <div class="panel-body">
                    <p>The Plan section allow us:</p>
                    <ul>
                        <li>to create a new indicator to be monitored over time, including charts;</li>
                        <li>to define objectives that ate connected to and might impact these indicators; and</li>
                        <li>to plan specific school based actions - specific activities - which can lead to attaining these objective.</li>
                    </ul>
                </div>
            </div>
            <div class="panel" style="border-color: #325FAD;">
                <div class="panel-heading monitoring-color" style="margin-top:0px;">
                    <h3 class="panel-title">Check</h3>
                </div>
                <div class="panel-body">
                    <p>The Check section allow us:</p>
                    <ul>
                        <li>to monitor the measured indicators over time;</li>
                        <li>to view defined objectives; and</li>
                        <li>to view actions and collect evidence of implementation;</li>
                    </ul>
                </div>
            </div>
            <div class="panel" style="border-color: #23A493;">
                <div class="panel-heading agreements-color" style="margin-top:0px;">
                    <h3 class="panel-title">Agreements</h3>
                </div>
                <div class="panel-body">
                    <p>The Agreements section allow us:</p>
                    <ul>
                        <li>to define new school agreements based on school meetings or extracted from previously successful strategies;</li>
                        <li>to define new agreements based on the success of actions evaluated in the system;</li>
                        <li>to list all the agreements that have been already defined.</li>
                    </ul>
                </div>
            </div>
            <div class="panel" style="border-color: #ffb736;">
                <div class="panel-heading management-color" style="margin-top:0px;">
                    <h3 class="panel-title">Manage</h3>
                </div>
                <div class="panel-body">
                    <p>The Manage section allow school staff:</p>
                    <ul>
                        <li>to access the administration painel.</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

@endsection
