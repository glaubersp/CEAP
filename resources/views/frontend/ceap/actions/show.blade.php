@extends('frontend.layouts.app')

@section('content')

    {{-- Modal Apagar Ação --}}
    <action-delete-modal :id="{{ $action->id }}"></action-delete-modal>

    {{-- Modal Arquivar Ação --}}
    <action-trash-modal :id="{{ $action->id }}"></action-trash-modal>

    {{-- Exibir Ação--}}
    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title">{{trans('ceap.actions.show.panel_title')}}</h3>
        </div>
        <div class="panel-body">
            {!! Form::model($action, [ 'method' =>'GET', 'route' => ['frontend.ceap.actions.edit', $action]]) !!}

            <div class="form-group col-md-12 col-sm-12">
                <div class="panel" style="border-color:#D8E6E7">
                    <!-- Nav tabs -->
                    <ul class="nav nav-tabs nav-justified" role="tablist">
                        <li role="presentation" class="active">
                            <a href="#action" aria-controls="action" role="tab" data-toggle="tab">
                                {{trans('ceap.menu.planning.action')}}
                            </a>
                        </li>
                        <li role="presentation">
                            <a href="#evidences" aria-controls="evidences" role="tab" data-toggle="tab">
                                {{trans('ceap.actions.evidences.evidence')}}
                                <span class="label label-default"
                                      style="font-size: 13px;">{{ count($action->evidences) }}</span>
                            </a>
                        </li>
                        <li role="presentation">
                            <a href="#evaluation" aria-controls="evaluation" role="tab" data-toggle="tab">
                                {{trans('ceap.actions.evaluations.evaluate')}}
                                <span class="label label-default"
                                      style="font-size: 13px;">{{ count($action->evaluations) }}</span>
                            </a>
                        </li>
                    </ul>

                    <!-- Tab panes -->
                    <div class="tab-content">
                        <div role="tabpanel" class="tab-pane fade in active" id="action">
                            <div class="panel-body">
                                <dl>
                                    <dd class="dd-title">{{ $action->name }}</dd>
                                    <dt>{{trans('ceap.actions.details.label')}}</dt>
                                    <dd style="margin-bottom:10px">
                                        <?php echo $action->details ?>
                                    </dd>
                                    <dt>{{trans('ceap.actions.goal.label')}}</dt>
                                    <dd style="margin-bottom:10px">{{ $action->goal->name }}</dd>
                                    <dt>{{trans('ceap.actions.indicator.label')}}</dt>
                                    <dd style="margin-bottom:10px">{{ $action->goal->indicator->name }}</dd>
                                    <dt>{{ trans('ceap.actions.evaluation_term.label') }}</dt>
                                    <dd style="margin-bottom:10px">
                                        <?php echo $action->evaluationTerm->year . ' - ' . $action->evaluationTerm->name ?>
                                    </dd>
                                    @if ($parent_action_collection->count() != 0)
                                        <dt>{{ trans('ceap.actions.show.related_actions') }}</dt>
                                        <dd>
                                            <table>
                                                <tr>
                                                    <th>{{ removeCollon(trans('ceap.actions.name.label')) }}</th>
                                                    <th>{{ removeCollon(trans('ceap.actions.evaluation_term.label')) }}</th>
                                                </tr>
                                                @foreach($parent_action_collection as $parent)
                                                    <tr>
                                                        <td>
                                                            <p>{{ $parent->name }}</p>
                                                        </td>
                                                        <td>{{ $parent->evaluationTerm->year . ' - '. $parent->evaluationTerm->name }}</td>
                                                    </tr>

                                                @endforeach
                                            </table>
                                        </dd>
                                    @endif
                                </dl>
                            </div>
                        </div>
                        <div role="tabpanel" class="tab-pane fade" id="evidences">
                            <div class="panel-body">
                                @if(count($action->evidences) != 0)
                                    {!! $dataTable->table() !!}
                                @else
                                    <dt>{{ trans('ceap.actions.show.no_evidence_label') }}</dt>
                                    <p>{{ trans('ceap.actions.show.no_evidence') }}</p>
                                    <a class="btn btn-primary panel-title-buttons btn-actions-evidences" href={{$action->id}}/evidences>{{ trans('ceap.actions.evidences.evidence') }}</a>
                                @endif
                            </div>
                        </div>
                        <div role="tabpanel" class="tab-pane fade" id="evaluation">
                            <div class="panel-body">
                                @if(count($action->evaluations) != 0)
                                    <dl>
                                        <dd class="dd-title">{{ $action->evaluations->last()->evaluation }}</dd>
                                        <dt>{{trans('ceap.actions.evaluations.modal.justification')}}</dt>
                                        <dd style="margin-bottom:10px">
                                            <?php echo $action->evaluations->last()->justification ?>
                                        </dd>
                                        @if(count($action->evaluations->last()->rating) != 0)
                                            <dt>{{trans('ceap.actions.evaluations.modal.result')}}</dt>
                                            <dd style="margin-bottom:10px">{{ $action->evaluations->last()->rating }}</dd>
                                        @endif
                                        <dt>{{trans('ceap.actions.evaluations.modal.date')}}</dt>
                                        <dd style="margin-bottom:10px">{{ (new \Carbon\Carbon($action->evaluations->last()->evaluation_date))->formatLocalized('%x') }}</dd>
                                    </dl>
                                @else
                                    <dt>{{ trans('ceap.actions.show.no_evaluation_label') }}</dt>
                                    <p>{{ trans('ceap.actions.show.no_evaluation') }}</p>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="form-group center-buttons col-md-12 col-sm-12">
                {{ link_to_route('frontend.ceap.actions.index', trans('ceap.button.back'), null,['type'=>'reset', 'class'=>'btn btn-primary']) }}
                @can('can-edit')
                    {{ Form::submit(trans('ceap.button.edit'), ['class'=>'btn btn-success']) }}
                @endcan
                @can('can-trash')
                    {{ Form::button(trans('ceap.button.trash'), ['class'=>'btn btn-warning deleteGroupBtn', 'data-toggle' => 'modal', 'data-target' => '#trashActionModal', 'data-title' => trans('ceap.actions.modal.trash.title')]) }}
                @endcan
                @can('can-delete')
                    {{ Form::button(trans('ceap.button.delete'), ['class'=>'btn btn-danger deleteGroupBtn', 'data-toggle' => 'modal', 'data-target' => '#deleteActionModal', 'data-title' => trans('ceap.actions.modal.trash.title')]) }}
                @endcan
            </div>
            {!! Form::close() !!}
        </div>
    </div>
@endsection


@push('after-styles')
    @include('frontend.includes.assets.datatables_styles')
    @include('frontend.includes.assets.summernote_styles')

    <style>
        /* bootstrap hack: fix content width inside hidden tabs */
        /* Source: http://bit.ly/2stcaoX */
        .tab-content > .tab-pane:not(.active),
        .pill-content > .pill-pane:not(.active) {
            display: block;
            height: 0;
            overflow-y: hidden;
        }

        table {
            width: 100%;
        }

        .panel.panel-default {
            border-top: 0;
        }

        .nav-tabs > li > a, .nav-tabs > li > a:hover {
            text-decoration: none;
            background-color: #eee;
            border-top: 1px solid #ddd;
            border-right: 1px solid #ddd;
            border-left: 1px solid #ddd;
        }

        .panel-heading {
            display: flex;
            justify-content: space-between;
        }

        .btn-mini {
            padding-bottom: 0;
            padding-top: 0;
        }

        ul .active {
            font-weight: bold;
        }

        @media print {
            .highcharts-container {
                margin: auto;
                margin-top: 15px;
                margin-bottom: 15px;
            }
        }

    </style>
@endpush

@push('after-scripts')
    @include('frontend.includes.assets.datatables_scripts')
    @include('frontend.includes.assets.summernote_scripts')


    <script type="text/javascript">
      $('.modal').on('submit', 'form[data-async]', function (event) {
        event.preventDefault()

        var $form = $(this).closest('form')
        var $target = $form.attr('data-target')

        $.ajax({
          type: $form.attr('method'),
          url: $form.attr('action'),
          data: $(this).closest('form').serialize(),
          dataType: 'json',

          success: function (jqXhr) {
            // Reload parent window
            location.reload()
          },
          error: function (jqXhr, json, errorThrown) {
            var errors = $.parseJSON(jqXhr.responseText)
            // Remove all previous errors
            $('.form-group').removeClass('has-error')
            $('.form-group').find('.remove-next').remove()
            // Show new errors
            $.each(errors, function (index, value) {
              $('#' + index).closest('.form-group').addClass('has-error')
                .append('<span class="help-block remove-next"><strong>' + value + '</strong></span></div>')
            })
          }
        })
      })
    </script>

    @if (count($errors) > 0)
        <script type="text/javascript">
          $('#actionEvidenceModal').modal('show')
        </script>
    @endif

@endpush
