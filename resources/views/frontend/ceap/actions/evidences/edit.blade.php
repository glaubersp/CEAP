@extends('frontend.layouts.app')

@section('content')
    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title"><b>{{ trans('ceap.actions.evidences.show.title') }}</b> {{ $action->name }}</h3>
        </div>
        <div class="panel-body">
            {{ Form::model($evidence, ['method' => 'PUT', 'route'=>['frontend.ceap.actions.evidences.update', $action->id, $evidence->id],
               'files'=>true]) }}
            <div class="form-group col-md-12 col-sm-12">
                {{ Form::label('title', trans('ceap.actions.evidences.modal.title'), ['class'=>'control-label']) }}
                <i class="fa fa-info-circle fa-fw" data-toggle="tooltip" data-placement="right"
                   title="{{ trans('tooltips.actions.evidences.title') }}"></i>
                {{ Form::text('title', null, ['placeholder' => trans(''), 'class' => 'form-control']) }}
            </div>
            <div class="form-group col-md-12 col-sm-12">
                {{ Form::label('details', trans('ceap.actions.evidences.modal.details'), ['class'=>'control-label']) }}
                <i class="fa fa-info-circle fa-fw" data-toggle="tooltip" data-placement="right"
                   title="{{ trans('tooltips.actions.evidences.details') }}"></i>
                {{ Form::textarea('details', null, ['type' => "text", 'rows' => "4", 'class' => 'form-control']) }}
            </div>
            <div class="form-group col-md-12 col-sm-12">
                {{ Form::label('file_name', trans('ceap.actions.evidences.show.files')) }}
                <i class="fa fa-info-circle fa-fw" data-toggle="tooltip" data-placement="right"
                   title="{{ trans('tooltips.actions.evidences.fileName') }}"></i>
                <br>
                <p>
                    @php
                        if($evidence->file_name == null) {
                            echo 'Nenhum arquivo adicionado';
                        } else {
                            echo link_to_asset('/files/'.$evidence->file_name, explode('/',$evidence->file_name)[1], ['target'=>'_blank']);
                        }
                    @endphp
                </p>
                <p><b>Para alterar o arquivo atual, use o botão abaixo: </b></p>
                {{ Form::file('file_name', null) }}
            </div>
            <div class="form-group center-buttons col-md-12 col-sm-12">
                {{ Form::submit(trans('ceap.button.save'), ['class'=>'btn btn-success']) }}
                {{ link_to_route('frontend.ceap.actions.evidences.show', trans('ceap.button.cancel'), [$action, $evidence],['type'=>'reset', 'class'=>'btn btn-danger']) }}
            </div>
            {{ Form::close() }}
        </div>
    </div>
@endsection

@push('after-styles')
    @include('frontend.includes.assets.summernote_styles')
@endpush

@push('after-scripts')
    @include('frontend.includes.assets.summernote_scripts')

    <script>
      //Summernote Editor
      $(document).ready(function () {
        $('#details').summernote({
          height: 200,
          lang: 'pt-BR',
        })
      })

    </script>
@endpush
