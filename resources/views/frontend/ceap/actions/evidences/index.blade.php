@extends('frontend.layouts.app')

@section('content')
    {{--Modal Coletar Evidências--}}
    <action-evidence-modal :action_id="{{$action->id}}"></action-evidence-modal>

    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title btn-actions-evidences">{{trans('ceap.actions.evidences.panel_title')}}</h3>
        </div>
        <div class="panel-body">
            <div class="form-group col-md-12 col-sm-12">
                <dl>
                    <dd class="dd-title">{{ $action->name }}</dd>
                    <dt>{{trans('ceap.actions.details.label')}}</dt>
                    <dd style="margin-bottom:10px">
                        <?php echo $action->details ?>
                    </dd>
                    <dt>{{trans('ceap.actions.goal.label')}}</dt>
                    <dd style="margin-bottom:10px">{{ $action->goal->name }}</dd>
                    <dt>{{trans('ceap.actions.indicator.label')}}</dt>
                    <dd style="margin-bottom:10px">{{ $action->goal->indicator->name }}</dd>
                </dl>
                <div class="panel" style="border-color:#D8E6E7">
                    <div class="panel-heading evidence-color" style="display: flex;justify-content: space-between;">
                        <h3 class="panel-title col-xs-9 col-md-10 col-sm-10 col-lg-10">{{trans('ceap.actions.evidences.evidence')}}
                            ({{ $action->evidences->count() }})
                        </h3>
                        <a class="btn btn-primary panel-title-buttons btn-actions-evidences" style="margin-top: 0px;" data-toggle="modal"
                           data-target="#actionEvidenceModal">{{trans('ceap.button.collect')}}</a>
                    </div>
                    <div class="panel-body">
                        {!! $dataTable->table() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="panel-group" id="accordion">

    </div>
@endsection

@push('after-styles')
    @include('frontend.includes.assets.datatables_styles')
    @include('frontend.includes.assets.summernote_styles')

@endpush

@push('after-scripts')
    @include('frontend.includes.assets.datatables_scripts')
    @include('frontend.includes.assets.summernote_scripts')
@endpush
