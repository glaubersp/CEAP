@extends('frontend.layouts.app')

@section('content')
    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title"><b>{{ trans('ceap.actions.evidences.show.title') }}</b> {{ $action->name }}</h3>
        </div>
        <div class="panel-body">
            {{ Form::model($evidence, ['method' =>'GET', 'route'=>['frontend.ceap.actions.evidences.edit', $action, $evidence],
               'files'=>true]) }}
            <div class="form-group col-md-12 col-sm-12">
                <dl>
                    <dt>{{trans('ceap.actions.evidences.modal.title')}}</dt>
                    <dd style="margin-bottom:10px">
                        {{ $evidence->title }}
                    </dd>
                    <dt>{{trans('ceap.actions.evidences.modal.details')}}</dt>
                    <dd style="margin-bottom:10px">
                        {!! $evidence->details !!}
                    </dd>
                    <dt>{{trans('ceap.actions.evidences.show.files')}}</dt>
                    <dd style="margin-bottom:10px">
                        @php
                            if($evidence->file_name == null) {
                                echo 'Nenhum arquivo adicionado';
                            } else {
                                echo link_to_asset('/files/'.$evidence->file_name, explode('/',$evidence->file_name)[1], ['target'=>'_blank']);
                            }
                        @endphp
                    </dd>
                </dl>
            </div>

            <div class="form-group center-buttons col-md-12 col-sm-12">
                {{ Form::submit(trans('ceap.button.edit'), ['class'=>'btn btn-success']) }}
                {{ link_to_route('frontend.ceap.actions.evidences.index', trans('ceap.button.cancel'), [$action],['type'=>'reset', 'class'=>'btn btn-danger']) }}
            </div>
            {{ Form::close() }}
        </div>
    </div>
@endsection