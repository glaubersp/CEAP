@extends('frontend.layouts.app')

@section('content')

    {{--Modal Avaliar Ação--}}
    <action-evaluation-modal></action-evaluation-modal>

    {{--Acompanhar Ações--}}
    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title btn-monitoring-actions">{{trans('ceap.actions.index.panel_title')}}</h3>
        </div>
        <div class="panel-body">
            <div class="select-temp">
                <label for="term-select">{{ trans('ceap.actions.evaluation_term.label') }}</label>
                {{ Form::select('term-select', $terms_select->prepend(trans('ceap.actions.evaluation_term.all_terms'), 0), null, ['placeholder' => trans('ceap.actions.evaluation_term.placeholder'), 'class' => 'form-control', 'style' => 'width:100%']) }}
            </div>
            <div class="checkbox-temp" id="actionTable">
                <label>
                    <input type="checkbox" id="showTrashedRegisters"> {{ trans('ceap.table.trashed_registers') }}
                </label>
            </div>
            {!! $dataTable->table() !!}

        </div>
    </div>
@endsection

@push('after-styles')
    @include('frontend.includes.assets.datatables_styles')

    <style>
        .dt-buttons.btn-group {
            float: right;
        }
    </style>

@endpush

@push('after-scripts')
    @include('frontend.includes.assets.datatables_scripts')

    <script type="text/javascript">


      $('select[name="term-select"] option:first-child').attr('disabled', 'disabled')

      $('#dataTableBuilder').find('tbody').on('change', 'select[name="priority"]', function () {
        $(this).closest('form').submit()
      })

      function alteraBusca(elem) {
        text = ''
        elem.childNodes.forEach(el => {
          return text += el.textContent + ' '
        })
        $('#dataTableBuilder').DataTable().search(text).draw()
      }

      function getActionsDataTable() {
        let table
        let permissions = {!! json_encode(auth()->user()->getAllPermissions()) !!};
        if ($.fn.DataTable.isDataTable('#dataTableBuilder')) {
          table = $('#dataTableBuilder').DataTable()
            .on( 'preDraw', function () {
              $('#new-select').append($('.select-temp').contents())
              $('#new-checkbox').append($('.checkbox-temp').contents())
            })
            .on( 'preInit.dt', function (e, settings) {
              //permissions[3] = can-create
              if(permissions[3] === undefined) {
                table.column(4).visible( false )
              } else {
                table.column(4).visible( true )
              }
            })
            .on('preXhr.dt', function (e, settings, data) {
              data.checked = $('#showTrashedRegisters:checked').length
              data.term_id = $('select[name="term-select"] option:selected').val()
            })
            .on('stateSaveParams.dt', function (e, settings, data) {
              data.checked = $('#showTrashedRegisters:checked').length
            })
            .on('stateLoaded.dt', function (e, settings, data) {
              if (data.checked === 1) {
                $('#showTrashedRegisters').prop('checked', true)
              }
            })
            .on('search.dt', (e, settings, data) => {
              let input = $('#' + settings.nTableWrapper.id + ' .dataTables_filter input')
              let value = input.val()
              // Change color of datatable search input when not empty
              if (value !== '') {
                input.css({
                  'box-shadow': 'inset 0 1px 1px rgba(0, 0, 0, 0.075), 0 0 3px rgba(255, 0, 0, 1)',
                  'border-color': 'red'
                })
              } else {
                input.css({
                  'box-shadow': 'inset 0 1px 1px rgba(0, 0, 0, 0.075), 0 0 3px rgba(255, 255, 255, 0.1)',
                  'border-color': '#ccd0d2'
                })
              }
            })
        }
        return table
      }

       $('#actionEvaluationModal').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget)
        var action_id = button.data('action-id')
        eventHub.$emit('selected-action-id', action_id)
      })

      $('#showTrashedRegisters, select[name="term-select"]').on('change', function () {
        getActionsDataTable().ajax.reload()
      })
      getActionsDataTable()
    </script>
@endpush
