@extends('frontend.layouts.app')

@section('content')

    <action-create-form
            title="{{trans('ceap.actions.create.panel_title')}}"
            :goals_select="{{ $goals_select }}"
            :indicators_select="{{ $indicators_select }}"
            :users_select="{{ $users_select }}"
            :terms_select="{{ $terms_select }}"
            :goals_indicators_map="{{ $goals_indicators_map }}"
            goal_id="{{ $goal_id }}"
    ></action-create-form>
@endsection

@push('after-styles')
    @include('frontend.includes.assets.summernote_styles')
@endpush

@push('after-scripts')
    @include('frontend.includes.assets.summernote_scripts')
@endpush