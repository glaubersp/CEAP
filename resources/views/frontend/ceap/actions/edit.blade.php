@extends('frontend.layouts.app')

@section('content')
    <action-edit-form
            title="{{trans('ceap.actions.edit.panel_title')}}"
            :goals_select="{{ $goals_select }}"
            :indicators_select="{{ $indicators_select }}"
            :users_select="{{ $users_select }}"
            :terms_select="{{ $terms_select }}"
            :goals_indicators_map="{{ $goals_indicators_map }}"
            :id="{{ $action->id }}"
    ></action-edit-form>
@endsection

@push('after-styles')
    @include('frontend.includes.assets.summernote_styles')
@endpush

@push('after-scripts')
    @include('frontend.includes.assets.summernote_scripts')
@endpush
