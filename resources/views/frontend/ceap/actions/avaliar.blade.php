@extends('frontend.layouts.app')

@section('content')
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title">{{trans('ceap.actions.evaluate.modalTitle')}}</h4>
                </div>
                <div class="modal-body">
                    <p>{{trans('ceap.actions.evaluate.modalBody')}}</p>
                    <p id="nome"></p>
                    <div>
                        <fieldset class="rating" style="margin-top: -9px;">
                            <input type="radio" id="5stars" name="rating"
                                   value="{{trans('ceap.actions.evaluate.starsRating.5stars')}}"/><label class="full"
                                                                                                         for="5stars"
                                                                                                         title="{{trans('ceap.actions.evaluate.starsRating.5stars')}}"></label>
                            <input type="radio" id="4stars" name="rating"
                                   value="{{trans('ceap.actions.evaluate.starsRating.4stars')}}"/><label class="full"
                                                                                                         for="4stars"
                                                                                                         title="{{trans('ceap.actions.evaluate.starsRating.4stars')}}"></label>
                            <input type="radio" id="3stars" name="rating"
                                   value="{{trans('ceap.actions.evaluate.starsRating.3stars')}}"/><label class="full"
                                                                                                         for="3stars"
                                                                                                         title="{{trans('ceap.actions.evaluate.starsRating.3stars')}}"></label>
                            <input type="radio" id="2stars" name="rating"
                                   value="{{trans('ceap.actions.evaluate.starsRating.2stars')}}"/><label class="full"
                                                                                                         for="2stars"
                                                                                                         title="{{trans('ceap.actions.evaluate.starsRating.2stars')}}"></label>
                            <input type="radio" id="1star" name="rating"
                                   value="{{trans('ceap.actions.evaluate.starsRating.1star')}}"/><label class="full"
                                                                                                        for="1star"
                                                                                                        title="{{trans('ceap.actions.evaluate.starsRating.1star')}}"></label>
                        </fieldset>
                        <span id="starsValue" style="margin-left: 20px;"></span>
                    </div>
                </div>
                <div class="modal-footer">
                    <a class="btn btn-default" data-dismiss="modal">{{trans('ceap.buttons.cancelBtn')}}</a>
                    <a class="btn btn-primary">{{trans('ceap.buttons.ratingBtn')}}</a>
                </div>
            </div>
        </div>
    </div>
    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title">{{trans('ceap.actions.evaluate.panelTitle')}}</h3>
        </div>
        <div class="panel-body">

            {!! $dataTable->table() !!}

        </div>
    </div>
@endsection

@push('after-styles')
    @include('frontend.includes.assets.datatables_styles')

    <style>
        fieldset, label {
            margin: 0;
            padding: 0;
        }

        body {
            margin: 20px;
        }

        h1 {
            font-size: 1.5em;
            margin: 10px;
        }

        /****** Style Star Rating Widget *****/

        .rating {
            border: none;
            float: left;
        }

        .rating > input {
            display: none;
        }

        .rating > label:before {
            margin: 5px;
            font-size: 1.25em;
            font-family: FontAwesome;
            display: inline-block;
            content: "\f005";
        }

        .rating > label {
            color: #ddd;
            float: right;
        }

        .rating > input:checked ~ label, /* show gold star when clicked */
        .rating:not(:checked) > label:hover, /* hover current star */
        .rating:not(:checked) > label:hover ~ label {
            color: black;
        }

        /* hover previous stars in list */

        .rating > input:checked + label:hover, /* hover current star when changing rating */
        .rating > input:checked ~ label:hover,
        .rating > label:hover ~ input:checked ~ label, /* lighten current selection */
        .rating > input:checked ~ label:hover ~ label {
            color: black;
        }
    </style>
@endpush

@push('after-scripts')
    @include('frontend.includes.assets.datatables_scripts')

    <script type="text/javascript">
        $('#dataTableBuilder tbody').on('change', 'select[name="priority"]', function () {
            $(this).closest('form').submit();
        });

        var valores = {!! json_encode($actions->toArray()) !!};

        $("#myModal").on("show.bs.modal", function (e) {
            var link = $(e.relatedTarget);

            var id = link.attr("href").split("#")[1];
            $('p[id="nome"]').text(valores[id]);
        });

        $('input:radio').change(
            function () {
                var userRating = this.value;
                $('span[id="starsValue"]').text("(" + userRating + ")");
            });
    </script>
@endpush