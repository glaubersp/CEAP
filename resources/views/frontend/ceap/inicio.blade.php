@extends('frontend.layouts.app')

@section('content')
    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title">{{trans('ceap.menu.home')}}</h3>
        </div>
        <div class="panel-body">
            <div class="row">
                <div class="col-sm-6 col-md-3">
                    <div class="thumbnail thumbnail-background">
                        <h1 class="badge">{{$indicators}}</h1>
                        <div class="caption">
                            <h3 style="margin-top: 5px;">{{trans('ceap.home.indicators')}}</h3>
                        </div>
                        <p><a href="{{ route('frontend.ceap.indicators.index') }}" class="btn btn-success"
                              role="button">{{trans('ceap.button.show')}}</a></p>
                    </div>
                </div>
                <div class="col-sm-6 col-md-3">
                    <div class="thumbnail thumbnail-background">
                        <h1 class="badge">{{$goals}}</h1>
                        <div class="caption">
                            <h3 style="margin-top: 5px;">{{trans('ceap.home.goals')}}</h3>
                        </div>
                        <p><a href="{{ route('frontend.ceap.goals.index') }}" class="btn btn-success"
                              role="button">{{trans('ceap.button.show')}}</a></p>
                    </div>
                </div>
                <div class="col-sm-6 col-md-3">
                    <div class="thumbnail thumbnail-background">
                        <h1 class="badge">{{$actions}}</h1>
                        <div class="caption">
                            <h3 style="margin-top: 5px;">{{trans('ceap.home.actions')}}</h3>
                        </div>
                        <p><a href="{{ route('frontend.ceap.actions.index') }}" class="btn btn-success"
                              role="button">{{trans('ceap.button.show')}}</a></p>
                    </div>
                </div>
                <div class="col-sm-6 col-md-3">
                    <div class="thumbnail thumbnail-background">
                        <h1 class="badge">{{$agreements}}</h1>
                        <div class="caption">
                            <h3 style="margin-top: 5px;">{{trans('ceap.home.agreements')}}</h3>
                        </div>
                        <p><a href="{{ route('frontend.ceap.agreements.index') }}" class="btn btn-success"
                              role="button">{{trans('ceap.button.show')}}</a></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('after-styles')
    <style>
        .breadcrumb {
            display: none;
        }
    </style>
@endpush
