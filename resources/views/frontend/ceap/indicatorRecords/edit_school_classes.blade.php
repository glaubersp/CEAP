@extends('frontend.layouts.app')

@section('content')

    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title">{{trans('ceap.indicators.records.edit.panel_title')}}</h3>
        </div>
        <div class="panel-body">
            {!! Form::open([ 'method' =>'PUT', 'route' => ['frontend.ceap.indicators.indicatorRecords.update', $indicator->id, 0]]) !!}

            @include('frontend.ceap.indicatorRecords.form')

            <div id="studentIndicators" class="form-group col-md-12 col-sm-12">
                <div class="form-group col-md-12 col-sm-12">
                    {{ Form::label('year', trans('ceap.indicators.records.year.label'), ['class'=>'control-label']) }}
                    <i class="fa fa-info-circle fa-fw" data-toggle="tooltip" data-placement="right"
                       title="Tooltip on right"></i>
                    {{ Form::select('year', $school_classes->toArray(), null, ['class' => 'form-control', 'placeholder' => trans('ceap.indicators.records.year.placeholder')]) }}

                    {!! $dataTable->table() !!}
                </div>
            </div>
            <div class="form-group center-buttons col-md-12 col-sm-12">
                {{ Form::submit(trans('ceap.button.save'), ['class'=>'btn btn-success']) }}
                {{ link_to_route('frontend.ceap.indicators.indicatorRecords.index', trans('ceap.button.cancel'), [$indicator->id],['type'=>'reset', 'class'=>'btn btn-danger']) }}
            </div>
            {!! Form::close() !!}
        </div>
    </div>

@endsection

@push('after-styles')
    @include('frontend.includes.assets.datatables_styles')
@endpush

@push('after-scripts')
    @include('frontend.includes.assets.datatables_scripts')

    <script>
      //versão para esconder inputs com radio buttons
      $('input[type=radio]').on('change onload', function () {
        if ($('input[name=evaluationCategory]').enabled === true) {
          if ($('input[name=evaluationCategory]:checked').val() === '1') {
            $('#studentIndicators').show()
            $('#otherIndicators').hide()
          } else {
            $('#studentIndicators').hide()
            $('#otherIndicators').show()
          }
        }
      })

      $(document).ready(function () {
        $('#evaluationBtnGroup').prop('disabled', true)
        if ($('input[name=evaluationCategory]:checked').val() === '1') {
          $('#studentIndicators').show()
          $('#otherIndicators').hide()
        } else {
          $('#studentIndicators').hide()
          $('#otherIndicators').show()
        }
      })

      //adiciona um .0 aos números inteiros: 1 -> 1.0
      $('input[type=number]').on('change click', function () {
        let value = $(this).value
        let num = parseFloat(value).toFixed(1)
        $(this).val(num)
      })

      //desabilita a primeira opção do select
      $('select option:first-child').attr('disabled', 'disabled')

      var detailsDatabase = {!! json_encode($indicator->description) !!}

      $(document).ready(function () {
        var myDiv = document.getElementById('indicatorDescription')
        myDiv.innerHTML = detailsDatabase
      })

      let classes = {!! json_encode($school_classes->toArray()) !!}

      // Reload datatable on select change
      $('select[name="year"]').change(function () {
        $('#dataTableBuilder').DataTable().ajax.reload()
      })
      // Put select value on request before datatable ajax method call
      $('#dataTableBuilder').on('preXhr.dt', function (e, settings, data) {
        data.year = $('select[name="year"] option:selected').text()
        data.indicator_id = $('input[name=indicator_id]').val()
      })
    </script>
@endpush