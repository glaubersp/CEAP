@extends('frontend.layouts.app')

@section('content')

    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title btn-indicators-add-value">{{trans('ceap.indicators.records.create.panel_title')}}</h3>
        </div>
        <div class="panel-body">
            {!! Form::open(['route' => ['frontend.ceap.indicators.indicatorRecords.index', $indicator->id]]) !!}

            <div class="col-md-12 col-sm-12">
            <dl>
                <dd class="dd-title">{{ $indicator->name }}</dd>
                <dt>{{trans('ceap.indicators.modal.description.label')}}</dt>
                <dd id="indicatorDescription" style="margin-bottom:10px">{{ $indicator->description }}</dd>
            </dl>
            </div>

            <div id="studentIndicators">
                <div class="form-group col-md-12 col-sm-12">
                    {{ Form::label('year', trans('ceap.indicators.indicatorRecords.show.year.label'), ['class'=>'control-label']) }}
                    <i class="fa fa-info-circle fa-fw" data-toggle="tooltip" data-placement="right"
                       title="Tooltip on right"></i>
                    {{ Form::select('year', $school_classes->toArray(), null, ['class' => 'form-control', 'placeholder' => trans('ceap.indicators.indicatorRecords.show.year.placeholder')]) }}

                    {!! $dataTable->table() !!}
                </div>
            </div>
            <div class="form-group col-md-6 col-sm-6" id="otherIndicators">
                {{ Form::label('indicatorValue', trans('ceap.indicators.value'), ['class'=>'control-label']) }}
                <i class="fa fa-info-circle fa-fw" data-toggle="tooltip" data-placement="right"
                   title="Tooltip on right"></i>
                <div class="input-group">
                    {{Form::number('indicatorValue', null,['placeholder' => '0.0', 'class' => 'form-control', 'step' => '0.1', 'min' => '0.0'])}}
                </div>
            </div>

            <div class="form-group center-buttons col-md-12 col-sm-12">
                {{ Form::submit(trans('ceap.button.save'), ['class'=>'btn btn-success', 'name' => 'saveIndicator']) }}
                {{ link_to_route('frontend.ceap.indicators.index', trans('ceap.button.cancel'), null, ['type'=>'reset', 'class'=>'btn btn-danger']) }}
            </div>
            {!! Form::close() !!}
        </div>
    </div>

@endsection

@push('after-styles')
    @include('frontend.includes.assets.datatables_styles')

    <style>
        #studentIndicators {
            display: none;
        }
    </style>
@endpush

@push('after-scripts')
    @include('frontend.includes.assets.datatables_scripts')

    <script>

        var detailsDatabase = {!! json_encode($indicator->description) !!};

        $(document).ready(function(){
            var myDiv = document.getElementById('indicatorDescription');
            myDiv.innerHTML = detailsDatabase;
        });

        $(document).ready(function () {
            $("#evaluationBtnGroup").prop("disabled", true);
            if ($('input[name=evaluationCategory]:checked').val() === "1") {
                $("#studentIndicators").show();
                $('#otherIndicators').hide();
            } else {
                $("#studentIndicators").hide();
                $('#otherIndicators').show();
            }
        })

        //adiciona um .0 aos números inteiros: 1 -> 1.0
        $('#indicatorValue').on('click change', function () {
            var value = document.getElementById("indicatorValue").value;
            var num = parseFloat(value).toFixed(1);
            $(this).val(num);
        });

        //desabilita a primeira opção do select
        $('select option:first-child').attr("disabled", "disabled");

        var classes = {!! json_encode($school_classes->toArray()) !!};

        // Reload datatable on select change
        $('select[name="year"]').change(function () {
            $('#dataTableBuilder').DataTable().ajax.reload();
        });
        // Put select value on request before datatable ajax method call
        $('#dataTableBuilder').on('preXhr.dt', function (e, settings, data) {
            data.year = $('select[name="year"] option:selected').text();
        });
    </script>
@endpush
