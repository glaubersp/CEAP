@php

    /**
     * Check if a select item should be checked or not based on:
     * - First time in this view and the item corresponds to the model
     * - Second time in this view and the item was selected before the validation have failed
     * @param $categoryId The item value
     * @param $indicatorId The model value
     * @return bool
     */
    function shouldBeChecked($categoryId, $indicatorId)
    {
        return (is_null(old('evaluationCategory')) && $categoryId == $indicatorId)
            || ($categoryId == old('evaluationCategory'));
    }

    /**
     * Check if the actual rote action name is a show and the fields should not be editable/active
     * @return bool
     */
    function shouldBeDisabled()
    {
        return true;
    }

    /**
     * Returns 'disabled' if we are in a show action.
     * @return string
     */
    function disabledIfShouldBeDisabled()
    {
        if (shouldBeDisabled()) {
            return ' disabled ';
        }
    }

@endphp

<div class="col-md-12 col-sm-12">
    <div class="col-md-12 col-sm-12">
        <dl>
            <dd class="dd-title">{{ $indicator->name }}</dd>
            <dt>{{trans('ceap.indicators.modal.description.label')}}</dt>
            <dd id="indicatorDescription" style="margin-bottom:10px">{{ $indicator->description }}</dd>
        </dl>
    </div>

    {{ Form::hidden('indicator_id', $indicator->id) }}
</div>
<div class="form-group col-md-12 col-sm-12" style="display: none">
    {{ Form::label('evaluationBtnGroup', trans('ceap.indicators.show.evaluationBtnGroup.label'), ['class'=>'control-label'. disabledIfShouldBeDisabled()]) }}
    <i class="fa fa-info-circle fa-fw" data-toggle="tooltip" data-placement="right" title="Tooltip on right"></i>
    <br>
    <div class="btn-group" data-toggle="buttons" role="group" aria-label="..." id="evaluationBtnGroup">
        @foreach($categories as $category)
            <label
                class="btn btn-default
                    @if(shouldBeDisabled())
                    disabled
                    @endif
                    @if( shouldBeChecked($category->id, $indicator->evaluationCategory->id) )
                    active
                    @endif
                    "
                @if(shouldBeDisabled())
                disabled="disabled"
                @endif
            >
                <input
                    type="radio"
                    autocomplete="off"
                    name="evaluationCategory"
                    value="{{ $category->id }}"
                    @if(shouldBeDisabled())
                    disabled
                    @endif
                    @if( shouldBeChecked($category->id, $indicator->evaluationCategory->id) )
                    checked="checked"
                    @endif
                >
                {{ $category->name }}
            </label>
        @endforeach
    </div>
    {{ Form::hidden('evaluationCategory', $indicator->evaluationCategory->id) }}
</div>
