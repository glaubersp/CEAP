@extends('frontend.layouts.app')

@section('content')
    <goal-edit-form
            title="{{ trans('ceap.goals.edit.panel_title') }}"
            :indicators_select="{{ $indicators_select }}"
            :id="{{ $goal->id }}"
    ></goal-edit-form>

@endsection

@push('after-styles')
    @include('frontend.includes.assets.summernote_styles')
@endpush

@push('after-scripts')
    @include('frontend.includes.assets.summernote_scripts')
@endpush