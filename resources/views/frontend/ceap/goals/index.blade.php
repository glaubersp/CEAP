@extends('frontend.layouts.app')

@section('content')

    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title btn-monitoring-goals">{{trans('ceap.goals.index.panel_title')}}</h3>
        </div>
        <div class="panel-body">
            <div class="checkbox-temp">
                <label>
                    <input type="checkbox" id="showTrashedRegisters"> {{ trans('ceap.table.trashed_registers') }}
                </label>
            </div>
            {!! $dataTable->table() !!}

        </div>
    </div>

@endsection

@push('after-styles')
    @include('frontend.includes.assets.datatables_styles')
@endpush

@push('after-scripts')
    @include('frontend.includes.assets.datatables_scripts')

    <script type="text/javascript">
      function alteraBusca (elem) {
        text = ''
        elem.childNodes.forEach(el => { return text += el.textContent + ' '})
        $('#dataTableBuilder').DataTable().search(text).draw()
      }

      function getGoalsDataTable () {
        let table
        let permissions = {!! json_encode(auth()->user()->getAllPermissions()) !!};
        if ($.fn.DataTable.isDataTable('#dataTableBuilder')) {
          table = $('#dataTableBuilder').DataTable()
            .on( 'preInit.dt', function (e, settings) {
              //permissions[3] = can-create
              if(permissions[3] === undefined) {
                table.column(3).visible( false )
              } else {
                table.column(3).visible( true )
              }
            })
            .on('preXhr.dt', function (e, settings, data) {
              data.checked = $('#showTrashedRegisters:checked').length
              $('#new-checkbox').append($('.checkbox-temp').contents())
            })
            .on('stateSaveParams.dt', function (e, settings, data) {
              data.checked = $('#showTrashedRegisters:checked').length
            })
            .on('stateLoaded.dt', function (e, settings, data) {
              if (data.checked === 1) {
                $('#showTrashedRegisters').prop('checked', true)
              }
            })
            .on('search.dt', (e, settings, data) => {
            let input = $('#' + settings.nTableWrapper.id + ' .dataTables_filter input')
            let value = input.val()
            // Change color of datatable search input when not empty
            if (value !== '') {
            input.css({
              'box-shadow': 'inset 0 1px 1px rgba(0, 0, 0, 0.075), 0 0 3px rgba(255, 0, 0, 1)',
              'border-color': 'red'
            })
          } else {
            input.css({
              'box-shadow': 'inset 0 1px 1px rgba(0, 0, 0, 0.075), 0 0 3px rgba(255, 255, 255, 0.1)',
              'border-color': '#ccd0d2'
            })
          }
        })
        }
        return table
      }

      $('#showTrashedRegisters').on('change', function () {
        getGoalsDataTable().ajax.reload()
      })
      getGoalsDataTable()
    </script>
@endpush
