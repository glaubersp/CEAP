@extends('frontend.layouts.app')

@section('content')

    {{-- Modal Arquivar Objetivo --}}
    <trash-goal-modal :id="{{ $goal->id }}"></trash-goal-modal>

    {{-- Modal Apagar Objetivo --}}
    <delete-goal-modal :id="{{ $goal->id }}"></delete-goal-modal>

    {{-- Exibir Objetivo --}}
    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title">{{trans('ceap.goals.show.panel_title')}}</h3>
        </div>
        <div class="panel-body">
            {!! Form::model($goal, [ 'method' =>'GET', 'route' => ['frontend.ceap.goals.edit', $goal]]) !!}

            <div class="form-group col-md-12 col-sm-12">
                <dl>
                    <dd class="dd-title">{{ $goal->name }}</dd>
                    <dt>{{trans('ceap.goals.justification.label')}}</dt>
                    <dd style="margin-bottom:10px">
                        <?php echo $goal->justification ?>
                    </dd>
                    <dt>{{trans('ceap.goals.indicator.label')}}</dt>
                    <dd style="margin-bottom:10px">{{ $goal->indicator->name }}</dd>
                </dl>
            </div>

            {!! $dataTable->table() !!}

            <div class="form-group center-buttons col-md-12 col-sm-12">
                @php
                    if (Request::session()->has('parent_route')) {
                        echo link_to_route('frontend.ceap.actions.index', trans('ceap.button.back'), null,['type'=>'reset', 'class'=>'btn btn-primary']);
                    }else{
                        echo link_to_route('frontend.ceap.goals.index', trans('ceap.button.back'), null,['type'=>'reset', 'class'=>'btn btn-primary']);
                    }
                @endphp
                @can('can-edit')
                {{ Form::submit(trans('ceap.button.edit'), ['class'=>'btn btn-success']) }}
                @endcan
                @can('can-trash')
                {{ Form::button(trans('ceap.button.trash'), ['class'=>'btn btn-warning deleteGroupBtn', 'data-toggle' => 'modal', 'data-target' => '#trashGoalModal', 'data-title' => 'Arquivar Objetivo']) }}
                @endcan
                @can('can-delete')
                {{ Form::button(trans('ceap.button.delete'), ['class'=>'btn btn-danger deleteGroupBtn', 'data-toggle' => 'modal', 'data-target' => '#deleteGoalModal', 'data-title' => 'Apagar Objetivo']) }}
                @endcan
            </div>
            {!! Form::close() !!}
        </div>
    </div>

@endsection

@push('after-styles')
    @include('frontend.includes.assets.datatables_styles')
@endpush

@push('after-scripts')
    @include('frontend.includes.assets.datatables_scripts')
    {{--<script type="text/javascript">--}}
      {{--$('select[name="term-select"] option:first-child').attr('disabled', 'disabled')--}}

      {{--$('#dataTableBuilder').find('tbody').on('change', 'select[name="priority"]', function () {--}}
        {{--$(this).closest('form').submit()--}}
      {{--})--}}

      {{--function alteraBusca(elem) {--}}
        {{--text = ''--}}
        {{--elem.childNodes.forEach(el => {--}}
          {{--return text += el.textContent + ' '--}}
        {{--})--}}
        {{--$('#dataTableBuilder').DataTable().search(text).draw()--}}
      {{--}--}}

      {{--function getActionsInGoalDataTable() {--}}
        {{--let table--}}
        {{--let permissions = {!! json_encode(auth()->user()->getAllPermissions()) !!};--}}
        {{--if ($.fn.DataTable.isDataTable('#dataTableBuilder')) {--}}
          {{--table = $('#dataTableBuilder').DataTable()--}}
            {{--.on( 'preDraw', function () {--}}
              {{--$('#new-select').append($('.select-temp').contents())--}}
              {{--$('#new-checkbox').append($('.checkbox-temp').contents())--}}
            {{--})--}}
            {{--.on( 'preInit.dt', function (e, settings) {--}}
              {{--//permissions[3] = can-create--}}
              {{--if(permissions[3] === undefined) {--}}
                {{--table.column(4).visible( false )--}}
              {{--} else {--}}
                {{--table.column(4).visible( true )--}}
              {{--}--}}
            {{--})--}}
            {{--.on('preXhr.dt', function (e, settings, data) {--}}
              {{--data.checked = $('#showTrashedRegisters:checked').length--}}
              {{--data.term_id = $('select[name="term-select"] option:selected').val()--}}
            {{--})--}}
            {{--.on('stateSaveParams.dt', function (e, settings, data) {--}}
              {{--data.checked = $('#showTrashedRegisters:checked').length--}}
            {{--})--}}
            {{--.on('stateLoaded.dt', function (e, settings, data) {--}}
              {{--if (data.checked === 1) {--}}
                {{--$('#showTrashedRegisters').prop('checked', true)--}}
              {{--}--}}
            {{--})--}}
            {{--.on('search.dt', (e, settings, data) => {--}}
              {{--let input = $('#' + settings.nTableWrapper.id + ' .dataTables_filter input')--}}
              {{--let value = input.val()--}}
              {{--// Change color of datatable search input when not empty--}}
              {{--if (value !== '') {--}}
                {{--input.css({--}}
                  {{--'box-shadow': 'inset 0 1px 1px rgba(0, 0, 0, 0.075), 0 0 3px rgba(255, 0, 0, 1)',--}}
                  {{--'border-color': 'red'--}}
                {{--})--}}
              {{--} else {--}}
                {{--input.css({--}}
                  {{--'box-shadow': 'inset 0 1px 1px rgba(0, 0, 0, 0.075), 0 0 3px rgba(255, 255, 255, 0.1)',--}}
                  {{--'border-color': '#ccd0d2'--}}
                {{--})--}}
              {{--}--}}
            {{--})--}}
        {{--}--}}
        {{--return table--}}
      {{--}--}}

      {{--$('#showTrashedRegisters, select[name="term-select"]').on('change', function () {--}}
        {{--getActionsInGoalDataTable().ajax.reload()--}}
      {{--})--}}
      {{--getActionsInGoalDataTable()--}}
     {{--</script>--}}
@endpush
