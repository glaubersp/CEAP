@extends('frontend.layouts.app')

@section('content')

    <goal-create-form
            title="{{ trans('ceap.goals.create.panel_title') }}"
            :indicators_select="{{ $indicators_select }}"
            :indicators_descriptions="{{ $indicators_descriptions }}"
            id="{{ $indicator_id }}"
    ></goal-create-form>

@endsection

@push('after-styles')
    @include('frontend.includes.assets.summernote_styles')
@endpush

@push('after-scripts')
    @include('frontend.includes.assets.summernote_scripts')
@endpush