@extends('frontend.layouts.extern')

@section('content')
    <div class="row">
        <div class="col-xs-4 col-sm-4 col-md-4 logotipo">
            <img alt="Logotipo" src="{{ url('/') }}/img/frontend/logo_escolas.png" class="logo-login">
        </div>

        <div class="col-xs-8 col-sm-8 col-md-8">
            {{--desktop--}}
            <div class="hidden-xs hidden-sm hidden-md text-center">
                <h1 class="text-center"
                    style="font-size: 45px;margin-top: 35px;text-align: left;">@lang('ceap.app.name')</h1>
            </div>
            {{--tablet--}}
            <div class="hidden-xs hidden-lg">
                <h1 class="text-center">@lang('ceap.app.name')</h1>
            </div>
            {{--mobile--}}
            <div class="hidden-sm hidden-md hidden-lg">
                <h3 class="text-center text">@lang('ceap.app.name')</h3>
            </div>

        </div>
    </div>
    <div class="row">
        <div class="col-md-12 col-sm-12">
            {{--desktop--}}
            <div class="hidden-xs hidden-sm hidden-md">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">@lang('ceap.app.description')</h3>
                    </div>
                    <div class="panel-body">
                        <p>Este projeto tem como objetivo implementar e investigar uma metodologia para a melhoria
                            da escola
                            pública.</p>
                        <p>Partimos do conceito de uma noção sistêmica do "clima escolar" avaliando e buscando
                            aprimorar as
                            condições de aprendizado, ensino e participação da comunidade escolar, promovendo uma
                            maior
                            abertura à transformações e inovação.</p>
                        <p>Para avaliar essas condições, fazemos uso de questionários preenchidos semestralmente
                            online por
                            todos os entes escolares (pais, alunos, gestores, e funcionários) sobre sua percepção de
                            quatro
                            fatores que contribuem para um bom clima escolar.</p>
                        <p>Envolvemos todos os atores na discussão e apreciação dos dados para definições de metas e
                            ações
                            para a escola. Como parte do projeto, desenvolvemos material formação, registro e tomada
                            de
                            decisões, bem como um sistema de software para gestão desses processos.</p>
                        <br>
                        <p>Financiamento: <a
                                    href="http://www.bv.fapesp.br/pt/auxilios/89923/construcao-de-uma-metodologia-participativa-para-a-transformacao-escolar-baseada-em-dados/">
                                FAPESP (2015-2018)
                            </a>
                        </p>
                        <br>
                        <p>Para mais informações, acesse <a href="https://ceap.nied.unicamp.br">Sistemas do CEAP</a></p>
                    </div>
                </div>
            </div>

            {{--tablet--}}
            <div class="hidden-xs hidden-lg">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">@lang('ceap.app.description')</h3>
                    </div>
                    <div class="panel-body">
                        <p>Este projeto tem como objetivo implementar e investigar uma metodologia para a melhoria
                            da escola
                            pública.</p>
                        <p>Partimos do conceito de uma noção sistêmica do "clima escolar" avaliando e buscando
                            aprimorar as
                            condições de aprendizado, ensino e participação da comunidade escolar, promovendo uma
                            maior
                            abertura à transformações e inovação.</p>
                        <p>Para avaliar essas condições, fazemos uso de questionários preenchidos semestralmente
                            online por
                            todos os entes escolares (pais, alunos, gestores, e funcionários) sobre sua percepção de
                            quatro
                            fatores que contribuem para um bom clima escolar.</p>
                        <p>Envolvemos todos os atores na discussão e apreciação dos dados para definições de metas e
                            ações
                            para a escola. Como parte do projeto, desenvolvemos material formação, registro e tomada
                            de
                            decisões, bem como um sistema de software para gestão desses processos.</p>
                        <br>
                        <p>Financiamento: <a
                                    href="http://www.bv.fapesp.br/pt/auxilios/89923/construcao-de-uma-metodologia-participativa-para-a-transformacao-escolar-baseada-em-dados/">
                                FAPESP (2015-2018)
                            </a>
                        </p>
                        <br>
                        <p>Para mais informações, acesse <a href="https://ceap.nied.unicamp.br">Sistemas do CEAP</a></p>
                    </div>
                </div>
            </div>
            {{--mobile--}}
            <div class="hidden-sm hidden-md hidden-lg">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">@lang('ceap.app.description')</h3>
                    </div>
                    <div class="panel-body">
                        <p>Este projeto tem como objetivo implementar e investigar uma metodologia para a melhoria
                            da escola
                            pública.</p>
                        <p>Partimos do conceito de uma noção sistêmica do "clima escolar" avaliando e buscando
                            aprimorar as
                            condições de aprendizado, ensino e participação da comunidade escolar, promovendo uma
                            maior
                            abertura à transformações e inovação.</p>
                        <p>Para avaliar essas condições, fazemos uso de questionários preenchidos semestralmente
                            online por
                            todos os entes escolares (pais, alunos, gestores, e funcionários) sobre sua percepção de
                            quatro
                            fatores que contribuem para um bom clima escolar.</p>
                        <p>Envolvemos todos os atores na discussão e apreciação dos dados para definições de metas e
                            ações
                            para a escola. Como parte do projeto, desenvolvemos material formação, registro e tomada
                            de
                            decisões, bem como um sistema de software para gestão desses processos.</p>
                        <br>
                        <p>Financiamento: <a
                                    href="http://www.bv.fapesp.br/pt/auxilios/89923/construcao-de-uma-metodologia-participativa-para-a-transformacao-escolar-baseada-em-dados/">
                                FAPESP (2015-2018)
                            </a>
                        </p>
                        <br>
                        <p>Para mais informações, acesse <a href="https://ceap.nied.unicamp.br">Sistemas do CEAP</a></p>
                        <p>O código-fonte do projeto encontra-se no <a
                                    href="https://github.com/glaubersp/CEAP">GitHub</a></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

