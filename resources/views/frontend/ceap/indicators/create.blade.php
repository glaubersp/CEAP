@extends('frontend.layouts.app')

@section('content')

    {{--Modal Criar Grupo--}}
    <create-indicator-group-modal
            :indicator_group_types="{{ $indicatorGroupTypes }}"
            :cycles="{{ $cycles }}"
    ></create-indicator-group-modal>

    {{--Modal Editar Grupo--}}
    <edit-indicator-group-modal
            :indicator_group_types="{{ $indicatorGroupTypes }}"
            :cycles="{{ $cycles }}"
            :id="{{ $indicatorGroupId ? $indicatorGroupId : '0' }}"
    ></edit-indicator-group-modal>

    {{--Modal Apagar Grupo--}}
    <delete-indicator-group-modal :id="{{ $indicatorGroupId ? $indicatorGroupId : '0' }}"></delete-indicator-group-modal>

    {{--Modal Criar Indicador--}}
    <create-indicator-modal></create-indicator-modal>

    {{--Modal Criar Indicador--}}
    <edit-indicator-modal></edit-indicator-modal>

    {{--Modal Apagar Indicador--}}
    <delete-indicator-modal></delete-indicator-modal>

    {{--Adicionar novo Indicador--}}
    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title btn-indicators-create">{{trans('ceap.indicators.create.panel_title')}}</h3>
        </div>

        <div class="panel-body">
            <indicator-group-panel
                    :indicator_group="{{ $indicatorGroups }}"
                    :indicator_id="{{ $indicatorGroupId ? $indicatorGroupId : '0' }}"
            ></indicator-group-panel>

            {{--Indicadores do Grupo--}}
            <div>
                <div class="panel panel-default indicatorsByGroupPanel">
                    <div class="panel-heading indicatorsByGroupPanelHeading">{{trans('ceap.indicators.group.indicators')}}
                        <div>
                            <button class="btn btn-success panel-title-buttons" data-toggle="modal" data-target="#createIndicatorModal">{{ trans('ceap.button.add') }}</button>
                        </div>
                    </div>
                    <div class="panel-body">
                        <div id='indicatorsByGroupPanelBody' class="form-group col-md-12 col-sm-12">
                            <!-- IndicatorsByGroupDataTable -->
                            {!! $dataTable->table() !!}
                        </div>
                    </div>
                </div>
            </div>
            {{--Fim do Indicadores do Grupo--}}
        </div>
    </div>
    {{--Fim do Adicionar novo Indicador--}}


@endsection

@push('after-styles')
    @include('frontend.includes.assets.datatables_styles')
    @include('frontend.includes.assets.summernote_styles')

    <style>
        .indicatorsByGroupPanel {
            display: none;
        }

        .indicatorsByGroupPanelHeading {
            display: flex;
            justify-content: space-between;
        }

        .group-buttons {
            padding: 15px;
            margin-top: 13px;
        }

        #app > div > div > div.col-md-10.col-sm-9 > div.alert.alert-danger,
        #createGroupModal > div > div > form > div.modal-body > div.form-group.col-md-12.col-sm-12 > div.alert.alert-success,
        #editGroupModal > div > div > form > div.modal-body > div > div:nth-child(1) > div.alert.alert-success,
        #createIndicatorModal > div > div > form > div.modal-body > div:nth-child(1) > div.alert.alert-success,
        #editIndicatorModal > div > div > form > div.modal-body > div:nth-child(1) > div.alert.alert-success {
            display: none;
        }

        #createIndicatorsTable, #editIndicatorsTable, #updateIndicatorsRegisterTable {
            width: 100% !important;
        }

        @media (max-width: 768px) {
            .group-buttons {
                padding: 0 15px 0 15px;
                margin: 0 0 15px 0;
                text-align: center;
            }
        }
        [v-cloak] {
            display: none
        }
    </style>
@endpush

@push('after-scripts')
    @include('frontend.includes.assets.datatables_scripts')
    @include('frontend.includes.assets.summernote_scripts')
@endpush
