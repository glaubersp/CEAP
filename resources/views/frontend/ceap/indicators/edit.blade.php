@extends('frontend.layouts.app')

@section('content')

    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title">{{trans('ceap.indicators.edit.panel_title')}}</h3>
        </div>
        <div class="panel-body">
            {!! Form::model($indicator, [ 'method' =>'PATCH', 'route' => ['frontend.ceap.indicators.update', $indicator->id]]) !!}

            @include('frontend.ceap.indicators.form')

            <div class="form-group center-buttons col-md-12 col-sm-12">
                {{ Form::submit(trans('ceap.button.save'), ['class'=>'btn btn-success']) }}
                {{ link_to_route('frontend.ceap.indicators.show', trans('ceap.button.cancel'), [$indicator],['type'=>'reset', 'class'=>'btn btn-danger']) }}
            </div>
            {!! Form::close() !!}
        </div>
    </div>

@endsection

@push('after-styles')
    @include('frontend.includes.assets.datatables_styles')
    @include('frontend.includes.assets.summernote_styles')

    <style>
        #evaluationBtnGroup > label.btn.btn-default.disabled.active:hover:not([enabled="enabled"]) {
            color: #636b6f;
            background-color: #e6e6e6;
            border-color: #adadad;
        }

        #studentIndicators {
            display: none;
        }

    </style>
@endpush

@push('after-scripts')
    @include('frontend.includes.assets.datatables_scripts')
    @include('frontend.includes.assets.summernote_scripts')

    <script>
        //Summernote Editor
        $(document).ready(function () {
            $('textarea[name=indicator_description]').summernote({
                height: 100,
                lang: 'pt-BR',
                placeholder: '{{trans('ceap.indicators.create.description.placeholder')}}'
            });

            $('#evaluationBtnGroup').on('click', function(e) {
                $('#evaluationBtnGroup').attr('disabled','disabled');
                if ($(this).is('[disabled]')) {
                    e.preventDefault();
                    e.stopImmediatePropagation();
                }
            });

        });

        //versão para esconder inputs com radio buttons
//        $("#studentIndicators").hide();
        $('input[type=radio]').on('change onload', function () {
            if ($('input[name=evaluationCategory]:checked').val() === "1") {
                $("#studentIndicators").show();
                $('#otherIndicators').hide();
            } else {
                $("#studentIndicators").hide();
                $('#otherIndicators').show();
            }
        });

        //adiciona um .0 aos números inteiros: 1 -> 1.0
        $('#indicatorValue').bind('keyup mouseup', function () {
            var value = document.getElementById("indicatorValue").value;
            var num = parseFloat(value).toFixed(1);
            $(this).val(num);
        });

        //desabilita a primeira opção do select
        $('select option:first-child').attr("disabled", "disabled");

        var classes = {!! json_encode($school_classes->toArray()) !!};

        // Reload datatable on select change
        $('select[name="year"]').change(function () {
            $('#dataTableBuilder').DataTable().ajax.reload();
        });
        // Put select value on request before datatable ajax method call
        $('#dataTableBuilder').on('preXhr.dt', function (e, settings, data) {
            data.year = $('select[name="year"] option:selected').text();
        });
    </script>
@endpush