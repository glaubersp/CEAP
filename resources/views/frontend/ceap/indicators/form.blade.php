@php

    /**
     * Check if a select item should be checked or not based on:
     * - First time in this view and the item corresponds to the model
     * - Second time in this view and the item was selected before the validation have failed
     * @param $categoryId The item value
     * @param $indicatorId The model value
     * @return bool
     */
    function shouldBeChecked($categoryId, $indicatorId)
    {
        return (is_null(old('evaluationCategory')) && $categoryId == $indicatorId)
            || ($categoryId == old('evaluationCategory'));
    }

    /**
     * Check if the actual rote action name is a show and the fields should not be editable/active
     * @return bool
     */
    function shouldBeDisabled()
    {
        if (explode('@', Route::getCurrentRoute()->getActionName())[1] == 'show') {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Returns 'disabled' if we are in a show action.
     * @return string
     */
    function disabledIfShouldBeDisabled()
    {
        if (shouldBeDisabled()) {
            return ' disabled ';
        }
    }
@endphp
<div class="form-group col-md-12 col-sm-12">
    {{ Form::label('indicatorName', trans('ceap.indicators.modal.name.label'), ['class'=>'control-label']) }}
    <i class="fa fa-info-circle fa-fw" data-toggle="tooltip" data-placement="right" title="Tooltip on right"></i>
    {{ Form::text('name', $indicator->name, ['placeholder' => 'Digite um nome para o indicator', 'class' => 'form-control',  disabledIfShouldBeDisabled() ]) }}
    {{ Form::hidden('indicator_id', $indicator->id) }}
</div>
<div class="form-group col-md-12 col-sm-12">
    <label class="control-label" for="indicator_description">{{trans('ceap.indicators.modal.description.label')}}</label>
    <i class="fa fa-info-circle fa-fw" data-toggle="tooltip" data-placement="right" title="Tooltip on right"></i>
    {{ Form::textarea('indicator_description', $indicator->description, ['size' => '30x5','placeholder' => trans('ceap.indicators.create.description.placeholder'), 'class' => 'form-control', disabledIfShouldBeDisabled()]) }}
</div>
{{--<div class="form-group col-md-12 col-sm-12">--}}
    {{--{{ Form::label('evaluationBtnGroup', trans('ceap.indicators.show.evaluationBtnGroup.label'), ['class'=>'control-label', 'disabled']) }}--}}
    {{--<i class="fa fa-info-circle fa-fw" data-toggle="tooltip" data-placement="right" title="Tooltip on right"></i>--}}
    {{--<br>--}}
    {{--<div class="btn-group" data-toggle="buttons" role="group" aria-label="..." id="evaluationBtnGroup">--}}
        {{--@foreach($categories as $category)--}}
            {{--<label--}}
                {{--class="btn btn-default--}}
                    {{--disabled--}}
                    {{--@if( shouldBeChecked($category->id, $indicator->evaluationCategory->id) )--}}
                    {{--active--}}
                    {{--@endif--}}
                    {{--"--}}
                {{--@if(shouldBeDisabled())--}}
                {{--disabled="disabled"--}}
                {{--@endif--}}
            {{-->--}}
                {{--<input--}}
                    {{--type="radio"--}}
                    {{--autocomplete="off"--}}
                    {{--name="evaluationCategory"--}}
                    {{--value="{{ $category->id }}"--}}
                    {{--@if(shouldBeDisabled())--}}
                    {{--disabled--}}
                    {{--@endif--}}
                    {{--@if( shouldBeChecked($category->id, $indicator->evaluationCategory->id) )--}}
                    {{--checked="checked"--}}
                    {{--@endif--}}
                {{-->--}}
                {{--{{ $category->name }}--}}
            {{--</label>--}}
        {{--@endforeach--}}
    {{--</div>--}}
{{--</div>--}}
{{--<div id="studentIndicators">--}}
    {{--<div class="form-group col-md-12 col-sm-12">--}}
        {{--{{ Form::label('year', trans('ceap.indicators.edit.year.label'), ['class'=>'control-label']) }}--}}
        {{--<i class="fa fa-info-circle fa-fw" data-toggle="tooltip" data-placement="right"--}}
           {{--title="Tooltip on right"></i>--}}
        {{--{{ Form::select('year', $school_classes->toArray(), null, ['class' => 'form-control', 'placeholder' => trans('ceap.indicators.edit.year.placeholder')]) }}--}}

        {{--{!! $dataTable->table() !!}--}}
    {{--</div>--}}
{{--</div>--}}
<div class="form-group col-md-6 col-sm-6" id="otherIndicators">
    {{ Form::label('indicatorValue', trans('ceap.indicators.records.value.label'), ['class'=>'control-label', disabledIfShouldBeDisabled()]) }}
    <i class="fa fa-info-circle fa-fw" data-toggle="tooltip" data-placement="right"
       title="Tooltip on right"></i>
    <div class="input-group">
        {{Form::number('indicatorValue', $indicator->indicatorRecords->last()->value,['placeholder' => '0.0', 'class' => 'form-control', 'step' => '0.1', 'min' => '0.0', disabledIfShouldBeDisabled()])}}
    </div>
</div>
