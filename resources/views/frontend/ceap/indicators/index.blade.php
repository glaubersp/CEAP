@extends('frontend.layouts.app')

@section('content')
    {{--Modal Criar Registros CEAP--}}
    <create-indicator-records-modal></create-indicator-records-modal>

    {{--Modal Atualizar Registros CEAP--}}
    <update-indicator-records-modal></update-indicator-records-modal>

    {{-- Modal Criar Registros - Classe --}}
    <create-rooms-indicator-records-modal></create-rooms-indicator-records-modal>

    {{-- Modal Atualizar Registros - Salas --}}
    <update-rooms-records-modal></update-rooms-records-modal>

    {{-- Modal Criar Registros - Escola --}}
    <create-school-indicator-records-modal></create-school-indicator-records-modal>

    {{-- Modal Atualizar Registros - Escola --}}
    <update-school-records-modal></update-school-records-modal>


    @if(old('indicatorName'))

        <div class="alert alert-success alert-dismissible" role="alert">

            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>

            <strong>{{trans('ceap.success')}} </strong>O indicador "{{old('indicatorName')}}" foi criado.

        </div>

    @endif

    {{--Acompanhar Indicadores--}}
    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title btn-monitoring-indicators">{{trans('ceap.indicators.index.panel_title')}}</h3>
        </div>
        <div class="panel-body">
            <indicator-panel
                    :indicator_group="{{ $indicatorGroups }}"
                    :indicator_id="{{ $indicatorGroupId ? $indicatorGroupId : '0' }}"
            ></indicator-panel>

            <div class="panel panel-default" id="indicatorsByGroupPanel">
                <div class="panel-heading indicatorsByGroupPanelHeading">{{trans('ceap.indicators.group.indicators')}}</div>
                <div class="panel-body">
                    <div id='indicatorsByGroupPanelBody' class="form-group col-md-12 col-sm-12">

                        {{ Form::open(['id'=>'showChartForm', 'route' =>'frontend.ceap.indicators.showChart']) }}
                        {{ Form::hidden('indicatorGroupType') }}

                        {{--CEAP Indicators Table--}}
                        <div id='ceapIndicatorsByGroupPanelBody' class="form-group col-md-12 col-sm-12">
                            <div class="checkbox-temp-ceap">
                                <label>
                                    <input type="checkbox" id="ceapTrashedRegisters"> {{ trans('ceap.table.trashed_registers') }}
                                </label>
                            </div>
                            <indicators-datatable id="ceapIndicatorsDataTable">
                                <template slot="table-header">
                                    <th></th>
                                    <th>@lang('ceap.table.indicator')</th>
                                    <th>@lang('ceap.table.description')</th>
                                    <th>@lang('ceap.table.updated_at')</th>
                                    <th>@lang('ceap.table.interactions')</th>
                                </template>
                            </indicators-datatable>
                        </div>

                        {{--Rooms Indicators Table--}}
                        <div id='roomsIndicatorsByGroupPanelBody' class="form-group col-md-12 col-sm-12">
                            <div class="checkbox-temp-rooms">
                                <label>
                                    <input type="checkbox" id="roomsTrashedRegisters"> {{ trans('ceap.table.trashed_registers') }}
                                </label>
                            </div>
                            <indicators-datatable id="roomsIndicatorsDataTable">
                                <template slot="table-header">
                                    <th></th>
                                    <th>@lang('ceap.table.indicator')</th>
                                    <th>@lang('ceap.table.description')</th>
                                    <th>@lang('ceap.table.updated_at')</th>
                                    <th>@lang('ceap.table.interactions')</th>
                                </template>
                            </indicators-datatable>
                        </div>

                        {{--School Indicators Table--}}
                        <div id='schoolIndicatorsByGroupPanelBody' class="form-group col-md-12 col-sm-12">
                            <div class="checkbox-temp-school">
                                <label>
                                    <input type="checkbox" id="schoolTrashedRegisters"> {{ trans('ceap.table.trashed_registers') }}
                                </label>
                            </div>
                            <indicators-datatable id="schoolIndicatorsDataTable">
                                <template slot="table-header">
                                    <th></th>
                                    <th>@lang('ceap.table.indicator')</th>
                                    <th>@lang('ceap.table.description')</th>
                                    <th>@lang('ceap.table.updated_at')</th>
                                    <th>@lang('ceap.table.interactions')</th>
                                </template>
                            </indicators-datatable>
                        </div>

                        {{ Form::close() }}

                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@push('after-styles')
    @include('frontend.includes.assets.datatables_styles')

    <style>
        #indicatorGroupInfo, #indicatorsByGroupPanel,
        #roomsIndicatorsByGroupPanelBody,
        #schoolIndicatorsByGroupPanelBody,
        #ceapIndicatorsByGroupPanelBody {
            display: none;
        }
    </style>
@endpush

@push('after-scripts')
    @include('frontend.includes.assets.datatables_scripts')

    {{--JavaScript for all modals--}}
    {{ script(mix('js/indicatorRecords.js')) }}

@endpush