@extends('frontend.layouts.app')

@section('content')
    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title">{{trans('ceap.indicators.show.panel_title')}}</h3>
        </div>
        <div class="panel-body">
            <div class="form-group col-md-12 col-sm-12">
                <dl>
                    <dd class="dd-title">{{ $indicator->name }}</dd>
                    <dt>{{trans('ceap.indicators.modal.description.label')}}</dt>
                    <dd style="margin-bottom:10px">
                        <div id='indicatorDescription'></div>
                    </dd>
                    <dt>{{trans('ceap.indicators.records.value.label')}}</dt>
                    <dd style="margin-bottom:10px"> {{$indicator->indicatorRecords->last()->value}}</dd>
                </dl>
            </div>

            <div class="form-group center-buttons col-md-12 col-sm-12">
                {{ link_to_route('frontend.ceap.indicators.index', trans('ceap.button.back'), ['indicatorGroupId' => $indicatorGroupId], ['type'=>'reset', 'class'=>'btn btn-primary']) }}
            </div>
        </div>
    </div>

@endsection

@push('after-styles')
    @include('frontend.includes.assets.datatables_styles')
    @include('frontend.includes.assets.summernote_styles')

    <style>
        #studentIndicators {
            display: none;
        }

    </style>
@endpush

@push('after-scripts')
    @include('frontend.includes.assets.datatables_scripts')
    @include('frontend.includes.assets.summernote_scripts')

    <script>
      $(document).ready(function () {
        var detailsDatabase = {!! json_encode($indicator->description) !!}

        $(document).ready(function () {
          var myDiv = document.getElementById('indicatorDescription')
          myDiv.innerHTML = detailsDatabase
        })

        $('textarea[name=indicator_description]').summernote({
          height: 100,
          lang: 'pt-BR',
          toolbar: [],
        }).summernote('disable')

        $('#evaluationBtnGroup, #indicatorValue').prop('disabled', true)
        if ($('input[name=evaluationCategory]:checked').val() === '1') {
          $('#studentIndicators').show()
          $('#otherIndicators').hide()
        } else {
          $('#studentIndicators').hide()
          $('#otherIndicators').show()
        }
      })

      //adiciona um .0 aos números inteiros: 1 -> 1.0
      $('input[type=number]').on('change click', function () {
        let value = $(this).value
        let num = parseFloat(value).toFixed(1)
        $(this).val(num)
      })

      //desabilita a primeira opção do select
      $('select option:first-child').attr('disabled', 'disabled')

      let classes = {!! json_encode($school_classes->toArray()) !!}

        // Reload datatable on select change
        $('select[name="year"]').change(function () {
          $('#dataTableBuilder').DataTable().ajax.reload()
        })
      // Put select value on request before datatable ajax method call
      $('#dataTableBuilder').on('preXhr.dt', function (e, settings, data) {
        data.year = $('select[name="year"] option:selected').text()
        data.indicator_id = $('input[name=indicator_id]').val()
      })
    </script>
@endpush