@extends('frontend.layouts.app')

@push('after-styles')
    {!! Charts::assets() !!}

    <style>
        /* bootstrap hack: fix content width inside hidden tabs */
        /* Source: http://bit.ly/2stcaoX */
        .tab-content > .tab-pane:not(.active),
        .pill-content > .pill-pane:not(.active) {
            display: block;
            height: 0;
            overflow-y: hidden;
        }

        .panel.panel-default {
            border-top: 0;
        }

        .nav-tabs > li > a, .nav-tabs > li > a:hover {
            text-decoration: none;
            background-color: #eee;
            border-top: 1px solid #ddd;
            border-right: 1px solid #ddd;
            border-left: 1px solid #ddd;
        }

        .panel-heading {
            display: flex;
            justify-content: space-between;
        }

        .btn-mini {
            padding-bottom: 0;
            padding-top: 0;
        }

        @media print {
            .highcharts-container {
                margin: auto;
                margin-top: 15px;
                margin-bottom: 15px;
            }
        }

    </style>
@endpush

@section('content')

    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title">{{trans('ceap.indicators.chart.title')}}</h3>
            <a class="btn btn-primary btn-mini btn-print-chart" id="print-all">{{trans('ceap.button.print')}}</a>
        </div>
        <div class="panel-body">
            @foreach($charts as $chart)
                {!! $chart->render() !!}
            @endforeach

            <div class="form-group center-buttons col-md-12 col-sm-12">
                {{
                    link_to_route(
                        'frontend.ceap.indicators.index',
                        trans('ceap.button.back'),
                        ['indicatorGroupId' => $indicatorGroupId],
                        ['type'=>'reset', 'class'=>'btn btn-success']
                    )
                 }}
            </div>
        </div>
    </div>

@endsection

@push('after-scripts')
    <script>
      Highcharts.setOptions({
        chart: {
          borderColor: '#7f7f7f',
          borderWidth: 1,
          borderRadius: 5,
          type: 'line',
        },
        lang: {
          months: ['Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho', 'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro'],
          shortMonths: ['Jan', 'Fev', 'Mar', 'Abr', 'Mai', 'Jun', 'Jul', 'Ago', 'Set', 'Out', 'Nov', 'Dez'],
          weekdays: ['Domingo', 'Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta', 'Sábado'],
          loading: ['Atualizando o gráfico...aguarde'],
          contextButtonTitle: 'Exportar gráfico',
          decimalPoint: ',',
          thousandsSep: '.',
          downloadJPEG: 'Baixar imagem JPEG',
          downloadPDF: 'Baixar arquivo PDF',
          downloadPNG: 'Baixar imagem PNG',
          downloadSVG: 'Baixar vetor SVG',
          printChart: 'Imprimir gráfico',
          rangeSelectorFrom: 'De',
          rangeSelectorTo: 'Para',
          rangeSelectorZoom: 'Zoom',
          resetZoom: 'Limpar Zoom',
          resetZoomTitle: 'Voltar Zoom para nível 1:1',
        },
      })

      // Source: http://bit.ly/2slQfBP
      let printCharts = function (charts) {
        let origDisplay = [],
          origParent = [],
          body = document.body,
          childNodes = body.childNodes,
          ELEMENT = 1
        // (1) default to all charts
        charts = charts || Highcharts.charts
        // (2) hide all body content
        Highcharts.each(childNodes, function (node, i) {
          if (node.nodeType === ELEMENT) {
            origDisplay[i] = node.style.display
            node.style.display = 'none'
          }

        })
        // (3) put the charts back in
        $.each(charts, function (i, chart) {
          origParent[i] = chart.container.parentNode
          body.appendChild(chart.container)
        })
        // (4) print
        window.print()
        // (5) allow the browser to prepare before reverting
        setTimeout(function () {
          // (6) put the charts back in
          $.each(charts, function (i, chart) {
            origParent[i].appendChild(chart.container)
          })
          // (7) restore all body content
          Highcharts.each(childNodes, function (node, i) {
            if (node.nodeType === 1) {
              node.style.display = origDisplay[i]
            }
          })
        }, 500)
      }

      $('#print-all').click(function (event) {
        printCharts()
      })

      $(document).ready(function () {
        Highcharts.charts.forEach(el => el.yAxis[0].setExtremes(ceap.y_min_val, ceap.y_max_val))
      })
    </script>
@endpush