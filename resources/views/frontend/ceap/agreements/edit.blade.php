@extends('frontend.layouts.app')

@section('content')

    <agreement-edit-form
            :id="{{ $agreement->id }}"
            agreement_title="{{ trans('ceap.agreements.edit.panel_title') }}"
            :topic_tags_list="{{ $topic_tag }}"
            :agreement_roles_list="{{ $roles }}"
            :related_actions="{{ $actions }}"
            :evaluation_cycles="{{ $cycles }}"
            :monitoring_strategies="{{ $monitoring_strategies }}"
    ></agreement-edit-form>

@endsection

@push('after-styles')
    @include('frontend.includes.assets.summernote_styles')

    <style>
        .select2-container--default .select2-selection--multiple .select2-selection__choice__remove {
            font-size: 22pt;
            height: 26px;
            margin-top: -14px;
            float: right;
            margin-left: 5px;
        }
    </style>
@endpush

@push('after-scripts')
    @include('frontend.includes.assets.summernote_scripts')

    <script type="text/javascript">

        $('select[name="topic_tags[]"]').select2();
        $('select[name="agreement_role[]"]').select2();
        $('select[name="actions[]"]').select2();

        $("select[name='evaluation_cycle_id'] option:first-child").attr('disabled', 'disabled');
        $("select[name='agreement_monitoring_strategy_id'] option:first-child").attr('disabled', 'disabled');

        //Summernote Editor
        $(document).ready(function () {
            $('textarea[name=details]').summernote({
                height: 200,
                lang: 'pt-BR'
            });
        });
    </script>
@endpush