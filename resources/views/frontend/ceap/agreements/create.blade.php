@extends('frontend.layouts.app')

@section('content')

    <agreement-create-form
            agreement_title="{{ trans('ceap.agreements.create.panel_title') }}"
            :topic_tags_list="{{ $topic_tags }}"
            :agreement_roles_list="{{ $agreement_roles }}"
            :related_actions="{{ $actions }}"
            :evaluation_cycles="{{ $evaluation_cycles }}"
            :monitoring_strategies="{{ $monitoring_strategies }}"
    ></agreement-create-form>

@endsection

@push('after-styles')
    @include('frontend.includes.assets.summernote_styles')
@endpush

@push('after-scripts')
    @include('frontend.includes.assets.summernote_scripts')
@endpush