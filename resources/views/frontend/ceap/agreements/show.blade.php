@extends('frontend.layouts.app')

@section('content')

    {{-- Modal Apagar Combinado--}}
    <agreement-delete-form :id="{{ $agreement->id }}"></agreement-delete-form>

    {{-- Exibir Combinado--}}
    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title">{{trans('ceap.agreements.show.panel_title')}}</h3>
        </div>
        <div class="panel-body">
            {!! Form::model($agreement, [ 'method' =>'GET', 'route' => ['frontend.ceap.agreements.edit', $agreement]]) !!}

            <div class="form-group col-md-12 col-sm-12">
                <dl>
                    <dd class="dd-title">{{ $agreement->title }}</dd>
                    <dt>{{trans('ceap.agreements.details.label')}}</dt>
                    <dd style="margin-bottom:10px">
                        <?php echo $agreement->details ?>
                    </dd>
                    <dt>{{trans('ceap.agreements.topic_tags.label')}}</dt>
                    <dd style="margin-bottom:10px; white-space: pre-wrap;">{{ $topic_tag_selected }}</dd>
                    <dt>{{trans('ceap.agreements.roles.label')}}</dt>
                    <dd style="margin-bottom:10px; white-space: pre-wrap;">{{ $roles_selected }}</dd>
                    <dt>{{trans('ceap.agreements.actions.label')}}</dt>
                    <dd style="margin-bottom:10px; white-space: pre-wrap;">{{ $actions_selected }}</dd>
                    <dt>{{trans('ceap.agreements.execution_cycle.label')}}</dt>
                    <dd style="margin-bottom:10px">{{ $agreement->evaluationCycle->name }}</dd>
                    <dt>{{trans('ceap.agreements.evaluation_strategy.label')}}</dt>
                    <dd style="margin-bottom:10px">{{ $agreement->agreementMonitoringStrategy->title }}</dd>
                </dl>
            </div>

            <div class="form-group center-buttons col-md-12 col-sm-12">
                {{ link_to_route('frontend.ceap.agreements.index', trans('ceap.button.back'), null,['type'=>'reset', 'class'=>'btn btn-primary']) }}
                @can('can-edit')
                {{ Form::submit(trans('ceap.button.edit'), ['class'=>'btn btn-success']) }}
                @endcan
                @can('can-trash')
                {{ Form::button(trans('ceap.button.delete'), ['class'=>'btn btn-danger deleteGroupBtn', 'data-toggle' => 'modal', 'data-target' => '#deleteAgreementModal', 'data-title' => 'Apagar Combinado']) }}
                @endcan
            </div>
            {!! Form::close() !!}
        </div>
    </div>
@endsection

@push('after-scripts')
@endpush
