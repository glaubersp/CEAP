@extends('frontend.layouts.app')

@section('content')

    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title">{{ trans('ceap.menu.help') }}</h3>
        </div>
        <div class="panel-body">
            <div class="panel" style="border-color:#F73545;">
                <div class="panel-heading planning-color" style="margin-top:0px;">
                    <h3 class="panel-title text-danger">Planejamento</h3>
                </div>
                <div class="panel-body">
                    <p>Na seção de Planejamento é possível:</p>
                    <ul>
                        <li>criar indicadores que poderão ser acompanhados ao longo do tempo, incluindo gráficos;</li>
                        <li>definir objetivos que estejam ligados e possam impactar esse indicadores; e</li>
                        <li>planejar ações da escola com o intuito de atingir os objetivos definidos;</li>
                    </ul>
                </div>
            </div>
            <div class="panel" style="border-color: #325FAD;">
                <div class="panel-heading monitoring-color" style="margin-top:0px;">
                    <h3 class="panel-title">Acompanhamento</h3>
                </div>
                <div class="panel-body">
                    <p>Na seção de Acompanhemento é possível:</p>
                    <ul>
                        <li>adicionar registros dos indicadores e acompar a evolução destes dados ao longo do tempo;</li>
                        <li>visualizar os objetivos definidos pela escola; e</li>
                        <li>visualizar as ações planejadas pela escola e coletar evidências.</li>
                    </ul>
                </div>
            </div>
            <div class="panel" style="border-color: #23A493;">
                <div class="panel-heading agreements-color" style="margin-top:0px;">
                    <h3 class="panel-title">Combinados</h3>
                </div>
                <div class="panel-body">
                    <p>Na seção de Combinados é possível:</p>
                    <ul>
                        <li>criar combinados escolares oriundos de reuniões escolares ou extraídos da rotina escolar;</li>
                        <li>definir combinados com base em ações de sucesso;</li>
                        <li>listar todos os combinados existentes.</li>
                    </ul>
                </div>
            </div>
            <div class="panel" style="border-color: #ffb736;">
                <div class="panel-heading management-color" style="margin-top:0px;">
                    <h3 class="panel-title">Gerenciar</h3>
                </div>
                <div class="panel-body">
                    <p>A seção Gerenciar permite à administração escolar:</p>
                    <ul>
                        <li>acessar o painel de Administração do sistema.</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

@endsection
