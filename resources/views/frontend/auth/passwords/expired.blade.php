@extends('frontend.layouts.app')

@section('title', app_name() . ' | Update Password')

@section('content')
    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title">{{ __('navs.frontend.user.password_expired') }}</h3>
        </div>
        <div class="panel-body">
            {{ html()->form('PATCH', route('frontend.auth.password.expired.update'))->open() }}
            <div class="form-group col-md-12 col-sm-12">
                <div class="alert alert-danger" role="alert">
                    {{ __('labels.frontend.passwords.expired_password_box_title') }}
                </div>
                {{--Senha antiga--}}
                <div class="form-group">
                    {{ html()->label(__('validation.attributes.frontend.old_password'))->for('old_password') }}

                    {{ html()->password('old_password')
                        ->class('form-control')
                        ->placeholder(__('validation.attributes.frontend.old_password'))
                        ->required() }}
                </div><!--form-group-->
                {{--Senha--}}
                <div class="form-group">
                    {{ html()->label(__('validation.attributes.frontend.password'))->for('password') }}

                    {{ html()->password('password')
                        ->class('form-control')
                        ->placeholder(__('validation.attributes.frontend.password'))
                        ->required() }}
                </div><!--form-group-->
                {{--Confirme sua senha--}}
                <div class="form-group">
                    {{ html()->label(__('validation.attributes.frontend.password_confirmation'))->for('password_confirmation') }}

                    {{ html()->password('password_confirmation')
                        ->class('form-control')
                        ->placeholder(__('validation.attributes.frontend.password_confirmation'))
                        ->required() }}
                </div><!--form-group-->
                <div class="form-group mb-0 clearfix">
                    {{ form_submit(__('labels.frontend.passwords.update_password_button')) }}
                </div><!--form-group-->
            </div>
            {{ html()->form()->close() }}
        </div>
    </div>
@endsection