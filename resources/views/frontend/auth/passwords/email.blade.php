@extends('frontend.layouts.extern')

@section('title', app_name() . ' | Reset Password')

@section('content')
    <div class="row">
        <div class="col-xs-4 col-sm-4 col-md-4 logotipo">
            <img alt="Logotipo" src="{{ asset('/img/frontend/logo_escolas.png') }}" class="logo-login">
        </div>

        <div class="col-xs-8 col-sm-8 col-md-8">
            {{--desktop--}}
            <div class="hidden-xs hidden-sm hidden-md text-center">
                <h1 class="text-center"
                    style="font-size: 45px;margin-top: 35px;text-align: left;">@lang('ceap.app.name')</h1>
            </div>
            {{--tablet--}}
            <div class="hidden-xs hidden-lg">
                <h1 class="text-center">@lang('ceap.app.name')</h1>
            </div>
            {{--mobile--}}
            <div class="hidden-sm hidden-md hidden-lg">
                <h3 class="text-center text">@lang('ceap.app.name')</h3>
            </div>

        </div>
    </div>
    <div class="row">

        <div class="col-md-8 col-md-offset-2">

                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif

                    {{ html()->form('POST', route('frontend.auth.password.email.post'))->open() }}
                        <div class="row">
                            <div class="col">
                                <div class="form-group">
                                    {{ html()->label(__('validation.attributes.frontend.email'))->for('email') }}

                                    {{ html()->email('email')
                                        ->class('form-control')
                                        ->placeholder(__('validation.attributes.frontend.email'))
                                        ->attribute('maxlength', 191)
                                        ->required()
                                        ->autofocus() }}
                                </div><!--form-group-->
                            </div><!--col-->
                        </div><!--row-->

                        <div class="row">
                            <div class="col">
                                <div class="form-group mb-0 clearfix">
                                    {{ form_submit(__('labels.frontend.passwords.send_password_reset_link_button')) }}
                                </div><!--form-group-->
                            </div><!--col-->
                        </div><!--row-->
                    {{ html()->form()->close() }}
                </div><!-- card-body -->
            </div><!-- card -->
        </div><!-- col-6 -->
    </div><!-- row -->
@endsection
