@extends('frontend.layouts.extern')

@section('title', app_name() . ' | Login')

@section('content')
    <div class="row">
        <div class="col-xs-4 col-sm-4 col-md-4 logotipo">
            <img alt="Logotipo" src="{{ asset('/img/frontend/logo_escolas.png') }}" class="logo-login">
        </div>

        <div class="col-xs-8 col-sm-8 col-md-8">
            {{--desktop--}}
            <div class="hidden-xs hidden-sm hidden-md text-center">
                <h1 class="text-center"
                    style="font-size: 39px;margin-top: 35px;text-align: left;">@lang('ceap.app.name')</h1>
            </div>
            {{--tablet--}}
            <div class="hidden-xs hidden-lg">
                <h1 class="text-center">@lang('ceap.app.name')</h1>
            </div>
            {{--mobile--}}
            <div class="hidden-sm hidden-md hidden-lg">
                <h3 class="text-center text">@lang('ceap.app.name')</h3>
            </div>

        </div>
    </div>
    <div class="row">
        <div class="col-md-8 col-sm-7">
            {{--desktop--}}
            <div class="hidden-xs hidden-sm hidden-md text-center text-well-login well well-lg">
                <h2>@lang('ceap.app.description')</h2>
            </div>
            {{--tablet--}}
            <div class="hidden-xs hidden-lg text-center text-well-login well well-sm">
                <h2>@lang('ceap.app.description')</h2>
            </div>
            {{--mobile--}}
            <div class="hidden-sm hidden-md hidden-lg text-center well well-sm">
                <h4>@lang('ceap.app.description')</h4>
            </div>
        </div>
        <div class="col-md-4 col-sm-5">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">{{ trans('ceap.login.name') }}
                        <br>
                    </h3>
                </div>
                <div class="panel-body">
                    {{ Form::open(['route' => 'frontend.auth.login.post', 'class' => 'form-horizontal']) }}

                    <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                        <div class="col-sm-5">
                            {{ Form::label('email', trans('ceap.login.email'), ['class' => 'control-label label-input']) }}
                        </div>
                        <div class="col-sm-7">
                            {{ Form::email('email', null, ['class' => 'form-control', 'maxlength' => '191', 'required' => 'required', 'autofocus' => 'autofocus', 'placeholder' => trans('ceap.login.email')]) }}

                            @if ($errors->has('email'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                        <div class="col-sm-5">
                            {{ Form::label('password', trans('ceap.login.password'), ['class' => 'control-label label-input']) }}
                        </div>
                        <div class="col-sm-7">
                            {{ Form::password('password', ['class' => 'form-control', 'required' => 'required', 'placeholder' => trans('ceap.login.password')]) }}

                            @if ($errors->has('password'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('password') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-10">
                            <div class="checkbox">
                                <label>
                                    {{ Form::checkbox('remember') }} {{ trans('ceap.login.remember') }}
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-10">
                            {{ Form::submit(trans('ceap.button.login'), ['class' => 'btn btn-primary', 'style' => 'margin-right:15px']) }}

                            {{ link_to_route('frontend.auth.password.reset', trans('ceap.login.forgot_password')) }}
                        </div>
                    </div>
                    {{ Form::close() }}
                </div>
            </div>
        </div>
    </div>
@endsection
