<!DOCTYPE html>
<html lang="{{ Lang::getLocale() }}">
    <head>
        <title>{{ trans('ceap.'.Route::currentRouteName()) }}</title>
        <meta charset="UTF-8">
        <meta name=description content="">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <!-- Bootstrap CSS -->
        <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css" rel="stylesheet">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
        <link href="https://fonts.googleapis.com/css?family=Dosis" rel="stylesheet">
        <style>
            body {
                margin: 20px;
                font-family: 'Dosis', sans-serif;
            }

            .header {
                display: flex;
                align-items: center;
                margin-bottom: 20px;
            }
            @media print {
                a[href]:after {
                    content: none !important;
                }
            }
        </style>
    </head>
    <body>
        <div class="header">
            <img alt="Logotipo" src="{{ url('/') }}/img/frontend/logo_escolas.png">
            <div>
                <h1 id='printTitle' style="margin-left:20px;margin-top: 0px;">{{ trans('ceap.'.Route::currentRouteName()) }}</h1>
                <p style="margin-left:20px;font-size: 20px;margin-top: -10px;">{{trans('ceap.table.print_at')}} {{\Carbon\Carbon::now()->toDateString()}}</p>
            </div>
        </div>
        <table class="table table-bordered table-condensed table-striped">
            @foreach($data as $row)
                @if ($row == reset($data)) 
                    <tr>
                        @foreach($row as $key => $value)
                            <th>{!! $key !!}</th>
                        @endforeach
                    </tr>
                @endif
                <tr>
                    @foreach($row as $key => $value)
                        @if(is_string($value) || is_numeric($value))
                            <td>{!! $value !!}</td>
                        @else
                            <td></td>
                        @endif
                    @endforeach
                </tr>
            @endforeach
        </table>
    </body>
</html>
