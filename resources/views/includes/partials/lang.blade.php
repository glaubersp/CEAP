@if (config('locale.status') && count(config('locale.languages')) > 1)
    <li class="dropdown">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
            <i class="fa fa-globe" aria-hidden="true"></i>
            {{ trans('menus.language-picker.language') }}
            <span class="caret"></span>
        </a>
        <ul class="dropdown-menu" role="menu">
            @foreach (array_keys(config('locale.languages')) as $lang)
                @if ($lang != app()->getLocale())
                    <li><a href="{{ url('/lang/'.$lang) }}"
                           class="dropdown-item">{{ __('menus.language-picker.langs.'.$lang) }}</a></li>
                @endif
            @endforeach
        </ul>
    </li>
@endif