@extends('errors::layout')

@section('title', trans('ceap.error.404_title'))

@section('message')

    <p>@lang('ceap.error.404_message_1')</p>
    <p>@lang('ceap.error.404_message_2')</p>

@endsection
