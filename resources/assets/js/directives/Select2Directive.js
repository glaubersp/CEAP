// Custom Select2
export const Select2 = {
  twoWay: true,
  inserted: function (el, binding, value) {
    let options = binding.value || {}
    let select2 = $(el).select2(options)

    select2.on('select2:select', function (e) {
      el.dispatchEvent(new Event('change', {target: e.target}))
    }).on('select2:unselect', function (e) {
      el.dispatchEvent(new Event('change', {target: e.target}))
    }).on('select2:open', function () {
      $('.select2-search--dropdown .select2-search__field').attr('placeholder', 'Pesquisar...')
    }).on('select2:close', function () {
      $('.select2-search--dropdown .select2-search__field').attr('placeholder', null)
    })
  }
}
