'use strict'

$(document).ready(function () {
  // Bootstrap Datepicker
  $('.date').datepicker({
    maxViewMode: 2,
    todayBtn: 'linked',
    clearBtn: true,
    language: 'pt-BR',
    multidate: false,
    autoclose: true,
    todayHighlight: true,
    orientation: 'auto',
    toggleActive: true
  })

  $('select option:first-child').removeAttr('hidden')
})

// Formatar o campo para mostrar 0,10
$('.realInput').on('click', function () {
  this.value = parseFloat(this.value).toFixed(2)
})

// Formatar o campo para mostrar 1,0
$('.numberInput').on('click', function () {
  this.value = parseFloat(this.value).toFixed(1)
})

function changeDatatableSearchFieldColor (e, settings, data) {
  let input = $('#' + settings.nTableWrapper.id + ' .dataTables_filter input')
  let value = input.val()
  // Change color of datatable search input when not empty
  if (value !== '') {
    input.css({
      'box-shadow': 'inset 0 1px 1px rgba(0, 0, 0, 0.075), 0 0 3px rgba(255, 0, 0, 1)',
      'border-color': 'red'
    })
  } else {
    input.css({
      'box-shadow': 'inset 0 1px 1px rgba(0, 0, 0, 0.075), 0 0 3px rgba(255, 255, 255, 0.1)',
      'border-color': '#ccd0d2'
    })
  }
}
