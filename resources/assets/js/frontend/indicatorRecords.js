'use strict'

let createSchoolIndicatorRecordsForm = $('#createSchoolIndicatorRecordsModal').find('form')[0]
let updateSchoolIndicatorRecordsForm = $('#updateSchoolIndicatorRecordsModal').find('form')[0]
let createRoomsIndicatorRecordsForm = $('#createRoomsIndicatorRecordsModal').find('form')[0]
let updateRoomsIndicatorRecordsForm = $('#updateRoomsIndicatorRecordsModal').find('form')[0]
let createCEAPIndicatorRecordsForm = $('#createCEAPIndicatorRecordsModal').find('form')[0]
let updateCEAPIndicatorRecordsForm = $('#updateCEAPIndicatorRecordsModal').find('form')[0]

// When the indicatorGroup select changes, update related information

// Custom DataTable
function getIndicatorsDataTable(type) {
  let table = '#' + type + 'IndicatorsDataTable'
  let permissions = ceap.permissions

  if (!$.fn.DataTable.isDataTable(table)) {
    table = $(table)
      .on('preInit.dt', function (e, settings) {
        // permissions[3] = can-create
        if (permissions[3] === undefined) {
          table.column(4).visible(false)
          table.buttons('.can-create').remove()
        }
      })
      .on('preXhr.dt', function (e, settings, data) {
        data.indicatorGroupId = $('select[name="indicatorGroupNameSelect"] option:selected').val()
        data.checked = $('#' + type + 'TrashedRegisters:checked').length
        $('#' + type + 'IndicatorsDataTable_wrapper > div:nth-child(1) > div:nth-child(2) > .checkbox-ceap').attr('id', 'new-checkbox-' + type)
        $('#new-checkbox-' + type).append($('.checkbox-temp-' + type).contents())
      })
      .on('search.dt', changeDatatableSearchFieldColor)
      .DataTable({
        ajax: route('frontend.ceap.indicators.index'),
        dom: '<\'row\'<\'col-xs-12 col-sm-6 col-md-6 separator-xs-top separator-xs-bottom\'B><\'col-xs-12 col-sm-6 col-md-6 separator-xs-top separator-xs-bottom\'f<\'.checkbox checkbox-ceap\'>>>' +
        '<\'row\'<\'col-xs-12 col-sm-6 col-md-6 separator-xs-top separator-xs-bottom\'l><\'col-xs-12 col-sm-6 col-md-6 separator-xs-top separator-xs-bottom\'p>>' +
        '<\'row\'tr>' + '<\'row\'<\'col-sm-6\'i><\'col-sm-6\'p>>',
        lengthMenu: [[5, 10, 25, 50], [5, 10, 25, 50]],
        pagingType: 'simple',
        select: {
          style: 'multi',
          selector: 'tbody > tr:has(:checkbox)'
        },
        'createdRow': function (row, data, index) {
          if (data.indicators_deleted_at !== null) {
            row.style.backgroundColor = '#ffcccc'
          }
        },
        buttons: [
          'excel',
          'print',
          {
            text: '<i class="fa fa-line-chart"/>' + ceap.buttonCreateGraph,
            init: function (dt, node, config) {
              let that = this
              dt.on('select.dt.DT deselect.dt.DT', function () {
                that.enable(dt.rows({selected: true}).any())
              })
              this.disable()
            },
            action: function (e, dt, node, config) {
              $('#showChartForm').submit()
            }
          },
          {
            text: '<i class="fa fa-plus-square-o"/>' + ceap.buttonAddRegisterValue,
            className: 'can-create',
            init: function (dt, node, config) {
              let that = this
            },
            action: function (e, dt, node, config) {
              if (!$.isEmptyObject(dt.data()[0])) {
                let type = dt.data()[0].indicator_group_type
                if (type === 'ceap') {
                  $('#createCEAPIndicatorRecordsModal').modal('show')
                } else if (type === 'rooms') {
                  $('#createRoomsIndicatorRecordsModal').modal('show')
                } else if (type === 'school') {
                  $('#createSchoolIndicatorRecordsModal').modal('show')
                }
              }
            }
          },
          {
            text: '<i class="fa fa-bar-chart"/>' + ceap.buttonUpdateRegisterValue,
            className: 'can-create',
            init: function (dt, node, config) {
            },
            action: function (e, dt, node, config) {
              if (!$.isEmptyObject(dt.data()[0])) {
                let type = dt.data()[0].indicator_group_type
                if (type === 'ceap') {
                  $('#updateCEAPIndicatorRecordsModal').modal('show')
                } else if (type === 'rooms') {
                  $('#updateRoomsIndicatorRecordsModal').modal('show')
                } else if (type === 'school') {
                  $('#updateSchoolIndicatorRecordsModal').modal('show')
                }
              }
            }
          }
        ],
        language: {
          url: ceap.datatableLanguageURL
        },
        rowId: 'indicator_id',
        columns: [
          {
            data: 'indicator_id',
            checkboxes: {
              selectRow: true
            },
            'createdCell': function (td, cellData, rowData, row, col) {
              if (rowData.indicators_deleted_at !== null) {
                this.api().cell(td).checkboxes.disable()
              }
            },
            searchable: false,
            orderable: false,
            exportable: false,
            printable: false,
            responsivePriority: 1,
            render: function (data, type, full, meta) {
              if (!full.indicators_deleted_at) {
                return '<td class=" dt-body-center" tabindex="0"><input type="checkbox" class="dt-checkboxes"></td>'
              } else {
                return ''
              }
            }
          },
          {
            data: 'indicator_name',
            searchable: true,
            orderable: true,
            exportable: true,
            printable: true,
            responsivePriority: 2
          },
          {
            data: 'indicator_description',
            render: function (data, type, full, meta) {
              return $('<div/>').html(full.indicator_description).text()
            },
            searchable: true,
            orderable: true,
            exportable: true,
            printable: true,
            responsivePriority: 4
          },
          {
            data: 'indicators_updated_at',
            searchable: true,
            orderable: true,
            exportable: true,
            printable: true,
            responsivePriority: 5,
            render: function (data, type, full, meta) {
              let localLocale = Moment(data).locale(document.documentElement.lang)
              return localLocale.format('L')
            }
          },
          {
            searchable: false,
            orderable: false,
            exportable: false,
            printable: false,
            responsivePriority: 3,
            render: function (data, type, full, meta) {
              if (!full.indicators_deleted_at) {
                let btn1Route = ceap.goalsCreateRoute
                let btn3Route = ceap.indicatorRecordsIndexRoute.replace(0, full.indicator_id)
                return '<div class="btn-group" role="group">' +
                  '<form method="GET" action="' + btn1Route + '" accept-charset="UTF-8">' +
                  '<input name="indicator_id" type="hidden" value="' + full.indicator_id + '">' +
                  '<button type="submit" class="btn btn-primary panel-title-buttons btn-goals-create" style="margin-top: 3px;margin-bottom: 3px;">' +
                  ceap.buttonDefineGoals +
                  '</button>' +
                  '</form>' + '</div>'
              } else {
                // botão restaurar
                return '<div class="btn-group" role="group">' +
                  '<form method="POST" action="' + ceap.indicatorsRestoreRoute + '" accept-charset="UTF-8">' +
                  '<input name="_token" type="hidden" value="' + ceap._token + '">' +
                  '<input name="indicator_id" type="hidden" value="' + full.indicator_id + '">' +
                  '<input name="indicator_group_id" type="hidden" value="' + full.indicator_group_id + '">' +
                  '<button type="submit" class="btn btn-primary panel-title-buttons" style="margin-top: 3px;margin-bottom: 3px;">' +
                  ceap.indicatorRestoreLabel +
                  '</button>' +
                  '</form>' + '</div>'
              }
            }
          }
        ],
        responsive: true,
        order: [[1, 'asc']],
        stateSave: true,
        stateDuration: -1
      })
  } else {
    table = $('#' + type + 'IndicatorsDataTable').DataTable().ajax.reload()
  }
  return table
}

$('#ceapTrashedRegisters').on('change', function () {
  getIndicatorsDataTable('ceap')
})

$('#roomsTrashedRegisters').on('change', function () {
  getIndicatorsDataTable('rooms')
})

$('#schoolTrashedRegisters').on('change', function () {
  getIndicatorsDataTable('school')
})

// End of Custom DataTable

// Handle Generate Graph form submission event
$('#showChartForm').on('submit', function (e) {
  let form = this
  let type = $('select[name="indicatorGroupNameSelect"] option:selected').val()
  $(form).append(
    $('<input>')
      .attr('type', 'hidden')
      .attr('name', 'indicatorGroupType')
      .val(type)
  )
  if (type === '1') {
    let rowsSelected = $('#ceapIndicatorsDataTable').DataTable().column(0).checkboxes.selected()
    let data = $('#ceapIndicatorsDataTable').DataTable().rows({selected: true}).data()
    // Iterate over all selected checkboxes
    $.each(rowsSelected, function (index, rowId) {
      // Create a hidden element
      $(form).append(
        $('<input>')
          .attr('type', 'hidden')
          .attr('name', 'indicators[]')
          .val(rowId)
      )
    })
  } else if (type === '2') {
    let rowsSelected = $('#roomsIndicatorsDataTable').DataTable().column(0).checkboxes.selected()
    let data = $('#roomsIndicatorsDataTable').DataTable().rows({selected: true}).data()
    // Iterate over all selected checkboxes
    $.each(rowsSelected, function (index, rowId) {
      // Create a hidden element
      $(form).append(
        $('<input>')
          .attr('type', 'hidden')
          .attr('name', 'indicators[]')
          .val(rowId)
      )
    })
  } else if (type === '3') {
    let rowsSelected = $('#schoolIndicatorsDataTable').DataTable().column(0).checkboxes.selected()
    let data = $('#schoolIndicatorsDataTable').DataTable().rows({selected: true}).data()
    // Iterate over all selected checkboxes
    $.each(rowsSelected, function (index, rowId) {
      // Create a hidden element
      $(form).append(
        $('<input>')
          .attr('type', 'hidden')
          .attr('name', 'indicators[]')
          .val(rowId)
      )
    })
  }
})

// Remove error messages when closing a modal
$('.btn-close-modal').on('click', cleanErrorMessages)

function cleanErrorMessages() {
  $('.alert-danger').each(function (index, element) {
    element.remove()
  })
}

$(document).ready(function () {
  // Trigger change on indicatorGroup select
  let indicatorGroupId = JSON.parse(sessionStorage.getItem('indicatorGroupId'))
  if (indicatorGroupId) {
    $('select[name="indicatorGroupNameSelect"]').val(indicatorGroupId)
    $('select[name="indicatorGroupNameSelect"]').trigger('change')
  }
})

function changeDatatableSearchFieldColor(e, settings, data) {
  let input = $('#' + settings.nTableWrapper.id + ' .dataTables_filter input')
  let value = input.val()
  // Change color of datatable search input when not empty
  if (value !== '') {
    input.css({
      'box-shadow': 'inset 0 1px 1px rgba(0, 0, 0, 0.075), 0 0 3px rgba(255, 0, 0, 1)',
      'border-color': 'red'
    })
  } else {
    input.css({
      'box-shadow': 'inset 0 1px 1px rgba(0, 0, 0, 0.075), 0 0 3px rgba(255, 255, 255, 0.1)',
      'border-color': '#ccd0d2'
    })
  }
}
