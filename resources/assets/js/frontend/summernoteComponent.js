module.exports = {
  template: '<textarea :name="name"></textarea>',

  props: {
    model: {
      required: true
    },

    name: {
      type: String,
      required: true
    },

    height: {
      type: String,
      default: '250'
    },

    lang: {
      type: String,
      default: 'pt-BR'
    },

    placeholder: {
      type: String
    },

    toolbar: {
      type: Array
    }
  },

  mounted () {
    let config = {
      height: this.height,
      lang: this.lang,
      name: this.name,
      placeholder: this.placeholder,
      toolbar: this.toolbar
    }

    let vm = this

    config.callbacks = {

      onInit: function () {
        $(vm.$el).summernote('code', vm.model)
      },

      onChange: function () {
        vm.$emit('change', $(vm.$el).summernote('code'))
      },

      onBlur: function () {
        vm.$emit('change', $(vm.$el).summernote('code'))
      }
    }

    $(this.$el).summernote(config)
  }
}
