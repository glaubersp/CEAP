import Lang from 'lang.js'
import messages from './messages.js'

const lang = new Lang({
  messages
})

export default lang
