// Source: https://github.com/laracasts/Vue-Forms/blob/master/public/js/app.js
export class Errors {
  /**
   * Create a new Errors instance.
   */
  constructor () {
    this.errors = {}
  }

  /**
   * Determine if an errors exists for the given field.
   *
   * @param {string} field
   */
  has (field) {
    return this.errors.hasOwnProperty(field)
  }

  /**
   * Determine if we have any errors.
   */
  any () {
    return Object.keys(this.errors).length > 0
  }

  /**
   * Retrieve the error message for a field.
   *
   * @param {string} field
   */
  get (field) {
    if (this.errors[field]) {
      return this.errors[field][0]
    }
  }

  /**
   * Record the new errors.
   *
   * @param {object} errors
   */
  record (errors) {
    this.errors = errors
  }

  /**
   * Clear one or all error fields.
   *
   * @param {string|null} field
   */
  clear (target) {
    // Select2
    if (target.className === 'select2-selection__rendered' || target.className === 'select2-search__field') {
      // multiple selection
      if (target.id === '') {
        let phrase = $(target).closest('span').attr('aria-owns')
        let multipleSelectId = phrase.slice(8, -8)
        delete this.errors[multipleSelectId]
      } else {
        // single selection
        let phrase = $(target).attr('id')
        let signleSelectId = phrase.slice(8, -10)
        delete this.errors[signleSelectId]
      }
      // Summernote
    } else if (target.className === 'note-editable panel-body') {
      delete this.errors['justification']
      let summernoteId = $(target).parent('div').parent('div').parent('div').children('textarea').attr('id')
      delete this.errors[summernoteId]
    }
    delete this.errors[target.id]
  }
}
