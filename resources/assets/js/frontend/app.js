/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */
// Datepicker
import datePicker from 'vue-bootstrap-datetimepicker'
import Datatable from 'vue2-datatable-component'
import lang from './language'

import { eventHub } from './eventHub'

window.eventHub = eventHub
window.Lang = lang
require('../bootstrap-frontend')
require('../plugins')
require('summernote/dist/summernote')
require('select2/dist/js/select2.full')
require('../plugin/datatables/datatables')

window.Vue = require('vue')
Vue.use(datePicker)
Vue.use(Datatable)

window.Moment = require('moment')

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

// actions
Vue.component('action-create-form', require('../components/frontend/actions/ActionCreateForm.vue'))
Vue.component('action-edit-form', require('../components/frontend/actions/ActionEditForm.vue'))
Vue.component('action-delete-modal', require('../components/frontend/actions/ActionDeleteModal.vue'))
Vue.component('action-trash-modal', require('../components/frontend/actions/ActionTrashModal.vue'))
Vue.component('action-evaluation-modal', require('../components/frontend/actions/ActionEvaluationModal.vue'))
Vue.component('action-evidence-modal', require('../components/frontend/actions/ActionEvidenceModal.vue'))

// agreements
Vue.component('agreement-create-form', require('../components/frontend/agreements/AgreementCreateForm.vue'))
Vue.component('agreement-edit-form', require('../components/frontend/agreements/AgreementEditForm.vue'))
Vue.component('agreement-delete-form', require('../components/frontend/agreements/AgreementDeleteForm.vue'))
Vue.component('agreement-trash-modal', require('../components/frontend/agreements/AgreementTrashModal.vue'))

// indicators groups
Vue.component('create-indicator-group-modal', require('../components/frontend/indicatorGroups/CreateIndicatorGroupsModal.vue'))
Vue.component('edit-indicator-group-modal', require('../components/frontend/indicatorGroups/EditIndicatorGroupsModal.vue'))
Vue.component('delete-indicator-group-modal', require('../components/frontend/indicatorGroups/DeleteIndicatorGroupsModal.vue'))
Vue.component('indicator-group-panel', require('../components/frontend/indicatorGroups/IndicatorGroupPanel.vue'))

// indicator
Vue.component('create-indicator-modal', require('../components/frontend/indicators/CreateIndicatorModal.vue'))
Vue.component('edit-indicator-modal', require('../components/frontend/indicators/EditIndicatorModal.vue'))
Vue.component('delete-indicator-modal', require('../components/frontend/indicators/DeleteIndicatorModal.vue'))
Vue.component('indicator-panel', require('../components/frontend/indicators/IndicatorPanel.vue'))

// indicator records
Vue.component('create-indicator-records-modal', require('../components/frontend/indicatorRecords/CreateCEAPIndicatorRecordsModal.vue'))
Vue.component('create-rooms-indicator-records-modal', require('../components/frontend/indicatorRecords/CreateRoomsIndicatorRecordsModal.vue'))
Vue.component('create-school-indicator-records-modal', require('../components/frontend/indicatorRecords/CreateSchoolIndicatorRecordsModal.vue'))
Vue.component('update-indicator-records-modal', require('../components/frontend/indicatorRecords/UpdateCEAPIndicatorRecordsModal.vue'))
Vue.component('update-rooms-records-modal', require('../components/frontend/indicatorRecords/UpdateRoomsIndicatorRecordsModal.vue'))
Vue.component('update-school-records-modal', require('../components/frontend/indicatorRecords/UpdateSchoolIndicatorRecordsModal.vue'))
Vue.component('edit-ceap-record-value', require('../components/frontend/indicatorRecords/td-EditCEAPRecordValue'))
Vue.component('edit-rooms-record-value', require('../components/frontend/indicatorRecords/td-EditRoomsRecordValue'))
Vue.component('edit-school-record-value', require('../components/frontend/indicatorRecords/td-EditSchoolRecordValue'))

// goals
Vue.component('goal-create-form', require('../components/frontend/goals/GoalCreateForm.vue'))
Vue.component('goal-edit-form', require('../components/frontend/goals/GoalEditForm.vue'))
Vue.component('delete-goal-modal', require('../components/frontend/goals/DeleteGoalModal.vue'))
Vue.component('trash-goal-modal', require('../components/frontend/goals/TrashGoalModal.vue'))

Vue.component('modal', require('../components/frontend/Modal.vue'))
Vue.component('panel', require('../components/frontend/Panel.vue'))
Vue.component('indicators-datatable', require('../components/frontend/IndicatorsDataTable.vue'))

Vue.component('summernote', require('./summernoteComponent.js'))

Vue.filter('trans', (...args) => {
  return lang.get(...args)
})

const app = new Vue({
  el: '#app'
})
