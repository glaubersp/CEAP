module.exports = {
  'extends': 'standard',
  'installedESLint': true,
  'parserOptions': {
    'ecmaVersion': 8,
    'sourceType': 'module',
    'ecmaFeatures': {
      'jsx': true
    }
  },
  'env': {
    'browser': true,
    'commonjs': true,
    'es6': true,
    'jquery': true
  },
  'globals': {
    '$': true,
    'jQuery': true,
    'Vue': true,
    'swal': true
  },
  'plugins': [
    'promise'
  ]
}