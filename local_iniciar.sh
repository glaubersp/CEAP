#!/bin/bash

if [[ ! -e "/usr/local/bin/yarn" ]]; then
    echo "Instale a ferramenta Yarn (https://yarnpkg.com) com o seguinte comando:"
    echo "brew install yarn"
    echo "Em seguida, execute novamente este script."
    #brew install yarn
    exit
fi

if [[ ! -e "/usr/local/bin/npm" ]]; then
    echo "Instale a ferramenta NodeJS (https://nodejs.org) com o seguinte comando:"
    echo "brew install nodejs"
    echo "Em seguida, execute novamente este script."
    #brew install nodejs
    exit
fi

if [[ ! -e ".env" ]]; then
    echo "Crie o arquivo .env a partir do arquivo .env.example."
    echo "Em seguida, execute este comando novamente."
    # cp .env.example .env
    exit
fi

rm *.lock
rm -fr vendor node_modules

composer install
yarn
php artisan key:generate
php artisan migrate:refresh --seed
php artisan clear-compiled
php artisan cache:clear
php artisan config:clear
php artisan debugbar:clear
php artisan route:clear
php artisan view:clear

npm run development
