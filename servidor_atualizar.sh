#!/bin/bash

rm *.lock
rm -fr vendor node_modules

composer install
npm install

php artisan clear-compiled
php artisan cache:clear
php artisan config:clear
php artisan route:clear
php artisan view:clear

npm run production

#sudo chown apache:apache -R /var/www/laravel