<?php

use Behat\Behat\Tester\Exception\PendingException;
use Behat\Behat\Context\Context;
use Behat\Gherkin\Node\PyStringNode;
use Behat\Gherkin\Node\TableNode;

/**
 * Defines application features from the specific context.
 */
class FeatureContext implements Context
{
    /**
     * Initializes context.
     *
     * Every scenario gets its own context instance.
     * You can also pass arbitrary arguments to the
     * context constructor through behat.yml.
     */
    public function __construct()
    {
    }


    /**
     * @Given there is agent A
     */
    public function thereIsAgentA()
    {
        throw new PendingException();
    }

    /**
     * @Given there is agent B
     */
    public function thereIsAgentB()
    {
        throw new PendingException();
    }

    /**
     * @Given there is agent J
     */
    public function thereIsAgentJ()
    {
        throw new PendingException();
    }

    /**
     * @Given there is agent K
     */
    public function thereIsAgentK()
    {
        throw new PendingException();
    }

    /**
     * @When I erase agent K's memory
     */
    public function iEraseAgentKSMemory()
    {
        throw new PendingException();
    }

    /**
     * @Then there should be agent J
     */
    public function thereShouldBeAgentJ()
    {
        throw new PendingException();
    }

    /**
     * @Then there should not be agent K
     */
    public function thereShouldNotBeAgentK()
    {
        throw new PendingException();
    }

    /**
     * @Given there is agent D
     */
    public function thereIsAgentD()
    {
        throw new PendingException();
    }

    /**
     * @Given there is agent M
     */
    public function thereIsAgentM()
    {
        throw new PendingException();
    }

    /**
     * @When I erase agent M's memory
     */
    public function iEraseAgentMSMemory()
    {
        throw new PendingException();
    }

    /**
     * @Then there should be agent D
     */
    public function thereShouldBeAgentD()
    {
        throw new PendingException();
    }

    /**
     * @Then there should not be agent M
     */
    public function thereShouldNotBeAgentM()
    {
        throw new PendingException();
    }
}
