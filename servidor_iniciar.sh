#!/bin/bash

rm *.lock
rm -fr vendor node_modules

composer install
yarn
php artisan key:generate
php artisan migrate:refresh --seed

npm run production