<?php

/**
 * All route names are prefixed with 'admin.ceap'.
 */
Route::group([
    'prefix'     => 'ceap',
    'as'         => 'ceap.',
    'namespace'  => 'CEAP',
], function () {
    Route::group([
        'middleware' => 'role:administrator',
    ], function () {
        /*
         * Evaluation Terms Management
         */
        Route::group(['namespace' => 'EvaluationTerm'], function () {
            Route::resource('evaluationTerm', 'EvaluationTermController', ['except' => ['show']]);
        });

        /*
         * Agreement Monitoring Strategies Management
         */
        Route::group(['namespace' => 'AgreementMonitoringStrategy'], function () {
            Route::resource('monitoringStrategy', 'AgreementMonitoringStrategyController', ['except' => ['show']]);
        });

        /*
         * Topic Tags Management
         */
        Route::group(['namespace' => 'TopicTag'], function () {
            Route::resource('topicTag', 'TopicTagController', ['except' => ['show']]);
        });

        /*
         * Agreement Roles Management
         */
        Route::group(['namespace' => 'AgreementRole'], function () {
            Route::resource('agreementRole', 'AgreementRoleController', ['except' => ['show']]);
        });
    });
});
