<?php

/**
 * CEAP frontend Controllers
 * All route names are prefixed with 'frontend.'
 */

Route::get('/', 'CEAPFrontendController@home')->name('index');
Route::get('about', 'CEAPFrontendController@about')->name('about');

/**
 * These frontend controllers require the user to be logged in
 * All route names are prefixed with 'frontend.ceap.'
 */
Route::group(['namespace' => 'CEAP', 'as' => 'ceap.', 'middleware' => 'auth'], function () {

    Route::get('help', 'CEAPController@help')->name('help');
    Route::get('files/{foldername}/{filename}', 'FileController@getFile')->where('filename', '^[^/]+$');

    Route::group(['middleware' => 'permission:can-create'], function () {
        Route::resource('goals', 'GoalController', ['only' => ['create', 'store']]);
        Route::resource('agreements', 'AgreementController', ['only' => ['create', 'store']]);
        Route::resource('actions', 'ActionController', ['only' => ['create', 'store']]);
        Route::resource('topic_tags', 'TopicTagController', ['only' => ['create', 'store']]);
        Route::resource('indicatorGroups', 'IndicatorGroupController', ['only' => ['create', 'store']]);
        Route::resource('indicators.indicatorRecords', 'IndicatorRecordController', ['only' => ['create', 'store']]);
        Route::resource('indicators', 'IndicatorController', ['only' => ['create', 'store']]);
        Route::resource('actions.evaluations', 'ActionEvaluationController', ['only' => ['create', 'store']]);
        Route::resource('actions.evidences', 'ActionEvidenceController', ['only' => ['create', 'store']]);
        Route::post('indicatorGroups/{indicatorGroup}/createGroupCEAP', 'IndicatorGroupController@createGroupCEAP')->name('indicatorGroups.createGroupCEAP');
        Route::post('indicatorGroups/{indicatorGroup}/createGroupRooms', 'IndicatorGroupController@createGroupRooms')->name('indicatorGroups.createGroupRooms');
        Route::post('indicatorGroups/{indicatorGroup}/createGroupSchool', 'IndicatorGroupController@createGroupSchool')->name('indicatorGroups.createGroupSchool');
    });

    Route::group(['middleware' => 'permission:can-edit'], function () {
        Route::resource('goals', 'GoalController', ['only' => ['edit','update']]);
        Route::resource('agreements', 'AgreementController', ['only' => ['edit','update']]);
        Route::resource('actions', 'ActionController', ['only' => ['edit', 'update']]);
        Route::resource('topic_tags', 'TopicTagController', ['only' => ['edit','update']]);
        Route::resource('indicatorGroups', 'IndicatorGroupController', ['only' => ['edit','update']]);
        Route::resource('indicators', 'IndicatorController', ['only' => ['edit','update']]);
        Route::patch('indicators/{indicator}/indicatorRecords/update', 'IndicatorRecordController@update')->name('indicators.indicatorRecords.update');
        Route::patch('actions/{action}/priority', 'ActionController@updatePriority')->name('actions.priorityUpdate');
        Route::get('indicators/{indicator}/indicatorRecords/edit', 'IndicatorRecordController@edit')->name('indicators.indicatorRecords.edit');
        Route::resource('actions.evaluations', 'ActionEvaluationController', ['only' => ['edit', 'update']]);
        Route::resource('actions.evidences', 'ActionEvidenceController', ['only' => ['edit', 'update']]);
    });

    Route::group(['middleware' => 'permission:can-read'], function () {
        Route::resource('goals', 'GoalController', ['only' => ['index', 'show']]);
        Route::resource('agreements', 'AgreementController', ['only' => ['index', 'show']]);
        Route::resource('actions', 'ActionController', ['only' => ['index', 'show']]);
        Route::resource('topic_tags', 'TopicTagController', ['only' => ['index', 'show']]);
        Route::resource('indicatorGroups', 'IndicatorGroupController', ['only' => ['index', 'show']]);
        Route::resource('indicators', 'IndicatorController', ['only' => ['index', 'show']]);
        Route::get('indicators/{indicator}/indicatorRecords', 'IndicatorRecordController@index')->name('indicators.indicatorRecords.index');
        Route::get('indicators/{indicator}/indicatorRecords/show', 'IndicatorRecordController@show')->name('indicators.indicatorRecords.show');
        Route::post('indicators/showchart', 'IndicatorController@showChart')->name('indicators.showChart');
        Route::resource('actions.evaluations', 'ActionEvaluationController', ['only' => ['index', 'show']]);
        Route::resource('actions.evidences', 'ActionEvidenceController', ['only' => ['index', 'show']]);
        Route::get('schoolClasses/years', 'SchoolClassController@getYears')->name('schoolClasses.years');
        Route::get('actionsjson/{action}/', 'ActionController@json')->name('actions.json');
        Route::get('schoolClasses/gradesByYear', 'SchoolClassController@getGradesByYear')->name('schoolClasses.gradesByYear');
        Route::get('schoolClasses/dataTable', 'SchoolClassController@getDataTable')->name('schoolClasses.dataTable');
        Route::get('indicatorGroups/{indicatorGroup}/getCEAPModalData', 'IndicatorGroupController@getCEAPModalData')->name('indicatorGroups.getCEAPModalData');
        Route::get('indicatorGroups/{indicatorGroup}/getCEAPIndicatorRecordsDataTable', 'IndicatorGroupController@getCEAPIndicatorRecordsDataTable')->name('indicatorGroups.getCEAPIndicatorRecordsDataTable');
        Route::post('indicatorGroups/{indicatorGroup}/updateGroupCEAP', 'IndicatorGroupController@updateGroupCEAP')
             ->name('indicatorGroups.updateGroupCEAP');
        Route::get('indicatorGroups/{indicatorGroup}/getRoomsModalData', 'IndicatorGroupController@getRoomsModalData')
             ->name('indicatorGroups.getRoomsModalData');
        Route::get('indicatorGroups/{indicatorGroup}/getSchoolModalData', 'IndicatorGroupController@getSchoolModalData')
             ->name('indicatorGroups.getSchoolModalData');
        Route::get('indicatorGroups/{indicatorGroup}/getRoomsIndicatorRecordsDataTable',
            'IndicatorGroupController@getRoomsIndicatorRecordsDataTable')
             ->name('indicatorGroups.getRoomsIndicatorRecordsDataTable');
        Route::get('indicatorGroups/{indicatorGroup}/getSchoolIndicatorRecordsDataTable',
            'IndicatorGroupController@getSchoolIndicatorRecordsDataTable')
             ->name('indicatorGroups.getSchoolIndicatorRecordsDataTable');
        Route::get('indicatorGroups/{indicatorGroup}/getSchoolIndicatorRecordsDataTable',
            'IndicatorGroupController@getSchoolIndicatorRecordsDataTable')
             ->name('indicatorGroups.getSchoolIndicatorRecordsDataTable');
        Route::post('indicatorGroups/{indicatorGroup}/updateGroupRooms', 'IndicatorGroupController@updateGroupRooms')
             ->name('indicatorGroups.updateGroupRooms');
        Route::post('indicatorGroups/{indicatorGroup}/updateGroupSchool', 'IndicatorGroupController@updateGroupSchool')
             ->name('indicatorGroups.updateGroupSchool');
    });

    Route::group(['middleware' => 'permission:can-trash|can-delete'], function () {
        Route::resource('goals', 'GoalController', ['only' => ['destroy']]);
        Route::resource('agreements', 'AgreementController', ['only' => ['destroy']]);
        Route::resource('actions', 'ActionController', ['only' => ['destroy']]);
        Route::resource('topic_tags', 'TopicTagController', ['only' => ['destroy']]);
        Route::resource('indicatorGroups', 'IndicatorGroupController', ['only' => ['destroy']]);
        Route::resource('indicators', 'IndicatorController', ['only' => ['destroy']]);
        Route::delete('indicators/{indicator}/indicatorRecords/destroy', 'IndicatorRecordController@destroy')->name('indicators.indicatorRecords.destroy');
        Route::resource('actions.evaluations', 'ActionEvaluationController', ['only' => ['destroy']]);
        Route::resource('actions.evidences', 'ActionEvidenceController', ['only' => ['destroy']]);
    });

    Route::group(['middleware' => 'permission:can-restore'], function () {
        Route::post('actions/restore', 'ActionController@restore')->name('actions.restore');
        Route::post('agreements/restore', 'AgreementController@restore')->name('agreements.restore');
        Route::post('goals/restore', 'GoalController@restore')->name('goals.restore');
        Route::post('indicators/restore', 'IndicatorController@restore')->name('indicators.restore');
    });
});