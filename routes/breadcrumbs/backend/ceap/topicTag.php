<?php

Breadcrumbs::register('admin.ceap.topicTag.index', function ($breadcrumbs) {
    $breadcrumbs->parent('admin.dashboard');
    $breadcrumbs->push(removeCollon(__('ceap.agreements.topic_tags.label')), route('admin.ceap.topicTag.index'));
});

Breadcrumbs::register('admin.ceap.topicTag.create', function ($breadcrumbs) {
    $breadcrumbs->parent('admin.ceap.topicTag.index');
    $breadcrumbs->push(__('ceap.button.create') . ' ' . removeCollon(__('ceap.agreements.topic_tags.label')), route('admin.ceap.topicTag.create'));
});

Breadcrumbs::register('admin.ceap.topicTag.edit', function ($breadcrumbs, $id) {
    $breadcrumbs->parent('admin.ceap.topicTag.index');
    $breadcrumbs->push(__('ceap.button.edit') . ' ' . removeCollon(__('ceap.agreements.topic_tags.label')), route('admin.ceap.topicTag.edit', $id));
});