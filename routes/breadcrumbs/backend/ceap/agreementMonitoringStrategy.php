<?php

Breadcrumbs::register('admin.ceap.monitoringStrategy.index', function ($breadcrumbs) {
    $breadcrumbs->parent('admin.dashboard');
    $breadcrumbs->push(removeCollon(__('ceap.agreements.evaluation_strategy.label')), route('admin.ceap.monitoringStrategy.index'));
});

Breadcrumbs::register('admin.ceap.monitoringStrategy.create', function ($breadcrumbs) {
    $breadcrumbs->parent('admin.ceap.monitoringStrategy.index');
    $breadcrumbs->push(__('ceap.button.create') . ' ' . removeCollon(__('ceap.agreements.evaluation_strategy.label')), route('admin.ceap.monitoringStrategy.create'));
});

Breadcrumbs::register('admin.ceap.monitoringStrategy.edit', function ($breadcrumbs, $id) {
    $breadcrumbs->parent('admin.ceap.monitoringStrategy.index');
    $breadcrumbs->push(__('ceap.button.edit') . ' '  . removeCollon(__('ceap.agreements.evaluation_strategy.label')), route('admin.ceap.monitoringStrategy.edit', $id));
});