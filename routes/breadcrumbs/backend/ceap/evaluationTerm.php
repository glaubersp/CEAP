<?php

Breadcrumbs::register('admin.ceap.evaluationTerm.index', function ($breadcrumbs) {
    $breadcrumbs->parent('admin.dashboard');
    $breadcrumbs->push(__('ceap.backend.evaluation_term.title'), route('admin.ceap.evaluationTerm.index'));
});

Breadcrumbs::register('admin.ceap.evaluationTerm.create', function ($breadcrumbs) {
    $breadcrumbs->parent('admin.ceap.evaluationTerm.index');
    $breadcrumbs->push(__('ceap.button.create') . ' ' . removeCollon(__('ceap.actions.evaluation_term.label')), route('admin.ceap.evaluationTerm.create'));
});

Breadcrumbs::register('admin.ceap.evaluationTerm.edit', function ($breadcrumbs, $id) {
    $breadcrumbs->parent('admin.ceap.evaluationTerm.index');
    $breadcrumbs->push(__('ceap.button.edit') . ' ' . removeCollon(__('ceap.actions.evaluation_term.label')), route('admin.ceap.evaluationTerm.edit', $id));
});