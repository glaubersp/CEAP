<?php

Breadcrumbs::register('admin.ceap.agreementRole.index', function ($breadcrumbs) {
    $breadcrumbs->parent('admin.dashboard');
    $breadcrumbs->push(removeCollon(__('ceap.agreements.roles.label')), route('admin.ceap.agreementRole.index'));
});

Breadcrumbs::register('admin.ceap.agreementRole.create', function ($breadcrumbs) {
    $breadcrumbs->parent('admin.ceap.agreementRole.index');
    $breadcrumbs->push(__('ceap.button.create') . ' ' . removeCollon(__('ceap.agreements.roles.label')), route('admin.ceap.agreementRole.create'));
});

Breadcrumbs::register('admin.ceap.agreementRole.edit', function ($breadcrumbs, $id) {
    $breadcrumbs->parent('admin.ceap.agreementRole.index');
    $breadcrumbs->push(__('ceap.button.edit') . ' ' . removeCollon(__('ceap.agreements.roles.label')), route('admin.ceap.agreementRole.edit', $id));
});