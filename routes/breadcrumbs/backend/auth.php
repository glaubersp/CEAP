<?php

require __DIR__.'/auth/user.php';
require __DIR__.'/auth/role.php';
require __DIR__.'/ceap/evaluationTerm.php';
require __DIR__.'/ceap/agreementMonitoringStrategy.php';
require __DIR__.'/ceap/topicTag.php';
require __DIR__.'/ceap/agreementRole.php';
