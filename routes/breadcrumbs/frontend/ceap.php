<?php

// Principal
Breadcrumbs::register('frontend.index', function ($breadcrumbs) {
    $breadcrumbs->push("/", route('frontend.index'));
});
Breadcrumbs::register('frontend.user.dashboard', function ($breadcrumbs) {
    $breadcrumbs->push(trans('navs.frontend.dashboard'), route('frontend.user.dashboard'));
});

// Principal/Minha Conta
Breadcrumbs::register('frontend.user.account', function ($breadcrumbs) {
    $breadcrumbs->parent('frontend.user.dashboard');
    $breadcrumbs->push(trans('navs.frontend.user.account'), route('frontend.user.account'));
});

// Principal/Minha Conta
Breadcrumbs::register('frontend.auth.password.expired', function ($breadcrumbs) {
    $breadcrumbs->parent('frontend.user.dashboard');
    $breadcrumbs->push(trans('navs.frontend.user.account'), route('frontend.auth.password.expired'));
});

// Principal/Ajuda
Breadcrumbs::register('frontend.ceap.help', function ($breadcrumbs) {
    $breadcrumbs->parent('frontend.user.dashboard');
    $breadcrumbs->push(trans('ceap.menu.help'), route('frontend.ceap.help'));
});

/*---------------------------------------------------------------------------------*/

// AÇÃO
// Principal/Acompanhar Ações/Avaliar Ação
Breadcrumbs::register('frontend.ceap.actions.avaliar', function ($breadcrumbs) {
    $breadcrumbs->parent('frontend.user.dashboard');
    $breadcrumbs->push(trans('ceap.actions.avaliar.panelTitle'), route('frontend.ceap.actions.avaliar'));
});

// Principal/Acompanhar Ações
Breadcrumbs::register('frontend.ceap.actions.index', function ($breadcrumbs) {
    $breadcrumbs->parent('frontend.user.dashboard');
    $breadcrumbs->push(trans('ceap.actions.index.panel_title'), route('frontend.ceap.actions.index'));
});

// Principal/Planejar nova Ação
Breadcrumbs::register('frontend.ceap.actions.create', function ($breadcrumbs) {
    $breadcrumbs->parent('frontend.user.dashboard');
    $breadcrumbs->push(trans('ceap.actions.create.panel_title'), route('frontend.ceap.actions.create'));
});

// Principal/Acompanhar Ações/Exibir Ação
Breadcrumbs::register('frontend.ceap.actions.show', function ($breadcrumbs, $action) {
    if (Request::session()->get('parent_route') === 'frontend.ceap.actions.index') {
        Request::session()->keep('parent_route');
        $breadcrumbs->parent('frontend.ceap.actions.index');
    } else {
        $breadcrumbs->parent('frontend.ceap.actions.index');
    }
    $breadcrumbs->push(trans('ceap.actions.show.panel_title'), route('frontend.ceap.actions.show', $action->id));
});

// Principal/Acompanhar Ações/Editar Ação
Breadcrumbs::register('frontend.ceap.actions.edit', function ($breadcrumbs, $action) {
    if (Request::session()->get('parent_route') === 'frontend.ceap.actions.index') {
        Request::session()->keep('parent_route');
        $breadcrumbs->parent('frontend.ceap.actions.index');
    } else {
        $breadcrumbs->parent('frontend.ceap.actions.index');
    }
    $breadcrumbs->push(trans('ceap.actions.edit.panel_title'), route('frontend.ceap.actions.edit', $action->id));
});

//Principal/Acompanhar Ações/$id (Avaliar Ações)
Breadcrumbs::register('frontend.ceap.actions.evaluations.index', function ($breadcrumbs, $action) {
    $breadcrumbs->parent('frontend.ceap.actions.index');
    $breadcrumbs->push(trans('ceap.actions.evaluations.modal.title'), route('frontend.ceap.actions.evaluations.index', $action->id));
});

//Principal/Acompanhar Ações/$id (Coletar Evidências)
Breadcrumbs::register('frontend.ceap.actions.evidences.index', function ($breadcrumbs, $action) {
    $breadcrumbs->parent('frontend.ceap.actions.index');
    $breadcrumbs->push(trans('ceap.actions.evidences.panel_title'), route('frontend.ceap.actions.evidences.index', [$action]));
});

// Principal/Acompanhar Ações/Coletar Evidências/Exibir Evidência
Breadcrumbs::register('frontend.ceap.actions.evidences.show', function ($breadcrumbs, $action, $evidence) {
    $breadcrumbs->parent('frontend.ceap.actions.evidences.index', $action, $evidence);
    $breadcrumbs->push(removeCollon(trans('ceap.actions.evidences.show.title')),
        route('frontend.ceap.actions.evidences.show', [$action, $evidence]));
});

//Principal/Acompanhar Ações/Coletar Evidências/Editar Evidência
Breadcrumbs::register('frontend.ceap.actions.evidences.edit', function ($breadcrumbs, $action, $evidence) {
    $breadcrumbs->parent('frontend.ceap.actions.evidences.index', $action, $evidence);
    $breadcrumbs->push(trans('ceap.actions.evidences.edit.title'),
        route('frontend.ceap.actions.evidences.edit', [$action, $evidence]));
});

/*---------------------------------------------------------------------------------*/

// INDICADOR
// Principal/Planejar novo Indicador
Breadcrumbs::register('frontend.ceap.indicators.create', function ($breadcrumbs) {
    $breadcrumbs->parent('frontend.user.dashboard');
    $breadcrumbs->push(trans('ceap.indicators.create.panel_title'), route('frontend.ceap.indicators.create'));
});

// Principal/Acompanhar Indicadores
Breadcrumbs::register('frontend.ceap.indicators.index', function ($breadcrumbs) {
    $breadcrumbs->parent('frontend.user.dashboard');
    $breadcrumbs->push(trans('ceap.indicators.index.panel_title'), route('frontend.ceap.indicators.index'));
});

// Principal/Acompanhar Indicadores/Gráfico de Indicador(es)
Breadcrumbs::register('frontend.ceap.indicators.showChart', function ($breadcrumbs) {
    $breadcrumbs->parent('frontend.ceap.indicators.index');
    $breadcrumbs->push(trans('ceap.indicators.chart.title'), route('frontend.ceap.indicators.showChart'));
});

// Principal/Acompanhar Indicadores/Exibir Indicador
Breadcrumbs::register('frontend.ceap.indicators.show', function ($breadcrumbs, $indicator) {
    if (Request::session()->get('parent_route') === 'frontend.ceap.actions.index') {
        Request::session()->keep('parent_route');
        $breadcrumbs->parent('frontend.ceap.actions.index');
    } else {
        $breadcrumbs->parent('frontend.ceap.indicators.index');
    }
    $breadcrumbs->push(trans('ceap.indicators.show.panel_title'),
        route('frontend.ceap.indicators.show', $indicator->id));
});
// Principal/Acompanhar Indicadores/Editar Indicador
Breadcrumbs::register('frontend.ceap.indicators.edit', function ($breadcrumbs, $indicator) {
    if (Request::session()->get('parent_route') === 'frontend.ceap.actions.index') {
        Request::session()->keep('parent_route');
        $breadcrumbs->parent('frontend.ceap.actions.index');
    } else {
        $breadcrumbs->parent('frontend.ceap.indicators.index');
    }
    $breadcrumbs->push(trans('ceap.indicators.edit.panel_title'),
        route('frontend.ceap.indicators.edit', $indicator->id));
});

// Principal/Acompanhar Indicadores/Registrar novo valor para Indicador
Breadcrumbs::register('frontend.ceap.indicators.indicatorRecords.create', function ($breadcrumbs, $indicator) {
    $breadcrumbs->parent('frontend.ceap.indicators.index');
    $breadcrumbs->push(trans('ceap.indicators.records.create.panel_title'),
        route('frontend.ceap.indicators.indicatorRecords.create', $indicator->id));
});

// Principal/Acompanhar Indicadores/Histórico do Indicador
Breadcrumbs::register('frontend.ceap.indicators.indicatorRecords.index', function ($breadcrumbs, $indicator) {
    $breadcrumbs->parent('frontend.ceap.indicators.index');
    $breadcrumbs->push(trans('ceap.indicators.records.index.panel_title'),
        route('frontend.ceap.indicators.indicatorRecords.index', $indicator->id));
});

// Principal/Acompanhar Indicadores/Histórico do Indicador/Editar Histórico do Indicador
Breadcrumbs::register('frontend.ceap.indicators.indicatorRecords.edit', function ($breadcrumbs, $indicator) {
    $breadcrumbs->parent('frontend.ceap.indicators.indicatorRecords.index', $indicator);
    $breadcrumbs->push(trans('ceap.indicators.records.edit.panel_title'),
        route('frontend.ceap.indicators.indicatorRecords.edit', $indicator->id));
});

/*---------------------------------------------------------------------------------*/

// OBJETIVO
// Principal/Planejar novo Objetivo
Breadcrumbs::register('frontend.ceap.goals.create', function ($breadcrumbs) {
    $breadcrumbs->parent('frontend.user.dashboard');
    $breadcrumbs->push(trans('ceap.goals.create.panel_title'), route('frontend.ceap.goals.create'));
});

// Principal/Acompanhar Objetivos
Breadcrumbs::register('frontend.ceap.goals.index', function ($breadcrumbs) {
    $breadcrumbs->parent('frontend.user.dashboard');
    $breadcrumbs->push(trans('ceap.goals.index.panel_title'), route('frontend.ceap.goals.index'));
});

// Principal/Acompanhar Objetivos/Exibir Objetivo
Breadcrumbs::register('frontend.ceap.goals.show', function ($breadcrumbs, $goal) {
    if (Request::session()->get('parent_route') === 'frontend.ceap.actions.index') {
        Request::session()->keep('parent_route');
        $breadcrumbs->parent('frontend.ceap.actions.index');
    } else {
        $breadcrumbs->parent('frontend.ceap.goals.index');
    }
    $breadcrumbs->push(trans('ceap.goals.show.panel_title'), route('frontend.ceap.goals.show', $goal->id));
});

// Principal/Acompanhar Objetivos/Editar Objetivo
Breadcrumbs::register('frontend.ceap.goals.edit', function ($breadcrumbs, $goal) {
    if (Request::session()->get('parent_route') === 'frontend.ceap.actions.index') {
        Request::session()->keep('parent_route');
        $breadcrumbs->parent('frontend.ceap.actions.index');
    } else {
        $breadcrumbs->parent('frontend.ceap.goals.index');
    }
    $breadcrumbs->push(trans('ceap.goals.edit.panel_title'), route('frontend.ceap.indicators.show', $goal->id));
});

/*---------------------------------------------------------------------------------*/

//COMBINADOS
// Principal/Criar Combinado
Breadcrumbs::register('frontend.ceap.agreements.create', function ($breadcrumbs) {
    $breadcrumbs->parent('frontend.user.dashboard');
    $breadcrumbs->push(trans('ceap.agreements.create.panel_title'), route('frontend.ceap.agreements.create'));
});

// Principal/Listar Combinado
Breadcrumbs::register('frontend.ceap.agreements.index', function ($breadcrumbs) {
    $breadcrumbs->parent('frontend.user.dashboard');
    $breadcrumbs->push(trans('ceap.agreements.index.panel_title'), route('frontend.ceap.agreements.index'));
});

// Principal/Listar Combinado/Exibir Combinado
Breadcrumbs::register('frontend.ceap.agreements.show', function ($breadcrumbs, $agreement) {
    $breadcrumbs->parent('frontend.ceap.agreements.index');
    $breadcrumbs->push(trans('ceap.agreements.show.panel_title'),
        route('frontend.ceap.agreements.show', $agreement->id));
});

// Principal/Listar Combinado/Editar Combinado
Breadcrumbs::register('frontend.ceap.agreements.edit', function ($breadcrumbs, $agreement) {
    $breadcrumbs->parent('frontend.ceap.agreements.index', $agreement);
    $breadcrumbs->push(trans('ceap.agreements.edit.panel_title'),
        route('frontend.ceap.agreements.show', $agreement->id));
});
