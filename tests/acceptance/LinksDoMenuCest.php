<?php


class LinksDoMenuCest
{
    public function _before(AcceptanceTester $I)
    {
        $I->amOnPage('/login');
        $I->fillField('//*[@name="email"]','admin@admin.com');
        $I->fillField('//*[@name="password"]','1234');
        $I->click('//*[@id="app"]/div/div[2]/div[2]/div/div[2]/form/div[4]/div/button');
    }

    public function _after(AcceptanceTester $I)
    {
        $I->amOnPage('/logout');
        $I->see('Sobre o projeto');
    }

    public function acessarPlanejarNovaAcao(AcceptanceTester $I)
    {
        $I->amOnPage('/dashboard');
        $I->wait(5);
        $I->seeLink('Planejar nova Ação', 'http://ceap-boilerplate.dev/actions/create');
        $I->click('//*[@id="planning-hover"]/a[1]');
        $I->wait(10);
        $I->see('Cancelar');
    }

    public function acessarCriarNovoObjetivo(AcceptanceTester $I)
    {
        $I->amOnPage('/dashboard');
        $I->seeLink('Criar novo Objetivo', 'http://ceap-boilerplate.dev/goals/create');
        $I->click('//*[@id="planning-hover"]/a[2]');
        $I->see('Cancelar');
    }

    public function acessarDefinirNovoIndicador(AcceptanceTester $I)
    {
        $I->amOnPage('/dashboard');
        $I->seeLink('Definir novo Indicador', 'http://ceap-boilerplate.dev/indicators/create');
        $I->click('//*[@id="planning-hover"]/a[3]');
        $I->see('Cancelar');
    }
    public function acessarPainelDeAcompanhamento(AcceptanceTester $I)
    {
        $I->amOnPage('/dashboard');
        $I->seeLink('Painel de acompanhamento', 'http://ceap-boilerplate.dev/monitoringChart');
        $I->click('//*[@id="monitoring-hover"]/a[1]');
        $I->see('Prioridade', '//*[@id="dataTableBuilder"]');
    }

    public function acessarColetarEvidenciasDeAcoes(AcceptanceTester $I)
    {
        $I->amOnPage('/dashboard');
        $I->seeLink('Coletar Evidências', 'http://ceap-boilerplate.dev/actionsCollectEvidences');
        $I->click('//*[@id="monitoring-hover"]/a[2]');
        $I->see('Evidências', '//*[@id="dataTableBuilder"]');
    }

    public function acessarListarAcoes(AcceptanceTester $I)
    {
        $I->amOnPage('/dashboard');
        $I->seeLink('Listar Ações', 'http://ceap-boilerplate.dev/actions');
        $I->click('//*[@id="monitoring-hover"]/a[3]');
        $I->see('Detalhamento', '//*[@id="dataTableBuilder"]');
    }

    public function acessarListarObjetivos(AcceptanceTester $I)
    {
        $I->amOnPage('/dashboard');
        $I->seeLink('Listar Objetivos', 'http://ceap-boilerplate.dev/goals');
        $I->click('//*[@id="monitoring-hover"]/a[4]');
        $I->see('Objetivo', '//*[@id="dataTableBuilder"]');
    }

    public function acessarListarIndicadores(AcceptanceTester $I)
    {
        $I->amOnPage('/dashboard');
        $I->seeLink('Listar Indicadores', 'http://ceap-boilerplate.dev/indicators');
        $I->click('//*[@id="monitoring-hover"]/a[5]');
        $I->see('Valor do Indicador', '//*[@id="dataTableBuilder"]');
    }

    public function acessarRegistrarNovoIndicador(AcceptanceTester $I)
    {
        $I->amOnPage('/dashboard');
        $I->seeLink('Registrar novo Indicador', 'http://ceap-boilerplate.dev/registrarindicator');
        $I->click('//*[@id="evaluation-hover"]/a[1]');
        $I->see('Cancelar');
    }

    public function acessarAvaliarAcoes(AcceptanceTester $I)
    {
        $I->amOnPage('/dashboard');
        $I->seeLink('Avaliar Ações', 'http://ceap-boilerplate.dev/showActionListForEvaluation');
        $I->click('//*[@id="evaluation-hover"]/a[2]');
        $I->see('Avaliar', '//*[@id="dataTableBuilder"]');
    }

    public function acessarCriarCombinado(AcceptanceTester $I)
    {
        $I->amOnPage('/dashboard');
        $I->seeLink('Criar Combinado', 'http://ceap-boilerplate.dev/agreements/create');
        $I->click('//*[@id="agreements-hover"]/a[1]');
        $I->see('Cancelar');
    }

    public function acessarListarCombinados(AcceptanceTester $I)
    {
        $I->amOnPage('/dashboard');
        $I->seeLink('Listar Combinados', 'http://ceap-boilerplate.dev/agreements');
        $I->click('//*[@id="agreements-hover"]/a[2]');
        $I->see('Combinado para', '//*[@id="dataTableBuilder"]');
    }
}
