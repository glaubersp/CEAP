# language: pt
Funcionalidade: Efetuar Login
  Como usuário,
  Eu quero acessar o sistema,
  Assim eu poderei executar as minhas tarefas.

  Cenário: Diretor entra no sistema
    Dado que o diretor tenha usuário cadastrado
      E tenha essas informações para acessar
    Quando eu colocar as informações de acesso
    Então eu consigo entrar no sistema
      E consigo visualizar as opções de direção.

  Cenário: Secretária entra no sistema
    Dado que a secretária tenha usuário cadastrado
      E tenha essas informações para acessar
    Quando eu colocar as informações de acesso
    Então eu consigo entrar no sistema
      E consigo visualizar as opções de direção.

  Cenário: Coordenador entra no sistema
    Dado que o coordenador tenha usuário cadastrado
      E tenha essas informações para acessar
    Quando eu colocar as informações de acesso
    Então eu consigo entrar no sistema
      E consigo visualizar as opções de direção.

  Cenário: Login incorreto
    Dado que o (diretor | secretária | coordenador) tenha usuário cadastrado
      E tenha essas informações para acessar
    Quando eu colocar as informações de acesso incorretamente
    Então eu não consigo entrar no sistema
      E visualizo uma mensagem de erro.
