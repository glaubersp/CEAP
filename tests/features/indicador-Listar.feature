# language: pt
@indicador
Funcionalidade: Acompanhar série temporal de indicador
    Como (Diretor | Coordenador)
    Eu preciso visualizar um gráfico temporal de um indicador
    Assim poderei acompanhar o desempenho do indicador

    Contexto:
        Dado que eu tenho permissão de leitura de indicador

    Esquema do Cenário: Diretor ou Coordenador acompanha indicador de aluno
        Dado que eu sou (Diretor | Coordenador)
        E que <indicador> existe
        E que <opção> existe
        Quando eu selecionar o tipo de indicador aluno
        E eu selecionar o indicador <indicador>
        E eu selecionar o tipo de agrupamento como <tipoAgrupamento>
        E eu selecionar o agrupamento <agrupamento>
        Então será exibido um gráfico com a série temporal do indicador <indicador> agrupado por <tipoAgrupamento> para o agrupamento <agrupamento>

        Exemplos:
            | indicador | tipoAgrupamento | agrupamento |
            | distorção idade-série | Aluno | aluno1 |
            | distorção idade-série | Sala | sala1 |
            | distorção idade-série | Série | série1 |
            | faltas | Aluno | aluno1 |
            | faltas | Sala | sala1 |
            | faltas | Série | série1 |
            | rendimento | Aluno | aluno1 |
            | rendimento | Sala | sala1 |
            | rendimento | Série | série1 |
            | indisciplina | Aluno | aluno1 |
            | indisciplina | Sala | sala1 |
            | indisciplina | Série | série1 |

    Esquema do Cenário: Diretor ou Coordenador acompanha indicador de escola
        Dado que eu sou (Diretor | Coordenador)
        E que <indicador> existe
        Quando eu selecionar o tipo de indicador escola
        Quando eu selecionar o <indicador>
        Então será exibido um gráfico com a série temporal de <indicador>

        Exemplos:
            | indicador |
            | Habilidades Acadêmicas - Expectativas Claras |
            | Habilidades Acadêmicas - Habilidades |
            | Habilidades Acadêmicas - Reconhecimento |
            | Habilidades Acadêmicas - Relacionamento |
            | Habilidades Interpessoais - Expectativas Claras |
            | Habilidades Interpessoais - Habilidades |
            | Habilidades Interpessoais - Reconhecimento |
            | Habilidades Interpessoais - Relacionamento |
            | Instrução - Expectativas Claras |
            | Instrução - Habilidades |
            | Instrução - Reconhecimento |
            | Instrução - Relacionamento |
            | Colaboração - Expectativas Claras |
            | Colaboração - Habilidades |
            | Colaboração - Reconhecimento |
            | Colaboração - Relacionamento |
            | Envolvimento de Pais/Responsáveis - Expectativas Claras |
            | Envolvimento de Pais/Responsáveis - Habilidades |
            | Envolvimento de Pais/Responsáveis - Reconhecimento |
            | Envolvimento de Pais/Responsáveis - Relacionamento |
            | Apoio da Comunidade - Expectativas Claras |
            | Apoio da Comunidade - Habilidades |
            | Apoio da Comunidade - Reconhecimento |
            | Apoio da Comunidade - Relacionamento |

    Cenário: Diretor acompanha indicador de professor
        Dado que eu sou o Diretor
        E que o indicador existe
        E que o professor existe
        Quando eu selecionar o tipo de indicador professor
        E selecionar o indicador <indicador>
        E selecionar o professor <professor>
        Então será exibido um gráfico com a série temporal do indicador <indicador> para o professor <professor>

        | indicador | professor |
        | faltas | professor1 |


    # Cenário: Diretor ou Coordenador acompanha indicador individual de aluno
    #     Dado que eu sou Diretor ou Coordenador
    #     E que o indicador existe
    #     E que o aluno existe
    #     Quando eu selecionar o indicador
    #     E eu selecionar o aluno
    #     Então será exibido um gráfico com a série temporal do indicador para o aluno

    # Cenário: Diretor ou Coordenador acompanha indicador de sala
    #     Dado que eu sou Diretor ou Coordenador
    #     E que o indicador existe
    #     E que a sala existe
    #     Quando eu selecionar o indicador
    #     E eu selecionar a sala a ser acompanhada
    #     Então será exibido um gráfico com a série temporal do indicador para a sala

    # Cenário: Diretor ou Coordenador acompanha indicador de série
    #     Dado que eu sou Diretor ou Coordenador
    #     E que o indicador existe
    #     E que a série existe
    #     Quando eu selecionar o indicador
    #     E eu selecionar a série a ser acompanhada
    #     Então será exibido um gráfico com a série temporal do indicador para a série
