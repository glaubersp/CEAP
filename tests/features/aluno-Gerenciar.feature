# language: pt
@aluno
Funcionalidade: Secretária gerencia dados de aluno
  Como uma Secretária
  Eu quero gerenciar os dados de um aluno
  Para que o aluno exista nos registros do sistema

  Contexto:
    Dado que eu tenho permissão de criação de aluno
    E que eu tenho permissão de listagem de alunos

  Cenário: Secretária cadastra aluno ingressante
    Dado que eu sou Secretária
    E que o nome do aluno não existe no sistema
    Quando eu cadastrar o nome do aluno
    E eu cadastrar a data de nascimento do aluno
    E eu cadastrar o código de matrícula do aluno
    E eu cadastrar a série em que ele será matrículado
    E eu cadastrar a sala em que ele será matrículado
    E eu cadastrar o ano para o qual ele será matrículado
    Então os dados do aluno estarão no banco de dados de alunos
    E o aluno estará matrículado

Cenário: Secretária matricula aluno já cadastrado
    Dado que eu sou Secretária
    E que o aluno existe no sistema
    Quando eu selecionar o aluno
    E eu cadastrar a série em que ele está matriculado
    E eu cadastrar a sala em que ele será matrículado
    E eu cadastrar o ano para o qual ele será matrículado
    Então o aluno estará matriculado
