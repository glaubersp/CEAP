# language: pt
Funcionalidade: Definir metas para a escola
  Como Coordenador ou Diretor
  Eu quero definir uma meta a ser atingida por um indicador
  Para que eu possa acompanhar o efeito da execução de ações sobre o desempenho do indicador

  Contexto:
    Dado que eu tenho permissão de criação de metas
    E que eu tenho permissão de visualização de metas

  Cenário: (Diretor | Coordenador) define nova meta
    Dado que a meta não exista
    E que eu seja (Coordenador | Diretor)
    Quando eu definir o nome da meta
    E associar um indicador a ser acompanhado
    E definir o valor a ser atingido pelo indicador
    E definir o prazo para a reavaliação da meta
    Então eu terei definido uma nova meta para um indicador
