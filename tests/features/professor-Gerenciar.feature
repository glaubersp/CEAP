# language: pt
@professor
Funcionalidade: Secretária gerencia dados do professor
  Como uma Secretária
  Eu quero gerenciar os dados de um professor
  Para que o professor exista nos registros do sistema

  Contexto:
    Dado que eu tenho permissão de criação de professor
    E que eu tenho permissão de listagem de professores

  Cenário: Secretária cadastra professor ingressante
    Dado que eu sou Secretária
    E que o professor não existe no sistema
    Quando eu cadastrar o nome do professor
    E eu cadastrar a data de ingresso na escola
    E eu cadastrar se o professor é efetivo
    Então os dados do professor estarão no banco de dados de professores
