# language: pt
Funcionalidade: Listas as metas cadastradas para a escola
  Como Coordenador ou Diretor
  Eu quero listar as metas cadastradas
  Para que eu possa acompanhar o efeito da execução de cada meta

  Contexto:
    Dado que eu tenho permissão de visualização de metas

  Cenário: (Diretor | Coordenador) lista metas existentes
    Dado que eu seja (Coordenador | Diretor)
    Quando eu selecionar visualizar as metas existentes
    Então eu verei uma lista com as metas atualmente cadastradas
    E o indicador associado à meta
    E o período previsto para reavaliação de cada meta
    E o valor atual do indicador associado
    E a meta a ser atingida para o indicador
