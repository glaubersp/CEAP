# language: pt
Funcionalidade: Avaliar a execução de ações
  Como um Diretor ou Coordenador
  Eu quero avaliar a execução das ações
  Para que eu possa acompanhar a execução da mesma e realocar a ação para outras pessoas.

  Contexto:
    Dado que eu tenho permissão de edição de ação
    E que eu tenho permissão de listagem de ações
    E que eu tenho permissão de visualiação de ação

  Cenário: Avaliar a execução de ações
    Dado que eu sou (Diretor | Coordenador)
    E que eu listei as ações existentes
    Quando eu escolher avaliar uma ação
    Então eu poderei escolher entre 3 cores para indicar o progresso da ação
    Quando eu escolher a cor associada
    Então eu terei avaliado a ação
