# language: pt
@indicador
Funcionalidade: Criar novo indicador
    Como Diretor
    Eu preciso criar um novo indicador
    Assim poderei acompanhar a série temporal deste indicador através de gráficos

    Contexto:
        Dado que eu tenho permissão de criação de indicador

    Esquema do Cenário: Diretor cria novo indicador
        Dado que eu sou Diretor
        E que o nome do indicador não existe no sistema
        Quando eu cadastrar o indicador chamado <indicador>
        E eu cadastrar a periodicidade de acompanhamento como sendo <periodicidade>
        E eu cadastrar o início do acompanhamento para <início>
        E eu cadastrar que indicador avalia <avaliados>
        Então os dados serão adicionados na tabela de indicadores do banco de dados

        Exemplos:
            | indicador | periodicidade | início | avaliados |
            | distorção idade-série | bimestral | bimestre1 | alunos |
            | faltas | bimestral |  bimestre1 | alunos |
            | rendimento | bimestral |  bimestre1 | alunos |
            | indisciplina | bimestral |  bimestre1 | alunos |
            | faltas | mensal |  mês1 | professores |
            | SARESP | anual |  ano1 | escola |
            | IDESP | anual |  ano1 | escola |
            | IDEB | bianual |  ano1 | escola |