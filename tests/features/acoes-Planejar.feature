# language: pt
Funcionalidade: Planejar ações para a escola
  Como um Diretor ou Coordenador
  Eu quero planejar uma ação associada a uma meta
  Para que eu possa acompanhar o desenvolvimento da ação ao longo do tempo

  Contexto:
    Dado que eu tenho permissão de edição de ação
    E que eu tenho permissão de listagem de ações
    E que eu tenho permissão de visualiação de ação

  Cenário: (Diretor | Coordenador) planeja nova ação
    Dado que eu sou (Diretor | Coordenador)
    E que a ação não exista
    Quando eu definir o nome da ação
    E escolher a meta associada
    E definir os responsáveis pela ação
    E definir o início previsto para a execução da ação
    E definir o término previsto para a execução da ação
    E definir, opcionalmente, o local de execução da ação
    E definir, opcionalmente, o custo envolvido na execução da ação
    Então eu terei cadastrado uma nova ação
    E os envolvidos serão avisados da nova ação cadatrada
