# language: pt
@indicador
Funcionalidade: Registrar valor para indicador
    Como (Diretor | Coordenador)
    Eu preciso registrar o valor de um indicador para um dado período de tempo
    Assim poderei acompanhar a série temporal do indicador cadastrado através de gráficos

    Contexto:
        Dado que eu tenho permissão de escrita de indicador
        E que eu tenho permissão de leitura de indicador

    Esquema do Cenário: Inserção de valor de indicador de aluno
        Dado que eu sou (Diretor | Coordenador)
        Quando eu escolher o nome do <indicador>
        E informar o <aluno> relacionado
        E informar o <período> de tempo sendo avaliado
        E informar o <valor> medido
        Então os dados serão adicionados na tabela de indicadores do banco de dados

        Exemplos:
            | indicador | aluno | período | valor |
            | distorção idade-série | aluno1 | bimestre | anos |
            | faltas | aluno1 | bimestre | quantidade |
            | rendimento | aluno1 | bimestre | nota |
            | indisciplina | aluno1 | bimestre | quantidade |

    Esquema do Cenário: Inserção de valor de indicador de escola
        Dado que eu sou (Diretor | Coordenador)
        Quando eu escolher o nome do <indicador>
        E informar o <período> de tempo sendo avaliado
        E informar o <valor> medido
        Então os dados serão adicionados na tabela de indicadores do banco de dados

        Exemplos:
            | indicador | período | valor |
            | Habilidades Acadêmicas - Expectativas Claras | ano-semestre | percentual |
            | Habilidades Acadêmicas - Habilidades | ano-semestre | percentual |
            | Habilidades Acadêmicas - Reconhecimento | ano-semestre | percentual |
            | Habilidades Acadêmicas - Relacionamento | ano-semestre | percentual |
            | Habilidades Interpessoais - Expectativas Claras | ano-semestre | percentual |
            | Habilidades Interpessoais - Habilidades | ano-semestre | percentual |
            | Habilidades Interpessoais - Reconhecimento | ano-semestre | percentual |
            | Habilidades Interpessoais - Relacionamento | ano-semestre | percentual |
            | Instrução - Expectativas Claras | ano-semestre | percentual |
            | Instrução - Habilidades | ano-semestre | percentual |
            | Instrução - Reconhecimento | ano-semestre | percentual |
            | Instrução - Relacionamento | ano-semestre | percentual |
            | Colaboração - Expectativas Claras | ano-semestre | percentual |
            | Colaboração - Habilidades | ano-semestre | percentual |
            | Colaboração - Reconhecimento | ano-semestre | percentual |
            | Colaboração - Relacionamento | ano-semestre | percentual |
            | Envolvimento de Pais/Responsáveis - Expectativas Claras | ano-semestre | percentual |
            | Envolvimento de Pais/Responsáveis - Habilidades | ano-semestre | percentual |
            | Envolvimento de Pais/Responsáveis - Reconhecimento | ano-semestre | percentual |
            | Envolvimento de Pais/Responsáveis - Relacionamento | ano-semestre | percentual |
            | Apoio da Comunidade - Expectativas Claras | ano-semestre | percentual |
            | Apoio da Comunidade - Habilidades | ano-semestre | percentual |
            | Apoio da Comunidade - Reconhecimento | ano-semestre | percentual |
            | Apoio da Comunidade - Relacionamento | ano-semestre | percentual |

    Esquema do Cenário: Inserção de valor de indicador de professor
        Dado que eu sou Diretor
        Quando eu escolher o nome do <indicador>
        E informar o <professor> relacionado
        E informar o <período> de tempo sendo avaliado
        E informar o <valor> medido
        Então os dados serão adicionados na tabela de indicadores do banco de dados

        Exemplos:
            | indicador | professor | período | valor |
            | faltas | professor1 | mês | quantidade |
