# language: pt
Funcionalidade: Exemplo
  Como um [papel]
  Eu quero [funcionalidade]
  Para que [benefício]

  Contexto:
    Dado there is agent A
    E there is agent B

  Cenário: Erasing agent memory
    Dado there is agent J
    E there is agent K
    Quando I erase agent K's memory
    Então there should be agent J
    Mas there should not be agent K

  Esquema do Cenário: Erasing other agents' memory
    Dado there is agent <agent1>
    E there is agent <agent2>
    Quando I erase agent <agent2>'s memory
    Então there should be agent <agent1>
    Mas there should not be agent <agent2>

    Cenários:
      | agent1 | agent2 |
      | D      | M      |
