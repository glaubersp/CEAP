# language: pt
Funcionalidade: Listar Combinados
  Como usuário,
  Eu quero listar todos os combinados,
  Assim eu consigo encontrar a informação que eu preciso.

  Cenário: Filtro de Combinado
    Dado que o usuário tenha permissão de "Leitura de Combinado"
    Quando eu adicionar um título no campo "Título"
      E selecionar o assunto relacionado no campo "Assuntos Relacionados"
      E escolher as pessoas envolvidas no campo "Pessoas Envolvidas"
    Então eu consigo filtrar as informações apertando o botão "Filtrar"
      E consigo visualizar a lista filtrada.

    Cenário: Limpar Filtro de Combinado
    Dado que o usuário tenha acesso a essa opção
    Quando apertar o botão "Limpar"
    Então os campos são limpos
      E consigo visualizar a lista inteira.

    Cenário: Exibição de um combinado
    Dado que o usuário tenha acesso a essa opção
    Quando seleciono um combinado na listagem
    Então eu sou direcionado para a tela "Exibição de Combinado"
      E consigo visualizar todas as informações do combinado.

    Cenário: Botão "Voltar" na tela "Exibição de Combinado"
    Dado que o usuário tenha acesso a essa opção
    Quando seleciono um combinado na listagem
    Então eu sou direcionado para a tela "Exibição de Combinado"
      E consigo visualizar todas as informações do combinado.
