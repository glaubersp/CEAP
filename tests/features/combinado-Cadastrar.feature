# language: pt
Funcionalidade: Cadastro de Combinado
  Como usuário,
  Eu quero cadastrar um combinado,
  Assim todos os usuários poderão visualizar essa informação.

  Cenário: Adicionar Cadastro de Combinado
    Dado que o usuário tenha permissão de "Leitura de Combinado"
    Quando eu adicionar um título no campo "Título"
      E descrever todas as ações no campo "Descrição das ações"
      E adicionar todos os assuntos no campo "Assuntos Relacionados"
      E adicionar todas as pessoas no campo "Pessoas Envolvidas"
      E escolher um período de execução no campo "Periodicidade da Execução"
      E adicionar uma forma de avaliação no campo "Forma de Avaliação/Acompanhamento"
    Então eu consigo gravar todas as informações apertando o botão "Confirmar"
      E aparece uma mensagem "Combinado gravado com sucesso!".

    Cenário: Cancelar Cadastro de Combinado
    Dado que o usuário tenha acesso a essa opção
    Quando apertar o botão "Cancelar"
    Então os campos são limpos
      E eu sou direcionado para a tela inicial do sistema.
