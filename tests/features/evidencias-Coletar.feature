# language: pt
Funcionalidade: Coletar evidências de execução de ações
  Como um Diretor ou Coordenador ou Professor
  Eu quero coletar evidências de execução de uma ação
  Para que eu possa documentar e acompanhar a execução da ação

  Contexto:
    Dado que eu tenho permissão de listagem de ações
    E que eu tenho permissão de visualiação de ação

  Cenário: Coletar evidência de execução de ações
    Dado que eu sou (Diretor | Coordenador | Professor)
    Quando eu selecionar uma meta
    E selecionar uma ação
    E adicionar um documento
    Então o item será listado como evidência
    Quando eu adicionar uma foto
    Então o item será listado como evidência
    Quando eu adicionar um vídeo
    Então o item será listado como evidência
    Quando eu adicionar um arquivo de áudio
    Então o item será listado como evidência
    Quando eu confirmar a coleta de evidência
    Então os itens listados como evidências serão associados à ação no banco de dados
