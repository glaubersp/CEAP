# language: pt
Funcionalidade: Listar ações para a escola
  Como um Diretor ou Coordenador ou Professor
  Eu quero listar as ações cadastradas
  Para que eu possa acompanhar cada ação individualmente

  Contexto:
    Dado que eu tenho permissão de listagem de ações
    E que eu tenho permissão de visualiação de ação

  Cenário: (Diretor | Coordenador | Professor) lista ações existentes
    Dado que eu sou (Diretor | Coordenador | Professor)
    Quando eu solicitar a lista de ações existentes
    Então eu verei a lista de ações existentes
    E a respectiva meta associada
    E a lista de responsáveis envolvidos
    E a data de início da ação
    E a data de término da ação
    E a situação atual da execução da ação
