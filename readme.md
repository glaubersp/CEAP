# Sistema para acompanhamento de ações escolares (SAAE)

O [Projeto CEAP](https://ceap.nied.unicamp.br) tem como objetivo implementar e investigar uma metodologia para a melhoria da escola pública.

Dentro deste contexto, o presente sistema, desenvolvido através de metodologia participativa com agentes escolares, busca suprir a necessidade de uma ferrramente que auxilie o acompanhamento das ações de melhoria desenvolvidas nas escolas públicas.

## Frameworks utilizados

O desenvolvimento desta ferramenta utiliza o framework Laravel, tendo como base o projeto [Laravel Boilerplate](http://laravel-boilerplate.com/).

As versões dos frameworks utilizados encontram-se no arquivo `composer.json`.

## Requisitos

Além dos requisitos originais do framework [Laravel 5.4](https://laravel.com/docs/5.4), os seguintes requisitos adicionais são necessários:

- PHP 7
- NodeJS 8

## Instalação

Para instalar o SAAE, basta executar o comando abaixo em uma pasta no diretório do servidor Web.

    composer create-project ceap/saae --stability=dev <NOME_DA_PASTA>
    cd <NOME_DA_PASTA>
    composer install
    npm install
    
   
Em seguida, edite o arquivo `.env` com suas configurações do banco de dados:

    DB_CONNECTION=mysql
    DB_HOST=127.0.0.1
    DB_PORT=3306
    DB_DATABASE=homestead
    DB_USERNAME=homestead
    DB_PASSWORD=secret

Em seguida, execute:

    php artisan key:generate
    php artisan migrate 


##Desenvolvimento

As atualizações do repositório do Laravel Boilerplate são adicionadas ao repositório local pelos comandos abaixo: 

### Adição do repositório do Laravel Boilerplate

    git remote add laravel-5-boilerplate https://github.com/rappasoft/laravel-5-boilerplate.git
    git subtree add --squash laravel-5-boilerplate `<TAG>`

## Atualização do repositório do Laravel Boilerplate

    git subtree pull --squash laravel-5-boilerplate <TAG>

**Obs:** Substituir `<TAG>` por `master` ou a tag de versão desejada.
