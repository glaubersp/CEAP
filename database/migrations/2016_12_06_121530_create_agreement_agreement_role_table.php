<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAgreementAgreementRoleTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('agreement_agreement_role', function (Blueprint $table) {
            $table->increments('id')->unsigned();

            $table->integer('agreement_id')->unsigned();
            $table->integer('agreement_role_id')->unsigned();

            $table->timestamps();

            $table->foreign('agreement_id')
                ->references('id')->on('agreements')
                ->onDelete('cascade')
                ->onUpdate('cascade');
            $table->foreign('agreement_role_id')
                ->references('id')->on('agreement_roles')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('agreement_agreement_role');
    }
}
