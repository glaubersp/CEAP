<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateEvaluationCyclesTable extends Migration
{
    /**
     * Run the migrations.
     * @table evaluation_cycles
     *
     * @return void
     */
    public function up()
    {
        Schema::create('evaluation_cycles', function (Blueprint $table) {
            $table->increments('id')->unsigned();

            $table->string('name')->unique();
            $table->integer('days');

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('evaluation_cycles');
    }
}
