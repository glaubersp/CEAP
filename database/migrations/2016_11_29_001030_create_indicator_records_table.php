<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateIndicatorRecordsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('indicator_records', function (Blueprint $table) {
            $table->increments('id')->unsigned();

            $table->decimal('value');
            $table->date('date');

            $table->integer('indicator_id')->unsigned();
            $table->integer('school_class_id')->unsigned()->nullable();
            $table->enum('school_grade_group', ['elementary_school', 'middle_school', 'high_school'])->nullable();

            $table->timestamps();
            $table->softDeletes();

            $table->foreign('indicator_id')
                ->references('id')->on('indicators')
                ->onDelete('cascade')
                ->onUpdate('cascade');
            $table->foreign('school_class_id')
                ->references('id')->on('school_classes')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('indicator_records');
    }
}
