<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateACtionAgreementTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('action_agreement', function (Blueprint $table) {
            $table->increments('id')->unsigned();

            $table->integer('action_id')->unsigned();
            $table->integer('agreement_id')->unsigned();

            $table->timestamps();

            $table->foreign('action_id')
                ->references('id')->on('actions')
                ->onDelete('cascade')
                ->onUpdate('cascade');
            $table->foreign('agreement_id')
                ->references('id')->on('agreements')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('action_agreement');
    }
}
