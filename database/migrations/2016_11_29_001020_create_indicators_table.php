<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateIndicatorsTable extends Migration
{
    /**
     * Run the migrations.
     * @table indicators
     *
     * @return void
     */
    public function up()
    {
        Schema::create('indicators', function (Blueprint $table) {
            $table->increments('id')->unsigned();

            $table->string('name')->unique();
            $table->longText('description');

            $table->integer('indicator_group_id')->unsigned();
            $table->integer('evaluation_category_id')->unsigned()->nullable();

            $table->timestamps();
            $table->softDeletes();

            $table->foreign('indicator_group_id')
                ->references('id')->on('indicator_groups')
                ->onDelete('cascade')
                ->onUpdate('cascade');
            $table->foreign('evaluation_category_id')
                ->references('id')->on('evaluation_categories')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('indicators');
    }
}
