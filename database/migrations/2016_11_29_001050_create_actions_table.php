<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateActionsTable extends Migration
{
    /**
     * Run the migrations.
     * @table actions
     *
     * @return void
     */
    public function up()
    {
        Schema::create('actions', function (Blueprint $table) {
            $table->increments('id')->unsigned()->unique();

            $table->string('name');
            $table->longText('details');
            $table->enum('priority', ['Unset', 'Low', 'Medium', 'High']);
	        $table->date('start_date');
	        $table->date('end_date');
            $table->integer('evaluation_term_id')->unsigned();
            $table->integer('goal_id')->unsigned();
            $table->integer('parent_id')->unsigned()->nullable();

            $table->timestamps();
            $table->softDeletes();

	        $table->foreign('evaluation_term_id')
	              ->references('id')->on('evaluation_terms')
	              ->onDelete('cascade')
	              ->onUpdate('cascade');

            $table->foreign('goal_id')
                ->references('id')->on('goals')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            $table->foreign('parent_id')
                ->references('id')->on('actions')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            $table->unique(['name', 'evaluation_term_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('actions');
    }
}
