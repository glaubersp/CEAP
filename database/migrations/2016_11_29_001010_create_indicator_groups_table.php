<?php

use App\Models\CEAP\IndicatorGroup;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateIndicatorGroupsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('indicator_groups', function (Blueprint $table) {
            $table->increments('id')->unsigned();

            $table->string('name')->unique();
            $table->text('description');

            $table->enum('type', [IndicatorGroup::CEAP, IndicatorGroup::SCHOOL, IndicatorGroup::ROOMS]);

            $table->integer('evaluation_cycle_id')->unsigned();
            $table->date('next_evaluation_date');

            $table->timestamps();
            $table->softDeletes();

            $table->foreign('evaluation_cycle_id')
                ->references('id')->on('evaluation_cycles')
                ->onDelete('cascade')
                ->onUpdate('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('indicator_groups');
    }
}
