<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGoalEvaluationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('goal_evaluations', function (Blueprint $table) {
            $table->increments('id')->unsigned();

            $table->integer('rating');
            $table->enum('evaluation',
                ['Unset', 'Not implemented', 'Not working', 'Partially working', 'Fully working']);
            $table->date('evaluation_date');
            $table->longText('justification');

            $table->timestamps();
            $table->softDeletes();

            $table->integer('user_id')->unsigned();
            $table->integer('goal_id')->unsigned();

            $table->foreign('user_id')
                ->references('id')->on('users')
                ->onDelete('cascade')
                ->onUpdate('cascade');
            $table->foreign('goal_id')
                ->references('id')->on('goals')
                ->onDelete('cascade')
                ->onUpdate('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('goal_evaluations');
    }
}
