<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAgreementTopicTagTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('agreement_topic_tag', function (Blueprint $table) {
            $table->increments('id')->unsigned();

            $table->integer('agreement_id')->unsigned();
            $table->integer('topic_tag_id')->unsigned();

            $table->timestamps();

            $table->foreign('agreement_id')
                ->references('id')->on('agreements')
                ->onDelete('cascade')
                ->onUpdate('cascade');
            $table->foreign('topic_tag_id')
                ->references('id')->on('topic_tags')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('agreement_topic_tag');
    }
}
