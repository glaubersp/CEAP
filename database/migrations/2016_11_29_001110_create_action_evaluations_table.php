<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateActionEvaluationsTable extends Migration
{
    /**
     * Run the migrations.
     * @table action_user
     *
     * @return void
     */
    public function up()
    {
        Schema::create('action_evaluations', function (Blueprint $table) {
            $table->increments('id')->unsigned();

            $table->enum('evaluation',
                ['Unset', 'Not implemented', 'Partially implemented', 'Fully implemented']);
            $table->integer('rating')->nullable();
            $table->longText('justification');
            $table->date('evaluation_date');

            $table->integer('user_id')->unsigned();
            $table->integer('action_id')->unsigned();

            $table->timestamps();
            $table->softDeletes();

            $table->foreign('user_id')
                ->references('id')->on('users')
                ->onDelete('cascade')
                ->onUpdate('cascade');
            $table->foreign('action_id')
                ->references('id')->on('actions')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('action_evaluations');
    }
}
