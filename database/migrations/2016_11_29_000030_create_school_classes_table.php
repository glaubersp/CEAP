<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSchoolClassesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('school_classes', function (Blueprint $table) {
            $table->increments('id')->unsigned();

            $table->unsignedInteger('year');
            $table->unsignedSmallInteger('grade');
            $table->char('room');
            $table->enum('school_grade_group', ['elementary_school', 'middle_school', 'high_school']);

            $table->unique(['year', 'grade', 'room']);

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('school_classes');
    }
}
