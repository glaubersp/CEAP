<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAgreementsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create('agreements', function (Blueprint $table) {
            $table->increments('id')->unsigned();

            $table->string('title')->unique();
            $table->longText('details');
            $table->integer('agreement_monitoring_strategy_id')->unsigned();
            $table->integer('evaluation_cycle_id')->unsigned();

            $table->timestamps();
            $table->softDeletes();

            $table->foreign('agreement_monitoring_strategy_id')
                ->references('id')->on('agreement_monitoring_strategies')
                ->onDelete('cascade')
                ->onUpdate('cascade');
            $table->foreign('evaluation_cycle_id')
                ->references('id')->on('evaluation_cycles')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('agreements');
    }
}
