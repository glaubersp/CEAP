<?php

use Illuminate\Database\Seeder;

/**
 * Class CEAPSeeder
 */
class CEAPSeeder extends Seeder
{
    use DisableForeignKeys;

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->disableForeignKeys();

        $this->call(EvaluationCategoriesSeeder::class);
        $this->call(EvaluationCyclesSeeder::class);
        $this->call(SchoolClassesTableSeeder::class);
        $this->call(IndicatorGroupsAndIndicatorsTableSeeder::class);
        $this->call(IndicatorRecordsTableSeeder::class);
        $this->call(TopicTagsTableSeeder::class);
        $this->call(AgreementMonitoringStrategiesTableSeeder::class);
        $this->call(AgreementRolesTableSeeder::class);
	    $this->call(EvaluationTermsTableSeeder::class);
	    $this->call(FAAgreementsTableSeeder::class);
        $this->call(FAGoalsAndActionsTableSeeder::class);

        $this->enableForeignKeys();
    }
}