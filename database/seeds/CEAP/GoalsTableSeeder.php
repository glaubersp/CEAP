<?php

use App\Models\CEAP\Goal;
use App\Models\CEAP\Indicator;
use Illuminate\Database\Seeder;

class GoalsTableSeeder extends Seeder
{
    use DisableForeignKeys, TruncateTable;

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $table_name = 'goals';

        $this->disableForeignKeys();
        $this->truncate($table_name);

        $indicators = Indicator::all();
        foreach ($indicators as $indicator) {
            $goal = new Goal();
            $goal->name = 'Melhorar em 10% o indicador ' . $indicator->name;
            $goal->indicator()->associate($indicator);
            $goal->save();
        }

        $this->enableForeignKeys();
    }
}
