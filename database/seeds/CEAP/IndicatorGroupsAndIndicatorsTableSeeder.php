<?php

use App\Models\CEAP\Indicator;
use App\Models\CEAP\IndicatorGroup;
use Carbon\Carbon;
use Illuminate\Database\Seeder;

class IndicatorGroupsAndIndicatorsTableSeeder extends Seeder
{
    use DisableForeignKeys, TruncateTable;

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $this->disableForeignKeys();

        $this->truncate('indicator_groups');
        $this->truncate('indicators');

        $indicator_group = new IndicatorGroup;
        $indicator_group->name = 'Indicadores CEAP';
        $indicator_group->description = 'Indicadores associados ao questionário TetraAnalytics utilizado pelo projeto.';
        $indicator_group->type = 'ceap';
        $indicator_group->evaluation_cycle_id = 6;
        $indicator_group->next_evaluation_date = Carbon::now()->addMonths(6);
        $indicator_group->save();

        $indicator_names = [
            [
                "Habilidades Acadêmicas - Expectativas Claras",
                "Habilidades Acadêmicas - Habilidades",
                "Habilidades Acadêmicas - Reconhecimento",
                "Habilidades Acadêmicas - Relacionamento",

                "Habilidades Interpessoais - Expectativas Claras",
                "Habilidades Interpessoais - Habilidades",
                "Habilidades Interpessoais - Reconhecimento",
                "Habilidades Interpessoais - Relacionamento",
            ],

            [
                "Instrução - Expectativas Claras",
                "Instrução - Habilidades",
                "Instrução - Reconhecimento",
                "Instrução - Relacionamento",

                "Colaboração - Expectativas Claras",
                "Colaboração - Habilidades",
                "Colaboração - Reconhecimento",
                "Colaboração - Relacionamento",
            ],

            [
                "Envolvimento dos Pais/Responsáveis - Expectativas Claras",
                "Envolvimento dos Pais/Responsáveis - Habilidades",
                "Envolvimento dos Pais/Responsáveis - Reconhecimento",
                "Envolvimento dos Pais/Responsáveis - Relacionamento",

                "Apoio da Comunidade - Expectativas Claras",
                "Apoio da Comunidade - Habilidades",
                "Apoio da Comunidade - Reconhecimento",
                "Apoio da Comunidade - Relacionamento",
            ],
        ];

        $indicator_description = [
            [
                "<p>Expectativas claras abrem caminho para o desenvolvimento das habilidades acadêmicas. Quando os alunos relatam confusão, é comum que não participem das atividades e tarefas. É importante garantir que os alunos tenham tanto o conhecimento básico necessário para que possam engajar nas tarefas, bem como clareza no seu entendimento das orientações, seja de forma escrita ou falada.</p>",
                "<p>Habilidades acadêmicas são construídas quando providenciamos muitas oportunidades para a prática, com retorno(feedback) que seja específico e dado no momento certo. Tendo em vista que a alfabetização é aplicada em qualquer área do conhecimento, a leitura é um bom indicador de habilidades acadêmicas.</p>",
                "<p>Reconhecer os esforços de alunos na medida em que tentam atingir as expectativas é essencial para que haja progresso contínuo. Professores devem, de maneira diligente, reconhecer os esforços que vão em direção aos objetivos traçados.</p>",
                "<p>Relacionamentos entre professores e alunos que são baseados em confiança e estima ajudam a fazer com que as expectativas sejam claras, e dão maior valor ao retorno(feedback). Um relacionamento positivo gera uma percepção de valor para cada tarefa e atividade.</p>",

                "<p>Expectativas claras quanto ao comportamento definem o contexto para o desenvolvimento de habilidades interpessoais. Quando alunos relatam confusão, eles usualmente não tem as habilidades necessárias para criar e manter relacionamentos duradouros com seus colegas. Cada escola deve ter a expectativa de que cada aluno tenha ao menos alguns bons amigos.</p>",
                "<p>Habilidades interpessoais são criadas ao se providenciar oportunidades o suficiente para a prática dessas habilidades com retorno(feedback) específico e dado no momento certo. Não assuma que essas habilidades se desenvolvam naturalmente. O resultado do desenvolvimento de habilidades interpessoais é um bom número de amigos na escola.</p>",
                "<p>A maioria dos alunos têm prazer em vir para a escola. Alguns alunos não gostam, mas mesmo estes podem olhar favoravelmente para a escola quando professores criam oportunidades para que os alunos encorajem uns aos outros.</p>",
                "<p>Relacionamentos entre alunos, construídos com base em confiança e respeito, criam uma sensação de segurança para todos os alunos. Quando essas relações de qualidade estão ausentes ou são fracas, os alunos não se sentem seguros na escola.</p>",
            ],

            [
                "<p>Professores podem sentir que não tem as condições que precisam para oferecer o apoio necessário a todos os seus alunos. Gestores devem deixar claro que a responsabilidade pelo aprendizado não é somente do aluno e deve ser compartilhada entre alunos e professores.</p>",
                "<p>Aponta o nível de apoio necessário para que professores possam minimizar o quanto alunos atrapalham as aulas. Professores que conseguem manter alunos interessados e engajados conseguem providenciar oportunidades de aprendizado que são mais efetivas, e vão ao encontro das expectativas.</p>",
                "<p>Esse item é uma indicação da proporção entre interações positivas e negativas entre professores e gestores. Interações positivas regulares criam um contexto para atividades de orientação, apoio e liderança pedagógica.</p>",
                "<p>Conexões pessoais significativas são essenciais para criar comunidades de aprendizado. Reconhecer a contribuição de professores e funcionários para a escola e encontrar caminhos para que cada professor e funcionário atinja objetivos profissionais é uma das principais funções de um gestor.</p>",

                "<p>Estabelecer momentos para a colaboração não é o suficiente. Muitas vezes, expectativas e regras para a colaboração não são explícitas. Gestores devem providenciar recomendações e critérios claros para os processos e resultados perados na condução de atividades colaborativas. Isso ajudará professores a traduzir as atividades de colaboração em melhorias mensuráveis em seu trabalho com os alunos.</p>",
                "<p>Colaboração efetiva precisa ser fomentada. Não assuma que professores podem desenvolver habilidades de colaboração em isolamento. Gestores devem providenciar tempo específico e formação para que a colaboração possa contribuir para os objetivos definidos pela escola.</p>",
                "<p>Esse item é uma indicação da proporção entre interações positivas e negativas entre professores. Interações positivas criam um contexto para colaboração e possibilidades de orientação.</p>",
                "<p>Quando professores conseguem criar amizades com colegas na escola, eles têm vontade de vir à escola. Esse item é um indicador da qualidade desses relacionamentos.</p>",
            ],

            [
                "<p>Pais e responsáveis querem o melhor para seus filhos, mas muitas vezes não entendem como devem trabalhar com a escola para atingir objetivos comuns. Quando pais entendem as expectativas que a escola tem quanto a sua participação, o envolvimento dos pais e responsáveis tem maior propósito, e como consequência, a performance dos alunos tem melhores chances de avanço.</p>",
                "<p>O desenvolvimento de habilidades requer retorno específico e dado no momento correto. Pais e responsáveis precisam de informação adequada sobre a performance de seus filhos para que possam contribuir no retorno(feedback) aos seus filhos.</p>",
                "<p>Esse item é uma indicação da proporção entre interações positivas e negativas entre professores e pais / responsáveis. Interações positivas criam um contexto para o envolvimento produtivo dos pais e responsáveis.</p>",
                "<p>Participação dos pais e responsáveis requer interações significativas com a escola. Essa percepção pode ser fomentada quando pais recebem comunicações da escola, buscam informação por telefone ou outros meios e vistam ou participam de atividades realizadas na escola.</p>",

                "<p>O entendimento comum leva a expectativas claras. Uma maneira de se atingir o entendimento comum é buscar consenso quanto às prioridades e regras da escola, através de oportunidades para que essa construção seja feita de maneira aberta e colaborativa.</p>",
                "<p>O apoio da comunidade é indicado pela participação. Pais, responsáveis e familiares contribuem para o sucesso da escola quando se envolvem com outros membros da comunidade no planejamento, promoção, coordenação e apoio às atividades escolares.</p>",
                "<p>Esse item é uma indicação da proporção entre interações positivas e negativas entre pais / responsáveis. Interações positivas criam o contexto para apoio da comunidade.</p>",
                "<p>O apoio da comunidade às atividades organizadas pela escola, como reuniões de pais, eventos, entre outros são uma medida de quanto a escola integra - se a comunidade que serve. Participação nas atividades escolares é um indicador apropriado da qualidade dos relacionamentos entre a comunidade e escola.</p>",
            ],
        ];

        $indicator_obj = new Indicator;
        for ($cat = 2; $cat <= 4; $cat++) {

            /** @var  string[][] $indicator_names */
            foreach ($indicator_names[$cat - 2] as $key => $indicator_name) {
                $indicator = clone $indicator_obj;
                $indicator->name = $indicator_name;
                $indicator->description = $indicator_description[$cat - 2][$key];
                $indicator->evaluation_category_id = $cat; // 2:escola, 3: professores, 4: pais/responsáveis
                $indicator_group->indicators()->save($indicator);
            }
        }

        // ==== Indicador de Salas ====

        $indicator_group_class = new IndicatorGroup;
        $indicator_group_class->name = 'Indicadores de Salas';
        $indicator_group_class->description = 'Grupo para indicadores associados às salas.';
        $indicator_group_class->evaluation_cycle_id = 6;
        $indicator_group_class->type = 'rooms';
        $indicator_group_class->next_evaluation_date = Carbon::now()->addMonths(6);
        $indicator_group_class->save();

        $indicator_class_name = "Faltas";
        $indicator_class_description = "<p>Total de faltas por salas.</p>";

        $indicator_class = new Indicator;
        $indicator_class->name = $indicator_class_name;
        $indicator_class->description = $indicator_class_description;
        $indicator_class->evaluation_category_id = 1; // 1:salas
        $indicator_group_class->indicators()->save($indicator_class);

        $this->enableForeignKeys();
    }
}
