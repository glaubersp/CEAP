<?php

use App\Models\CEAP\Action;
use App\Models\CEAP\Evidence;
use Carbon\Carbon;
use Illuminate\Database\Seeder;

class EvidenceTableSeeder extends Seeder
{
    use DisableForeignKeys, TruncateTable;

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $table_name = 'evidences';

        $this->disableForeignKeys();
        $this->truncate($table_name);

        $model = new Evidence();
        $model->title = 'Título da evidência';
        $model->details = 'Descrição da evidência';
        $model->file_name = 'arquivo.png';
        $model->created_at = Carbon::now();
        $model->updated_at = Carbon::now();

        $action = Action::find(1);
        $model->action()->associate($action);

        $model->save();

        $model = new Evidence();
        $model->title = 'Título da evidência 2';
        $model->details = 'Descrição da evidência 2';
        $model->file_name = 'arquivo2.png';
        $model->created_at = Carbon::now();
        $model->updated_at = Carbon::now();

        $action = Action::find(2);
        $model->action()->associate($action);

        $model->save();

        $this->enableForeignKeys();
    }
}
