<?php

use App\Models\Access\User\User;
use App\Models\CEAP\Action;
use App\Models\CEAP\Indicator;
use Carbon\Carbon;
use Illuminate\Database\Seeder;

class ActionsTableSeeder extends Seeder
{
    use DisableForeignKeys, TruncateTable;

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $table_name = 'actions';

        $this->disableForeignKeys();
        $this->truncate($table_name);

        $action_obj = new Action;
        $action_obj->start_date = Carbon::now();
        $action_obj->end_date = Carbon::now()->addMonth(2);
        $action_obj->created_at = Carbon::now();
        $action_obj->updated_at = Carbon::now();

        $user = User::find(2);
        $goal = Indicator::findByName('Habilidades Interpessoais - Expectativas Claras')->first()->goal;

        $action = clone $action_obj;
        $action->name = 'Organização do espaço físico';
        $action->details = 'Posicionamento de mesas indicando uma fila única.';
        $action->goal()->associate($goal);
        $action->save();
        $action->users()->attach($user);

        $action = clone $action_obj;
        $action->name = 'Definição de regras claras de comportamento para os alunos';
        $action->details = 'Cartaz com regras \"positivas\" (o que se deve fazer, e não o que não se deve fazer).';
        $action->goal()->associate($goal);
        $action->save();
        $action->users()->attach($user);

        $action = clone $action_obj;
        $action->name = 'Identificação do último aluno da fila';
        $action->details = 'Cartaz para o último da fila.';
        $action->goal()->associate($goal);
        $action->save();
        $action->users()->attach($user);

        $action = clone $action_obj;
        $action->name = 'Feedback dos funcionários da cozinha';
        $action->details = 'Discussão com funcionários da cozinha sobre demanda.';
        $action->goal()->associate($goal);
        $action->save();
        $action->users()->attach($user);

        $user = User::find(2);
        $goal = Indicator::findByName('Habilidades Acadêmicas - Relacionamento')->first()->goal;

        $action = clone $action_obj;
        $action->name = 'Retomar professores coordenadores de sala';
        $action->details = 'Eleição no começo de 2017.';
        $action->goal()->associate($goal);
        $action->save();
        $action->users()->attach($user);

        $action = clone $action_obj;
        $action->name = 'Professor Orientador do Grêmio';
        $action->details = 'Eleger um professor para orientar as ações do Grêmio estudantil.';
        $action->goal()->associate($goal);
        $action->save();
        $action->users()->attach($user);

        $user = User::find(2);
        $goal = Indicator::findByName('Habilidades Acadêmicas - Expectativas Claras')->first()->goal;

        $action = clone $action_obj;
        $action->name = 'Transparência no planejamento e metas';
        $action->details = 'Professores devem esclarecer objetivos no começo da aula e explicar planejamento de curto prazo escrevendo na lousa ou colocando papel na parede.';
        $action->goal()->associate($goal);
        $action->save();
        $action->users()->attach($user);

        $action = clone $action_obj;
        $action->name = 'Fortalecer altas perspectivas para o futuro dos alunos (médio->pós-escola)';
        $action->details = 'Chamar ex-alunos, psicólogo da secretaria ou outra forma de apoio para dar palestra sobre opções.';
        $action->goal()->associate($goal);
        $action->save();
        $action->users()->attach($user);

        $action = clone $action_obj;
        $action->name = 'Fortalecer altas perspectivas para o futuro dos alunos entre anos (finais->médio)';
        $action->details = 'Oferecer tempo para discutir as expectativas que diferem de um ciclo para outro.';
        $action->goal()->associate($goal);
        $action->save();
        $action->users()->attach($user);

        $user = User::find(2);
        $goal = Indicator::findByName('Habilidades Acadêmicas - Reconhecimento')->first()->goal;

        $action = clone $action_obj;
        $action->name = 'Fotos das atividades';
        $action->details = 'Postagem de fotos no mural com as atividades/produções dos alunos.';
        $action->goal()->associate($goal);
        $action->save();
        $action->users()->attach($user);

        $user = User::find(2);
        $goal = Indicator::findByName('Instrução - Expectativas Claras')->first()->goal;

        $action = clone $action_obj;
        $action->name = 'Criar Combinados';
        $action->details = 'Criar informativo de combinados para ser entregue a novos professores.';
        $action->goal()->associate($goal);
        $action->save();
        $action->users()->attach($user);

        $user = User::find(2);
        $goal = Indicator::findByName('Apoio da Comunidade - Expectativas Claras')->first()->goal;

        $action = clone $action_obj;
        $action->name = 'Criar lista de emails';
        $action->details = 'Solicitar emails aos pais e criar lista de emails.';
        $action->goal()->associate($goal);
        $action->save();
        $action->users()->attach($user);

        $action = clone $action_obj;
        $action->name = 'Questionar os pais sobre meios para comunicação';
        $action->details = 'Formulário entregue em reunião de pais para definir possíveis canais de comunicação.';
        $action->goal()->associate($goal);
        $action->save();
        $action->users()->attach($user);

        $action = clone $action_obj;
        $action->name = 'Criar uma página web';
        $action->details = 'Página web a ser criada por um dos pais.';
        $action->goal()->associate($goal);
        $action->save();
        $action->users()->attach($user);

        $action = clone $action_obj;
        $action->name = 'Informativo impresso (Jormal da FA)';
        $action->details = 'Colocar na escola, por e-mail, website. Envolver alunos na criação/atividade didática.';
        $action->goal()->associate($goal);
        $action->save();
        $action->users()->attach($user);

        $action = clone $action_obj;
        $action->name = 'Envolver pais nas discussões sobre carreiras com alunos';
        $action->details = 'Palestra no começo do ano para os pais.';
        $action->goal()->associate($goal);
        $action->save();
        $action->users()->attach($user);

        $user = User::find(2);
        $goal = Indicator::findByName('Apoio da Comunidade - Habilidades')->first()->goal;

        $action = clone $action_obj;
        $action->name = 'Festa da Primavera';
        $action->details = 'Executar a festa.';
        $action->goal()->associate($goal);
        $action->save();
        $action->users()->attach($user);

        $action = clone $action_obj;
        $action->name = 'Festa da Primavera - Feedback';
        $action->details = 'Angariar retorno dos participantes através de questionário.';
        $action->goal()->associate($goal);
        $action->save();
        $action->users()->attach($user);

        $action = clone $action_obj;
        $action->name = 'Melhorar a contribuição financeira da APM';
        $action->details = 'Envio de carta para pedido de apoio.';
        $action->goal()->associate($goal);
        $action->save();
        $action->users()->attach($user);

        $action = clone $action_obj;
        $action->name = 'Protesto contra corte de verbas';
        $action->details = 'Pais devem enviar e-mail para secretaria para protestar contra o corte.';
        $action->goal()->associate($goal);
        $action->save();
        $action->users()->attach($user);

        $this->enableForeignKeys();
    }
}
