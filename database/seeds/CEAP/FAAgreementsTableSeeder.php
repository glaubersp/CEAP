<?php

use App\Models\CEAP\Agreement;
use App\Models\CEAP\AgreementMonitoringStrategy;
use App\Models\CEAP\AgreementRole;
use App\Models\CEAP\EvaluationCycle;
use App\Models\CEAP\TopicTag;
use Illuminate\Database\Seeder;

class FAAgreementsTableSeeder extends Seeder
{
    use DisableForeignKeys, TruncateTable;

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $table_name = 'agreements';

        $this->disableForeignKeys();
        $this->truncate($table_name);

        $agreement_model = new Agreement();
        $agreement_model->evaluationCycle()->associate(EvaluationCycle::find(1));
        $agreement_model->agreementMonitoringStrategy()->associate(AgreementMonitoringStrategy::find(1));

        $agreement = clone $agreement_model;
        $agreement->title = 'Compromisso quanto à pontualidade do Horário';
        $agreement->details = '<p>O professor que chegar para a aula com 15 minutos ou mais de atraso não dará aula e ficará com falta.</p>'
            . '<p>O aluno que chegar para a primeira aula atrasado até 10 minutos subirá para a sala de aula mas ficará com falta na aula em questão.</p>'
            . '<p>O aluno que chegar para a primeira aula atrasado 10 minutos ou mais, deverá esperar pelo início da próxima às 7h50.';
        $agreement->save();
        $agreement->topicTags()->attach(TopicTag::find(1));
        foreach ([2, 3, 4, 5] as $id) {
            $role = AgreementRole::find($id);
            $agreement->agreementRoles()->attach($role);
        }

        $agreement = clone $agreement_model;
        $agreement->title = 'Uso do Banheiro';
        $agreement->details = '<p>Ninguém vai no banheiro na primeira aula nem na aula após o intervalo.</p>'
            . '<p>Vai no banheiro um aluno por vez.</p>'
            . '<p>O controle de quem está fora da sala fica com o professor.</p>';
        $agreement->save();
        $agreement->topicTags()->attach(TopicTag::find(8));
        foreach ([1, 5] as $id) {
            $role = AgreementRole::find($id);
            $agreement->agreementRoles()->attach($role);
        }

        $agreement = clone $agreement_model;
        $agreement->title = 'Troca de professores entre os horários de aula';
        $agreement->details = '<p>O sinal só bate na entrada, no intervalo (início e fim) e na saída.</p>'
            . '<p>O professor deve monitorar a duração das suas aulas porque não há sinal entre as aulas.</p>'
            . '<p>O professor da sala mais tranquila deve iniciar a troca de professores na troca de aulas. Após a chegada do professor que entrará para a próxima aula, o professor da aula atual deve liberar a sala o mais rápido possível.</p>';
        $agreement->save();
        $agreement->topicTags()->attach(TopicTag::find(2));
        foreach ([3, 5] as $id) {
            $role = AgreementRole::find($id);
            $agreement->agreementRoles()->attach($role);
        }

        $agreement = clone $agreement_model;
        $agreement->title = 'Uso do Uniforme';
        $agreement->details = '<p>O professor (ou aluno representante, quando estiver estabelecido) anota o nome dos alunos que não estão uniformizados e avisa a secretaria no fim de cada aula.</p>';
        $agreement->save();
        $agreement->topicTags()->attach(TopicTag::find(3));
        foreach ([1, 3, 4, 5] as $id) {
            $role = AgreementRole::find($id);
            $agreement->agreementRoles()->attach($role);
        }

        $agreement = clone $agreement_model;
        $agreement->title = 'Controle de Presença de alunos';
        $agreement->details = '<p>São controladas pela secretaria, mas o professor deve fazer sua chamada em toda a aula na folha designada. No final do período o professor deve entregar a folha para a secretaria diariamente ou deixar na sala de professores para ser recolhida.</p>';
        $agreement->save();
        $agreement->topicTags()->attach(TopicTag::find(9));
        foreach ([1, 3, 5] as $id) {
            $role = AgreementRole::find($id);
            $agreement->agreementRoles()->attach($role);
        }

        $agreement = clone $agreement_model;
        $agreement->title = 'Professor responsável do dia';
        $agreement->details = '<p>Deve cuidar do bom andamento do corredor.</p>'
            . '<p>Auxiliar no retorno do intervalo.</p>'
            . '<p>Estar atendo a qualquer atividade diferente na escola, interagindo com as funcionárias do apoio escolar.</p>'
            . '<p>Auxílio para as funcionárias da secretaria em casos de indisciplina e ocorrências de indisciplina.</p>'
            . '<p>Auxiliar no controle da fila da merenda, podendo sair 5 minutos antes com a própria turma para alocar as turmas.</p>';
        $agreement->save();
        $agreement->topicTags()->attach(TopicTag::find(1));
        foreach ([3, 5] as $id) {
            $role = AgreementRole::find($id);
            $agreement->agreementRoles()->attach($role);
        }

        $agreement = clone $agreement_model;
        $agreement->title = 'Aluno em aula vaga';
        $agreement->details = '<p>O professor não deve aceitar aluno de outra sala assistindo à sua disciplina.</p>'
            . '<p>Informar a secretaria quando um aluno estiver fora da aula dele.</p>';
        $agreement->save();
        $agreement->topicTags()->attach(TopicTag::find(10));
        foreach ([1, 3, 4, 5] as $id) {
            $role = AgreementRole::find($id);
            $agreement->agreementRoles()->attach($role);
        }

        $agreement = clone $agreement_model;
        $agreement->title = 'Materiais';
        $agreement->details = '<p>Temos 1 Datashow móvel, 1 sala com Datashow e 1 sala com TV. '
            . '<p>É possível agendar as turmas para as salas de TV e Datashow, além de agendar o uso do Datashow móvel.</p>'
            . '<p>O agendamento deve ser feito com antecedência de pelo menos 1 dia na agenda na parede da sala de professores.</p>';
        $agreement->save();
        $agreement->topicTags()->attach(TopicTag::find(4));
        foreach ([2, 5] as $id) {
            $role = AgreementRole::find($id);
            $agreement->agreementRoles()->attach($role);
        }

        $agreement = clone $agreement_model;
        $agreement->title = 'Entrega do Livro didático';
        $agreement->details = '<p>Decidir na reunião de início de período se os livros didáticos serão entregues aos alunos ou se ficarão nos armários das salas de aula.</p>';
        $agreement->save();
        $agreement->topicTags()->attach(TopicTag::find(4));
        foreach ([1, 2, 5] as $id) {
            $role = AgreementRole::find($id);
            $agreement->agreementRoles()->attach($role);
        }

        $agreement = clone $agreement_model;
        $agreement->title = 'Caderno do aluno';
        $agreement->details = '<p>Separar, contar e entregar para os alunos os cadernos o mais rápido possível devido à falta de espaço.</p>';
        $agreement->save();
        $agreement->topicTags()->attach(TopicTag::find(4));
        foreach ([1, 2, 5] as $id) {
            $role = AgreementRole::find($id);
            $agreement->agreementRoles()->attach($role);
        }

        $agreement = clone $agreement_model;
        $agreement->title = 'Kit escolar';
        $agreement->details = '<p>Enquanto não houver o kit, os alunos devem ser orientados a trazer material (caderno, caneta e lápis) de casa.</p>';
        $agreement->save();
        $agreement->topicTags()->attach(TopicTag::find(4));
        foreach ([1, 2, 5] as $id) {
            $role = AgreementRole::find($id);
            $agreement->agreementRoles()->attach($role);
        }

        $agreement = clone $agreement_model;
        $agreement->title = 'Uso da bola fora da Educação Física sob supervisão de professor';
        $agreement->details = '<p>Definir com os professores o que fazer.</p>';
        $agreement->save();
        $agreement->topicTags()->attach(TopicTag::find(5));
        foreach ([1, 2, 3, 4, 5] as $id) {
            $role = AgreementRole::find($id);
            $agreement->agreementRoles()->attach($role);
        }

        $agreement = clone $agreement_model;
        $agreement->title = 'Uso da bola fora da Educação Física em aula vaga';
        $agreement->details = '<p>Só temos uma bola para emprestar.</p>'
            . '<p>O aluno deve retirar a bola na secretaria e o mesmo aluno deve devolver a bola no fim do horário vago.</p>'
            . '<p>Caso haja outra aula vaga depois, o aluno deve devolver a bola e ir buscar na outra aula.</p>';
        $agreement->save();
        $agreement->topicTags()->attach(TopicTag::find(5));
        foreach ([1, 2, 3, 4, 5] as $id) {
            $role = AgreementRole::find($id);
            $agreement->agreementRoles()->attach($role);
        }

        $agreement = clone $agreement_model;
        $agreement->title = 'Aulas fora da sala';
        $agreement->details = '<p>O professor pode dar aula no pátio, na sala de computadores, nas mesas do pátio descoberto.</p>'
            . '<p>O professor deve, no entanto, tratar o espaço como espaço de aula, lembrando que outras salas também estão tendo aula.</p>';
        $agreement->save();
        $agreement->topicTags()->attach(TopicTag::find(6));
        foreach ([1, 2, 5] as $id) {
            $role = AgreementRole::find($id);
            $agreement->agreementRoles()->attach($role);
        }

        $agreement = clone $agreement_model;
        $agreement->title = 'Material para aula';
        $agreement->details = '<p>O professor deve levantar o material necessário para a aula com antecedência e retirar o material na secretaria antes do início da aula.</p>'
            . '<p>Caso seja necessário enviar aluno para buscar o material, deverá ser enviado apenas no começo da aula e um único aluno, para evitar atrapalhar o andamento dos trabalhos da secretaria.</p>';
        $agreement->save();
        $agreement->topicTags()->attach(TopicTag::find(4));
        foreach ([1, 4, 5] as $id) {
            $role = AgreementRole::find($id);
            $agreement->agreementRoles()->attach($role);
        }

        $agreement = clone $agreement_model;
        $agreement->title = 'Uso do celular pelos alunos';
        $agreement->details = '<p>O aluno deve manter o celular desligado durante a aula, podendo utilizá-lo nos momentos devidamente autorizados pelo professor para fins didáticos.</p>'
            . '<p>Em caso de desobediência, o professor pode retirar o aparelho do aluno ou encaminhar o para a direção.</p>';
        $agreement->save();
        $agreement->topicTags()->attach(TopicTag::find(4));
        foreach ([1, 2, 5] as $id) {
            $role = AgreementRole::find($id);
            $agreement->agreementRoles()->attach($role);
        }

        $agreement = clone $agreement_model;
        $agreement->title = 'Alunos que chegam depois do professor nas trocas de aulas';
        $agreement->details = '<p>O aluno que chegar após o professor no início das aulas deverá entrar na sala mas assumirá a falta da aula em questão.</p>';
        $agreement->save();
        $agreement->topicTags()->attach(TopicTag::find(7));
        foreach ([1, 4, 5] as $id) {
            $role = AgreementRole::find($id);
            $agreement->agreementRoles()->attach($role);
        }

        $this->enableForeignKeys();
    }
}
