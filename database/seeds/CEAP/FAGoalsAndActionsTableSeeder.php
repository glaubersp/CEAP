<?php

use App\Models\Auth\User;
use App\Models\CEAP\Action;
use App\Models\CEAP\ActionEvaluation;
use App\Models\CEAP\EvaluationTerm;
use App\Models\CEAP\Evidence;
use App\Models\CEAP\Goal;
use App\Models\CEAP\Indicator;
use Carbon\Carbon;
use Illuminate\Database\Seeder;


class FAGoalsAndActionsTableSeeder extends Seeder
{
    use DisableForeignKeys, TruncateTable;

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->disableForeignKeys();
        $this->truncate('action_evaluations');
        $this->truncate('evidences');
        $this->truncate('actions');
        $this->truncate('goals');

        // Default models for object cloning
        $goal_obj = new Goal();
        $goal_obj->justification = 'Inserir alguma justificativa para este objetivo.';

	    $term = EvaluationTerm::find(1);

        $action_obj = new Action;
        $action_obj->details = 'Detalhar melhor esta ação.';
        $action_obj->start_date = Carbon::now();
        $action_obj->end_date = Carbon::now()->addMonth(2);
        $action_obj->evaluationTerm()->associate($term);

        $evidence_obj = new Evidence();
        $evidence_obj->details = 'Detalhar melhor esta evidência.';

        $evaluation_obj = new ActionEvaluation();
        $evaluation_obj->setEvaluationDateAttribute(Carbon::now());

        $user = User::find(2);

        // Define Indicator and related goal and actions
        $indicator = Indicator::findByName('Habilidades Interpessoais - Expectativas Claras')->first();

        $goal = clone $goal_obj;
        $goal->name = 'Organização do espaço físico da cantina';
        $goal->indicator()->associate($indicator);
        $goal->save();
        $action = clone $action_obj;
        $action->name = 'Posicionamento de mesas indicando uma fila única';
        $action->goal()->associate($goal);
        $action->save();
        $action->users()->attach($user);
        $evidence = clone $evidence_obj;
        $evidence->title = 'Feito mas parou de ser feito. Será retomado.';
        $evidence->action()->associate($action);
        $evidence->user()->associate($user);
        $evidence->save();
        $evidence = clone $evidence_obj;
        $evidence->title = 'Alunos não obedecem o grêmio.';
        $evidence->action()->associate($action);
        $evidence->user()->associate($user);
        $evidence->save();
        $evaluation = clone $evaluation_obj;
        $evaluation->evaluation = 'Fully implemented';
        $evaluation->justification = 'Posicionamento funcionou; espaço físico é pequeno; é preciso de um funcionário para monitorar.';
        $evaluation->rating = 4;
        $evaluation->action()->associate($action);
        $evaluation->user()->associate($user);
        $evaluation->save();


        $goal = clone $goal_obj;
        $goal->name = 'Adultos devem supervisionar alimentação';
        $goal->indicator()->associate($indicator);
        $goal->save();
        $action = clone $action_obj;
        $action->name = 'Definir professores e funcionários que possam acompanhar a alimentação.';
        $action->goal()->associate($goal);
        $action->save();
        $action->users()->attach($user);
        $evidence = clone $evidence_obj;
        $evidence->title = '“Professor responsável” sai 5 minutos antes para adiantar fila; Pessoal da secretaria monitorará a área.';
        $evidence->action()->associate($action);
        $evidence->user()->associate($user);
        $evidence->save();


        $goal = clone $goal_obj;
        $goal->name = 'Definição de regras claras de comportamento para os alunos';
        $goal->indicator()->associate($indicator);
        $goal->save();
        $action = clone $action_obj;
        $action->name = 'Cartaz com regras "positivas" (o que se deve fazer, e não o que não se deve fazer).';
        $action->goal()->associate($goal);
        $action->save();
        $action->users()->attach($user);
        $evidence = clone $evidence_obj;
        $evidence->title = 'Cartazes foram removidos.';
        $evidence->action()->associate($action);
        $evidence->user()->associate($user);
        $evidence->save();
        $evidence = clone $evidence_obj;
        $evidence->title = 'Revisão do cartaz pelo Grêmio – texto maior, linguagem positiva.';
        $evidence->action()->associate($action);
        $evidence->user()->associate($user);
        $evidence->save();
        $evidence = clone $evidence_obj;
        $evidence->title = 'Novos cartazes feitos pela profa. Claudia.';
        $evidence->action()->associate($action);
        $evidence->user()->associate($user);
        $evidence->save();

        $goal = clone $goal_obj;
        $goal->name = 'Identificação do último aluno da fila';
        $goal->indicator()->associate($indicator);
        $goal->save();
        $action = clone $action_obj;
        $action->name = 'Cartaz para o último da fila.';
        $action->goal()->associate($goal);
        $action->save();
        $action->users()->attach($user);
        $evidence = clone $evidence_obj;
        $evidence->title = 'O cartaz não foi utilizado pelos alunos.';
        $evidence->action()->associate($action);
        $evidence->user()->associate($user);
        $evidence->save();


        $goal = clone $goal_obj;
        $goal->name = 'Feedback dos funcionários da cozinha';
        $goal->indicator()->associate($indicator);
        $goal->save();
        $action = clone $action_obj;
        $action->name = 'Discussão com funcionários da cozinha sobre demanda.';
        $action->goal()->associate($goal);
        $action->save();
        $action->users()->attach($user);
        $evidence = clone $evidence_obj;
        $evidence->title = 'Identificou-se que a  quantidade de alimentos é apropriada.';
        $evidence->action()->associate($action);
        $evidence->user()->associate($user);
        $evidence->save();


        $indicator = Indicator::findByName('Habilidades Acadêmicas - Relacionamento')->first();

        $goal = clone $goal_obj;
        $goal->name = 'Retomar professores coordenadores de sala';
        $goal->indicator()->associate($indicator);
        $goal->save();
        $action = clone $action_obj;
        $action->name = 'Professores se auto-indicarão no começo do ano letivo.';
        $action->goal()->associate($goal);
        $action->save();
        $action->users()->attach($user);
        $evidence = clone $evidence_obj;
        $evidence->title = 'Professores foram definidos durante a ATPC.';
        $evidence->action()->associate($action);
        $evidence->user()->associate($user);
        $evidence->save();
        $evidence = clone $evidence_obj;
        $evidence->title = 'Os professores coordenadores foram publicados no jornal da escola.';
        $evidence->action()->associate($action);
        $evidence->user()->associate($user);
        $evidence->save();

        $goal = clone $goal_obj;
        $goal->name = 'Retomar professor-orientador do Grêmio';
        $goal->indicator()->associate($indicator);
        $goal->save();
        $action = clone $action_obj;
        $action->name = 'Pedir voluntários para orientação.';
        $action->goal()->associate($goal);
        $action->save();
        $action->users()->attach($user);
        $evidence = clone $evidence_obj;
        $evidence->title = 'Professores César e Beatriz se voluntariaram.';
        $evidence->action()->associate($action);
        $evidence->user()->associate($user);
        $evidence->save();
        $evidence = clone $evidence_obj;
        $evidence->title = 'Publicada chamada para a criação de chapas no jornal da escola.';
        $evidence->action()->associate($action);
        $evidence->user()->associate($user);
        $evidence->save();

        $goal = clone $goal_obj;
        $goal->name = 'Realizar eleição do Grêmio';
        $goal->indicator()->associate($indicator);
        $goal->save();
        $action = clone $action_obj;
        $action->name = 'Montar grêmio com participantes dos dois períodos.';
        $action->goal()->associate($goal);
        $action->save();
        $action->users()->attach($user);
        $evidence = clone $evidence_obj;
        $evidence->title = 'Alunos são do período matutino e não podem estar na escola de tarde..';
        $evidence->action()->associate($action);
        $evidence->user()->associate($user);
        $evidence->save();

        $action = clone $action_obj;
        $action->name = 'Realizar assembléia para validar as chapas.';
        $action->goal()->associate($goal);
        $action->save();
        $action->users()->attach($user);

        $action = clone $action_obj;
        $action->name = 'Executar projeto de divulgação multimídia.';
        $action->goal()->associate($goal);
        $action->save();
        $action->users()->attach($user);

        $action = clone $action_obj;
        $action->name = 'Solicitar urnas eletrônicas para Abril.';
        $action->goal()->associate($goal);
        $action->save();
        $action->users()->attach($user);
        $evidence = clone $evidence_obj;
        $evidence->title = 'Dulce vai enviar ofício.';
        $evidence->action()->associate($action);
        $evidence->user()->associate($user);
        $evidence->save();
        $evidence = clone $evidence_obj;
        $evidence->title = 'Custo da urna.';
        $evidence->details = 'Para solicitar a urna, é necessário pagar a diária do funcionário que acompanha a urna. Isto inviabiliza o uso da urna pela escola';
        $evidence->action()->associate($action);
        $evidence->user()->associate($user);
        $evidence->save();
        $evaluation = clone $evaluation_obj;
        $evaluation->evaluation = 'Not implemented';
        $evaluation->justification = 'Custo da urna inviabiliza o uso pela escola.';
        $evaluation->action()->associate($action);
        $evaluation->user()->associate($user);
        $evaluation->save();

        $goal = clone $goal_obj;
        $goal->name = 'Tratar os delitos dos alunos através da implementação da Justiça Reparativa';
        $goal->indicator()->associate($indicator);
        $goal->save();
        $action = clone $action_obj;
        $action->name = 'Implementar projeto de justiça reparativa.';
        $action->goal()->associate($goal);
        $action->save();
        $action->users()->attach($user);
        $evidence = clone $evidence_obj;
        $evidence->title = 'Piloto a ser implementado pela Tati.';
        $evidence->action()->associate($action);
        $evidence->user()->associate($user);
        $evidence->save();
        $evidence = clone $evidence_obj;
        $evidence->title = 'Esboço inicial do projeto foi apresentado na reunião de Planejamento.';
        $evidence->action()->associate($action);
        $evidence->user()->associate($user);
        $evidence->save();

        $goal = clone $goal_obj;
        $goal->name = 'Tentar ser menos punitivo';
        $goal->indicator()->associate($indicator);
        $goal->save();
        $action = clone $action_obj;
        $action->name = 'Criar texto para trabalhar na acolhida da escola (Tati).';
        $action->goal()->associate($goal);
        $action->save();
        $action->users()->attach($user);
        $evidence = clone $evidence_obj;
        $evidence->title = 'Texto escrito pela professora Tatiana foi trabalhado no primeiro dia de aula e publicado como encarte no jornal da escola.';
        $evidence->action()->associate($action);
        $evidence->user()->associate($user);
        $evidence->save();

        $action = clone $action_obj;
        $action->name = 'Implantar projeto pilto das assembléias.';
        $action->goal()->associate($goal);
        $action->save();
        $action->users()->attach($user);
        $evidence = clone $evidence_obj;
        $evidence->title = 'Assunto foi introduzido no Planejaento de março e será discutido nos próximos ATPCs.';
        $evidence->action()->associate($action);
        $evidence->user()->associate($user);
        $evidence->save();

        $action = clone $action_obj;
        $action->name = 'Discutir os combinados nas salas de aula.';
        $action->goal()->associate($goal);
        $action->save();
        $action->users()->attach($user);
        $evidence = clone $evidence_obj;
        $evidence->title = 'Foram apresentados para os alunos no primeiro dia de aula.';
        $evidence->action()->associate($action);
        $evidence->user()->associate($user);
        $evidence->save();


        $indicator = Indicator::findByName('Habilidades Acadêmicas - Expectativas Claras')->first();

        $goal = clone $goal_obj;
        $goal->name = 'Professores devem esclarecer objetivos no começo da aula e ter explicar planejamento de curto prazo';
        $goal->indicator()->associate($indicator);
        $goal->save();
        $action = clone $action_obj;
        $action->name = 'Postar papel/escrever na lousa o objetivo claro da aula/atividade e do curto prazo.';
        $action->goal()->associate($goal);
        $action->save();
        $action->users()->attach($user);
        $evidence = clone $evidence_obj;
        $evidence->title = 'Alguns professores seguem o combinado, outros não (alguns, não sabem quando entram na escola).';
        $evidence->action()->associate($action);
        $evidence->user()->associate($user);
        $evidence->save();
        $evidence = clone $evidence_obj;
        $evidence->title = 'A pedido da gestão, sugere-se uma discussão mais aprofundada sobre o tema.';
        $evidence->action()->associate($action);
        $evidence->user()->associate($user);
        $evidence->save();

        $goal = clone $goal_obj;
        $goal->name = 'Fortalecer altas perspectivas para o futuro dos alunos (médio->pós-escola)';
        $goal->indicator()->associate($indicator);
        $goal->save();
        $action = clone $action_obj;
        $action->name = 'Chamar ex-alunos, psicólogo da secretaria, ou outra forma de apoio para falar sobre opções.';
        $action->goal()->associate($goal);
        $action->save();
        $action->users()->attach($user);

        $goal = clone $goal_obj;
        $goal->name = 'Fortalecer altas perspectivas para o futuro dos alunos entre anos (finais->médio)';
        $goal->indicator()->associate($indicator);
        $goal->save();
        $action = clone $action_obj;
        $action->name = 'Oferecer tempo para discutir as expectativas que diferem de um ciclo para outro.';
        $action->goal()->associate($goal);
        $action->save();
        $action->users()->attach($user);


        $indicator = Indicator::findByName('Habilidades Acadêmicas - Reconhecimento')->first();

        $goal = clone $goal_obj;
        $goal->name = 'Fotos das atividades';
        $goal->indicator()->associate($indicator);
        $goal->save();
        $action = clone $action_obj;
        $action->name = 'Postagem de fotos no mural com as atividades/produções dos alunos.';
        $action->goal()->associate($goal);
        $action->save();
        $action->users()->attach($user);
        $evidence = clone $evidence_obj;
        $evidence->title = 'Fotos das atividades de 2016 publicadas no jornal da escola.';
        $evidence->action()->associate($action);
        $evidence->user()->associate($user);
        $evidence->save();
        $evidence = clone $evidence_obj;
        $evidence->title = 'Incorporado ao projeto de publicação do jornal da escola.';
        $evidence->action()->associate($action);
        $evidence->user()->associate($user);
        $evidence->save();


        $indicator = Indicator::findByName('Instrução - Expectativas Claras')->first();

        $goal = clone $goal_obj;
        $goal->name = 'Definir Combinados';
        $goal->indicator()->associate($indicator);
        $goal->save();
        $action = clone $action_obj;
        $action->name = 'Criar informativo de combinados.';
        $action->goal()->associate($goal);
        $action->save();
        $action->users()->attach($user);
        $evidence = clone $evidence_obj;
        $evidence->title = 'Criado e entregue aos professores durante o Planejamento [Anexar combinados ao sistema].';
        $evidence->action()->associate($action);
        $evidence->user()->associate($user);
        $evidence->save();

        $action = clone $action_obj;
        $action->name = 'Realizar reforço periódico entre os membros dos combinados';
        $action->goal()->associate($goal);
        $action->save();
        $action->users()->attach($user);


        $indicator = Indicator::findByName('Apoio da Comunidade - Expectativas Claras')->first();

        $goal = clone $goal_obj;
        $goal->name = 'Melhorar a divulgação de eventos e atividades da escola';
        $goal->indicator()->associate($indicator);
        $goal->save();
        $action = clone $action_obj;
        $action->name = 'Criar lista de emails.';
        $action->goal()->associate($goal);
        $action->save();
        $action->users()->attach($user);
        $evidence = clone $evidence_obj;
        $evidence->title = 'Lista de e-mails criada por alunas, com 50+ pais.';
        $evidence->action()->associate($action);
        $evidence->user()->associate($user);
        $evidence->save();
        $evidence = clone $evidence_obj;
        $evidence->title = 'Respostas aos questionários aumentaram.';
        $evidence->action()->associate($action);
        $evidence->user()->associate($user);
        $evidence->save();
        $evidence = clone $evidence_obj;
        $evidence->title = 'Diálogo entre pais por e-mail.';
        $evidence->action()->associate($action);
        $evidence->user()->associate($user);
        $evidence->save();
        $evaluation = clone $evaluation_obj;
        $evaluation->evaluation = 'Fully implemented';
        $evaluation->justification = 'Em pleno funcionamento.';
        $evaluation->rating = 5;
        $evaluation->action()->associate($action);
        $evaluation->user()->associate($user);
        $evaluation->save();

        $action = clone $action_obj;
        $action->name = 'Identificar novo pai/responsável para lista de e-mails (mãe/aluno saiu da escola).';
        $action->goal()->associate($goal);
        $action->save();
        $action->users()->attach($user);

        $action = clone $action_obj;
        $action->name = 'Coletar possibilidades de canais de informação entre os pais.';
        $action->goal()->associate($goal);
        $action->save();
        $action->users()->attach($user);
        $evidence = clone $evidence_obj;
        $evidence->title = 'Formulário entregue em reunião de pais. Falta tabular os dados.';
        $evidence->action()->associate($action);
        $evidence->user()->associate($user);
        $evidence->save();

        $action = clone $action_obj;
        $action->name = 'Criar uma página web.';
        $action->goal()->associate($goal);
        $action->save();
        $action->users()->attach($user);
        $evidence = clone $evidence_obj;
        $evidence->title = 'Página criada mas precisa ser migrada para o sistema mais flexível, discussão sobre conteúdo.';
        $evidence->action()->associate($action);
        $evidence->user()->associate($user);
        $evidence->save();

        $action = clone $action_obj;
        $action->name = 'Criar informativo impresso (Jormal da FA).';
        $action->details = 'Colocar na escola, por e-mail, website. Envolver alunos na criação/atividade didática.';
        $action->goal()->associate($goal);
        $action->save();
        $action->users()->attach($user);
        $evidence = clone $evidence_obj;
        $evidence->title = 'Criada a pasta online com o modelo para o jornal.';
        $evidence->action()->associate($action);
        $evidence->user()->associate($user);
        $evidence->save();
        $evidence = clone $evidence_obj;
        $evidence->title = 'Primeira edição foi publicada em Fevereiro/2017.';
        $evidence->action()->associate($action);
        $evidence->user()->associate($user);
        $evidence->save();

        $indicator = Indicator::findByName('Apoio da Comunidade - Expectativas Claras')->first();


        $goal = clone $goal_obj;
        $goal->name = 'Envolver pais nas discussões sobre carreiras com alunos';
        $goal->indicator()->associate($indicator);
        $goal->save();
        $action = clone $action_obj;
        $action->name = 'Realizar palestra no começo do ano para os pais.';
        $action->goal()->associate($goal);
        $action->save();
        $action->users()->attach($user);


        $indicator = Indicator::findByName('Apoio da Comunidade - Habilidades')->first();

        $goal = clone $goal_obj;
        $goal->name = 'Festa da Primavera';
        $goal->indicator()->associate($indicator);
        $goal->save();
        $action = clone $action_obj;
        $action->name = 'Planejar e executar a festa em 2016.';
        $action->goal()->associate($goal);
        $action->save();
        $action->users()->attach($user);
        $evidence = clone $evidence_obj;
        $evidence->title = 'Pouca participação do 3o.Ano do EM.';
        $evidence->action()->associate($action);
        $evidence->user()->associate($user);
        $evidence->save();
        $evidence = clone $evidence_obj;
        $evidence->title = 'Bingo não foi bem organizado.';
        $evidence->action()->associate($action);
        $evidence->user()->associate($user);
        $evidence->save();
        $evaluation = clone $evaluation_obj;
        $evaluation->evaluation = 'Fully implemented';
        $evaluation->justification = 'Faltou comunicação das apresentações da festa.';
        $evaluation->rating = 4;
        $evaluation->action()->associate($action);
        $evaluation->user()->associate($user);
        $evaluation->save();

        $action = clone $action_obj;
        $action->name = 'Angariar retorno dos participantes através de questionário.';
        $action->goal()->associate($goal);
        $action->save();
        $action->users()->attach($user);
        $evidence = clone $evidence_obj;
        $evidence->title = 'Questionário criado, mas não foi enviado para os pais.';
        $evidence->action()->associate($action);
        $evidence->user()->associate($user);
        $evidence->save();

        $goal = clone $goal_obj;
        $goal->name = 'Melhorar a contribuição financeira da APM';
        $goal->indicator()->associate($indicator);
        $goal->save();
        $action = clone $action_obj;
        $action->name = 'Envio de carta para pedido de apoio.';
        $action->goal()->associate($goal);
        $action->save();
        $action->users()->attach($user);

        $goal = clone $goal_obj;
        $goal->name = 'Protesto contra corte de verbas';
        $goal->indicator()->associate($indicator);
        $goal->save();
        $action = clone $action_obj;
        $action->name = 'Pais devem enviar e-mail para secretaria para protestar contra o corte.';
        $action->goal()->associate($goal);
        $action->save();
        $action->users()->attach($user);

        $indicator = Indicator::findByName('Colaboração - Expectativas Claras')->first();

        $goal = clone $goal_obj;
        $goal->name = 'Planejar os eventos da escola com antecedência.';
        $goal->indicator()->associate($indicator);
        $goal->save();
        $action = clone $action_obj;
        $action->name = 'Planejar os eventos na reunião do começo do ano.';
        $action->goal()->associate($goal);
        $action->save();
        $action->users()->attach($user);
        $evidence = clone $evidence_obj;
        $evidence->title = 'Já tem a semana de prova..';
        $evidence->action()->associate($action);
        $evidence->user()->associate($user);
        $evidence->save();

        $indicator = Indicator::findByName('Habilidades Acadêmicas - Habilidades')->first();

        $goal = clone $goal_obj;
        $goal->name = 'Melhorar as habilidades de leitura dos alunos.';
        $goal->indicator()->associate($indicator);
        $goal->save();
        $action = clone $action_obj;
        $action->name = 'Utilizar o Whatsapp para enviar textos para os alunos.';
        $action->goal()->associate($goal);
        $action->save();
        $action->users()->attach($user);
        $evidence = clone $evidence_obj;
        $evidence->title = 'A ação está em andamento em uma sala de aula';
        $evidence->action()->associate($action);
        $evidence->user()->associate($user);
        $evidence->save();

        $action = clone $action_obj;
        $action->name = 'Projeto Leitura.';
        $action->details = 'Ler, na primeira aula do dia, algumas páginas de um livro.';
        $action->goal()->associate($goal);
        $action->save();
        $action->users()->attach($user);
        $evidence = clone $evidence_obj;
        $evidence->title = 'Já aconteceu mas não está em andamento.';
        $evidence->action()->associate($action);
        $evidence->user()->associate($user);
        $evidence->save();

        $this->enableForeignKeys();
    }
}
