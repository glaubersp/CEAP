<?php

use App\Models\CEAP\AgreementRole;
use Illuminate\Database\Seeder;

class AgreementRolesTableSeeder extends Seeder
{
    use DisableForeignKeys, TruncateTable;

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $table_name = 'agreement_roles';

        $this->disableForeignKeys();
        $this->truncate($table_name);

        //        Aluno, Gestor, Professor, Funcionário, '
        $model = new AgreementRole();
        $model->name = 'Alunos';
        $model->save();

        $model = new AgreementRole();
        $model->name = 'Gestores';
        $model->save();

        $model = new AgreementRole();
        $model->name = 'Gerentes de Organização Escolar';
        $model->save();

        $model = new AgreementRole();
        $model->name = 'Agentes de Organização Escolar';
        $model->save();

        $model = new AgreementRole();
        $model->name = 'Professores';
        $model->save();

        $model = new AgreementRole();
        $model->name = 'Pais/Responsáveis';
        $model->save();

        $this->enableForeignKeys();
    }
}
