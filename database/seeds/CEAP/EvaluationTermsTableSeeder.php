<?php

use App\Models\CEAP\EvaluationTerm;
use Illuminate\Database\Seeder;

class EvaluationTermsTableSeeder extends Seeder
{
	use DisableForeignKeys, TruncateTable;
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
	    $table_name = 'evaluation_terms';

	    $this->disableForeignKeys();
	    $this->truncate($table_name);

	    $term_model = new EvaluationTerm();
	    $term_model->year = 2017;
        $term_model->active = 0;

	    $sem1 = clone $term_model;
        $sem1->name = 'Semestre 1';
        $sem1->sequence = 1;
	    $sem1->save();

        $sem1 = clone $term_model;
        $sem1->name = 'Semestre 2';
        $sem1->sequence = 2;
        $sem1->save();

        $term_model->year = 2018;

        $sem1 = clone $term_model;
        $sem1->name = 'Semestre 1';
        $sem1->sequence = 1;
        $sem1->save();

        $sem1 = clone $term_model;
        $sem1->name = 'Semestre 2';
        $sem1->sequence = 2;
        $sem1->save();

        $term_model->year = 2019;

        $sem1 = clone $term_model;
        $sem1->name = 'Semestre 1';
        $sem1->sequence = 1;
        $sem1->save();

        $sem1 = clone $term_model;
        $sem1->name = 'Semestre 2';
        $sem1->sequence = 2;
        $sem1->save();
    }
}
