<?php

use App\Models\CEAP\AgreementMonitoringStrategy;
use Illuminate\Database\Seeder;

class AgreementMonitoringStrategiesTableSeeder extends Seeder
{
    use DisableForeignKeys, TruncateTable;

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $table_name = 'agreement_roles';

        $this->disableForeignKeys();
        $this->truncate($table_name);

        //        Aluno, Gestor, Professor, Funcionário, '
        $model = new AgreementMonitoringStrategy();
        $model->title = 'ATPC';
        $model->save();

        $model = new AgreementMonitoringStrategy();
        $model->title = 'Gerentes de Organização Escolar';
        $model->save();

        $model = new AgreementMonitoringStrategy();
        $model->title = 'Agentes de Organização Escolar';
        $model->save();

        $this->enableForeignKeys();
    }
}
