<?php

use App\Models\CEAP\Goal;
use App\Models\CEAP\GoalEvaluation;
use Carbon\Carbon;
use Illuminate\Database\Seeder;

class GoalEvaluationsTableSeeder extends Seeder
{
    use DisableForeignKeys, TruncateTable;

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $table_name = 'goal_evaluations';

        $this->disableForeignKeys();
        $this->truncate($table_name);

        $goal_evaluation_model = new GoalEvaluation();

        foreach (Goal::all() as $goal) {
            $indicator = $goal->indicator;

            $goal_evaluation = clone $goal_evaluation_model;
            $goal_evaluation->goal()->associate($goal);
            $goal_evaluation->stars = 3;
            $goal_evaluation->justification = 'Teste';
            $goal_evaluation->setEvaluationDateAttribute(Carbon::now());
            $goal_evaluation->save();
        }

        $this->enableForeignKeys();
    }
}
