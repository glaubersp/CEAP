<?php

use App\Models\CEAP\Indicator;
use App\Models\CEAP\IndicatorRecord;
use App\Models\CEAP\SchoolClass;
use Illuminate\Database\Seeder;

class IndicatorRecordsTableSeeder extends Seeder
{
    use DisableForeignKeys, TruncateTable;

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $table_name = 'indicator_records';

        $this->disableForeignKeys();
        $this->truncate($table_name);

        $indicator_names = [
            [
                "Habilidades Acadêmicas - Expectativas Claras",
                "Habilidades Acadêmicas - Habilidades",
                "Habilidades Acadêmicas - Reconhecimento",
                "Habilidades Acadêmicas - Relacionamento",

                "Habilidades Interpessoais - Expectativas Claras",
                "Habilidades Interpessoais - Habilidades",
                "Habilidades Interpessoais - Reconhecimento",
                "Habilidades Interpessoais - Relacionamento",
            ],

            [
                "Instrução - Expectativas Claras",
                "Instrução - Habilidades",
                "Instrução - Reconhecimento",
                "Instrução - Relacionamento",

                "Colaboração - Expectativas Claras",
                "Colaboração - Habilidades",
                "Colaboração - Reconhecimento",
                "Colaboração - Relacionamento",
            ],

            [
                "Envolvimento dos Pais/Responsáveis - Expectativas Claras",
                "Envolvimento dos Pais/Responsáveis - Habilidades",
                "Envolvimento dos Pais/Responsáveis - Reconhecimento",
                "Envolvimento dos Pais/Responsáveis - Relacionamento",

                "Apoio da Comunidade - Expectativas Claras",
                "Apoio da Comunidade - Habilidades",
                "Apoio da Comunidade - Reconhecimento",
                "Apoio da Comunidade - Relacionamento",
            ],
        ];

        // Relatório 2015 - Middle School
        $indicator_record_obj = new IndicatorRecord;
        $indicator_record_obj->setDateAttribute('15/11/2015');
        $indicator_record_obj->school_grade_group = 'middle_school';

        $indicator = Indicator::findByName($indicator_names[0][0])->first();
        $indicator_record = clone $indicator_record_obj;
        $indicator_record->value = 39;
        $indicator_record->indicator()->associate($indicator);
        $indicator_record->save();

        $indicator = Indicator::findByName($indicator_names[0][1])->first();
        $indicator_record = clone $indicator_record_obj;
        $indicator_record->value = 49;
        $indicator_record->indicator()->associate($indicator);
        $indicator_record->save();

        $indicator = Indicator::findByName($indicator_names[0][2])->first();
        $indicator_record = clone $indicator_record_obj;
        $indicator_record->value = 61;
        $indicator_record->indicator()->associate($indicator);
        $indicator_record->save();

        $indicator = Indicator::findByName($indicator_names[0][3])->first();
        $indicator_record = clone $indicator_record_obj;
        $indicator_record->value = 67;
        $indicator_record->indicator()->associate($indicator);
        $indicator_record->save();

        $indicator = Indicator::findByName($indicator_names[0][4])->first();
        $indicator_record = clone $indicator_record_obj;
        $indicator_record->value = 58;
        $indicator_record->indicator()->associate($indicator);
        $indicator_record->save();

        $indicator = Indicator::findByName($indicator_names[0][5])->first();
        $indicator_record = clone $indicator_record_obj;
        $indicator_record->value = 64;
        $indicator_record->indicator()->associate($indicator);
        $indicator_record->save();

        $indicator = Indicator::findByName($indicator_names[0][6])->first();
        $indicator_record = clone $indicator_record_obj;
        $indicator_record->value = 81;
        $indicator_record->indicator()->associate($indicator);
        $indicator_record->save();

        $indicator = Indicator::findByName($indicator_names[0][7])->first();
        $indicator_record = clone $indicator_record_obj;
        $indicator_record->value = 66;
        $indicator_record->indicator()->associate($indicator);
        $indicator_record->save();

        $indicator = Indicator::findByName($indicator_names[1][0])->first();
        $indicator_record = clone $indicator_record_obj;
        $indicator_record->value = 0;
        $indicator_record->indicator()->associate($indicator);
        $indicator_record->save();

        $indicator = Indicator::findByName($indicator_names[1][1])->first();
        $indicator_record = clone $indicator_record_obj;
        $indicator_record->value = 0;
        $indicator_record->indicator()->associate($indicator);
        $indicator_record->save();

        $indicator = Indicator::findByName($indicator_names[1][2])->first();
        $indicator_record = clone $indicator_record_obj;
        $indicator_record->value = 0;
        $indicator_record->indicator()->associate($indicator);
        $indicator_record->save();

        $indicator = Indicator::findByName($indicator_names[1][3])->first();
        $indicator_record = clone $indicator_record_obj;
        $indicator_record->value = 0;
        $indicator_record->indicator()->associate($indicator);
        $indicator_record->save();

        $indicator = Indicator::findByName($indicator_names[1][4])->first();
        $indicator_record = clone $indicator_record_obj;
        $indicator_record->value = 0;
        $indicator_record->indicator()->associate($indicator);
        $indicator_record->save();

        $indicator = Indicator::findByName($indicator_names[1][5])->first();
        $indicator_record = clone $indicator_record_obj;
        $indicator_record->value = 0;
        $indicator_record->indicator()->associate($indicator);
        $indicator_record->save();

        $indicator = Indicator::findByName($indicator_names[1][6])->first();
        $indicator_record = clone $indicator_record_obj;
        $indicator_record->value = 0;
        $indicator_record->indicator()->associate($indicator);
        $indicator_record->save();

        $indicator = Indicator::findByName($indicator_names[1][7])->first();
        $indicator_record = clone $indicator_record_obj;
        $indicator_record->value = 0;
        $indicator_record->indicator()->associate($indicator);
        $indicator_record->save();

        $indicator = Indicator::findByName($indicator_names[2][0])->first();
        $indicator_record = clone $indicator_record_obj;
        $indicator_record->value = 0;
        $indicator_record->indicator()->associate($indicator);
        $indicator_record->save();

        $indicator = Indicator::findByName($indicator_names[2][1])->first();
        $indicator_record = clone $indicator_record_obj;
        $indicator_record->value = 0;
        $indicator_record->indicator()->associate($indicator);
        $indicator_record->save();

        $indicator = Indicator::findByName($indicator_names[2][2])->first();
        $indicator_record = clone $indicator_record_obj;
        $indicator_record->value = 0;
        $indicator_record->indicator()->associate($indicator);
        $indicator_record->save();

        $indicator = Indicator::findByName($indicator_names[2][3])->first();
        $indicator_record = clone $indicator_record_obj;
        $indicator_record->value = 0;
        $indicator_record->indicator()->associate($indicator);
        $indicator_record->save();

        $indicator = Indicator::findByName($indicator_names[2][4])->first();
        $indicator_record = clone $indicator_record_obj;
        $indicator_record->value = 0;
        $indicator_record->indicator()->associate($indicator);
        $indicator_record->save();

        $indicator = Indicator::findByName($indicator_names[2][5])->first();
        $indicator_record = clone $indicator_record_obj;
        $indicator_record->value = 0;
        $indicator_record->indicator()->associate($indicator);
        $indicator_record->save();

        $indicator = Indicator::findByName($indicator_names[2][6])->first();
        $indicator_record = clone $indicator_record_obj;
        $indicator_record->value = 0;
        $indicator_record->indicator()->associate($indicator);
        $indicator_record->save();

        $indicator = Indicator::findByName($indicator_names[2][7])->first();
        $indicator_record = clone $indicator_record_obj;
        $indicator_record->value = 0;
        $indicator_record->indicator()->associate($indicator);
        $indicator_record->save();

        // Relatório 2015 - High School
        $indicator_record_obj = new IndicatorRecord;
        $indicator_record_obj->setDateAttribute('15/11/2015');
        $indicator_record_obj->school_grade_group = 'high_school';

        $indicator = Indicator::findByName($indicator_names[0][0])->first();
        $indicator_record = clone $indicator_record_obj;
        $indicator_record->value = 54;
        $indicator_record->indicator()->associate($indicator);
        $indicator_record->save();

        $indicator = Indicator::findByName($indicator_names[0][1])->first();
        $indicator_record = clone $indicator_record_obj;
        $indicator_record->value = 35;
        $indicator_record->indicator()->associate($indicator);
        $indicator_record->save();

        $indicator = Indicator::findByName($indicator_names[0][2])->first();
        $indicator_record = clone $indicator_record_obj;
        $indicator_record->value = 51;
        $indicator_record->indicator()->associate($indicator);
        $indicator_record->save();

        $indicator = Indicator::findByName($indicator_names[0][3])->first();
        $indicator_record = clone $indicator_record_obj;
        $indicator_record->value = 72;
        $indicator_record->indicator()->associate($indicator);
        $indicator_record->save();

        $indicator = Indicator::findByName($indicator_names[0][4])->first();
        $indicator_record = clone $indicator_record_obj;
        $indicator_record->value = 63;
        $indicator_record->indicator()->associate($indicator);
        $indicator_record->save();

        $indicator = Indicator::findByName($indicator_names[0][5])->first();
        $indicator_record = clone $indicator_record_obj;
        $indicator_record->value = 71;
        $indicator_record->indicator()->associate($indicator);
        $indicator_record->save();

        $indicator = Indicator::findByName($indicator_names[0][6])->first();
        $indicator_record = clone $indicator_record_obj;
        $indicator_record->value = 72;
        $indicator_record->indicator()->associate($indicator);
        $indicator_record->save();

        $indicator = Indicator::findByName($indicator_names[0][7])->first();
        $indicator_record = clone $indicator_record_obj;
        $indicator_record->value = 46;
        $indicator_record->indicator()->associate($indicator);
        $indicator_record->save();

        $indicator = Indicator::findByName($indicator_names[1][0])->first();
        $indicator_record = clone $indicator_record_obj;
        $indicator_record->value = 29;
        $indicator_record->indicator()->associate($indicator);
        $indicator_record->save();

        $indicator = Indicator::findByName($indicator_names[1][1])->first();
        $indicator_record = clone $indicator_record_obj;
        $indicator_record->value = 35;
        $indicator_record->indicator()->associate($indicator);
        $indicator_record->save();

        $indicator = Indicator::findByName($indicator_names[1][2])->first();
        $indicator_record = clone $indicator_record_obj;
        $indicator_record->value = 100;
        $indicator_record->indicator()->associate($indicator);
        $indicator_record->save();

        $indicator = Indicator::findByName($indicator_names[1][3])->first();
        $indicator_record = clone $indicator_record_obj;
        $indicator_record->value = 100;
        $indicator_record->indicator()->associate($indicator);
        $indicator_record->save();

        $indicator = Indicator::findByName($indicator_names[1][4])->first();
        $indicator_record = clone $indicator_record_obj;
        $indicator_record->value = 94;
        $indicator_record->indicator()->associate($indicator);
        $indicator_record->save();

        $indicator = Indicator::findByName($indicator_names[1][5])->first();
        $indicator_record = clone $indicator_record_obj;
        $indicator_record->value = 88;
        $indicator_record->indicator()->associate($indicator);
        $indicator_record->save();

        $indicator = Indicator::findByName($indicator_names[1][6])->first();
        $indicator_record = clone $indicator_record_obj;
        $indicator_record->value = 100;
        $indicator_record->indicator()->associate($indicator);
        $indicator_record->save();

        $indicator = Indicator::findByName($indicator_names[1][7])->first();
        $indicator_record = clone $indicator_record_obj;
        $indicator_record->value = 59;
        $indicator_record->indicator()->associate($indicator);
        $indicator_record->save();

        $indicator = Indicator::findByName($indicator_names[2][0])->first();
        $indicator_record = clone $indicator_record_obj;
        $indicator_record->value = 82;
        $indicator_record->indicator()->associate($indicator);
        $indicator_record->save();

        $indicator = Indicator::findByName($indicator_names[2][1])->first();
        $indicator_record = clone $indicator_record_obj;
        $indicator_record->value = 72;
        $indicator_record->indicator()->associate($indicator);
        $indicator_record->save();

        $indicator = Indicator::findByName($indicator_names[2][2])->first();
        $indicator_record = clone $indicator_record_obj;
        $indicator_record->value = 78;
        $indicator_record->indicator()->associate($indicator);
        $indicator_record->save();

        $indicator = Indicator::findByName($indicator_names[2][3])->first();
        $indicator_record = clone $indicator_record_obj;
        $indicator_record->value = 96;
        $indicator_record->indicator()->associate($indicator);
        $indicator_record->save();

        $indicator = Indicator::findByName($indicator_names[2][4])->first();
        $indicator_record = clone $indicator_record_obj;
        $indicator_record->value = 72;
        $indicator_record->indicator()->associate($indicator);
        $indicator_record->save();

        $indicator = Indicator::findByName($indicator_names[2][5])->first();
        $indicator_record = clone $indicator_record_obj;
        $indicator_record->value = 50;
        $indicator_record->indicator()->associate($indicator);
        $indicator_record->save();

        $indicator = Indicator::findByName($indicator_names[2][6])->first();
        $indicator_record = clone $indicator_record_obj;
        $indicator_record->value = 68;
        $indicator_record->indicator()->associate($indicator);
        $indicator_record->save();

        $indicator = Indicator::findByName($indicator_names[2][7])->first();
        $indicator_record = clone $indicator_record_obj;
        $indicator_record->value = 47;
        $indicator_record->indicator()->associate($indicator);
        $indicator_record->save();

        // Relatório 2016 - Middle School
        $indicator_record_obj = new IndicatorRecord;
        $indicator_record_obj->setDateAttribute('23/10/2016');
        $indicator_record_obj->school_grade_group = 'middle_school';

        $indicator = Indicator::findByName($indicator_names[0][0])->first();
        $indicator_record = clone $indicator_record_obj;
        $indicator_record->value = 17;
        $indicator_record->indicator()->associate($indicator);
        $indicator_record->save();

        $indicator = Indicator::findByName($indicator_names[0][1])->first();
        $indicator_record = clone $indicator_record_obj;
        $indicator_record->value = 41;
        $indicator_record->indicator()->associate($indicator);
        $indicator_record->save();

        $indicator = Indicator::findByName($indicator_names[0][2])->first();
        $indicator_record = clone $indicator_record_obj;
        $indicator_record->value = 56;
        $indicator_record->indicator()->associate($indicator);
        $indicator_record->save();

        $indicator = Indicator::findByName($indicator_names[0][3])->first();
        $indicator_record = clone $indicator_record_obj;
        $indicator_record->value = 70;
        $indicator_record->indicator()->associate($indicator);
        $indicator_record->save();

        $indicator = Indicator::findByName($indicator_names[0][4])->first();
        $indicator_record = clone $indicator_record_obj;
        $indicator_record->value = 52;
        $indicator_record->indicator()->associate($indicator);
        $indicator_record->save();

        $indicator = Indicator::findByName($indicator_names[0][5])->first();
        $indicator_record = clone $indicator_record_obj;
        $indicator_record->value = 72;
        $indicator_record->indicator()->associate($indicator);
        $indicator_record->save();

        $indicator = Indicator::findByName($indicator_names[0][6])->first();
        $indicator_record = clone $indicator_record_obj;
        $indicator_record->value = 83;
        $indicator_record->indicator()->associate($indicator);
        $indicator_record->save();

        $indicator = Indicator::findByName($indicator_names[0][7])->first();
        $indicator_record = clone $indicator_record_obj;
        $indicator_record->value = 64;
        $indicator_record->indicator()->associate($indicator);
        $indicator_record->save();

        $indicator = Indicator::findByName($indicator_names[1][0])->first();
        $indicator_record = clone $indicator_record_obj;
        $indicator_record->value = 0;
        $indicator_record->indicator()->associate($indicator);
        $indicator_record->save();

        $indicator = Indicator::findByName($indicator_names[1][1])->first();
        $indicator_record = clone $indicator_record_obj;
        $indicator_record->value = 0;
        $indicator_record->indicator()->associate($indicator);
        $indicator_record->save();

        $indicator = Indicator::findByName($indicator_names[1][2])->first();
        $indicator_record = clone $indicator_record_obj;
        $indicator_record->value = 0;
        $indicator_record->indicator()->associate($indicator);
        $indicator_record->save();

        $indicator = Indicator::findByName($indicator_names[1][3])->first();
        $indicator_record = clone $indicator_record_obj;
        $indicator_record->value = 0;
        $indicator_record->indicator()->associate($indicator);
        $indicator_record->save();

        $indicator = Indicator::findByName($indicator_names[1][4])->first();
        $indicator_record = clone $indicator_record_obj;
        $indicator_record->value = 0;
        $indicator_record->indicator()->associate($indicator);
        $indicator_record->save();

        $indicator = Indicator::findByName($indicator_names[1][5])->first();
        $indicator_record = clone $indicator_record_obj;
        $indicator_record->value = 0;
        $indicator_record->indicator()->associate($indicator);
        $indicator_record->save();

        $indicator = Indicator::findByName($indicator_names[1][6])->first();
        $indicator_record = clone $indicator_record_obj;
        $indicator_record->value = 0;
        $indicator_record->indicator()->associate($indicator);
        $indicator_record->save();

        $indicator = Indicator::findByName($indicator_names[1][7])->first();
        $indicator_record = clone $indicator_record_obj;
        $indicator_record->value = 0;
        $indicator_record->indicator()->associate($indicator);
        $indicator_record->save();

        $indicator = Indicator::findByName($indicator_names[2][0])->first();
        $indicator_record = clone $indicator_record_obj;
        $indicator_record->value = 0;
        $indicator_record->indicator()->associate($indicator);
        $indicator_record->save();

        $indicator = Indicator::findByName($indicator_names[2][1])->first();
        $indicator_record = clone $indicator_record_obj;
        $indicator_record->value = 0;
        $indicator_record->indicator()->associate($indicator);
        $indicator_record->save();

        $indicator = Indicator::findByName($indicator_names[2][2])->first();
        $indicator_record = clone $indicator_record_obj;
        $indicator_record->value = 0;
        $indicator_record->indicator()->associate($indicator);
        $indicator_record->save();

        $indicator = Indicator::findByName($indicator_names[2][3])->first();
        $indicator_record = clone $indicator_record_obj;
        $indicator_record->value = 0;
        $indicator_record->indicator()->associate($indicator);
        $indicator_record->save();

        $indicator = Indicator::findByName($indicator_names[2][4])->first();
        $indicator_record = clone $indicator_record_obj;
        $indicator_record->value = 0;
        $indicator_record->indicator()->associate($indicator);
        $indicator_record->save();

        $indicator = Indicator::findByName($indicator_names[2][5])->first();
        $indicator_record = clone $indicator_record_obj;
        $indicator_record->value = 0;
        $indicator_record->indicator()->associate($indicator);
        $indicator_record->save();

        $indicator = Indicator::findByName($indicator_names[2][6])->first();
        $indicator_record = clone $indicator_record_obj;
        $indicator_record->value = 0;
        $indicator_record->indicator()->associate($indicator);
        $indicator_record->save();

        $indicator = Indicator::findByName($indicator_names[2][7])->first();
        $indicator_record = clone $indicator_record_obj;
        $indicator_record->value = 0;
        $indicator_record->indicator()->associate($indicator);
        $indicator_record->save();

        // Relatório 2016 - high_school
        $indicator_record_obj = new IndicatorRecord;
        $indicator_record_obj->setDateAttribute('23/10/2016');
        $indicator_record_obj->school_grade_group = 'high_school';

        $indicator = Indicator::findByName($indicator_names[0][0])->first();
        $indicator_record = clone $indicator_record_obj;
        $indicator_record->value = 32;
        $indicator_record->indicator()->associate($indicator);
        $indicator_record->save();

        $indicator = Indicator::findByName($indicator_names[0][1])->first();
        $indicator_record = clone $indicator_record_obj;
        $indicator_record->value = 42;
        $indicator_record->indicator()->associate($indicator);
        $indicator_record->save();

        $indicator = Indicator::findByName($indicator_names[0][2])->first();
        $indicator_record = clone $indicator_record_obj;
        $indicator_record->value = 53;
        $indicator_record->indicator()->associate($indicator);
        $indicator_record->save();

        $indicator = Indicator::findByName($indicator_names[0][3])->first();
        $indicator_record = clone $indicator_record_obj;
        $indicator_record->value = 70;
        $indicator_record->indicator()->associate($indicator);
        $indicator_record->save();

        $indicator = Indicator::findByName($indicator_names[0][4])->first();
        $indicator_record = clone $indicator_record_obj;
        $indicator_record->value = 66;
        $indicator_record->indicator()->associate($indicator);
        $indicator_record->save();

        $indicator = Indicator::findByName($indicator_names[0][5])->first();
        $indicator_record = clone $indicator_record_obj;
        $indicator_record->value = 76;
        $indicator_record->indicator()->associate($indicator);
        $indicator_record->save();

        $indicator = Indicator::findByName($indicator_names[0][6])->first();
        $indicator_record = clone $indicator_record_obj;
        $indicator_record->value = 82;
        $indicator_record->indicator()->associate($indicator);
        $indicator_record->save();

        $indicator = Indicator::findByName($indicator_names[0][7])->first();
        $indicator_record = clone $indicator_record_obj;
        $indicator_record->value = 55;
        $indicator_record->indicator()->associate($indicator);
        $indicator_record->save();

        $indicator = Indicator::findByName($indicator_names[1][0])->first();
        $indicator_record = clone $indicator_record_obj;
        $indicator_record->value = 25;
        $indicator_record->indicator()->associate($indicator);
        $indicator_record->save();

        $indicator = Indicator::findByName($indicator_names[1][1])->first();
        $indicator_record = clone $indicator_record_obj;
        $indicator_record->value = 42;
        $indicator_record->indicator()->associate($indicator);
        $indicator_record->save();

        $indicator = Indicator::findByName($indicator_names[1][2])->first();
        $indicator_record = clone $indicator_record_obj;
        $indicator_record->value = 100;
        $indicator_record->indicator()->associate($indicator);
        $indicator_record->save();

        $indicator = Indicator::findByName($indicator_names[1][3])->first();
        $indicator_record = clone $indicator_record_obj;
        $indicator_record->value = 100;
        $indicator_record->indicator()->associate($indicator);
        $indicator_record->save();

        $indicator = Indicator::findByName($indicator_names[1][4])->first();
        $indicator_record = clone $indicator_record_obj;
        $indicator_record->value = 100;
        $indicator_record->indicator()->associate($indicator);
        $indicator_record->save();

        $indicator = Indicator::findByName($indicator_names[1][5])->first();
        $indicator_record = clone $indicator_record_obj;
        $indicator_record->value = 100;
        $indicator_record->indicator()->associate($indicator);
        $indicator_record->save();

        $indicator = Indicator::findByName($indicator_names[1][6])->first();
        $indicator_record = clone $indicator_record_obj;
        $indicator_record->value = 100;
        $indicator_record->indicator()->associate($indicator);
        $indicator_record->save();

        $indicator = Indicator::findByName($indicator_names[1][7])->first();
        $indicator_record = clone $indicator_record_obj;
        $indicator_record->value = 33;
        $indicator_record->indicator()->associate($indicator);
        $indicator_record->save();

        $indicator = Indicator::findByName($indicator_names[2][0])->first();
        $indicator_record = clone $indicator_record_obj;
        $indicator_record->value = 93;
        $indicator_record->indicator()->associate($indicator);
        $indicator_record->save();

        $indicator = Indicator::findByName($indicator_names[2][1])->first();
        $indicator_record = clone $indicator_record_obj;
        $indicator_record->value = 74;
        $indicator_record->indicator()->associate($indicator);
        $indicator_record->save();

        $indicator = Indicator::findByName($indicator_names[2][2])->first();
        $indicator_record = clone $indicator_record_obj;
        $indicator_record->value = 88;
        $indicator_record->indicator()->associate($indicator);
        $indicator_record->save();

        $indicator = Indicator::findByName($indicator_names[2][3])->first();
        $indicator_record = clone $indicator_record_obj;
        $indicator_record->value = 97;
        $indicator_record->indicator()->associate($indicator);
        $indicator_record->save();

        $indicator = Indicator::findByName($indicator_names[2][4])->first();
        $indicator_record = clone $indicator_record_obj;
        $indicator_record->value = 86;
        $indicator_record->indicator()->associate($indicator);
        $indicator_record->save();

        $indicator = Indicator::findByName($indicator_names[2][5])->first();
        $indicator_record = clone $indicator_record_obj;
        $indicator_record->value = 69;
        $indicator_record->indicator()->associate($indicator);
        $indicator_record->save();

        $indicator = Indicator::findByName($indicator_names[2][6])->first();
        $indicator_record = clone $indicator_record_obj;
        $indicator_record->value = 66;
        $indicator_record->indicator()->associate($indicator);
        $indicator_record->save();

        $indicator = Indicator::findByName($indicator_names[2][7])->first();
        $indicator_record = clone $indicator_record_obj;
        $indicator_record->value = 56;
        $indicator_record->indicator()->associate($indicator);
        $indicator_record->save();

        // Classes Records
        $indicator = Indicator::findByName('Faltas')->first();
        $indicator_class_record_obj = new IndicatorRecord;
        $indicator_class_record_obj->setDateAttribute('01/03/2016');

        $year = '2016';
        $school_classes = SchoolClass::where('year', '=', $year)->get();

        foreach ($school_classes as $school_class) {
            $value = random_int(5, 10);
            if (!is_null($value)) {
                $indicator_record = clone $indicator_class_record_obj;
                $indicator_record->value = (float)$value;
                $indicator_record->schoolClass()->associate($school_class);
                $indicator_record->indicator()->associate($indicator);
                $indicator_record->save();
            }
        }

        // Classes Records
        $indicator_class_record_obj = new IndicatorRecord;
        $indicator_class_record_obj->setDateAttribute('01/03/2017');

        $year = '2017';
        $school_classes = SchoolClass::where('year', '=', $year)->get();

        foreach ($school_classes as $school_class) {
            $value = random_int(5, 10);
            if (!is_null($value)) {
                $indicator_record = clone $indicator_class_record_obj;
                $indicator_record->value = (float)$value;
                $indicator_record->schoolClass()->associate($school_class);
                $indicator_record->indicator()->associate($indicator);
                $indicator_record->save();
            }
        }

        $this->enableForeignKeys();
    }
}
