<?php

use App\Models\CEAP\TopicTag;
use Illuminate\Database\Seeder;

class TopicTagsTableSeeder extends Seeder
{
    use DisableForeignKeys, TruncateTable;

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $table_name = 'topic_tags';

        $this->disableForeignKeys();
        $this->truncate($table_name);

        $topic_tag_obj = new TopicTag();

        $topic_tag = clone $topic_tag_obj;
        $topic_tag->name = 'Horário';
        $topic_tag->save();

        $topic_tag = clone $topic_tag_obj;
        $topic_tag->name = 'Aulas';
        $topic_tag->save();

        $topic_tag = clone $topic_tag_obj;
        $topic_tag->name = 'Uniforme';
        $topic_tag->save();

        $topic_tag = clone $topic_tag_obj;
        $topic_tag->name = 'Materiais Didáticos';
        $topic_tag->save();

        $topic_tag = clone $topic_tag_obj;
        $topic_tag->name = 'Atividade Física';
        $topic_tag->save();

        $topic_tag = clone $topic_tag_obj;
        $topic_tag->name = 'Atividade Extraclasse';
        $topic_tag->save();

        $topic_tag = clone $topic_tag_obj;
        $topic_tag->name = 'Indisciplina';
        $topic_tag->save();

        $topic_tag = clone $topic_tag_obj;
        $topic_tag->name = 'Banheiro';
        $topic_tag->save();

        $topic_tag = clone $topic_tag_obj;
        $topic_tag->name = 'Presença';
        $topic_tag->save();

        $topic_tag = clone $topic_tag_obj;
        $topic_tag->name = 'Falta de Professor';
        $topic_tag->save();

        $this->enableForeignKeys();
    }
}
