<?php

use App\Models\CEAP\SchoolClass;
use Illuminate\Database\Seeder;

class SchoolClassesTableSeeder extends Seeder
{
    use DisableForeignKeys, TruncateTable;

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $table_name = 'school_classes';

        $this->disableForeignKeys();
        $this->truncate($table_name);

        $model = new SchoolClass();
        $model->year = 2016;
        $model->school_grade_group = 'middle_school';

        $class = clone $model;
        $class->grade = 6;
        $class->room = 'A';
        $class->save();

        $class = clone $model;
        $class->grade = 6;
        $class->room = 'B';
        $class->save();

        $class = clone $model;
        $class->grade = 7;
        $class->room = 'A';
        $class->save();

        $class = clone $model;
        $class->grade = 7;
        $class->room = 'B';
        $class->save();

        $class = clone $model;
        $class->grade = 8;
        $class->room = 'A';
        $class->save();

        $class = clone $model;
        $class->grade = 8;
        $class->room = 'B';
        $class->save();

        $class = clone $model;
        $class->grade = 9;
        $class->room = 'A';
        $class->save();

        $class = clone $model;
        $class->grade = 9;
        $class->room = 'B';
        $class->save();

        $model->school_grade_group = 'high_school';

        $class = clone $model;
        $class->grade = 11;
        $class->room = 'A';
        $class->save();

        $class = clone $model;
        $class->grade = 11;
        $class->room = 'B';
        $class->save();

        $class = clone $model;
        $class->grade = 12;
        $class->room = 'A';
        $class->save();

        $class = clone $model;
        $class->grade = 12;
        $class->room = 'B';
        $class->save();

        $class = clone $model;
        $class->grade = 13;
        $class->room = 'A';
        $class->save();

        $class = clone $model;
        $class->grade = 13;
        $class->room = 'B';
        $class->save();

        $model = new SchoolClass();
        $model->year = 2017;
        $model->school_grade_group = 'middle_school';

        $class = clone $model;
        $class->grade = 6;
        $class->room = 'A';
        $class->save();

        $class = clone $model;
        $class->grade = 6;
        $class->room = 'B';
        $class->save();

        $class = clone $model;
        $class->grade = 7;
        $class->room = 'A';
        $class->save();

        $class = clone $model;
        $class->grade = 7;
        $class->room = 'B';
        $class->save();

        $class = clone $model;
        $class->grade = 8;
        $class->room = 'A';
        $class->save();

        $class = clone $model;
        $class->grade = 8;
        $class->room = 'B';
        $class->save();

        $class = clone $model;
        $class->grade = 9;
        $class->room = 'A';
        $class->save();

        $class = clone $model;
        $class->grade = 9;
        $class->room = 'B';
        $class->save();

        $model->school_grade_group = 'high_school';

        $class = clone $model;
        $class->grade = 11;
        $class->room = 'A';
        $class->save();

        $class = clone $model;
        $class->grade = 11;
        $class->room = 'B';
        $class->save();

        $class = clone $model;
        $class->grade = 12;
        $class->room = 'A';
        $class->save();

        $class = clone $model;
        $class->grade = 12;
        $class->room = 'B';
        $class->save();

        $class = clone $model;
        $class->grade = 13;
        $class->room = 'A';
        $class->save();

        $class = clone $model;
        $class->grade = 13;
        $class->room = 'B';
        $class->save();

        $this->enableForeignKeys();
    }
}
