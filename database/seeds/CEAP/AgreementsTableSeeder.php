<?php

use App\Models\CEAP\Agreement;
use App\Models\CEAP\AgreementRole;
use App\Models\CEAP\EvaluationCycle;
use App\Models\CEAP\TopicTag;
use Carbon\Carbon;
use Illuminate\Database\Seeder;

class AgreementsTableSeeder extends Seeder
{
    use DisableForeignKeys, TruncateTable;

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $table_name = 'agreements';

        $this->disableForeignKeys();
        $this->truncate($table_name);

        $model = new Agreement();
        $model->title = 'Manter o bom convivio na escola';
        $model->details = 'Para que o ambiente da escola seja agradável, todos devem se respeitar.\n' . ' Além disso, todos devem prezar pela limpeza do ambiente da escola.\n' . ' Também devemos lembrar de cumprimentar os amigos e evitar criar conflitos com os colegas.';
        $model->evaluationCycle()->associate(EvaluationCycle::find(3));
        $model->created_at = Carbon::now();
        $model->updated_at = Carbon::now();
        $model->save();

        foreach ([1, 2] as $id) {
            $tag = TopicTag::find($id);
            $model->topicTags()->attach($tag);
        }

        foreach ([1, 2, 5] as $id) {
            $role = AgreementRole::find($id);
            $model->agreementRoles()->attach($role);
        }

        $this->enableForeignKeys();
    }
}
