<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

/**
 * Class PermissionRoleTableSeeder.
 */
class PermissionRoleTableSeeder extends Seeder
{
    use DisableForeignKeys;

    /**
     * Run the database seed.
     *
     * @return void
     */
    public function run()
    {
        $this->disableForeignKeys();

        // Create Roles
        $admin = Role::create(['name' => config('access.users.admin_role')]);
        $diretor = Role::create(['name' => 'diretor']);
        $secretaria = Role::create(['name' => 'secretaria']);
        $professor = Role::create(['name' => 'professor']);

        // Create Permissions
        $view_backend = Permission::create(['name' => 'view-backend']);
        $manage_users = Permission::create(['name' => 'manage-users']);
//        $manageUsers->display_name = 'ceap.permissions.manage-users';
        $manage_roles = Permission::create(['name' => 'manage-roles']);
        $manage_ceap = Permission::create(['name' => 'manage-ceap']);
//        $manageRoles->display_name = 'ceap.permissions.manage-roles';
        // CEAP Permissions
        $can_create = Permission::create(['name' => 'can-create']);
//        $ceap->display_name = 'ceap.permissions.can-create';
        $can_edit = Permission::create(['name' => 'can-edit']);
//        $ceap->display_name = 'ceap.permissions.can-edit';
        $can_read = Permission::create(['name' => 'can-read']);
//        $ceap->display_name = 'ceap.permissions.can-read';
        $can_trash = Permission::create(['name' => 'can-trash']);
//        $ceap->display_name = 'ceap.permissions.can-trash';
        $can_restore = Permission::create(['name' => 'can-restore']);
//        $ceap->display_name = 'ceap.permissions.can-restore';
        $can_delete = Permission::create(['name' => 'can-delete']);
//        $ceap->display_name = 'ceap.permissions.can-delete';

        // ALWAYS GIVE ADMIN ROLE ALL PERMISSIONS
        $admin->givePermissionTo($view_backend, $manage_users, $manage_roles, $manage_ceap, $can_create, $can_edit, $can_delete,
            $can_read, $can_trash, $can_restore);

        // Assign Permissions to other Roles
        $diretor->givePermissionTo($view_backend, $manage_users, $manage_roles, $manage_ceap, $can_create, $can_edit, $can_delete,
            $can_read, $can_trash, $can_restore);

        $secretaria->givePermissionTo($can_create, $can_edit, $can_read, $can_trash, $can_restore);

        $professor->givePermissionTo($can_read);

        $this->enableForeignKeys();
    }
}
