<?php

use App\Models\Auth\User;
use Illuminate\Database\Seeder;

/**
 * Class UserTableSeeder.
 */
class UserTableSeeder extends Seeder
{
    use DisableForeignKeys;

    /**
     * Run the database seed.
     *
     * @return void
     */
    public function run()
    {
        $this->disableForeignKeys();

        // Add the master administrator, user id of 1
        User::create([
            'first_name'        => 'Admin',
            'last_name'         => 'Istrator',
            'email'             => 'admin@admin.com',
            'password'          => bcrypt('CEAP12345678'),
            'confirmation_code' => md5(uniqid(mt_rand(), true)),
            'confirmed'         => true,
        ]);

        User::create([
            'first_name'        => 'Diretor',
            'last_name'         => 'User',
            'email'             => 'diretor@diretor.com',
            'password'          => bcrypt('CEAP12345678'),
            'confirmation_code' => md5(uniqid(mt_rand(), true)),
            'confirmed'         => true,
        ]);
        User::create([
            'first_name'        => 'Secretária',
            'last_name'         => 'User',
            'email'             => 'secretaria@secretaria.com',
            'password'          => bcrypt('CEAP12345678'),
            'confirmation_code' => md5(uniqid(mt_rand(), true)),
            'confirmed'         => true,
        ]);
        User::create([
            'first_name'        => 'Professor',
            'last_name'         => 'User',
            'email'             => 'professor@professor.com',
            'password'          => bcrypt('CEAP12345678'),
            'confirmation_code' => md5(uniqid(mt_rand(), true)),
            'confirmed'         => true,
        ]);

        $this->enableForeignKeys();
    }
}
