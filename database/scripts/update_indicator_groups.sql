ALTER TABLE `indicator_groups`
  CHANGE `type` `type` ENUM ('ceap', 'rooms', 'school')
CHARACTER SET utf8mb4
COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'school';

UPDATE `indicator_groups`
SET `type` = 'ceap'
WHERE `id` = 1;
