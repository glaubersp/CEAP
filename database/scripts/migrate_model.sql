SET @OLD_UNIQUE_CHECKS = @@UNIQUE_CHECKS, UNIQUE_CHECKS = 0;
SET @OLD_FOREIGN_KEY_CHECKS = @@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS = 0;
SET @OLD_SQL_MODE = @@SQL_MODE, SQL_MODE = 'TRADITIONAL';

ALTER TABLE `ceap`.`indicators`
  DROP FOREIGN KEY `indicators_evaluation_category_id_foreign`,
  DROP FOREIGN KEY `indicators_evaluation_cycle_id_foreign`;

CREATE TABLE IF NOT EXISTS `ceap`.`action_agreement` (
  `id`           INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `action_id`    INT(10) UNSIGNED NOT NULL,
  `agreement_id` INT(10) UNSIGNED NOT NULL,
  `created_at`   TIMESTAMP        NULL     DEFAULT NULL,
  `updated_at`   TIMESTAMP        NULL     DEFAULT NULL,
  PRIMARY KEY (`id`),
  INDEX `action_agreement_action_id_foreign` (`action_id` ASC),
  INDEX `action_agreement_agreement_id_foreign` (`agreement_id` ASC),
  CONSTRAINT `action_agreement_action_id_foreign`
  FOREIGN KEY (`action_id`)
  REFERENCES `ceap`.`actions` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `action_agreement_agreement_id_foreign`
  FOREIGN KEY (`agreement_id`)
  REFERENCES `ceap`.`agreements` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE
)
  ENGINE = InnoDB
  DEFAULT CHARACTER SET = utf8mb4
  COLLATE = utf8mb4_unicode_ci;

ALTER TABLE `ceap`.`action_evaluations`
  CHANGE COLUMN `rating` `rating` INT(11) NULL DEFAULT NULL;

CREATE TABLE IF NOT EXISTS `ceap`.`indicator_groups` (
  `id`                   INT(10) UNSIGNED             NOT NULL AUTO_INCREMENT,
  `name`                 VARCHAR(191)
                         CHARACTER SET 'utf8mb4'
                         COLLATE 'utf8mb4_unicode_ci' NOT NULL,
  `description`          TEXT CHARACTER SET 'utf8mb4'
                         COLLATE 'utf8mb4_unicode_ci' NOT NULL,
  `type`                 ENUM ('rooms', 'school')
                         CHARACTER SET 'utf8mb4'
                         COLLATE 'utf8mb4_unicode_ci' NOT NULL,
  `evaluation_cycle_id`  INT(10) UNSIGNED             NOT NULL,
  `next_evaluation_date` DATE                         NOT NULL,
  `created_at`           TIMESTAMP                    NULL     DEFAULT NULL,
  `updated_at`           TIMESTAMP                    NULL     DEFAULT NULL,
  `deleted_at`           TIMESTAMP                    NULL     DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `indicator_groups_name_unique` (`name` ASC),
  INDEX `indicator_groups_evaluation_cycle_id_foreign` (`evaluation_cycle_id` ASC),
  CONSTRAINT `indicator_groups_evaluation_cycle_id_foreign`
  FOREIGN KEY (`evaluation_cycle_id`)
  REFERENCES `ceap`.`evaluation_cycles` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE
)
  ENGINE = InnoDB
  AUTO_INCREMENT = 3
  DEFAULT CHARACTER SET = utf8mb4
  COLLATE = utf8mb4_unicode_ci;

ALTER TABLE `ceap`.`indicators`
  DROP COLUMN `evaluation_cycle_id`,
  CHANGE COLUMN `evaluation_category_id` `evaluation_category_id` INT(10) UNSIGNED NULL DEFAULT NULL,
  ADD COLUMN `indicator_group_id` INT(10) UNSIGNED NOT NULL
  AFTER `description`,
  ADD INDEX `indicators_indicator_group_id_foreign` (`indicator_group_id` ASC),
  DROP INDEX `indicators_evaluation_cycle_id_foreign`;

ALTER TABLE `ceap`.`indicators`
  ADD CONSTRAINT `indicators_evaluation_category_id_foreign`
FOREIGN KEY (`evaluation_category_id`)
REFERENCES `ceap`.`evaluation_categories` (`id`)
  ON DELETE CASCADE
  ON UPDATE CASCADE,
  ADD CONSTRAINT `indicators_indicator_group_id_foreign`
FOREIGN KEY (`indicator_group_id`)
REFERENCES `ceap`.`indicator_groups` (`id`)
  ON DELETE CASCADE
  ON UPDATE CASCADE;


SET SQL_MODE = @OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS = @OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS = @OLD_UNIQUE_CHECKS;
