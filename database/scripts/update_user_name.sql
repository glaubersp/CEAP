ALTER TABLE `users`
  CHANGE `name` `first_name` VARCHAR(191)
CHARACTER SET utf8mb4
COLLATE utf8mb4_unicode_ci NOT NULL  DEFAULT '';
ALTER TABLE `users`
  ADD COLUMN `last_name` VARCHAR(191)
CHARACTER SET utf8mb4
COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT ''
  AFTER `first_name`;

DELIMITER $$
DROP PROCEDURE IF EXISTS update_users$$
CREATE PROCEDURE update_users()
  BEGIN
    DECLARE v_max INT DEFAULT 0;
    DECLARE v_counter INT DEFAULT 0;
    DECLARE v_id INT DEFAULT 0;
    DECLARE v_full_name VARCHAR(255);
    DECLARE v_first_name VARCHAR(255);
    DECLARE v_last_name VARCHAR(255);

    SET v_counter = 1;
    SELECT count(DISTINCT id)
    FROM users
    INTO v_max;
    WHILE v_counter <= v_max DO
      SELECT
        id,
        first_name
      FROM users
      WHERE id = v_counter
      INTO v_id, v_full_name;
      SELECT SUBSTRING_INDEX(v_full_name, ' ', 1)
      INTO v_first_name;
      SELECT SUBSTRING_INDEX(v_full_name, ' ', -1)
      INTO v_last_name;
      UPDATE users
      SET first_name = v_first_name, last_name = v_last_name
      WHERE id = v_id;
      SET v_counter = v_counter + 1;
    END WHILE;
    COMMIT;
  END$$
DELIMITER ;
CALL update_users();