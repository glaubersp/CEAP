# ************************************************************
# Sequel Pro SQL dump
# Version 4900
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: 127.0.0.1 (MySQL 5.7.19)
# Database: ceap
# Generation Time: 2017-08-15 14:14:36 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
SET NAMES utf8mb4;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

DELETE FROM `permissions` WHERE id=1;
DELETE FROM `permissions` WHERE id=2;
DELETE FROM `permissions` WHERE id=3;
INSERT INTO `permissions` (`id`, `NAME`, `display_name`, `created_at`, `updated_at`)
VALUES
	(1,'view-backend','ceap.permissions.view-backend','2017-08-14 19:26:59','2017-08-14 19:26:59'),
	(2,'manage-users','ceap.permissions.manage-users','2017-08-14 19:26:59','2017-08-14 19:26:59'),
	(3,'manage-roles','ceap.permissions.manage-roles','2017-08-14 19:26:59','2017-08-14 19:26:59'),
	(4,'can-create','ceap.permissions.can-create','2017-08-14 19:26:59','2017-08-14 19:26:59'),
	(5,'can-edit','ceap.permissions.can-edit','2017-08-14 19:26:59','2017-08-14 19:26:59'),
	(6,'can-read','ceap.permissions.can-read','2017-08-14 19:26:59','2017-08-14 19:26:59'),
	(7,'can-trash','ceap.permissions.can-trash','2017-08-14 19:26:59','2017-08-14 19:26:59'),
	(8,'can-restore','ceap.permissions.can-restore','2017-08-14 19:26:59','2017-08-14 19:26:59'),
	(9,'can-delete','ceap.permissions.can-delete','2017-08-14 19:27:00','2017-08-14 19:27:00');

/*!40000 ALTER TABLE `permissions` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
