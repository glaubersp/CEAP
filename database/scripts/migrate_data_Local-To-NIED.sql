SET @OLD_UNIQUE_CHECKS = @@UNIQUE_CHECKS, UNIQUE_CHECKS = 0;
SET @OLD_FOREIGN_KEY_CHECKS = @@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS = 0;
SET @OLD_SQL_MODE = @@SQL_MODE, SQL_MODE = 'TRADITIONAL';

ALTER TABLE `ceap`.`indicators`
  DROP FOREIGN KEY `indicators_evaluation_category_id_foreign`,
  DROP FOREIGN KEY `indicators_evaluation_cycle_id_foreign`;

CREATE TABLE IF NOT EXISTS `ceap`.`action_agreement` (
  `id`           INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `action_id`    INT(10) UNSIGNED NOT NULL,
  `agreement_id` INT(10) UNSIGNED NOT NULL,
  `created_at`   TIMESTAMP        NULL     DEFAULT NULL,
  `updated_at`   TIMESTAMP        NULL     DEFAULT NULL,
  PRIMARY KEY (`id`),
  INDEX `action_agreement_action_id_foreign` (`action_id` ASC),
  INDEX `action_agreement_agreement_id_foreign` (`agreement_id` ASC),
  CONSTRAINT `action_agreement_action_id_foreign`
  FOREIGN KEY (`action_id`)
  REFERENCES `ceap`.`actions` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `action_agreement_agreement_id_foreign`
  FOREIGN KEY (`agreement_id`)
  REFERENCES `ceap`.`agreements` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE
)
  ENGINE = InnoDB
  DEFAULT CHARACTER SET = utf8mb4
  COLLATE = utf8mb4_unicode_ci;

ALTER TABLE `ceap`.`action_evaluations`
  CHANGE COLUMN `rating` `rating` INT(11) NULL DEFAULT NULL;

CREATE TABLE IF NOT EXISTS `ceap`.`indicator_groups` (
  `id`                   INT(10) UNSIGNED             NOT NULL AUTO_INCREMENT,
  `name`                 VARCHAR(191)
                         CHARACTER SET 'utf8mb4'
                         COLLATE 'utf8mb4_unicode_ci' NOT NULL,
  `description`          TEXT CHARACTER SET 'utf8mb4'
                         COLLATE 'utf8mb4_unicode_ci' NOT NULL,
  `type`                 ENUM ('rooms', 'school')
                         CHARACTER SET 'utf8mb4'
                         COLLATE 'utf8mb4_unicode_ci' NOT NULL,
  `evaluation_cycle_id`  INT(10) UNSIGNED             NOT NULL,
  `next_evaluation_date` DATE                         NOT NULL,
  `created_at`           TIMESTAMP                    NULL     DEFAULT NULL,
  `updated_at`           TIMESTAMP                    NULL     DEFAULT NULL,
  `deleted_at`           TIMESTAMP                    NULL     DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `indicator_groups_name_unique` (`name` ASC),
  INDEX `indicator_groups_evaluation_cycle_id_foreign` (`evaluation_cycle_id` ASC),
  CONSTRAINT `indicator_groups_evaluation_cycle_id_foreign`
  FOREIGN KEY (`evaluation_cycle_id`)
  REFERENCES `ceap`.`evaluation_cycles` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE
)
  ENGINE = InnoDB
  AUTO_INCREMENT = 3
  DEFAULT CHARACTER SET = utf8mb4
  COLLATE = utf8mb4_unicode_ci;

ALTER TABLE `ceap`.`indicators`
  DROP COLUMN `evaluation_cycle_id`,
  CHANGE COLUMN `evaluation_category_id` `evaluation_category_id` INT(10) UNSIGNED NULL DEFAULT NULL,
  ADD COLUMN `indicator_group_id` INT(10) UNSIGNED NOT NULL
  AFTER `description`,
  ADD INDEX `indicators_indicator_group_id_foreign` (`indicator_group_id` ASC),
  DROP INDEX `indicators_evaluation_cycle_id_foreign`;

ALTER TABLE `ceap`.`indicators`
  ADD CONSTRAINT `indicators_evaluation_category_id_foreign`
FOREIGN KEY (`evaluation_category_id`)
REFERENCES `ceap`.`evaluation_categories` (`id`)
  ON DELETE CASCADE
  ON UPDATE CASCADE,
  ADD CONSTRAINT `indicators_indicator_group_id_foreign`
FOREIGN KEY (`indicator_group_id`)
REFERENCES `ceap`.`indicator_groups` (`id`)
  ON DELETE CASCADE
  ON UPDATE CASCADE;


DELETE FROM `agreement_agreement_role`
WHERE `id` = '74';
DELETE FROM `agreement_agreement_role`
WHERE `id` = '84';
DELETE FROM `agreement_agreement_role`
WHERE `id` = '83';
DELETE FROM `agreement_agreement_role`
WHERE `id` = '82';
DELETE FROM `agreement_agreement_role`
WHERE `id` = '81';
DELETE FROM `agreement_agreement_role`
WHERE `id` = '80';
DELETE FROM `agreement_agreement_role`
WHERE `id` = '79';
DELETE FROM `agreement_topic_tag`
WHERE `id` = '18';
DELETE FROM `agreement_agreement_role`
WHERE `id` = '77';
DELETE FROM `agreement_agreement_role`
WHERE `id` = '76';
DELETE FROM `agreement_agreement_role`
WHERE `id` = '75';
DELETE FROM `agreement_agreement_role`
WHERE `id` = '72';
DELETE FROM `agreement_agreement_role`
WHERE `id` = '73';
DELETE FROM `agreement_agreement_role`
WHERE `id` = '86';
DELETE FROM `agreement_agreement_role`
WHERE `id` = '71';
DELETE FROM `agreement_agreement_role`
WHERE `id` = '70';
DELETE FROM `agreement_agreement_role`
WHERE `id` = '69';
DELETE FROM `agreement_agreement_role`
WHERE `id` = '68';
DELETE FROM `agreement_agreement_role`
WHERE `id` = '67';
DELETE FROM `agreement_agreement_role`
WHERE `id` = '66';
DELETE FROM `agreement_agreement_role`
WHERE `id` = '65';
DELETE FROM `agreement_agreement_role`
WHERE `id` = '64';
DELETE FROM `agreement_agreement_role`
WHERE `id` = '63';
DELETE FROM `agreement_agreement_role`
WHERE `id` = '62';
DELETE FROM `agreement_agreement_role`
WHERE `id` = '85';
DELETE FROM `agreement_agreement_role`
WHERE `id` = '87';
DELETE FROM `agreement_agreement_role`
WHERE `id` = '60';
DELETE FROM `agreement_agreement_role`
WHERE `id` = '101';
DELETE FROM `evidences`
WHERE `id` = '39';
DELETE FROM `agreement_agreement_role`
WHERE `id` = '110';
DELETE FROM `agreement_agreement_role`
WHERE `id` = '109';
DELETE FROM `agreement_agreement_role`
WHERE `id` = '108';
DELETE FROM `agreement_agreement_role`
WHERE `id` = '107';
DELETE FROM `agreement_agreement_role`
WHERE `id` = '106';
DELETE FROM `agreement_agreement_role`
WHERE `id` = '105';
DELETE FROM `agreement_agreement_role`
WHERE `id` = '104';
DELETE FROM `agreement_agreement_role`
WHERE `id` = '103';
DELETE FROM `agreement_agreement_role`
WHERE `id` = '102';
DELETE FROM `agreement_agreement_role`
WHERE `id` = '100';
DELETE FROM `agreement_agreement_role`
WHERE `id` = '88';
DELETE FROM `agreement_agreement_role`
WHERE `id` = '99';
DELETE FROM `agreement_agreement_role`
WHERE `id` = '98';
DELETE FROM `agreement_agreement_role`
WHERE `id` = '97';
DELETE FROM `agreement_agreement_role`
WHERE `id` = '96';
DELETE FROM `agreement_agreement_role`
WHERE `id` = '95';
DELETE FROM `agreement_agreement_role`
WHERE `id` = '94';
DELETE FROM `agreement_agreement_role`
WHERE `id` = '93';
DELETE FROM `agreement_agreement_role`
WHERE `id` = '92';
DELETE FROM `agreement_agreement_role`
WHERE `id` = '91';
DELETE FROM `agreement_agreement_role`
WHERE `id` = '90';
DELETE FROM `agreement_agreement_role`
WHERE `id` = '89';
DELETE FROM `agreement_agreement_role`
WHERE `id` = '61';
DELETE FROM `agreement_agreement_role`
WHERE `id` = '78';
DELETE FROM `agreement_agreement_role`
WHERE `id` = '59';
DELETE FROM `migrations`
WHERE `id` = '44';
DELETE FROM `migrations`
WHERE `id` = '35';
DELETE FROM `migrations`
WHERE `id` = '36';
DELETE FROM `migrations`
WHERE `id` = '37';
DELETE FROM `migrations`
WHERE `id` = '38';
DELETE FROM `migrations`
WHERE `id` = '39';
DELETE FROM `migrations`
WHERE `id` = '40';
DELETE FROM `migrations`
WHERE `id` = '41';
DELETE FROM `migrations`
WHERE `id` = '42';
DELETE FROM `migrations`
WHERE `id` = '43';
DELETE FROM `migrations`
WHERE `id` = '45';
DELETE FROM `migrations`
WHERE `id` = '33';
DELETE FROM `migrations`
WHERE `id` = '46';
DELETE FROM `action_evaluations`
WHERE `id` = '11';
DELETE FROM `action_evaluations`
WHERE `id` = '10';
DELETE FROM `action_evaluations`
WHERE `id` = '9';
DELETE FROM `action_evaluations`
WHERE `id` = '8';
DELETE FROM `action_evaluations`
WHERE `id` = '7';
DELETE FROM `action_evaluations`
WHERE `id` = '6';
DELETE FROM `action_evaluations`
WHERE `id` = '5';
DELETE FROM `agreement_agreement_role`
WHERE `id` = '58';
DELETE FROM `migrations`
WHERE `id` = '34';
DELETE FROM `agreements`
WHERE `id` = '18';
DELETE FROM `migrations`
WHERE `id` = '32';
DELETE FROM `agreement_agreement_role`
WHERE `id` = '55';
DELETE FROM `migrations`
WHERE `id` = '30';
DELETE FROM `migrations`
WHERE `id` = '29';
DELETE FROM `migrations`
WHERE `id` = '28';
DELETE FROM `migrations`
WHERE `id` = '27';
DELETE FROM `migrations`
WHERE `id` = '31';
DELETE FROM `agreement_agreement_role`
WHERE `id` = '57';
DELETE FROM `agreement_agreement_role`
WHERE `id` = '56';

INSERT INTO `migrations` VALUES ('13', '2016_11_29_001040_create_goals_table', '1');
INSERT INTO `indicator_records`
VALUES ('108', '6.00', '2016-03-01', '25', '12', NULL, '2017-05-02 10:01:47', '2017-05-02 10:01:47', NULL);
INSERT INTO `indicator_records`
VALUES ('117', '9.00', '2017-03-01', '25', '21', NULL, '2017-05-02 10:01:47', '2017-05-02 10:01:47', NULL);
INSERT INTO `indicator_records`
VALUES ('116', '8.00', '2017-03-01', '25', '20', NULL, '2017-05-02 10:01:47', '2017-05-02 10:01:47', NULL);
INSERT INTO `indicator_records`
VALUES ('115', '7.00', '2017-03-01', '25', '19', NULL, '2017-05-02 10:01:47', '2017-05-02 10:01:47', NULL);
INSERT INTO `indicator_records`
VALUES ('114', '8.00', '2017-03-01', '25', '18', NULL, '2017-05-02 10:01:47', '2017-05-02 10:01:47', NULL);
INSERT INTO `indicator_records`
VALUES ('113', '8.00', '2017-03-01', '25', '17', NULL, '2017-05-02 10:01:47', '2017-05-02 10:01:47', NULL);
INSERT INTO `indicator_records`
VALUES ('112', '7.00', '2017-03-01', '25', '16', NULL, '2017-05-02 10:01:47', '2017-05-02 10:01:47', NULL);
INSERT INTO `indicator_records`
VALUES ('111', '10.00', '2017-03-01', '25', '15', NULL, '2017-05-02 10:01:47', '2017-05-02 10:01:47', NULL);
INSERT INTO `indicator_records`
VALUES ('110', '5.00', '2016-03-01', '25', '14', NULL, '2017-05-02 10:01:47', '2017-05-02 10:01:47', NULL);
INSERT INTO `migrations` VALUES ('22', '2016_12_06_121525_create_agreement_topic_tag_table', '1');
INSERT INTO `indicator_records`
VALUES ('109', '8.00', '2016-03-01', '25', '13', NULL, '2017-05-02 10:01:47', '2017-05-02 10:01:47', NULL);
INSERT INTO `indicator_records`
VALUES ('107', '6.00', '2016-03-01', '25', '11', NULL, '2017-05-02 10:01:47', '2017-05-02 10:01:47', NULL);
INSERT INTO `indicator_records`
VALUES ('119', '9.00', '2017-03-01', '25', '23', NULL, '2017-05-02 10:01:47', '2017-05-02 10:01:47', NULL);
INSERT INTO `indicator_records`
VALUES ('106', '5.00', '2016-03-01', '25', '10', NULL, '2017-05-02 10:01:47', '2017-05-02 10:01:47', NULL);
INSERT INTO `indicator_records`
VALUES ('105', '9.00', '2016-03-01', '25', '9', NULL, '2017-05-02 10:01:47', '2017-05-02 10:01:47', NULL);
INSERT INTO `indicator_records`
VALUES ('104', '6.00', '2016-03-01', '25', '8', NULL, '2017-05-02 10:01:47', '2017-05-02 10:01:47', NULL);
INSERT INTO `indicator_records`
VALUES ('103', '5.00', '2016-03-01', '25', '7', NULL, '2017-05-02 10:01:47', '2017-05-02 10:01:47', NULL);
INSERT INTO `indicator_records`
VALUES ('102', '7.00', '2016-03-01', '25', '6', NULL, '2017-05-02 10:01:47', '2017-05-02 10:01:47', NULL);
INSERT INTO `indicator_records`
VALUES ('101', '9.00', '2016-03-01', '25', '5', NULL, '2017-05-02 10:01:47', '2017-05-02 10:01:47', NULL);
INSERT INTO `indicator_records`
VALUES ('100', '5.00', '2016-03-01', '25', '4', NULL, '2017-05-02 10:01:47', '2017-05-02 10:01:47', NULL);
INSERT INTO `indicator_records`
VALUES ('99', '10.00', '2016-03-01', '25', '3', NULL, '2017-05-02 10:01:47', '2017-05-02 10:01:47', NULL);
INSERT INTO `indicator_records`
VALUES ('98', '10.00', '2016-03-01', '25', '2', NULL, '2017-05-02 10:01:47', '2017-05-02 10:01:47', NULL);
INSERT INTO `indicator_records`
VALUES ('97', '6.00', '2016-03-01', '25', '1', NULL, '2017-05-02 10:01:47', '2017-05-02 10:01:47', NULL);
INSERT INTO `migrations` VALUES ('23', '2016_12_06_121530_create_agreement_agreement_role_table', '1');
INSERT INTO `indicator_records`
VALUES ('118', '6.00', '2017-03-01', '25', '22', NULL, '2017-05-02 10:01:47', '2017-05-02 10:01:47', NULL);
INSERT INTO `indicator_records`
VALUES ('120', '5.00', '2017-03-01', '25', '24', NULL, '2017-05-02 10:01:47', '2017-05-02 10:01:47', NULL);
INSERT INTO `migrations` VALUES ('12', '2016_11_29_001030_create_indicator_records_table', '1');
INSERT INTO `migrations` VALUES ('9', '2016_11_29_000030_create_school_classes_table', '1');
INSERT INTO `migrations` VALUES ('11', '2016_11_29_001020_create_indicators_table', '1');
INSERT INTO `migrations` VALUES ('14', '2016_11_29_001045_create_goal_evaluations_table', '1');
INSERT INTO `migrations` VALUES ('15', '2016_11_29_001050_create_actions_table', '1');
INSERT INTO `indicator_groups` VALUES
  ('1', 'Indicadores CEAP', 'Indicadores associados ao questionário TetraAnalytics utilizado pelo projeto.', 'school',
   '6', '2017-11-02', '2017-05-02 10:01:45', '2017-05-02 10:01:45', NULL);
INSERT INTO `migrations` VALUES ('16', '2016_11_29_001100_create_action_user_table', '1');
INSERT INTO `migrations` VALUES ('17', '2016_11_29_001110_create_action_evaluations_table', '1');
INSERT INTO `migrations` VALUES ('18', '2016_12_06_110030_create_topic_tags_table', '1');
INSERT INTO `migrations` VALUES ('19', '2016_12_06_111725_create_agreement_roles_table', '1');
INSERT INTO `migrations` VALUES ('20', '2016_12_06_111730_create_agreement_monitoring_strategies_table', '1');
INSERT INTO `migrations` VALUES ('10', '2016_11_29_001010_create_indicator_groups_table', '1');
INSERT INTO `migrations` VALUES ('8', '2016_11_29_000020_create_evaluation_cycles_table', '1');
INSERT INTO `indicator_records`
VALUES ('121', '8.00', '2017-03-01', '25', '25', NULL, '2017-05-02 10:01:47', '2017-05-02 10:01:47', NULL);
INSERT INTO `migrations` VALUES ('7', '2016_11_29_000010_create_evaluation_categories_table', '1');
INSERT INTO `migrations` VALUES ('6', '2016_07_03_062439_create_history_tables', '1');
INSERT INTO `migrations` VALUES ('5', '2015_12_29_015055_setup_access_tables', '1');
INSERT INTO `migrations` VALUES ('4', '2015_12_28_171741_create_social_logins_table', '1');
INSERT INTO `migrations` VALUES ('21', '2016_12_06_121519_create_agreements_table', '1');
INSERT INTO `migrations` VALUES ('2', '2014_10_12_000000_create_users_table', '1');
INSERT INTO `migrations` VALUES ('1', '2014_04_02_193005_create_translations_table', '1');
INSERT INTO `indicators` VALUES
  ('25', 'Faltas', '<p>Total de faltas por salas.</p>', '2', '1', '2017-05-02 10:01:45', '2017-05-02 10:01:45', NULL);
INSERT INTO `indicator_records`
VALUES ('124', '5.00', '2017-03-01', '25', '28', NULL, '2017-05-02 10:01:47', '2017-05-02 10:01:47', NULL);
INSERT INTO `indicator_records`
VALUES ('123', '9.00', '2017-03-01', '25', '27', NULL, '2017-05-02 10:01:47', '2017-05-02 10:01:47', NULL);
INSERT INTO `indicator_records`
VALUES ('122', '9.00', '2017-03-01', '25', '26', NULL, '2017-05-02 10:01:47', '2017-05-02 10:01:47', NULL);
INSERT INTO `migrations` VALUES ('3', '2014_10_12_100000_create_password_resets_table', '1');
INSERT INTO `indicator_groups` VALUES
  ('2', 'Indicadores de Salas', 'Grupo para indicadores associados às salas.', 'rooms', '6', '2017-11-02',
   '2017-05-02 10:01:45', '2017-05-02 10:01:45', NULL);

UPDATE `agreement_agreement_role`
SET `created_at` = '2017-04-23 14:02:48', `updated_at` = '2017-05-02 10:01:48'
WHERE `id` = '44';
UPDATE `action_user`
SET `created_at` = '2017-04-23 14:02:49', `updated_at` = '2017-05-02 10:01:49'
WHERE `id` = '32';
UPDATE `action_user`
SET `created_at` = '2017-04-23 14:02:48', `updated_at` = '2017-05-02 10:01:48'
WHERE `id` = '21';
UPDATE `action_user`
SET `created_at` = '2017-04-23 14:02:48', `updated_at` = '2017-05-02 10:01:48'
WHERE `id` = '22';
UPDATE `action_user`
SET `created_at` = '2017-04-23 14:02:48', `updated_at` = '2017-05-02 10:01:48'
WHERE `id` = '23';
UPDATE `action_user`
SET `created_at` = '2017-04-23 14:02:48', `updated_at` = '2017-05-02 10:01:48'
WHERE `id` = '24';
UPDATE `action_user`
SET `created_at` = '2017-04-23 14:02:48', `updated_at` = '2017-05-02 10:01:48'
WHERE `id` = '25';
UPDATE `action_user`
SET `created_at` = '2017-04-23 14:02:48', `updated_at` = '2017-05-02 10:01:48'
WHERE `id` = '26';
UPDATE `action_user`
SET `created_at` = '2017-04-23 14:02:48', `updated_at` = '2017-05-02 10:01:48'
WHERE `id` = '27';
UPDATE `action_user`
SET `created_at` = '2017-04-23 14:02:48', `updated_at` = '2017-05-02 10:01:48'
WHERE `id` = '28';
UPDATE `action_user`
SET `created_at` = '2017-04-23 14:02:48', `updated_at` = '2017-05-02 10:01:48'
WHERE `id` = '29';
UPDATE `action_user`
SET `created_at` = '2017-04-23 14:02:48', `updated_at` = '2017-05-02 10:01:48'
WHERE `id` = '30';
UPDATE `action_user`
SET `created_at` = '2017-04-23 14:02:49', `updated_at` = '2017-05-02 10:01:49'
WHERE `id` = '31';
UPDATE `action_user`
SET `created_at` = '2017-04-23 14:02:49', `updated_at` = '2017-05-02 10:01:49'
WHERE `id` = '33';
UPDATE `action_user`
SET `created_at` = '2017-04-23 14:02:48', `updated_at` = '2017-05-02 10:01:48'
WHERE `id` = '19';
UPDATE `action_user`
SET `created_at` = '2017-04-23 14:02:49', `updated_at` = '2017-05-02 10:01:49'
WHERE `id` = '34';
UPDATE `agreement_agreement_role`
SET `created_at` = '2017-04-23 14:02:47', `updated_at` = '2017-05-02 10:01:47'
WHERE `id` = '1';
UPDATE `agreement_agreement_role`
SET `created_at` = '2017-04-23 14:02:47', `updated_at` = '2017-05-02 10:01:47'
WHERE `id` = '2';
UPDATE `agreement_agreement_role`
SET `created_at` = '2017-04-23 14:02:47', `updated_at` = '2017-05-02 10:01:47'
WHERE `id` = '3';
UPDATE `agreement_agreement_role`
SET `created_at` = '2017-04-23 14:02:47', `updated_at` = '2017-05-02 10:01:47'
WHERE `id` = '4';
UPDATE `agreement_agreement_role`
SET `created_at` = '2017-04-23 14:02:47', `updated_at` = '2017-05-02 10:01:47'
WHERE `id` = '5';
UPDATE `agreement_agreement_role`
SET `created_at` = '2017-04-23 14:02:47', `updated_at` = '2017-05-02 10:01:47'
WHERE `id` = '6';
UPDATE `agreement_agreement_role`
SET `created_at` = '2017-04-23 14:02:47', `updated_at` = '2017-05-02 10:01:47'
WHERE `id` = '7';
UPDATE `agreement_agreement_role`
SET `created_at` = '2017-04-23 14:02:47', `updated_at` = '2017-05-02 10:01:47'
WHERE `id` = '8';
UPDATE `agreement_agreement_role`
SET `created_at` = '2017-04-23 14:02:47', `updated_at` = '2017-05-02 10:01:47'
WHERE `id` = '9';
UPDATE `agreement_agreement_role`
SET `created_at` = '2017-04-23 14:02:47', `updated_at` = '2017-05-02 10:01:47'
WHERE `id` = '10';
UPDATE `agreement_agreement_role`
SET `created_at` = '2017-04-23 14:02:47', `updated_at` = '2017-05-02 10:01:47'
WHERE `id` = '11';
UPDATE `action_user`
SET `created_at` = '2017-04-23 14:02:48', `updated_at` = '2017-05-02 10:01:48'
WHERE `id` = '20';
UPDATE `action_user`
SET `created_at` = '2017-04-23 14:02:48', `updated_at` = '2017-05-02 10:01:48'
WHERE `id` = '18';
UPDATE `agreement_agreement_role`
SET `created_at` = '2017-04-23 14:02:48', `updated_at` = '2017-05-02 10:01:48'
WHERE `id` = '13';
UPDATE `action_user`
SET `created_at` = '2017-04-23 14:02:48', `updated_at` = '2017-05-02 10:01:48'
WHERE `id` = '3';
UPDATE `action_user`
SET `created_at` = '2017-04-23 14:02:48', `updated_at` = '2017-05-02 10:01:48'
WHERE `id` = '1';
UPDATE `roles`
SET `created_at` = '2017-04-23 14:02:45', `updated_at` = '2017-05-02 10:01:45'
WHERE `id` = '4';
UPDATE `roles`
SET `created_at` = '2017-04-23 14:02:45', `updated_at` = '2017-05-02 10:01:45'
WHERE `id` = '3';
UPDATE `roles`
SET `created_at` = '2017-04-23 14:02:45', `updated_at` = '2017-05-02 10:01:45'
WHERE `id` = '2';
UPDATE `roles`
SET `created_at` = '2017-04-23 14:02:45', `updated_at` = '2017-05-02 10:01:45'
WHERE `id` = '1';
UPDATE `permissions`
SET `created_at` = '2017-04-23 14:02:45', `updated_at` = '2017-05-02 10:01:45'
WHERE `id` = '3';
UPDATE `permissions`
SET `created_at` = '2017-04-23 14:02:45', `updated_at` = '2017-05-02 10:01:45'
WHERE `id` = '2';
UPDATE `permissions`
SET `created_at` = '2017-04-23 14:02:45', `updated_at` = '2017-05-02 10:01:45'
WHERE `id` = '1';
UPDATE `migrations`
SET `migration` = '2017_04_04_131153_create_sessions_table'
WHERE `id` = '26';
UPDATE `migrations`
SET `migration` = '2016_12_16_153841_create_evidences_table'
WHERE `id` = '25';
UPDATE `migrations`
SET `migration` = '2016_12_06_122030_create_action_agreement_table'
WHERE `id` = '24';
UPDATE `action_user`
SET `created_at` = '2017-04-23 14:02:48', `updated_at` = '2017-05-02 10:01:48'
WHERE `id` = '2';
UPDATE `action_user`
SET `created_at` = '2017-04-23 14:02:48', `updated_at` = '2017-05-02 10:01:48'
WHERE `id` = '4';
UPDATE `action_user`
SET `created_at` = '2017-04-23 14:02:48', `updated_at` = '2017-05-02 10:01:48'
WHERE `id` = '17';
UPDATE `action_user`
SET `created_at` = '2017-04-23 14:02:48', `updated_at` = '2017-05-02 10:01:48'
WHERE `id` = '5';
UPDATE `action_user`
SET `created_at` = '2017-04-23 14:02:48', `updated_at` = '2017-05-02 10:01:48'
WHERE `id` = '6';
UPDATE `action_user`
SET `created_at` = '2017-04-23 14:02:48', `updated_at` = '2017-05-02 10:01:48'
WHERE `id` = '7';
UPDATE `action_user`
SET `created_at` = '2017-04-23 14:02:48', `updated_at` = '2017-05-02 10:01:48'
WHERE `id` = '8';
UPDATE `action_user`
SET `created_at` = '2017-04-23 14:02:48', `updated_at` = '2017-05-02 10:01:48'
WHERE `id` = '9';
UPDATE `action_user`
SET `created_at` = '2017-04-23 14:02:48', `updated_at` = '2017-05-02 10:01:48'
WHERE `id` = '10';
UPDATE `action_user`
SET `created_at` = '2017-04-23 14:02:48', `updated_at` = '2017-05-02 10:01:48'
WHERE `id` = '11';
UPDATE `action_user`
SET `created_at` = '2017-04-23 14:02:48', `updated_at` = '2017-05-02 10:01:48'
WHERE `id` = '12';
UPDATE `action_user`
SET `created_at` = '2017-04-23 14:02:48', `updated_at` = '2017-05-02 10:01:48'
WHERE `id` = '13';
UPDATE `action_user`
SET `created_at` = '2017-04-23 14:02:48', `updated_at` = '2017-05-02 10:01:48'
WHERE `id` = '14';
UPDATE `action_user`
SET `created_at` = '2017-04-23 14:02:48', `updated_at` = '2017-05-02 10:01:48'
WHERE `id` = '15';
UPDATE `action_user`
SET `created_at` = '2017-04-23 14:02:48', `updated_at` = '2017-05-02 10:01:48'
WHERE `id` = '16';
UPDATE `agreement_agreement_role`
SET `created_at` = '2017-04-23 14:02:47', `updated_at` = '2017-05-02 10:01:47'
WHERE `id` = '12';
UPDATE `agreement_agreement_role`
SET `created_at` = '2017-04-23 14:02:48', `updated_at` = '2017-05-02 10:01:48'
WHERE `id` = '14';
UPDATE `agreement_agreement_role`
SET `created_at` = '2017-04-23 14:02:48', `updated_at` = '2017-05-02 10:01:48'
WHERE `id` = '45';
UPDATE `agreement_topic_tag`
SET `created_at` = '2017-04-23 14:02:47', `updated_at` = '2017-05-02 10:01:47'
WHERE `id` = '4';
UPDATE `agreement_topic_tag`
SET `created_at` = '2017-04-23 14:02:48', `updated_at` = '2017-05-02 10:01:48'
WHERE `id` = '15';
UPDATE `agreement_topic_tag`
SET `created_at` = '2017-04-23 14:02:48', `updated_at` = '2017-05-02 10:01:48'
WHERE `id` = '14';
UPDATE `agreement_topic_tag`
SET `created_at` = '2017-04-23 14:02:48', `updated_at` = '2017-05-02 10:01:48'
WHERE `id` = '13';
UPDATE `agreement_topic_tag`
SET `created_at` = '2017-04-23 14:02:48', `updated_at` = '2017-05-02 10:01:48'
WHERE `id` = '12';
UPDATE `agreement_topic_tag`
SET `created_at` = '2017-04-23 14:02:48', `updated_at` = '2017-05-02 10:01:48'
WHERE `id` = '11';
UPDATE `agreement_topic_tag`
SET `created_at` = '2017-04-23 14:02:48', `updated_at` = '2017-05-02 10:01:48'
WHERE `id` = '10';
UPDATE `agreement_topic_tag`
SET `created_at` = '2017-04-23 14:02:48', `updated_at` = '2017-05-02 10:01:48'
WHERE `id` = '9';
UPDATE `agreement_topic_tag`
SET `created_at` = '2017-04-23 14:02:48', `updated_at` = '2017-05-02 10:01:48'
WHERE `id` = '8';
UPDATE `agreement_topic_tag`
SET `created_at` = '2017-04-23 14:02:48', `updated_at` = '2017-05-02 10:01:48'
WHERE `id` = '7';
UPDATE `agreement_topic_tag`
SET `created_at` = '2017-04-23 14:02:48', `updated_at` = '2017-05-02 10:01:48'
WHERE `id` = '6';
UPDATE `agreement_topic_tag`
SET `created_at` = '2017-04-23 14:02:47', `updated_at` = '2017-05-02 10:01:47'
WHERE `id` = '5';
UPDATE `agreement_topic_tag`
SET `created_at` = '2017-04-23 14:02:47', `updated_at` = '2017-05-02 10:01:47'
WHERE `id` = '3';
UPDATE `agreement_topic_tag`
SET `created_at` = '2017-04-23 14:02:48', `updated_at` = '2017-05-02 10:01:48'
WHERE `id` = '17';
UPDATE `agreement_topic_tag`
SET `created_at` = '2017-04-23 14:02:47', `updated_at` = '2017-05-02 10:01:47'
WHERE `id` = '2';
UPDATE `agreement_topic_tag`
SET `created_at` = '2017-04-23 14:02:47', `updated_at` = '2017-05-02 10:01:47'
WHERE `id` = '1';
UPDATE `agreement_agreement_role`
SET `created_at` = '2017-04-23 14:02:48', `updated_at` = '2017-05-02 10:01:48'
WHERE `id` = '43';
UPDATE `agreement_agreement_role`
SET `created_at` = '2017-04-23 14:02:48', `updated_at` = '2017-05-02 10:01:48'
WHERE `id` = '54';
UPDATE `agreement_agreement_role`
SET `created_at` = '2017-04-23 14:02:48', `updated_at` = '2017-05-02 10:01:48'
WHERE `id` = '53';
UPDATE `agreement_agreement_role`
SET `created_at` = '2017-04-23 14:02:48', `updated_at` = '2017-05-02 10:01:48'
WHERE `id` = '52';
UPDATE `agreement_agreement_role`
SET `created_at` = '2017-04-23 14:02:48', `updated_at` = '2017-05-02 10:01:48'
WHERE `id` = '51';
UPDATE `agreement_agreement_role`
SET `created_at` = '2017-04-23 14:02:48', `updated_at` = '2017-05-02 10:01:48'
WHERE `id` = '50';
UPDATE `agreement_agreement_role`
SET `created_at` = '2017-04-23 14:02:48', `updated_at` = '2017-05-02 10:01:48'
WHERE `id` = '49';
UPDATE `agreement_agreement_role`
SET `created_at` = '2017-04-23 14:02:48', `updated_at` = '2017-05-02 10:01:48'
WHERE `id` = '48';
UPDATE `agreement_agreement_role`
SET `created_at` = '2017-04-23 14:02:48', `updated_at` = '2017-05-02 10:01:48'
WHERE `id` = '47';
UPDATE `agreement_agreement_role`
SET `created_at` = '2017-04-23 14:02:48', `updated_at` = '2017-05-02 10:01:48'
WHERE `id` = '46';
UPDATE `agreement_topic_tag`
SET `created_at` = '2017-04-23 14:02:48', `updated_at` = '2017-05-02 10:01:48'
WHERE `id` = '16';
UPDATE `agreement_agreement_role`
SET `created_at` = '2017-04-23 14:02:48', `updated_at` = '2017-05-02 10:01:48'
WHERE `id` = '42';
UPDATE `agreement_agreement_role`
SET `created_at` = '2017-04-23 14:02:48', `updated_at` = '2017-05-02 10:01:48'
WHERE `id` = '15';
UPDATE `agreement_agreement_role`
SET `created_at` = '2017-04-23 14:02:48', `updated_at` = '2017-05-02 10:01:48'
WHERE `id` = '29';
UPDATE `agreement_agreement_role`
SET `created_at` = '2017-04-23 14:02:48', `updated_at` = '2017-05-02 10:01:48'
WHERE `id` = '16';
UPDATE `agreement_agreement_role`
SET `created_at` = '2017-04-23 14:02:48', `updated_at` = '2017-05-02 10:01:48'
WHERE `id` = '17';
UPDATE `agreement_agreement_role`
SET `created_at` = '2017-04-23 14:02:48', `updated_at` = '2017-05-02 10:01:48'
WHERE `id` = '18';
UPDATE `agreement_agreement_role`
SET `created_at` = '2017-04-23 14:02:48', `updated_at` = '2017-05-02 10:01:48'
WHERE `id` = '19';
UPDATE `agreement_agreement_role`
SET `created_at` = '2017-04-23 14:02:48', `updated_at` = '2017-05-02 10:01:48'
WHERE `id` = '20';
UPDATE `agreement_agreement_role`
SET `created_at` = '2017-04-23 14:02:48', `updated_at` = '2017-05-02 10:01:48'
WHERE `id` = '21';
UPDATE `agreement_agreement_role`
SET `created_at` = '2017-04-23 14:02:48', `updated_at` = '2017-05-02 10:01:48'
WHERE `id` = '22';
UPDATE `agreement_agreement_role`
SET `created_at` = '2017-04-23 14:02:48', `updated_at` = '2017-05-02 10:01:48'
WHERE `id` = '23';
UPDATE `agreement_agreement_role`
SET `created_at` = '2017-04-23 14:02:48', `updated_at` = '2017-05-02 10:01:48'
WHERE `id` = '24';
UPDATE `agreement_agreement_role`
SET `created_at` = '2017-04-23 14:02:48', `updated_at` = '2017-05-02 10:01:48'
WHERE `id` = '25';
UPDATE `agreement_agreement_role`
SET `created_at` = '2017-04-23 14:02:48', `updated_at` = '2017-05-02 10:01:48'
WHERE `id` = '26';
UPDATE `agreement_agreement_role`
SET `created_at` = '2017-04-23 14:02:48', `updated_at` = '2017-05-02 10:01:48'
WHERE `id` = '28';
UPDATE `agreement_agreement_role`
SET `created_at` = '2017-04-23 14:02:48', `updated_at` = '2017-05-02 10:01:48'
WHERE `id` = '30';
UPDATE `agreement_agreement_role`
SET `created_at` = '2017-04-23 14:02:48', `updated_at` = '2017-05-02 10:01:48'
WHERE `id` = '41';
UPDATE `agreement_agreement_role`
SET `created_at` = '2017-04-23 14:02:48', `updated_at` = '2017-05-02 10:01:48'
WHERE `id` = '31';
UPDATE `agreement_agreement_role`
SET `created_at` = '2017-04-23 14:02:48', `updated_at` = '2017-05-02 10:01:48'
WHERE `id` = '32';
UPDATE `agreement_agreement_role`
SET `created_at` = '2017-04-23 14:02:48', `updated_at` = '2017-05-02 10:01:48'
WHERE `id` = '33';
UPDATE `agreement_agreement_role`
SET `created_at` = '2017-04-23 14:02:48', `updated_at` = '2017-05-02 10:01:48'
WHERE `id` = '34';
UPDATE `agreement_agreement_role`
SET `created_at` = '2017-04-23 14:02:48', `updated_at` = '2017-05-02 10:01:48'
WHERE `id` = '35';
UPDATE `agreement_agreement_role`
SET `created_at` = '2017-04-23 14:02:48', `updated_at` = '2017-05-02 10:01:48'
WHERE `id` = '36';
UPDATE `agreement_agreement_role`
SET `created_at` = '2017-04-23 14:02:48', `updated_at` = '2017-05-02 10:01:48'
WHERE `id` = '37';
UPDATE `agreement_agreement_role`
SET `created_at` = '2017-04-23 14:02:48', `updated_at` = '2017-05-02 10:01:48'
WHERE `id` = '38';
UPDATE `agreement_agreement_role`
SET `created_at` = '2017-04-23 14:02:48', `updated_at` = '2017-05-02 10:01:48'
WHERE `id` = '39';
UPDATE `agreement_agreement_role`
SET `created_at` = '2017-04-23 14:02:48', `updated_at` = '2017-05-02 10:01:48'
WHERE `id` = '40';
UPDATE `history_types`
SET `created_at` = '2017-04-23 14:02:45', `updated_at` = '2017-05-02 10:01:45'
WHERE `id` = '2';
UPDATE `history_types`
SET `created_at` = '2017-04-23 14:02:45', `updated_at` = '2017-05-02 10:01:45'
WHERE `id` = '1';
UPDATE `agreement_agreement_role`
SET `created_at` = '2017-04-23 14:02:48', `updated_at` = '2017-05-02 10:01:48'
WHERE `id` = '27';

# MANUAL
UPDATE `indicators`
SET `indicator_group_id` = `1`
WHERE `indicator_group_id` = `0`;
INSERT INTO `action_evaluations` (`id`, `evaluation`, `rating`, `justification`, `evaluation_date`, `user_id`, `action_id`, `created_at`, `updated_at`, `deleted_at`)
VALUES
  (5, 'Not implemented', 1, 'Foi criado online e link enviado para direção, mas nunca foi enviado aos pais.',
   '2017-04-23', 1, 29, '2017-04-23 19:57:56', '2017-04-23 19:57:56', NULL),
  (6, 'Fully implemented', 2,
   'Alunos criaram o cartaz três vezes. O terceiro tinha linguagem positiva. No entanto, todos foram removidos pelos alunos. O resultado foi considerado insatisfatório do modo como foi implementado, já que não houve uma sensibilização dos alunos/funcionários sobre o cartaz, e nem temos evidência do cartaz implementado.',
   '2017-04-23', 1, 3, '2017-04-23 20:06:19', '2017-04-23 20:06:19', NULL),
  (7, 'Fully implemented', 5,
   'Foi utilizado no planejamento de 2017 e serviu como pauta de discussão. Foi impresso e entregue aos professores e deve ser utilizado novamente todo ano, de acordo com o feedback da Diretora. Foi referenciado ao longo de ATPCs no início de 2017, demonstrando sua utilidade como documento de \'acordos\'.',
   '2017-04-23', 1, 20, '2017-04-23 20:57:10', '2017-04-23 20:57:10', NULL),
  (8, 'Fully implemented', 5,
   'O aumento de resposta dos pais ao questionário e o feedback dos pais participantes do GT é positiva. Motivou a tentativa de expandir para grupos de Whatsapp (dentre outros) - o que não ocorreu, mas não invalida o valor da lista.',
   '2017-04-23', 1, 22, '2017-04-23 21:19:47', '2017-04-23 21:19:47', NULL),
  (9, 'Fully implemented', 5,
   'Sugere-se a importância de verificar, periodicamente a adequação da merenda ao número de alunos.', '2017-04-23', 1,
   5, '2017-04-23 21:25:58', '2017-04-23 21:25:58', NULL),
  (10, 'Not implemented', 1,
   'Apesar de ter sido sugerida no GT, nunca foi implementada. Como a fila parece estar funcionando bem por conta da disposição das mesas indicando a ordem da fila, não parece ser necessária.',
   '2017-04-24', 1, 4, '2017-04-24 16:41:02', '2017-04-24 16:41:02', NULL),
  (11, 'Not implemented', 1, 'Não foi realizado em 2017.', '2017-04-24', 1, 27, '2017-04-24 16:41:48',
   '2017-04-24 16:41:48', NULL);

INSERT INTO `agreements` (`id`, `title`, `details`, `agreement_monitoring_strategy_id`, `evaluation_cycle_id`, `created_at`, `updated_at`, `deleted_at`)
VALUES
  (18, 'Combinados devem ser entregues a professores no planejanento',
   'Servem como pauta de discussão para todos, bem como orientação para os mais novos.<br><p><br></p>', 2, 7,
   '2017-04-23 21:06:39', '2017-04-23 21:06:39', NULL);

INSERT INTO `agreement_topic_tag` (`id`, `agreement_id`, `topic_tag_id`, `created_at`, `updated_at`)
VALUES
  (18, 18, 2, '2017-04-23 21:06:39', '2017-04-23 21:06:39');

INSERT INTO `evidences` (`id`, `title`, `details`, `file_name`, `action_id`, `user_id`, `created_at`, `updated_at`, `deleted_at`)
VALUES
  (39, 'Segunda edição publicada',
   'Seguiu o mesmo modelo de fevereiro, com pautas propostas pelos professores e coletadas com alunos.',
   'evidences/evidence_39_chicão_mar_2017.pdf', 26, 1, '2017-04-23 21:14:18', '2017-04-23 21:14:18', NULL);

SET SQL_MODE = @OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS = @OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS = @OLD_UNIQUE_CHECKS;
