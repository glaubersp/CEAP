#!/bin/bash

rm *.lock
rm -fr vendor node_modules

composer install
npm install
php artisan key:generate
npm run production
cd ../
tar --exclude=./CEAP/.idea --exclude=./CEAP/.github --exclude=./CEAP/.git --exclude=./CEAP/.sonarlint --exclude=./CEAP/bin --exclude=./CEAP/node_modules --exclude=./CEAP/tests -jcvf ceap.tar.gz CEAP CEAP/.env*
