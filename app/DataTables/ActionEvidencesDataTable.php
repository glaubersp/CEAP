<?php

namespace App\DataTables;

use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Yajra\DataTables\DataTables;
use Yajra\DataTables\Services\DataTable;
use function explode;
use function link_to_asset;

class ActionEvidencesDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param \Yajra\DataTables\DataTables $dataTables
     * @param \Illuminate\Database\Query\Builder $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable(DataTables $dataTables, $query)
    {
        return $dataTables->query($query)
            ->editColumn('evidence_title', function ($item) {
                return link_to_route('frontend.ceap.actions.evidences.show', $item->evidence_title,
                    ['action' => $item->evidence_action_id, 'evidence' => $item->evidence_id]);
            })
            ->editColumn('evidence_file_name', function ($item) {
                if($item->evidence_file_name === null) {
                    return '&mdash;';
                } else if ($item->evidence_file_name) {
                    // File name on the database is considered to be folder/filename.ext
                    $title = explode('/', $item->evidence_file_name)[1];
                    // FileController deals with URL file/folder/filename.ext
                    return link_to_asset(Storage::url($item->evidence_file_name), $title, ['target' => '_blank']);
                }
            })
	        ->editColumn('evidence_date', function ($item) {
                return $item->evidence_date ? with(new Carbon($item->evidence_date))->formatLocalized('%x') : '';
	        })
            ->rawColumns(['evidence_title', 'evidence_details', 'evidence_file_name']);
    }

    /**
     * Get the query object to be processed by dataTables.
     *
     * @return \Illuminate\Database\Query\Builder
     */
    public function query()
    {
        return $query = DB::table('evidences')
            ->join('users', 'evidences.user_id', '=', 'users.id')
            ->select('evidences.id as evidence_id',
                'evidences.title as evidence_title',
                'evidences.details as evidence_details',
                'evidences.file_name as evidence_file_name',
                'evidences.action_id as evidence_action_id',
                'evidences.updated_at as evidence_date',
                'users.id as user_id')
            ->selectRaw('concat(users.first_name, " ", users.last_name) as user_name');
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        $lang = app()->getLocale();
        return $this->builder()
            ->columns($this->getColumns())
            ->setTableAttribute([
                'class' => 'table dt-responsive table-condensed table-hover display',
                'width' => '100%',
            ])
            ->parameters([
	            'dom'           =>
		            "<'row'<'col-xs-12 col-sm-6 col-md-6 separator-xs-top separator-xs-bottom'B><'col-xs-12 col-sm-6 col-md-6 separator-xs-top separator-xs-bottom'f<'#new-checkbox.checkbox checkbox-ceap'>>>" .
		            "<'row'<'col-xs-12 col-sm-6 col-md-6 separator-xs-top separator-xs-bottom'l>>" .
                    "<'row'<'col-sm-12'tr>>" .
		            "<'row'<'col-sm-6'i><'col-sm-6'p>>",
                'lengthMenu' => [[5, 10, 25, 50], [5, 10, 25, 50]],
                'pagingType' => "simple",
                'buttons'    => [
                    'export',
                    'print'
                ],
                'language'   => [
                    'url' => url("datatable/{$lang}.json")
                ],
                'responsive' => true,
                'order'      => [[2, 'desc']],
            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            [
                'data'               => 'evidence_title',
                'name'               => 'evidences.title as evidence_title',
                'title'              => removeCollon(trans('ceap.actions.evidences.modal.title')),
                'searchable'         => true,
                'orderable'          => true,
                'exportable'         => true,
                'printable'          => true,
                'responsivePriority' => 1,
                'width' => '20%',
            ],
            [
                'data'       => 'evidence_details',
                'name'       => 'evidences.details as evidence_details',
                'title'      => removeCollon(trans('ceap.actions.evidences.modal.details')),
                'searchable' => true,
                'orderable'  => true,
                'exportable' => true,
                'printable'  => true,
	            'width' => '40%',
            ],
            [
                'data'       => 'evidence_date',
                'name'       => 'evidences.updated_at as evidence_date',
                'title'      => removeCollon(trans('ceap.table.updated_at')),
                'searchable' => true,
                'orderable'  => true,
                'exportable' => true,
                'printable'  => true,
                'width' => '10%',
            ],
	        [
		        'data'       => 'evidence_file_name',
		        'name'       => 'evidences.file_name as evidence_file_name',
		        'title'      => removeCollon(trans('ceap.actions.evidences.file')),
		        'searchable' => true,
		        'orderable'  => true,
		        'exportable' => true,
		        'printable'  => true,
		        'width' => '20%',
	        ],
            [
                'data'       => 'user_name',
                'name'       => 'users.name as user_name',
                'title'              => removeCollon(trans('ceap.table.user')),
                'searchable' => true,
                'orderable'  => true,
                'exportable' => true,
                'printable'  => true,
                'width' => '20%',
            ],
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return trans('ceap.datatable.filenames.evidences') . '_' . Carbon::now()->toAtomString();
    }
}
