<?php

namespace App\DataTables;

use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\DataTables;
use Yajra\DataTables\Services\DataTable;

class ActionEvaluationDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param \Yajra\DataTables\DataTables $dataTables
     * @param \Illuminate\Database\Query\Builder $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable(DataTables $dataTables, $query)
    {
        return $dataTables->query($query)
            ->editColumn('action_evaluation_evaluation', function ($evaluation_item) {
                return trans('ceap.actions.evaluations.modal.evaluation_options.' . $evaluation_item->action_evaluation_evaluation);
            })
            ->editColumn('action_evaluation_rating', function ($evaluation_item) {
                if ($evaluation_item->action_evaluation_evaluation === 'Not implemented') {
                    return '&mdash;';
                } else {
                    return $evaluation_item->action_evaluation_rating;
                }
            })
            ->rawColumns(['action_evaluation_evaluation', 'action_evaluation_justification']);
    }

    /**
     * Get the query object to be processed by dataTables.action_evaluations.justification as action_evaluation_justification
     *
     * @return \Illuminate\Database\Query\Builder
     */
    public function query()
    {
        return $query = DB::table('action_evaluations')
            ->join('users', 'action_evaluations.user_id', '=', 'users.id')
            ->select('action_evaluations.id as action_evaluation_id',
                'action_evaluations.evaluation as action_evaluation_evaluation',
                'action_evaluations.rating as action_evaluation_rating',
                'action_evaluations.justification as action_evaluation_justification',
                'users.id as user_id')
            ->selectRaw('concat(users.first_name, " ", users.last_name) as user_name');;
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        $lang = app()->getLocale();
        return $this->builder()->columns($this->getColumns())
            ->setTableAttribute([
                'class' => 'table dt-responsive table-condensed table-hover display',
                'width' => '100%',
            ])
            ->parameters([
	            'dom'           =>
		            "<'row'<'col-xs-12 col-sm-6 col-md-6 separator-xs-top separator-xs-bottom'B><'col-xs-12 col-sm-6 col-md-6 separator-xs-top separator-xs-bottom'f<'#new-checkbox.checkbox checkbox-ceap'>>>" .
		            "<'row'<'col-xs-12 col-sm-6 col-md-6 separator-xs-top separator-xs-bottom'l>>" .
                    "<'row'<'col-sm-12'tr>>" .
		            "<'row'<'col-sm-6'i><'col-sm-6'p>>",
                'lengthMenu' => [[5, 10, 25, 50], [5, 10, 25, 50]],
                'pagingType' => "simple",
                'buttons'    => [
                    'export',
                    'print'
                ],
                'language'   => [
                    'url' => url("datatable/{$lang}.json")
                ],
                'responsive' => true,
                'order'      => [[0, 'asc']],
                'stateSave'  => true,
            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            [
                'data'               => 'action_evaluation_evaluation',
                'name'               => 'action_evaluations.evaluation as action_evaluation_evaluation',
                'title'              => removeCollon(trans('ceap.actions.evaluations.modal.evaluation')),
                'searchable'         => true,
                'orderable'          => true,
                'exportable'         => true,
                'printable'          => true,
                'responsivePriority' => 1,
            ],
            [
                'data'               => 'action_evaluation_rating',
                'name'               => 'action_evaluations.rating as action_evaluation_rating',
                'title'              => removeCollon(trans('ceap.actions.evaluations.modal.result')),
                'searchable'         => true,
                'orderable'          => true,
                'exportable'         => true,
                'printable'          => true,
                'responsivePriority' => 1,
            ],
            [
                'data'               => 'action_evaluation_justification',
                'name'               => 'action_evaluations.justification as action_evaluation_justification',
                'title'              => removeCollon(trans('ceap.actions.evaluations.modal.justification')),
                'searchable'         => true,
                'orderable'          => true,
                'exportable'         => true,
                'printable'          => true,
                'responsivePriority' => 3,
            ],
            [
                'data'               => 'user_name',
                'name'               => 'users.name as user_name',
                'title'              => removeCollon(trans('ceap.table.user')),
                'searchable'         => true,
                'orderable'          => true,
                'exportable'         => true,
                'printable'          => true,
                'responsivePriority' => 2,
            ],
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return trans('ceap.datatable.filenames.evaluations') . '_' . Carbon::now()->toAtomString();
    }
}
