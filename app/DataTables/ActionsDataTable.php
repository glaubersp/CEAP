<?php

namespace App\DataTables;

use Carbon\Carbon;
use Form;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\DataTables;
use Yajra\DataTables\Services\DataTable;

class ActionsDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param \Yajra\DataTables\DataTables $dataTables
     * @param \Illuminate\Database\Query\Builder $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable(DataTables $dataTables, $query)
    {
        $checked = $this->request()->input('checked');
        if ($checked === "0") {
            $query->whereNull('actions.deleted_at');
        }

        $term_id = $this->request()->input('term_id');
        if ($term_id !== null && $term_id !== "0") {
            $query->where('actions.evaluation_term_id', '=', $term_id);
        }

        return $dataTables->query($query)
            ->editColumn('action_name', function ($item) {
                if (!$item->action_deleted_at) {
                    return link_to_route('frontend.ceap.actions.show', $item->action_name,
                        ['action' => $item->action_id]);
                } else {
                    return $item->action_name;
                }
            })
            ->editColumn('goal_name', function ($item) {
                return "<a style='white-space: pre-wrap;' onclick='alteraBusca(this)'>" . $item->goal_name . '</a>';
            })
            ->editColumn('indicator_name', function ($item) {
                return "<a style='white-space: pre-wrap;' onclick='alteraBusca(this)'>" . $item->indicator_name . '</a>';
            })
            ->editColumn('interactions', function ($item) {
                if (!$item->action_deleted_at) {
                    return '<div class="btn-group" role="group">'
                        . Form::open([
                            'method' => 'GET',
                            'route'  => ['frontend.ceap.actions.evidences.index', $item->action_id],
                        ])
                        . Form::button(trans('ceap.actions.evidences.evidence'), [
                            'type'        => 'submit',
                            'class'       => 'btn btn-primary panel-title-buttons btn-actions-evidences',
                            'style'       => 'margin-top: 3px;margin-bottom: 3px;',
                            'data-toggle' => 'modal',
                            'data-target' => '#actionEvidenceModal',
                        ])
                        . Form::close()
                        . '<a class="btn btn-primary panel-title-buttons evaluateBtn btn-actions-evaluate" style="margin-top: 0;"'
                        . ' data-toggle="modal" data-target="#actionEvaluationModal" data-action-id="'
                        . $item->action_id
                        . '">'
                        . trans('ceap.button.plan')
                        . '</a>'
                        . '</div>';
                } else {
                    return '<div class="btn-group" role="group">'
                        . Form::open([
                            'method' => 'POST',
                            'route'  => ['frontend.ceap.actions.restore'],
                        ])
                        . Form::hidden('action_id', $item->action_id)
                        . Form::button(trans('ceap.button.restore'),
                            [
                                'type'  => 'submit',
                                'class' => 'btn btn-primary panel-title-buttons btn-actions-evaluate',
                                'style' => 'margin-top: 3px;margin-bottom: 3px;',
                            ])
                        . Form::close()
                        . '</div>';
                }

            })
            ->rawColumns([
                'action_name',
                'goal_name',
                'indicator_name',
                'action_priority',
                'action_evaluation_term_id',
                'interactions',
            ]);
    }

    /**
     * Get query source of dataTables
     *
     * @return \Illuminate\Database\Query\Builder
     */
    public function query()
    {
        return $query = DB::table('indicators')
            ->join('goals', 'indicators.id', '=', 'goals.indicator_id')
            ->join('actions', 'goals.id', '=', 'actions.goal_id')
            ->join('evaluation_terms', 'evaluation_terms.id', '=', 'actions.evaluation_term_id')
            ->select('actions.id as action_id', 'actions.name as action_name', 'actions.priority as action_priority',
                'actions.updated_at as action_updated_at', 'actions.evaluation_term_id  as action_evaluation_term_id',
                'actions.deleted_at as action_deleted_at',
                'goals.id as goal_id', 'goals.name as goal_name',
                'indicators.id as indicator_id', 'indicators.name as indicator_name')
            ->selectRaw('concat(evaluation_terms.year, " - ", evaluation_terms.name) as evaluation_term');
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        $lang = app()->getLocale();
        return $this->builder()->columns($this->getColumns())
            ->setTableAttribute([
                'class' => 'table dt-responsive table-condensed table-hover display',
                'width' => '100%',
            ])
            ->parameters([
                'dom'        =>
                    "<'row'<'col-xs-12 col-sm-6 col-md-6 separator-xs-bottom'<'#new-select.select select-ceap'>><'col-xs-12 col-sm-6 col-md-6 separator-xs-top separator-xs-bottom'f<'#new-checkbox.checkbox checkbox-ceap'>>>" .
                    "<'row'<'col-xs-12 col-sm-6 col-md-6 separator-xs-top separator-xs-bottom'l><'col-xs-12 col-sm-6 col-md-6 separator-xs-top separator-xs-bottom'B>>" .
                    "<'row'<'col-sm-12'tr>>" .
                    "<'row'<'col-sm-6'i><'col-sm-6'p>>",
                'lengthMenu' => [[5, 10, 25, 50], [5, 10, 25, 50]],
                'pagingType' => 'simple',
                'buttons'    => [
                    'export',
                    'print',
                ],
                'language'   => [
                    'url' => url("datatable/{$lang}.json"),
                ],
                'responsive' => true,
                'order'      => [[3, 'desc']],
                'stateSave'  => true,
                'createdRow' => 'function (row, data, index) {if (data.action_deleted_at !== null) {row.style.backgroundColor = "#ffcccc";}}',
            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            [
                'data'               => 'action_name',
                'name'               => 'actions.name as action_name',
                'title'              => removeCollon(trans('ceap.actions.name.label')),
                'searchable'         => true,
                'orderable'          => true,
                'exportable'         => true,
                'printable'          => true,
                'responsivePriority' => 1,
            ],
            [
                'data'               => 'goal_name',
                'name'               => 'goals.name as goal_name',
                'title'              => removeCollon(trans('ceap.table.goal')),
                'searchable'         => true,
                'orderable'          => true,
                'exportable'         => true,
                'printable'          => true,
                'responsivePriority' => 4,
            ],
            [
                'data'               => 'indicator_name',
                'name'               => 'indicators.name as indicator_name',
                'title'              => removeCollon(trans('ceap.table.indicator')),
                'searchable'         => true,
                'orderable'          => true,
                'exportable'         => true,
                'printable'          => true,
                'responsivePriority' => 5,
            ],
            [
                'data'               => 'evaluation_term',
                'name'               => 'actions.evaluation_term_id as evaluation_term',
                'title'              => removeCollon(trans('ceap.actions.evaluation_term.label')),
                'searchable'         => true,
                'orderable'          => true,
                'exportable'         => true,
                'printable'          => true,
                'responsivePriority' => 3,
            ],
            [
                'data'               => 'interactions',
                'title'              => trans('ceap.table.interactions'),
                'searchable'         => false,
                'orderable'          => false,
                'exportable'         => false,
                'printable'          => false,
                'responsivePriority' => 2,
            ],
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return trans('ceap.datatable.filenames.actions') . '_' . Carbon::now()->toAtomString();
    }
}
