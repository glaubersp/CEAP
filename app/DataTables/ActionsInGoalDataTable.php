<?php

namespace App\DataTables;

use App\Models\CEAP\Goal;
use Carbon\Carbon;
use Form;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\DataTables;
use Yajra\DataTables\Services\DataTable;

class ActionsInGoalDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param \Yajra\DataTables\DataTables $dataTables
     * @param \Illuminate\Database\Query\Builder $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable(DataTables $dataTables, $query)
    {
        return $dataTables->query($query)
            ->editColumn('action_name', function ($item) {
                if (!$item->action_deleted_at) {
                    return link_to_route('frontend.ceap.actions.show', $item->action_name,
                        ['action' => $item->action_id]);
                } else {
                    return $item->action_name;
                }
            })
            ->rawColumns([
                'action_name',
                'action_deleted_at'
            ]);
    }

    /**
     * Get query source of dataTables
     *
     * @return \Illuminate\Database\Query\Builder
     */
    public function query()
    {
        return $query = DB::table('indicators')
            ->join('goals', 'indicators.id', '=', 'goals.indicator_id')
            ->join('actions', 'goals.id', '=', 'actions.goal_id')
            ->select('actions.id as action_id', 'actions.name as action_name', 'actions.priority as action_priority',
                'actions.deleted_at as action_deleted_at', 'goals.id as goal_id');
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        $lang = app()->getLocale();
        return $this->builder()->columns($this->getColumns())
            ->setTableAttribute([
                'class' => 'table dt-responsive table-condensed table-hover display',
                'width' => '100%',
            ])
            ->parameters([
                'dom'        =>
                    "<'row'<'col-xs-12 col-sm-6 col-md-6 separator-xs-bottom'<'#new-select.select select-ceap'>><'col-xs-12 col-sm-6 col-md-6 separator-xs-top separator-xs-bottom'f<'#new-checkbox.checkbox checkbox-ceap'>>>" .
                    "<'row'<'col-xs-12 col-sm-6 col-md-6 separator-xs-top separator-xs-bottom'l><'col-xs-12 col-sm-6 col-md-6 separator-xs-top separator-xs-bottom'B>>" .
                    "<'row'<'col-sm-12'tr>>" .
                    "<'row'<'col-sm-6'i><'col-sm-6'p>>",
                'lengthMenu' => [[5, 10, 25, 50], [5, 10, 25, 50]],
                'pagingType' => 'simple',
                'buttons'    => [
                    'export',
                    'print',
                ],
                'language'   => [
                    'url' => url("datatable/{$lang}.json"),
                ],
                'responsive' => true,
//                'stateSave'  => true,
//                'order'      => [[1, 'asc']],
                'createdRow' => 'function (row, data, index) {if (data.action_deleted_at !== null) {row.style.backgroundColor = "#ffcccc";}}',
            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            [
                'data'               => 'action_name',
                'name'               => 'actions.name as action_name',
                'title'              => removeCollon(trans('ceap.actions.name.label')),
                'searchable'         => true,
                'orderable'          => true,
                'exportable'         => true,
                'printable'          => true,
                'responsivePriority' => 1,
            ]
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return trans('ceap.datatable.filenames.actions') . '_' . Carbon::now()->toAtomString();
    }
}
