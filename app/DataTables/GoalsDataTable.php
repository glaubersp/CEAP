<?php

namespace App\DataTables;

use Carbon\Carbon;
use Form;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\DataTables;
use Yajra\DataTables\Services\DataTable;

class GoalsDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param \Yajra\DataTables\DataTables $dataTables
     * @param \Illuminate\Database\Query\Builder $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable(DataTables $dataTables, $query)
    {
        $checked = $this->request()->input('checked');
        if ($checked === "0") {
            $query->whereNull('goals.deleted_at');
        }

        return $dataTables->query($query)
            ->editColumn('goal_name', function ($item) {
                if (!$item->goal_deleted_at) {
                    return link_to_route('frontend.ceap.goals.show', $item->goal_name, ['goal' => $item->goal_id]);
                }

                return $item->goal_name;
            })
            ->editColumn('indicator_name', function ($item) {
                return "<a style='white-space: pre-wrap;' onclick='alteraBusca(this)'>" . $item->indicator_name . '</a>';
            })
            ->editColumn('goal_updated_at', function ($item) {
                return $item->goal_updated_at ? with(new Carbon($item->goal_updated_at))->formatLocalized('%x') : '';
            })
            ->addColumn('interactions', function ($item) {
                if (!$item->goal_deleted_at) {
                    return link_to_route('frontend.ceap.actions.create', trans('ceap.button.add_action'),
                        ['goal_id' => $item->goal_id],
                        ['class' => 'btn btn-primary panel-title-buttons btn-actions-create']);
                } else {
                    return '<div class="btn-group" role="group">'
                           . Form::open([
                            'method' => 'POST',
                            'route'  => ['frontend.ceap.goals.restore'],
                        ])
                           . Form::button(trans('ceap.button.restore'),
                            [
                                'type'  => 'submit',
                                'class' => 'btn btn-primary panel-title-buttons btn-actions-evaluate',
                                'style' => 'margin-top: 3px;margin-bottom: 3px;',
                            ])
                           . Form::hidden('goal_id', $item->goal_id)
                           . Form::close()
                           . '</div>';
                }
            })
            ->rawColumns(['goal_name', 'indicator_name', 'interactions']);
    }

    /**
     * Get the query object to be processed by dataTables.
     *
     * @return \Illuminate\Database\Query\Builder
     */
    public function query()
    {
        return $query = DB::table('goals')
            ->join('indicators', 'indicators.id', '=', 'goals.indicator_id')
            ->select('goals.id as goal_id', 'goals.name as goal_name', 'goals.updated_at as goal_updated_at',
                'goals.deleted_at as goal_deleted_at', 'indicators.id as indicator_id',
                'indicators.name as indicator_name');
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        $lang = app()->getLocale();
        return $this->builder()->columns($this->getColumns())
            ->setTableAttribute([
                'class' => 'table dt-responsive table-condensed table-hover display',
                'width' => '100%',
            ])
            ->parameters([
                'dom'        =>
                    "<'row'<'col-xs-12 col-sm-6 col-md-6 separator-xs-top separator-xs-bottom'B><'col-xs-12 col-sm-6 col-md-6 separator-xs-top separator-xs-bottom'f<'#new-checkbox.checkbox checkbox-ceap'>>>" .
                    "<'row'<'col-xs-12 col-sm-6 col-md-6 separator-xs-top separator-xs-bottom'l>>" .
                    "<'row'<'col-sm-12'tr>>" .
                    "<'row'<'col-sm-6'i><'col-sm-6'p>>",
                'lengthMenu' => [[5, 10, 25, 50], [5, 10, 25, 50]],
                'pagingType' => "simple",
                'buttons'    => [
                    'export',
                    'print'
                ],
                'language'   => [
                    'url' => url("datatable/{$lang}.json"),
                ],
                'responsive' => true,
                'order'      => [[0, 'asc']],
                'stateSave'  => true,
                'createdRow' => 'function (row, data, index) {if (data.goal_deleted_at !== null) {row.style.backgroundColor = "#ffcccc";}}',
            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            [
                'data'               => 'goal_name',
                'name'               => 'goals.name as goal_name',
                'title'              => removeCollon(trans('ceap.table.goal')),
                'searchable'         => true,
                'orderable'          => true,
                'exportable'         => true,
                'printable'          => true,
                'responsivePriority' => 1,
            ],
            [
                'data'               => 'indicator_name',
                'name'               => 'indicators.name as indicator_name',
                'title'              => removeCollon(trans('ceap.table.indicator')),
                'searchable'         => true,
                'orderable'          => true,
                'exportable'         => true,
                'printable'          => true,
                'responsivePriority' => 2,
            ],
            [
                'data'               => 'goal_updated_at',
                'name'               => 'goals.updated_at as goal_updated_at',
                'title'              => removeCollon(trans('ceap.table.updated_at')),
                'searchable'         => true,
                'orderable'          => true,
                'exportable'         => true,
                'printable'          => true,
                'responsivePriority' => 2,
            ],
            [
                'data'               => 'interactions',
                'title'              => trans('ceap.table.interactions'),
                'searchable'         => false,
                'orderable'          => false,
                'exportable'         => false,
                'printable'          => false,
                'responsivePriority' => 2,
            ],
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return trans('ceap.datatable.filenames.goals') . '_' . Carbon::now()->toAtomString();
    }
}
