<?php

namespace App\DataTables;

use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\DataTables;
use Yajra\DataTables\Services\DataTable;

class IndicatorsByGroupDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param \Yajra\DataTables\DataTables $dataTables
     * @param \Illuminate\Database\Query\Builder $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable(DataTables $dataTables, $query)
    {
        return $dataTables->query($query)
            ->editColumn('interactions', function ($item) {
                return '<div class="btn-group" role="group">'
                    . link_to('#', trans('ceap.button.edit'), [
                        'class'             => 'btn btn-primary panel-title-buttons edit-indicator-btn',
                        'style'             => 'margin-top: 3px;margin-bottom: 3px;border-radius: 4px;',
                        'data-toggle'       => 'modal',
                        'data-target'       => '#editIndicatorModal',
                        'data-title'        => trans('ceap.button.edit'),
                        'data-indicator_id' => $item->indicator_id,
                    ])
                    . link_to("#", trans('ceap.button.delete'), [
                        'class'             => 'btn btn-danger panel-title-buttons',
                        'style'             => 'margin-top: 3px;margin-bottom: 3px;border-radius: 4px;',
                        'data-toggle'       => 'modal',
                        'data-target'       => '#deleteIndicatorModal',
                        'data-title'        => trans('ceap.button.delete'),
                        'data-indicator_id' => $item->indicator_id,
                    ])
                    . '</div>';
            })
            ->rawColumns(['indicator_name', 'indicator_description', 'interactions']);
    }

    /**
     * Get the query object to be processed by dataTables.
     *
     * @return \Illuminate\Database\Query\Builder
     */
    public function query()
    {
        $query = DB::table('indicators')
            ->select('indicators.id AS indicator_id',
                'indicators.name AS indicator_name',
                'indicators.description AS indicator_description',
                'indicators.indicator_group_id AS indicator_group_id')
            ->where('indicators.deleted_at', null);

        $indicator_group_id = $this->request()->get('indicatorGroupId');
        if (is_numeric($indicator_group_id)) {
            $query->where('indicator_group_id', $indicator_group_id);
        } else {
            $query->where('indicator_group_id', '');
        }

        return $query;
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        $lang = app()->getLocale();
        return $this->builder()->columns($this->getColumns())
            ->setTableAttribute([
                'class' => 'table dt-responsive table-condensed table-hover display',
                'width' => '100%',
                'id'    => 'indicatorsByGroupDT',
            ])
            ->parameters([
                'dom'        => "<'row'<'col-sm-12'tr>>",
                'language'   => [
                    'url' => url("datatable/{$lang}.json"),
                ],
                'buttons'    => [
                    'export',
                    'print',
                ],
                'responsive' => true,
                'paging'     => false,
                'searching'  => false,
                'info'       => false,
                'order'      => [[0, 'asc']],
                'autoWidth'  => false,
            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            [
                'data'               => 'indicator_name',
                'name'               => 'indicators.name AS indicator_name',
                'title'              => removeCollon(trans('ceap.table.indicator')),
                'width'              => '20%',
                'searchable'         => true,
                'orderable'          => true,
                'exportable'         => true,
                'printable'          => true,
                'responsivePriority' => 1,
            ],
            [
                'data'               => 'indicator_description',
                'name'               => 'indicators.description AS indicator_description',
                'title'              => removeCollon(trans('ceap.table.description')),
                'width'              => '70%',
                'searchable'         => true,
                'orderable'          => true,
                'exportable'         => true,
                'printable'          => true,
                'responsivePriority' => 3,
            ],
            [
                'data'               => 'interactions',
                'title'              => trans('ceap.table.interactions'),
                'width'              => '10%',
                'searchable'         => false,
                'orderable'          => false,
                'exportable'         => false,
                'printable'          => false,
                'responsivePriority' => 2,
            ],
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return trans('ceap.datatable.filenames.indicators') . '_' . Carbon::now()->toAtomString();
    }
}
