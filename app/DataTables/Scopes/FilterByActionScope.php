<?php

namespace App\DataTables\Scopes;

use App\Models\CEAP\Action;
use Yajra\DataTables\Contracts\DataTableScope;

/**
 * Class FilterByActionScope
 * @package App\DataTables\Scopes
 */
class FilterByActionScope implements DataTableScope
{
    /**
     * @var Action
     */
    protected $action;

    /**
     * @var string
     */
    protected $relation;

    /**
     * FilterByActionScope constructor.
     * @param Action $action
     * @param string $relation
     */
    public function __construct(Action $action, $relation = 'action_id')
    {
        $this->action = $action;
        $this->relation = $relation;
    }

    /**
     * Apply a query scope.
     *
     * @param \Illuminate\Database\Query\Builder|\Illuminate\Database\Eloquent\Builder $query
     * @return mixed
     */
    public function apply($query)
    {
        return $query->where($this->relation, $this->action->id);
    }
}
