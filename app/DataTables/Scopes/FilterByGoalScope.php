<?php

namespace App\DataTables\Scopes;

use App\Models\CEAP\Goal;
use Yajra\DataTables\Contracts\DataTableScope;

/**
 * Class FilterByActionScope
 * @package App\DataTables\Scopes
 */
class FilterByGoalScope implements DataTableScope
{
    /**
     * @var Goal
     */
    protected $goal;

    /**
     * @var string
     */
    protected $relation;

    /**
     * FilterByGoalScope constructor.
     * @param Goal $goal
     * @param string $relation
     */
    public function __construct(Goal $goal, $relation = 'goal_id')
    {
        $this->goal = $goal;
        $this->relation = $relation;
    }

    /**
     * Apply a query scope.
     *
     * @param \Illuminate\Database\Query\Builder|\Illuminate\Database\Eloquent\Builder $query
     * @return mixed
     */
    public function apply($query)
    {
        return $query->where($this->relation, $this->goal->id);
    }
}
