<?php

namespace App\DataTables;

use Carbon\Carbon;
use Form;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\DataTables;
use Yajra\DataTables\Services\DataTable;

class AgreementsDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param \Yajra\DataTables\DataTables $dataTables
     * @param \Illuminate\Database\Query\Builder $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable(DataTables $dataTables, $query)
    {
        $checked = $this->request()->input('checked');
        if ($checked === "0") {
            $query->whereNull('agreements.deleted_at');
        }

        return $dataTables->query($query)
            ->editColumn('agreement_title', function ($item) {
                if (!$item->agreement_deleted_at) {
                    return link_to_route('frontend.ceap.agreements.show', $item->agreement_title,
                        ['agreement' => $item->agreement_id]);
                } else {
                    return $item->agreement_title;
                }
            })
            ->editColumn('agreement_details', function ($item) {
                return $item->agreement_details;
            })
            ->editColumn('agreement_role_names', function ($item) {
                $text = '';
                $names = explode(',', $item->agreement_role_names);
                foreach ($names as $name) {
                    $text .= "<a style='white-space: pre-wrap;' onclick='alteraBusca(this)'>" . $name . '</a></br>';
                }
                return "<p style='white-space: pre-wrap;'>" . $text . '</p>';
            })
            ->editColumn('action_names', function ($item) {
	            if ($item->action_names === null) {
		            return '&mdash;';
	            } else {
		            return "<a style='white-space: pre-wrap;' onclick='alteraBusca(this)'>" . $item->action_names . '</a>';
	            }
            })
            ->editColumn('evaluation_cycle_name', function ($item) {
                $evaluation_cycle_name  = trans('ceap.agreements.cycles.' . $item->evaluation_cycle_name);
                return "<a style='white-space: pre-wrap;' onclick='alteraBusca(this)'>" . $evaluation_cycle_name . '</a>';
            })
            ->addColumn('interactions', function ($item) {
                if (!$item->agreement_deleted_at) {
                    return '<div class="btn-group" role="group">'
                           . '<a class="btn btn-primary panel-title-buttons" style="margin-top: 0;"'
                           . ' data-toggle="modal" data-target="#agreementTrashModal" data-agreement-id="'
                           . $item->agreement_id
                           . '">'
                           . trans('ceap.button.trash')
                           . '</a>'
                           . '</div>';
                } else {
                    return '<div class="btn-group" role="group">'
                           . Form::open([
                            'method' => 'POST',
                            'route'  => ['frontend.ceap.agreements.restore'],
                        ])
                           . Form::hidden('agreement_id', $item->agreement_id)
                           . Form::button(trans('ceap.button.restore'),
                            [
                                'type'  => 'submit',
                                'class' => 'btn btn-primary panel-title-buttons',
                                'style' => 'margin-top: 3px;margin-bottom: 3px;',
                            ])
                           . Form::close()
                           . '</div>';
                }
            })
            ->rawColumns([
                'agreement_title',
                'agreement_details',
                'agreement_role_names',
                'action_names',
                'evaluation_cycle_name',
                'interactions'
            ]);
    }

    /**
     * Get the query object to be processed by dataTables.
     *
     * @return \Illuminate\Database\Query\Builder
     */
    public function query()
    {
        return $query = DB::table('agreements')
            ->join('evaluation_cycles', 'evaluation_cycles.id', '=', 'agreements.evaluation_cycle_id')
            ->leftJoin('agreement_agreement_role', 'agreement_agreement_role.agreement_id', '=', 'agreements.id')
            ->leftJoin('agreement_roles', 'agreement_roles.id', '=', 'agreement_agreement_role.agreement_role_id')
            ->leftJoin('action_agreement', 'action_agreement.agreement_id', '=', 'agreements.id')
            ->leftJoin('actions', 'actions.id', '=', 'action_agreement.action_id')
            ->select('agreements.id AS agreement_id',
                'agreements.title AS agreement_title',
                'agreements.details AS agreement_details',
                'evaluation_cycles.id AS evaluation_cycle_id',
                'evaluation_cycles.name AS evaluation_cycle_name',
                'agreements.deleted_at AS agreement_deleted_at'
            )
            ->selectRaw('group_concat(DISTINCT agreement_roles.id SEPARATOR ",\n") AS agreement_role_ids')
            ->selectRaw('group_concat(DISTINCT agreement_roles.name ORDER BY agreement_roles.name ASC SEPARATOR ",") AS agreement_role_names')
            ->selectRaw('group_concat(DISTINCT actions.id SEPARATOR ",\n") AS action_ids')
            ->selectRaw('group_concat(DISTINCT actions.name ORDER BY actions.name ASC SEPARATOR ",\n") AS action_names')
            ->groupBy('agreements.id', 'agreements.title', 'agreements.details', 'evaluation_cycles.id', 'evaluation_cycles.name', 'agreements.deleted_at');
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        $lang = config('app.locale');
        return $this->builder()
            ->columns($this->getColumns())
            ->minifiedAjax('')
            ->setTableAttribute([
                'class' => 'table dt-responsive table-condensed table-hover display',
                'width' => '100%',
            ])
            ->parameters([
                'responsive'    => true,
                'stateSave'     => true,
                'stateDuration' => '-1',
                'lengthMenu'    => [[5, 10, 25, 50], [5, 10, 25, 50]],
                'pagingType'    => 'simple',
                'order'         => [[0, 'asc']],
                'buttons'    => [
                    'export',
                    'print'
                ],
                'language'      => [
                    'url' => url("datatable/{$lang}.json"),
                ],
                'dom'           =>
                    "<'row'<'col-xs-12 col-sm-6 col-md-6 separator-xs-top separator-xs-bottom'B><'col-xs-12 col-sm-6 col-md-6 separator-xs-top separator-xs-bottom'f<'#new-checkbox.checkbox checkbox-ceap'>>>" .
                    "<'row'<'col-xs-12 col-sm-6 col-md-6 separator-xs-top separator-xs-bottom'l>>" .
                    "<'row'<'col-sm-12'tr>>" .
                    "<'row'<'col-sm-6'i><'col-sm-6'p>>",
                'createdRow'    => 'function (row, data, index) {if (data.agreement_deleted_at !== null) {row.style.backgroundColor = "#ffcccc";}}',
            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            [
                'data'               => 'agreement_title',
                'name'               => 'agreements.title AS agreement_title',
                'title'              => removeCollon(trans('ceap.agreements.title.label')),
                'searchable'         => true,
                'orderable'          => true,
                'exportable'         => true,
                'printable'          => true,
                'responsivePriority' => 1,
                'width'              => '20%',
            ],
            [
                'data'               => 'agreement_details',
                'name'               => 'agreements.details AS agreement_details',
                'title'              => removeCollon(trans('ceap.agreements.details.label')),
                'searchable'         => true,
                'orderable'          => true,
                'exportable'         => true,
                'printable'          => true,
                'responsivePriority' => 3,
                'width'              => '20%',
            ],
            [
                'data'               => 'agreement_role_names',
                'name'               => 'agreement_roles.name',
                'title'              => removeCollon(trans('ceap.agreements.roles.label')),
                'searchable'         => true,
                'orderable'          => false,
                'exportable'         => true,
                'printable'          => true,
                'responsivePriority' => 2,
                'width'              => '15%',
            ],
            [
                'data'               => 'action_names',
                'name'               => 'actions.name',
                'title'              => removeCollon(trans('ceap.agreements.actions.label')),
                'searchable'         => true,
                'orderable'          => false,
                'exportable'         => true,
                'printable'          => false,
                'responsivePriority' => 4,
                'width'              => '15%',
            ],
            [
                'data'               => 'evaluation_cycle_name',
                'name'               => 'evaluation_cycles.name AS evaluation_cycle_name',
                'title'              => removeCollon(trans('ceap.agreements.execution_cycle.label')),
                'searchable'         => true,
                'orderable'          => true,
                'exportable'         => true,
                'printable'          => false,
                'responsivePriority' => 5,
                'width'              => '20%',
            ],
            [
                'data'               => 'interactions',
                'title'              => trans('ceap.table.interactions'),
                'searchable'         => false,
                'orderable'          => false,
                'exportable'         => false,
                'printable'          => false,
                'responsivePriority' => 5,
                'width'              => '10%',
            ],
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return trans('ceap.datatable.filenames.agreements') . '_' . Carbon::now()->toAtomString();
    }
}
