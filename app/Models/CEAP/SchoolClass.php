<?php

namespace App\Models\CEAP;

use Askedio\SoftCascade\Traits\SoftCascadeTrait;
use DB;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class SchoolClass
 * @package App\Models\CEAP
 */
class SchoolClass extends Model
{
    use SoftDeletes;
    use SoftCascadeTrait;

    /*
     * Existing types for Indicator Groups.
     */
    public const ELEMENTARY = 'elementary_school';
    public const MIDDLE = 'middle_school';
    public const HIGH = 'high_school';

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

	/**
	 * List of related Eloquent models to be soft deleted.
	 *
	 * @var array
	 */
	protected $softCascade = ['indicatorRecords'];

    /**
     * Get a collection of SchoolGradeGroups with respective translations
     * @return mixed
     */
    public static function getSchoolGradeGroups()
    {
        $resp = collect([]);
        $resp->put(self::ELEMENTARY, trans('ceap.indicators.records.school_grade_group.' . self::ELEMENTARY));
        $resp->put(self::MIDDLE, trans('ceap.indicators.records.school_grade_group.' . self::MIDDLE));
        $resp->put(self::HIGH, trans('ceap.indicators.records.school_grade_group.' . self::HIGH));

        return $resp;
    }

    /**
     * @param $indicatorId
     * @param $recordDate
     * @param $schoolClassYear
     * @return \Illuminate\Support\Collection
     */
    public static function getSchoolClassesRecords($indicatorId, $recordDate, $schoolClassYear)
    {
        return DB::table('school_classes')
            ->join('indicator_records', 'school_classes.id', '=', 'indicator_records.school_class_id')
            ->select('school_classes.id as school_class_id',
                'school_classes.year as school_year',
                'school_classes.grade AS school_grade',
                'school_classes.room as school_room',
                'indicator_records.id AS indicator_record_id',
                'indicator_records.indicator_id AS indicator_id',
                'indicator_records.value AS indicator_record_value',
                'indicator_records.date AS indicator_record_date')
            ->where('indicator_records.indicator_id', '=', $indicatorId)
            ->where('indicator_records.date', '=', $recordDate)
            ->where('school_classes.year', $schoolClassYear)->get();
    }

    /**
     * Gets the IndicatorRecords related to this SchoolClass
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function indicatorRecords()
    {
        return $this->hasMany(IndicatorRecord::class);
    }

    /**
     * Scope a query to only get distinct years.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeDistinctYears(Builder $query)
    {
        return $query->select('year')->distinct();
    }

    /**
     * Scope a query to only get distinct grades filtered by Year.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param string $year
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeDistinctGradesByYear(Builder $query, $year)
    {
        return $query->where('year', $year)->select('grade')->distinct();
    }

    /**
     * Scope a query to get distinct grades filtered by Year.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param string $year
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeDistinctGradesAndRoomsByYear(Builder $query, $year)
    {
        return $query->where('year', $year)->select(['grade', 'room'])->distinct();
    }

    /**
     * Scope a query to get IndicatorRecords filtered by Year.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param string $year
     * @param string $grade
     * @param string $room
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeIndicatorRecordsByYear(Builder $query, $year)
    {
        return $query
            ->leftJoin('indicator_records', 'school_classes.id', '=', 'indicator_records.school_class_id')
            ->where('year', $year)
            ->latest('date');
    }

    /**
     * Scope a query to get IndicatorRecords filtered by Year, Grade and Room.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param string $year
     * @param string $grade
     * @param string $room
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeIndicatorRecordsByYearGradeAndRoom(Builder $query, $year, $grade, $room)
    {
        return $query
            ->leftJoin('indicator_records', 'school_classes.id', '=', 'indicator_records.school_class_id')
            ->where('year', $year)
            ->where('grade', $grade)
            ->where('room', $room)
            ->latest('date');
    }

    /**
     * Scope a query to get Distinct Rooms filtered by Year and Grade.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param string $year
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeDistinctRoomsByYearAndGrade(Builder $query, $year, $grade)
    {
        return $query->where('year', $year)->where('grade', $grade)->select('room')->distinct();
    }

    /**
     * Scope a query to get the SchoolClass related to given Year, Grade and Room.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param $year
     * @param $grade
     * @param $room
     * @return Builder
     */
    public function scopeGetSchoolClassByYearGradeAndRoom(Builder $query, $year, $grade, $room)
    {
        return $query
            ->where('year', $year)
            ->where('grade', $grade)
            ->where('room', $room);
    }

    /**
     * @param $schoolClassYear
     * @return \Illuminate\Support\Collection
     */
    public static function getSchoolClassesForYear($schoolClassYear)
    {
        return DB::table('school_classes')
            ->select('school_classes.id as indicator_record_id',
                'school_classes.year as school_year',
                'school_classes.grade AS school_grade',
                'school_classes.room as school_room', DB::raw('0.0 as indicator_record_value'))
            ->where('school_classes.year', $schoolClassYear)->get();

    }
}
