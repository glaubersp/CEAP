<?php

namespace App\Models\CEAP;

use App\Models\Auth\Traits\Attribute\TopicTagAttribute;
use Askedio\SoftCascade\Traits\SoftCascadeTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class TopicTag
 * @package App\Models\CEAP
 */
class TopicTag extends Model
{

    use SoftDeletes;
    use SoftCascadeTrait;
    use TopicTagAttribute;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

	/**
	 * List of related Eloquent models to be soft deleted.
	 *
	 * @var array
	 */
	protected $softCascade = [];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function agreements()
    {
        return $this->belongsToMany(Agreement::class)->withTimestamps();
    }
}
