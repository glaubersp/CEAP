<?php

namespace App\Models\CEAP;

use Askedio\SoftCascade\Traits\SoftCascadeTrait;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


/**
 * Class Indicator
 * @package App\Models\CEAP
 */
class Indicator extends Model
{
    use SoftDeletes;
    use SoftCascadeTrait;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'indicators';

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

	/**
	 * List of related Eloquent models to be soft deleted.
	 *
	 * @var array
	 */
	protected $softCascade = ['goal', 'indicatorRecords'];

    /**
     * Get the Meta record associated to this Indicador.
     */
    public function goal()
    {
        return $this->hasOne(Goal::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function indicatorGroup()
    {
        return $this->belongsTo(IndicatorGroup::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function evaluationCategory()
    {
        return $this->belongsTo(EvaluationCategory::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function indicatorRecords()
    {
        return $this->hasMany(IndicatorRecord::class);
    }

    /**
     * Scope to filter Indicators by name.
     *
     * @param Builder $query
     * @param string $name
     * @return Builder
     */
    public function scopeFindByName(Builder $query, $name)
    {
        return $query->where('name', $name);
    }

    /**
     * @param Builder $query
     * @return Builder $this
     */
    public function scopeGetRoomsIndicatorDataForGraphic(Builder $query)
    {
        return $query
            ->join('indicator_records', 'indicator_records.indicator_id', '=', 'indicators.id')
            ->join('school_classes', 'school_classes.id', '=', 'indicator_records.school_class_id')
            ->select('indicator_records.value', 'indicator_records.date', 'indicator_records.indicator_id',
                'indicator_records.school_class_id', 'school_classes.year', 'school_classes.grade',
                'school_classes.room')->distinct();
    }

    /**
     * @param Builder $query
     * @return Builder $this
     */
    public function scopeGetSchoolIndicatorDataForGraphic(Builder $query)
    {
        return $query
            ->join('indicator_records', 'indicator_records.indicator_id', '=', 'indicators.id')
            ->select('indicators.id AS indicator_id', 'indicators.name AS indicator_name'
                , 'indicator_records.id AS indicator_record_id'
                , 'indicator_records.value AS indicator_record_value'
                , 'indicator_records.date AS indicator_record_date')
            ->where('indicators.id', $this->id);
    }
}
