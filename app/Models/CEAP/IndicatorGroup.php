<?php

namespace App\Models\CEAP;

use Askedio\SoftCascade\Traits\SoftCascadeTrait;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;

/**
 * Class IndicatorGroup
 * @package App\Models\CEAP
 */
class IndicatorGroup extends Model {
	use SoftDeletes;
	use SoftCascadeTrait;

	/*
	 * Existing types for Indicator Groups.
	 */
	public const CEAP = 'ceap';
	public const ROOMS = 'rooms';
	public const SCHOOL = 'school';

	/**
	 * The attributes that should be mutated to dates.
	 *
	 * @var array
	 */
	protected $dates = [ 'next_evaluation_date', 'deleted_at' ];

	/**
	 * List of related Eloquent models to be soft deleted.
	 *
	 * @var array
	 */
	protected $softCascade = [ 'indicators' ];

	/**
	 * Get a collection of IndicatorGroup types with respective translations
	 * @return mixed
	 */
	public static function getIndicatorGroupTypes() {
		$resp = collect( [] );
		$resp->put( self::CEAP, trans( 'ceap.indicators.group.type.' . self::CEAP ) );
		$resp->put( self::ROOMS, trans( 'ceap.indicators.group.type.' . self::ROOMS ) );
		$resp->put( self::SCHOOL, trans( 'ceap.indicators.group.type.' . self::SCHOOL ) );

		return $resp;
	}

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 */
	public function evaluationCycle() {
		return $this->belongsTo( EvaluationCycle::class );
	}

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\HasMany
	 */
	public function indicators() {
		return $this->hasMany( Indicator::class );
	}

	/**
	 * @return string
	 */
	public function getNextEvaluationDateAttribute() {
        return Carbon::parse($this->attributes['next_evaluation_date'])->format('Y-m-d');
	}

	/**
	 * @param $date
	 *
	 * @throws \InvalidArgumentException
	 */
	public function setNextEvaluationDateAttribute( string $date ) {
        $this->attributes['next_evaluation_date'] = Carbon::createFromFormat('Y-m-d', $date);

	}

	/**
	 * @param Builder $query
	 *
	 * @return Builder $this
	 * @internal param $group_id
	 */
	public function scopeGetType( Builder $query ) {
		return $query->select( 'type' );
	}

	/**
	 * @param Builder $query
	 * @param $group_id
	 *
	 * @return Builder $this
	 */
	public function scopeGetReportDates( Builder $query, $group_id ) {
		return $query
			->join( 'indicators', 'indicators.indicator_group_id', '=', 'indicator_groups.id' )
			->join( 'indicator_records', 'indicator_records.indicator_id', '=', 'indicators.id' )
			->where( 'indicator_groups.id', $group_id )
            ->where( 'indicators.deleted_at', null )
			->select( 'indicator_records.date' )->distinct();
	}

	/**
	 * @param Builder $query
	 * @param $indicatorGroupId
	 *
	 * @return Builder $this
	 */
	public function scopeGetRoomsReportDates( Builder $query, $indicatorGroupId ) {
		return $query
			->join( 'indicators', 'indicators.indicator_group_id', '=', 'indicator_groups.id' )
			->join( 'indicator_records', 'indicator_records.indicator_id', '=', 'indicators.id' )
			->join( 'school_classes', 'school_classes.id', '=', 'indicator_records.school_class_id' )
			->where( 'indicator_groups.id', $indicatorGroupId )
            ->where( 'indicators.deleted_at', null )
			->select( 'school_classes.year', 'indicator_records.date', 'indicator_records.indicator_id',
				'indicators.name' )
			->distinct()
			->orderBy( 'indicator_records.indicator_id' )
			->orderBy( 'school_classes.year' )
			->orderBy( 'indicator_records.date' );
	}

	/**
	 * @param Builder $query
	 * @param $indicatorGroupId
	 *
	 * @return Builder $this
	 */
	public function scopeGetCEAPReportDates( Builder $query, $indicatorGroupId ) {
		return $query
			->join( 'indicators', 'indicators.indicator_group_id', '=', 'indicator_groups.id' )
			->join( 'indicator_records', 'indicator_records.indicator_id', '=', 'indicators.id' )
			->where( 'indicator_groups.id', $indicatorGroupId )
            ->where( 'indicators.deleted_at', null )
			->select( 'indicator_records.date', 'indicator_records.school_grade_group' )->distinct();
	}

	/**
	 * @param Builder $query
	 * @param $indicatorGroupId
	 *
	 * @return Builder $this
	 */
	public function scopeGetSchoolReportDates( Builder $query, $indicatorGroupId ) {
		return $query
			->join( 'indicators', 'indicators.indicator_group_id', '=', 'indicator_groups.id' )
			->join( 'indicator_records', 'indicator_records.indicator_id', '=', 'indicators.id' )
			->where( 'indicator_groups.id', $indicatorGroupId )
            ->where( 'indicators.deleted_at', null )
			->select( 'indicator_records.date' )->distinct();
	}

	/**
	 * @param Builder $query
	 * @param $group_id
	 * @param $report_date
	 *
	 * @return Builder $this
	 */
	public function scopeGetSchoolGradeGroups( Builder $query, $group_id, $report_date ) {
		return $query->join( 'indicators', 'indicators.indicator_group_id', '=', 'indicator_groups.id' )
		             ->join( 'indicator_records', 'indicator_records.indicator_id', '=', 'indicators.id' )
		             ->where( 'indicator_groups.id', $group_id )
		             ->where( 'indicator_records.date', $report_date )
                     ->where( 'indicators.deleted_at', null )
		             ->select( 'indicator_records.school_grade_group' )->distinct();
	}

	/**
	 * @param Builder $query
	 * @param $group_id
	 *
	 * @return Builder $this
	 */
	public function scopeGetReportDatesAndSchoolGradeGroups( Builder $query, $group_id ) {
		return $query->join( 'indicators', 'indicators.indicator_group_id', '=', 'indicator_groups.id' )
		             ->join( 'indicator_records', 'indicator_records.indicator_id', '=', 'indicators.id' )
		             ->where( 'indicator_groups.id', $group_id )
                     ->where( 'indicators.deleted_at', null )
		             ->select( 'indicator_records.date', 'indicator_records.school_grade_group' )->distinct();
	}

	/**
	 * @param Builder $query
	 * @param $group_id
	 * @param $report_date
	 * @param $grade_group
	 *
	 * @return Builder $this
	 */
	public function scopeGetIndicatorsFilteredByIndicatorGroupReportDateAndSchoolGradeGroup(
		Builder $query,
		$group_id,
		$report_date,
		$grade_group
	) {
		return $query->join( 'indicators', 'indicators.indicator_group_id', '=', 'indicator_groups.id' )
		             ->join( 'indicator_records', 'indicator_records.indicator_id', '=', 'indicators.id' )
		             ->where( 'indicator_groups.id', $group_id )
		             ->where( 'indicator_records.date', $report_date )
		             ->where( 'indicator_records.school_grade_group', $grade_group )
                     ->where( 'indicators.deleted_at', null )
		             ->select( 'indicators.id AS indicator_id',
			             'indicators.name AS indicator_name',
			             'indicator_records.id AS indicator_record_id',
			             'indicator_records.value AS indicator_record_value',
			             'indicator_records.date AS record_date',
			             'indicator_records.school_grade_group AS record_school_grade_group' );
	}

	/**
	 * @param Builder $query
	 * @param $group_id
	 *
	 * @return Builder $this
	 */
	public function scopeGetReportedDatesAndSchoolGradeGroupsForGroupId( Builder $query, $group_id ) {
		return $query->join( 'indicators', 'indicators.indicator_group_id', '=', 'indicator_groups.id' )
		             ->join( 'indicator_records', 'indicator_records.indicator_id', '=', 'indicators.id' )
		             ->where( 'indicator_groups.id', $group_id )
		             ->select( 'indicator_records.date', 'indicator_records.school_grade_group' )->distinct();
	}

	public function scopeGetIndicatorRecordsForForGroupIdAndRecordDate( Builder $query, $groupId, $recordDate ) {
		return $query->join( 'indicators', 'indicators.indicator_group_id', '=', 'indicator_groups.id' )
		             ->join( 'indicator_records', 'indicator_records.indicator_id', '=', 'indicators.id' )
		             ->where( 'indicator_groups.id', $groupId )
		             ->where( 'indicator_records.date', $recordDate )
		             ->where( 'indicators.deleted_at', null )
		             ->select( 'indicators.id as indicator_id', 'indicators.name as indicator_name',
			             'indicator_records.id as indicator_record_id',
			             'indicator_records.value as indicator_record_value' );
	}

	/**
	 * @param Builder $query
	 * @param $groupId
	 *
	 * @return Builder $this
	 */
	public function scopeGetIndicatorsFilteredByGroup( Builder $query, $groupId ) {
		return $query->join( 'indicators', 'indicators.indicator_group_id', '=', 'indicator_groups.id' )
		             ->where( 'indicator_groups.id', $groupId )
		             ->where( 'indicators.deleted_at', null )
		             ->select( 'indicators.id as indicator_record_id', 'indicators.name as indicator_name',
			             DB::raw( '0.0 as record_value' ) );
	}
}
