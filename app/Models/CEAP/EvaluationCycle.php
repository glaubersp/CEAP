<?php

namespace App\Models\CEAP;

use Askedio\SoftCascade\Traits\SoftCascadeTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class EvaluationCycle
 * @package App\Models\CEAP
 */
class EvaluationCycle extends Model
{

    use SoftDeletes;
    use SoftCascadeTrait;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'evaluation_cycles';

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

	/**
	 * List of related Eloquent models to be soft deleted.
	 *
	 * @var array
	 */
	protected $softCascade = ['indicators', 'agreements'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function indicators()
    {
        return $this->hasMany(Indicator::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function agreements()
    {
        return $this->hasMany(Agreement::class);
    }

    /**
     * @return array|\Illuminate\Contracts\Translation\Translator|null|string
     */
    public function getNameAttribute($value)
    {
        return trans('ceap.agreements.cycles.' . $value);
    }
}
