<?php

namespace App\Models\CEAP;

use Askedio\SoftCascade\Traits\SoftCascadeTrait;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class GoalEvaluation
 * @package App\Models\CEAP
 */
class GoalEvaluation extends Model
{

    use SoftDeletes;
    use SoftCascadeTrait;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'goal_evaluations';

    /**
     * @var array
     */
    protected $dates = ['evaluation_date', 'deleted_at'];

    /**
     * Allowed collumns to execute mass storage
     * @var array
     */
    protected $fillable = ['rating', 'evaluation_date', 'justification'];

	/**
	 * List of related Eloquent models to be soft deleted.
	 *
	 * @var array
	 */
	protected $softCascade = [];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function goal()
    {
        return $this->belongsTo(Goal::class);
    }

    /**
     * @return string
     */
    public function getEvaluationDateAttribute()
    {
        return Carbon::parse($this->attributes['evaluation_date'])->format('Y-m-d');
    }

    /**
     * @param $date
     */
    public function setEvaluationDateAttribute($date)
    {
        $this->attributes['evaluation_date'] = Carbon::createFromFormat('Y-m-d', $date);
    }
}
