<?php

namespace App\Models\CEAP;

use Askedio\SoftCascade\Traits\SoftCascadeTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


/**
 * Class Agreement
 * @package App\Models\CEAP
 */
class Agreement extends Model
{
    use SoftDeletes;
    use SoftCascadeTrait;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

	/**
	 * List of related Eloquent models to be soft deleted.
	 *
	 * @var array
	 */
	protected $softCascade = [];

    /**
     * Return the list of TopicTags to be shown in the view
     * @return array of strings
     */
    public function topicTagsList()
    {
        $r = [];
        foreach ($this->topicTags as $tag) {
            array_push($r, $tag->name);
        }
        return $r;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function topicTags()
    {
        return $this->belongsToMany(TopicTag::class)->withTimestamps();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function agreementRoles()
    {
        return $this->belongsToMany(AgreementRole::class)->withTimestamps();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function agreementMonitoringStrategy()
    {
        return $this->belongsTo(AgreementMonitoringStrategy::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function evaluationCycle()
    {
        return $this->belongsTo(EvaluationCycle::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function actions()
    {
        return $this->belongsToMany(Action::class)->withTimestamps();
    }
}
