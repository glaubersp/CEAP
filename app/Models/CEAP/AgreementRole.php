<?php

namespace App\Models\CEAP;

use App\Models\Auth\Traits\Attribute\AgreementRoleAttribute;
use Askedio\SoftCascade\Traits\SoftCascadeTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


/**
 * Class AgreementRole
 * @package App\Models\CEAP
 */
class AgreementRole extends Model
{
    use SoftDeletes;
    use SoftCascadeTrait;
    use AgreementRoleAttribute;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

	/**
	 * List of related Eloquent models to be soft deleted.
	 *
	 * @var array
	 */
	protected $softCascade = [];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function agreements()
    {
        return $this->belongsToMany(Agreement::class);
    }
}
