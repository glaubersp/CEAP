<?php

namespace App\Models\CEAP;

use App\Models\Auth\User;
use Askedio\SoftCascade\Traits\SoftCascadeTrait;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Action
 * @package App\Models\CEAP
 */
class Action extends Model
{

    use SoftDeletes;
	use SoftCascadeTrait;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'actions';

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['start_date', 'end_date', 'deleted_at'];


	/**
	 * List of related Eloquent models to be soft deleted.
	 *
	 * @var array
	 */
	protected $softCascade = ['evidences', 'evaluations'];


    /**
     * Gets the Goal that owns this Action.
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function goal()
    {
        return $this->belongsTo(Goal::class);
    }

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 */
	public function evaluationTerm()
	{
		return $this->belongsTo(EvaluationTerm::class);
	}

    /**
     * Gets the related Evidences
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function evidences()
    {
        return $this->hasMany(Evidence::class);
    }

    /**
     * Get the related Evaluations
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function evaluations()
    {
        return $this->hasMany(ActionEvaluation::class);
    }

    /**
     * Gets the Users that own this Action.
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function users()
    {
        return $this->belongsToMany(User::class)->withTimestamps();
    }

    /**
     * Gets the related Agreements
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function agreements()
    {
        return $this->belongsToMany(Agreement::class)->withTimestamps();
    }

    /**
     * @return string
     */
    public function getStartDateAttribute()
    {
        return Carbon::parse($this->attributes['start_date'])->format('Y-m-d');
    }

    /**
     * @param $date
     */
    public function setStartDateAttribute($date)
    {
        $this->attributes['start_date'] = Carbon::createFromFormat('Y-m-d', $date);
    }

    /**
     * @return string
     */
    public function getEndDateAttribute()
    {
        return Carbon::parse($this->attributes['end_date'])->format('Y-m-d');
    }

    /**
     * @param $date
     */
    public function setEndDateAttribute($date)
    {
        $this->attributes['end_date'] = Carbon::createFromFormat('Y-m-d', $date);
    }

    public function childAction()
    {
        return $this->hasOne(Action::class, 'parent_id', 'id');
    }

    public function parentAction()
    {
        return $this->belongsTo(Action::class, 'parent_id');
    }
}
