<?php

namespace App\Models\CEAP;

use App\Models\Auth\User;
use Askedio\SoftCascade\Traits\SoftCascadeTrait;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


/**
 * Class ActionEvaluation
 * @package App\Models\CEAP
 */
class ActionEvaluation extends Model
{
    use SoftDeletes;
    use SoftCascadeTrait;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'action_evaluations';

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['evaluation_date', 'deleted_at'];

    /**
     * Allowed collumns to execute mass storage
     * @var array
     */
    protected $fillable = ['evaluation', 'rating', 'justification', 'user_id', 'action_id'];

	/**
	 * List of related Eloquent models to be soft deleted.
	 *
	 * @var array
	 */
	protected $softCascade = [];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function action()
    {
        return $this->belongsTo(Action::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }


    /**
     * @return string|\Symfony\Component\Translation\TranslatorInterface
     */
    public function getEvaluationAttribute()
    {
        $text = $this->attributes['evaluation'];
        $translation = trans('ceap.actions.evaluations.modal.evaluation_options.' . $text);
        return $translation;
    }

    /**
     * @return string
     */
    public function getEvaluationDateAttribute()
    {
        return Carbon::parse($this->attributes['evaluation_date'])->format('Y-m-d');
    }

    /**
     * @param Carbon $date
     */
    public function setEvaluationDateAttribute(string $date)
    {
        $this->attributes['evaluation_date'] = Carbon::createFromFormat('Y-m-d', $date);
    }
}