<?php

namespace App\Models\CEAP;

use Askedio\SoftCascade\Traits\SoftCascadeTrait;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class IndicatorRecord
 * @package App\Models\CEAP
 */
class IndicatorRecord extends Model
{

    use SoftDeletes;
    use SoftCascadeTrait;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'indicator_records';

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['date', 'deleted_at'];

	/**
	 * List of related Eloquent models to be soft deleted.
	 *
	 * @var array
	 */
	protected $softCascade = [];


	/**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function indicator()
    {
        return $this->belongsTo(Indicator::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function schoolClass()
    {
        return $this->belongsTo(SchoolClass::class);
    }

    /**
     * @return string
     */
    public function getDateAttribute()
    {
        return Carbon::parse($this->attributes['date'])->format('Y-m-d');
    }

    /**
     * @param $date
     * @throws \InvalidArgumentException
     */
    public function setDateAttribute(string $date)
    {
        $this->attributes['date'] = Carbon::createFromFormat('Y-m-d', $date);

    }
}
