<?php

namespace App\Models\CEAP;

use Askedio\SoftCascade\Traits\SoftCascadeTrait;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\Auth\Traits\Attribute\EvaluationTermAttribute;

class EvaluationTerm extends Model
{

	use SoftDeletes;
	use SoftCascadeTrait;
	use EvaluationTermAttribute;

	/**
	 * The table associated with the model.
	 *
	 * @var string
	 */
	protected $table = 'evaluation_terms';

	/**
	 * The attributes that should be mutated to dates.
	 *
	 * @var array
	 */
	protected $dates = ['deleted_at'];

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\HasMany
	 */
	public function actions() {
		return $this->hasMany(Action::class);
	}

    /**
     * @param Builder $query
     * @return Builder
     */
    public function scopeCurrent(Builder $query){
        $year = Carbon::now()->year;
        $month = Carbon::now()->month;
        $sequence = 1 <= $month && $month <= 6 ? 1 : 2;
        return $query->where('year', '=', $year)->where('sequence', '=', $sequence)->first();
    }

    /**
     * @param Builder $query
     * @return Builder
     */
    public function scopeNext(Builder $query){
        $year = Carbon::now()->year;
        $month = Carbon::now()->month;
        $sequence = 1 <= $month && $month <= 6 ? 1 : 2;
        if($sequence === 1) {
            $sequence = 2;
        } else {
            $sequence = 1;
            $year = $year + 1;
        }
        return $query->where('year', '=', $year)->where('sequence', '=', $sequence)->first();
    }
}
