<?php

namespace App\Models\CEAP;

use Askedio\SoftCascade\Traits\SoftCascadeTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\Auth\Traits\Attribute\AgreementMonitoringStrategyAttribute;


/**
 * Class AgreementMonitoringStrategy
 * @package App\Models\CEAP
 */
class AgreementMonitoringStrategy extends Model
{

    use SoftDeletes;
    use SoftCascadeTrait;
    use AgreementMonitoringStrategyAttribute;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

	/**
	 * List of related Eloquent models to be soft deleted.
	 *
	 * @var array
	 */
	protected $softCascade = ['agreements'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function agreements()
    {
        return $this->hasMany(Agreement::class);
    }
}
