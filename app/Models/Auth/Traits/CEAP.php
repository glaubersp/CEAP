<?php

namespace App\Models\Auth\Traits;

use App\Models\CEAP\Action;

trait CEAP
{
    public function actions()
    {
        return $this->belongsToMany(Action::class)->withTimestamps();
    }
}