<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\Auth\Role;
use App\Models\Auth\User;
use App\Models\CEAP\AgreementMonitoringStrategy;
use App\Models\CEAP\AgreementRole;
use App\Models\CEAP\EvaluationTerm;
use App\Models\CEAP\TopicTag;

/**
 * Class DashboardController.
 */
class DashboardController extends Controller
{
    /**
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $users = User::count();
        $roles = Role::count();
        $agreement_roles = AgreementRole::count();
        $evaluation_terms = EvaluationTerm::count();
        $topic_tags = TopicTag::count();
        $monitoring_strategy = AgreementMonitoringStrategy::count();

        return view('backend.dashboard', compact("users", "roles", "agreement_roles", "evaluation_terms", "topic_tags", "monitoring_strategy"));
    }
}
