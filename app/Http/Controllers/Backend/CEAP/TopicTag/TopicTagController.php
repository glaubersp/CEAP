<?php

namespace App\Http\Controllers\Backend\CEAP\TopicTag;

use App\Http\Controllers\Controller;
use App\Models\CEAP\TopicTag;
use Illuminate\Http\Request;

class TopicTagController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $topic_tags = TopicTag::all()->sortBy('name');
        $topic_tags_count = TopicTag::count();

        return view('backend.ceap.topicTags.index', compact('topic_tags', 'topic_tags_count'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.ceap.topicTags.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name'       => 'required|unique:topic_tags,name',
        ]);

        $topic_tag = new TopicTag();
        $topic_tag->name = $request->name;

        $topic_tag->save();

        return redirect()->route('admin.ceap.topicTag.index')->withFlashSuccess(trans('ceap.backend.topic_tag.store'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(TopicTag $topicTag)
    {
        return view('backend.ceap.topicTags.edit', compact('topicTag'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, TopicTag $topicTag)
    {
        $this->validate($request, [
            'topic_tag_name'       => 'required|unique:topic_tags,name',
        ]);

        $topicTag->name = $request->topic_tag_name;

        $topicTag->save();

        return redirect()->route('admin.ceap.topicTag.index')->withFlashSuccess(trans('ceap.backend.topic_tag.update'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function destroy(TopicTag $topicTag)
    {
        $topicTag->delete();

        return redirect()->route('admin.ceap.topicTag.index')->withFlashSuccess(trans('ceap.backend.topic_tag.destroy'));
    }
}
