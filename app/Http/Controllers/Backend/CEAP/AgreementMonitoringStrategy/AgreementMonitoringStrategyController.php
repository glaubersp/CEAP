<?php

namespace App\Http\Controllers\Backend\CEAP\AgreementMonitoringStrategy;

use App\Http\Controllers\Controller;
use App\Models\CEAP\AgreementMonitoringStrategy;
use Illuminate\Http\Request;

class AgreementMonitoringStrategyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $monitoring_strategies = AgreementMonitoringStrategy::all()->sortBy('title');
        $monitoring_strategy_count = AgreementMonitoringStrategy::count();

        return view('backend.ceap.agreementMonitoringStrategies.index', compact('monitoring_strategies', 'monitoring_strategy_count'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.ceap.agreementMonitoringStrategies.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'title'       => 'required|unique:agreement_monitoring_strategies,title',
        ]);

        $monitoring_strategy = new AgreementMonitoringStrategy();
        $monitoring_strategy->title = $request->title;

        $monitoring_strategy->save();

        return redirect()->route('admin.ceap.monitoringStrategy.index')->withFlashSuccess(trans('ceap.backend.monitoring_strategy.store'));

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param AgreementMonitoringStrategy $monitoring_strategy_id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit(AgreementMonitoringStrategy $monitoringStrategy)
    {
        return view('backend.ceap.agreementMonitoringStrategies.edit', compact('monitoringStrategy'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, AgreementMonitoringStrategy $monitoringStrategy)
    {
        $this->validate($request, [
            'title'       => 'required|unique:agreement_monitoring_strategies,title',
        ]);

        $monitoringStrategy->title = $request->title;

        $monitoringStrategy->save();

        return redirect()->route('admin.ceap.monitoringStrategy.index')->withFlashSuccess(trans('ceap.backend.monitoring_strategy.update'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function destroy(AgreementMonitoringStrategy $monitoringStrategy)
    {
        $monitoringStrategy->delete();

        return redirect()->route('admin.ceap.monitoringStrategy.index')->withFlashSuccess(trans('ceap.backend.monitoring_strategy.destroy'));
    }
}
