<?php

namespace App\Http\Controllers\Backend\CEAP\AgreementRole;

use App\Http\Controllers\Controller;
use App\Models\CEAP\AgreementRole;
use Illuminate\Http\Request;

class AgreementRoleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $agreement_roles = AgreementRole::all()->sortBy('name');
        $agreement_roles_count = AgreementRole::count();

        return view('backend.ceap.agreementRoles.index', compact('agreement_roles', 'agreement_roles_count'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.ceap.agreementRoles.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'agreement_roles_name'       => 'required|unique:agreement_roles,name',
        ]);

        $agreement_role = new AgreementRole();
        $agreement_role->name = $request->agreement_roles_name;

        $agreement_role->save();

        return redirect()->route('admin.ceap.agreementRole.index')->withFlashSuccess(trans('ceap.backend.agreement_role.store'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(AgreementRole $agreementRole)
    {
        return view('backend.ceap.agreementRoles.edit', compact('agreementRole'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, AgreementRole $agreementRole)
    {
        $this->validate($request, [
            'agreement_roles_name'       => 'required|unique:agreement_roles,name',
        ]);

        $agreementRole->name = $request->agreement_roles_name;

        $agreementRole->save();

        return redirect()->route('admin.ceap.agreementRole.index')->withFlashSuccess(trans('ceap.backend.agreement_role.update'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(AgreementRole $agreementRole)
    {
        $agreementRole->delete();

        return redirect()->route('admin.ceap.agreementRole.index')->withFlashSuccess(trans('ceap.backend.agreement_role.destroy'));
    }
}
