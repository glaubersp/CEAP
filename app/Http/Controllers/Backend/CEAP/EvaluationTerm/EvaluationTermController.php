<?php

namespace App\Http\Controllers\Backend\CEAP\EvaluationTerm;

use App\Models\CEAP\EvaluationTerm;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class EvaluationTermController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $terms_select = EvaluationTerm::all()->sortBy('year');
        $terms_count = EvaluationTerm::count();

        return view('backend.ceap.evaluationTerms.index', compact('terms_select', 'terms_count'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $terms_select = EvaluationTerm::pluck('name', 'sequence')->unique();

        return view('backend.ceap.evaluationTerms.create', compact('terms_select'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'year'       => 'required|unique:evaluation_terms,year',
            'cycle'      => 'required',
        ]);

        $cycles = $request->cycle;

        if($cycles == 2) {
            for ($i = 1; $i <= $cycles; $i++) {
                $term_model = new EvaluationTerm();
                $term_model->year = $request->year;
                $term_model->sequence = $i;
                $term_model->name = 'Semestre ' . $i;
                $term_model->active = 0;
                $term_model->save();
            }
        }

        return redirect()->route('admin.ceap.evaluationTerm.index')->withFlashSuccess(trans('ceap.backend.evaluation_term.store'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(EvaluationTerm $evaluationTerm)
    {
        $terms_select = EvaluationTerm::pluck('name', 'sequence')->unique();

        return view('backend.ceap.evaluationTerms.edit', compact('evaluationTerm', 'terms_select'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, EvaluationTerm $evaluationTerm)
    {
        $this->validate($request, [
            'year'       => 'required',
            'name'       => 'required',
        ]);

        $evaluationTerm->year = $request->year;
        $evaluationTerm->name = $request->name;
        $evaluationTerm->sequence = 1;
        $evaluationTerm->active = 0;
        $evaluationTerm->save();

        return redirect()->route('admin.ceap.evaluationTerm.index')->withFlashSuccess(trans('ceap.backend.evaluation_term.update'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function destroy(EvaluationTerm $evaluationTerm)
    {
        $evaluationTerm->delete();

        return redirect()->route('admin.ceap.evaluationTerm.index')->withFlashSuccess(trans('ceap.backend.evaluation_term.destroy'));
    }
}
