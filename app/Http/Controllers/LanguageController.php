<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use JavaScript;

/**
 * Class LanguageController.
 */
class LanguageController extends Controller
{
    /**
     * @param $locale
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function swap($locale)
    {
        if (array_key_exists($locale, config('locale.languages'))) {
            session()->put('locale', $locale);
            Carbon::setLocale($locale);
        }

        /*
         * Set JavaScript ceap.locale variable.
         */
        JavaScript::put('locale', $locale);
        JavaScript::put('datatableLanguageURL', url("datatable/{$locale}.json"));

        return redirect()->back();
    }
}
