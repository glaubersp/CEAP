<?php

namespace App\Http\Controllers\Frontend\CEAP;

use App\DataTables\ActionEvidencesDataTable;
use App\DataTables\ActionsDataTable;
use App\DataTables\Scopes\FilterByActionScope;
use App\Http\Controllers\Controller;
use App\Models\Auth\User;
use App\Models\CEAP\Action;
use App\Models\CEAP\EvaluationTerm;
use App\Models\CEAP\Goal;
use App\Models\CEAP\Indicator;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

/**
 * Class AcaoController
 * @package App\Http\Controllers\Frontend\CEAP
 */
class ActionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param ActionsDataTable $dataTable
     * @return \Illuminate\Http\JsonResponse|\Illuminate\View\View
     */
    public function index(ActionsDataTable $dataTable, Request $request)
    {
        $terms_select = EvaluationTerm::select(DB::raw('concat(evaluation_terms.year, " - ", evaluation_terms.name) as terms'),
            'id')->pluck('terms', 'id');

        return $dataTable->render('frontend.ceap.actions.index', ['terms_select' => $terms_select]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $goal_id = session()->get('goal_id');
        $indicator_id = session()->get('indicator_id');

        $indicators_select = Indicator::pluck('name', 'id');

        $goals = Goal::with([
            'indicator' => function ($query) {
                $query->select('id', 'name')->get();
            },
        ])->select('id', 'name', 'indicator_id')->orderBy('name', 'asc')->get();

        $goals_select = collect([]);
        $goals_indicators_map = collect([]);
        foreach ($goals as $goal) {
            $goals_select->put($goal->id, $goal->name);
            $goals_indicators_map->put($goal->id, $goal->indicator->toArray());
        }

        $users_select = User::select(DB::raw('concat(users.first_name, " ", users.last_name) as user_name'),
            'id')->pluck('user_name', 'id');

        $terms_select = EvaluationTerm::select(DB::raw('concat(evaluation_terms.year, " - ", evaluation_terms.name) as terms'),
            'id')->pluck('terms', 'id');

        session()->reflash(['indicator_id']);

        if (request()->ajax()) {
            $result = collect([]);
            $result->put('goals_select', $goals_select);
            $result->put('goals_indicators_map', $goals_indicators_map);
            $result->put('users_select', $users_select);
            $result->put('terms_select', $terms_select);
            $result->put('goal_id', $goal_id);
            $result->put('indicator_id', $indicator_id);
            $result->put('indicators_select', $indicators_select);
            return $result->toJson();
        }

        $goal_id = $request->input('goal_id');
        $indicator_id = $request->input('indicator_id');

        return view('frontend.ceap.actions.create',
            compact(
                'goals_select', 'goals_indicators_map',
                'users_select', 'terms_select', 'goal_id', 'indicator_id', 'indicators_select'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (request()->ajax()) {
            session()->reflash(['indicator_id']);

            $this->validate($request, [
                'goalSelect' => 'required',
                'name'       => 'required|unique:actions,name',
                'details'    => 'required',
                'users'      => 'required',
                'terms'      => 'required',
            ]);

            $action = new Action;
            $action->name = $request->name;
            $action->details = $request->details;
            $action->setStartDateAttribute(Carbon::now()->format('Y-m-d'));
            $action->setEndDateAttribute(Carbon::now()->format('Y-m-d'));
            $action->evaluationTerm()->associate($request->terms);
            $action->goal()->associate($request->goalSelect);
            $action->save();

            $action->users()->sync($request->users);

            session()->flash('flash_success',
                trans('ceap.createSuccessMessage', ['item' => 'ação', 'name' => $action->name]));

            return $action->toJson();
        }

        session()->reflash(['indicator_id']);

        $this->validate($request, [
            'goalSelect' => 'required',
            'name'       => 'required|unique:actions,name',
            'details'    => 'required',
            'users'      => 'required',
            'terms'      => 'required',
        ]);

        $request->session()->forget('errors');

        $action = new Action;
        $action->name = $request->name;
        $action->details = $request->details;
        $action->setStartDateAttribute(Carbon::now()->format('Y-m-d'));
        $action->setEndDateAttribute(Carbon::now()->format('Y-m-d'));
        $action->evaluationTerm()->associate($request->terms);
        $action->goal()->associate($request->goalSelect);
        $action->save();

        $action->users()->sync($request->users);

        session()->flash('flash_success',
            trans('ceap.createSuccessMessage', ['item' => 'ação', 'name' => $action->name]));

        if ($request->input('saveAction') != null) {
            return redirect()->route('frontend.ceap.actions.index');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param Request $request
     * @param Action $action
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, ActionEvidencesDataTable $dataTable, Action $action)
    {
        $action->load([
            'goal'           => function ($query) {
                $query->select('id', 'name', 'indicator_id')->get();
            },
            'goal.indicator' => function ($query) {
                $query->select('id', 'name')->get();
            },
            'users'          => function ($query) {
                $query->select('users.id', 'first_name', 'last_name')->get();
            },
            'evaluationTerm'  => function ($query) {
                $query->select('id', 'year', 'name');
            },
            'evaluations'    => function ($query) {
                $query->select('action_id', 'id', 'evaluation', 'rating', 'justification',
                    'evaluation_date')->get();
            },
            'evidences'    => function ($query) {
                $query->select('action_id', 'id')->get();
            },
            'parentAction' => function($query) {
                $query->with('parentAction')->withTrashed();
            }
        ]);

        $users_selected = $action->users->reduce(function ($carry, $user) {
            return $carry . ', ' . $user["name"];
        });

        $users_selected = trim($users_selected, ', ');

        $terms_select = $action->evaluationTerm->terms;

        $parent_action_collection = collect([]);
        $parent_action = $action->parentAction;

        while($parent_action != null) {
            $parent_action_collection->push($parent_action);
            $parent_action = $parent_action->parentAction;
        }

        return $dataTable->addScope(new FilterByActionScope($action))
            ->render('frontend.ceap.actions.show',
                compact('action', 'users_selected', 'terms_select', 'parent_action_collection'));
    }

    public function json(Action $action)
    {
        if (request()->ajax()) {
            $action->load([
                'goal'           => function ($query) {
                    $query->select('id', 'name', 'indicator_id')->get();
                },
                'goal.indicator' => function ($query) {
                    $query->select('id', 'name')->get();
                },
                'users'          => function ($query) {
                    $query->select('users.id', 'first_name', 'last_name')->get();
                },
                'evaluationTerm'          => function ($query) {
                    $query->select('id', 'year', 'name')->get();
                },
            ]);

            $users_selected = $action->users->reduce(function ($carry, $user) {
                return $carry . ', ' . $user["name"];
            });

            $users_selected = trim($users_selected, ', ');


            $result = collect([]);
            $result->put('action', $action);
            $result->put('users_selected', $users_selected);
            return $result->toJson();
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Action $action)
    {
        $action->load([
            'goal'           => function ($query) {
                $query->select('id', 'name', 'indicator_id')->get();
            },
            'goal.indicator' => function ($query) {
                $query->select('id', 'name')->get();
            },
            'users'          => function ($query) {
                $query->select('users.id', 'first_name', 'last_name')->get();
            },
        ]);

        $indicators_select = Indicator::pluck('name', 'id');

        $goals = Goal::with([
            'indicator' => function ($query) {
                $query->select('id', 'name')->get();
            },
        ])->select('id', 'name', 'indicator_id')->orderBy('name', 'asc')->get();

        $goals_select = collect([]);
        $goals_indicators_map = collect([]);
        foreach ($goals as $goal) {
            $goals_select->put($goal->id, $goal->name);
            $goals_indicators_map->put($goal->id, $goal->indicator->toArray());
        }

        $users_select = User::select(DB::raw('concat(users.first_name, " ", users.last_name) as user_name'),
            'id')->pluck('user_name', 'id');
        $users_selected = $action->users->reduce(function ($carry, $tag) {
            return $carry->push($tag["id"]);
        }, collect([]));

        $terms_select = EvaluationTerm::select(DB::raw('concat(evaluation_terms.year, " - ", evaluation_terms.name) as terms'),
            'id')->pluck('terms', 'id');

        if (request()->ajax()) {
            $result = collect([]);
            $result->put('action', $action);
            $result->put('users_select', $users_select);
            $result->put('users_selected', $users_selected);
            $result->put('terms_select', $terms_select);
            $result->put('goals_select', $goals_select);
            $result->put('goals_indicators_map', $goals_indicators_map);
            $result->put('indicators_select', $indicators_select);
            return $result->toJson();
        }
        return view('frontend.ceap.actions.edit',
            compact('action', 'users_select', 'users_selected', 'terms_select', 'goals_select', 'goals_indicators_map',
                'indicators_select'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Action $action)
    {
        $this->validate($request, [
            'name'       => 'required',
            'goalSelect' => 'required',
            'details'    => 'required',
            'users'      => 'required',
            'terms'      => 'required',
        ]);

        $request->session()->forget('errors');

        $action->name = $request->name;
        $action->details = $request->details;
        $action->setStartDateAttribute(Carbon::now()->format('Y-m-d'));
        $action->setEndDateAttribute(Carbon::now()->format('Y-m-d'));
        $action->evaluationTerm()->associate($request->terms);
        $action->goal()->associate($request->goalSelect);
        $action->save();

        $action->users()->sync($request->users);

        session()->flash('flash_success',
            trans('ceap.editSuccessMessage', ['item' => 'ação', 'name' => $action->name]));

        if ($request->ajax()) {
            return ['status' => 'Success'];
        }
        return redirect()->route('frontend.ceap.actions.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Request $request
     * @param Action $action
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, Action $action)
    {

        if (request()->forceDelete === 'true') {
            $action->forceDelete();
            session()->flash('flash_success', trans('ceap.actions.modal.delete.success', ['item' => 'ação']));
            return collect([])->toJson();
        }

        if (request()->forceDelete === 'false') {
            $action->delete();
            session()->flash('flash_success', trans('ceap.actions.modal.trash.success', ['item' => 'ação']));
            return collect([])->toJson();
        }

        return redirect()->route('frontend.ceap.actions.index');
    }


    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function restore(Request $request)
    {
        $id = $request->get('action_id');
        Action::withTrashed()
            ->where('id', $id)
            ->restore();

        session()->flash('flash_success', trans('ceap.actions.restore.text', ['item' => 'ação']));

        return redirect()->route('frontend.ceap.actions.index');
    }

    /**
     * @param Request $request
     * @param Action $action
     * @return $this
     */
    public function updatePriority(Request $request, Action $action)
    {
        $action->priority = $request->priority;
        $action->save();
        return redirect()->back()->withInput([$action->name]);
    }
}
