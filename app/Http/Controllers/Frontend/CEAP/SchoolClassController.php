<?php

namespace App\Http\Controllers\Frontend\CEAP;

use App\Http\Controllers\Controller;
use App\Models\CEAP\SchoolClass;
use DataTables;
use DB;
use Illuminate\Http\Request;

/**
 * Class SchoolClassController
 * @package App\Http\Controllers\Frontend\CEAP
 */
class SchoolClassController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * @return \Illuminate\Support\Collection|static
     */
    public function getYears()
    {
        return SchoolClass::all()->unique('year');
    }

    /**
     * @param $year
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function getGradesByYear($year)
    {
        return SchoolClass::where('year', $year)->get();
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function getDataTable(Request $request)
    {
        if ($request->ajax() === true) {
            $indicator_group_id = $request->get('indicatorGroupId');
            if (!is_numeric($indicator_group_id)) {
                $indicator_group_id = '';
            }

            $year = substr($request->get('year'), 1);
            if (!is_numeric($year)) {
                $year = '';
            }

            if ($request->get('indicator_id')) {
                $indicator_id = $request->get('indicator_id');
                $query = DB::table('school_classes')
                    ->join('indicator_records', 'school_classes.id',
                        '=', 'indicator_records.school_class_id')
                    ->select('school_classes.id as school_class_id',
                        'school_classes.year as school_year',
                        'school_classes.grade AS school_grade',
                        'school_classes.room as school_room',
                        'indicator_records.id AS indicator_record_id',
                        'indicator_records.indicator_id AS indicator_id',
                        'indicator_records.value AS indicator_record_value',
                        'indicator_records.date AS indicator_record_date')
                    ->where('indicator_records.indicator_id', '=', $indicator_id)
                    ->where('school_classes.year', $year);
            } else {
                $query = DB::table('school_classes')
                    ->select('school_classes.id',
                        'school_classes.year as school_year',
                        'school_classes.grade as school_grade',
                        'school_classes.room as school_room')
                    ->where('school_classes.year', $year);
            }

            return DataTables::of($query)
                ->make(true);

        }
    }
}
