<?php

namespace App\Http\Controllers\Frontend\CEAP;

use App\DataTables\AgreementsDataTable;
use App\Http\Controllers\Controller;
use App\Models\CEAP\Action;
use App\Models\CEAP\Agreement;
use App\Models\CEAP\AgreementMonitoringStrategy;
use App\Models\CEAP\AgreementRole;
use App\Models\CEAP\EvaluationCycle;
use App\Models\CEAP\TopicTag;
use Illuminate\Http\Request;
use JavaScript;
use function route;

class AgreementController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\Response|\Illuminate\View\View
     */
    public function index(AgreementsDataTable $dataTable)
    {
        JavaScript::put([
            'agreements_title_label'           => removeCollon(trans('ceap.agreements.title.label')),
            'agreements_details_label'         => removeCollon(trans('ceap.agreements.details.label')),
            'agreements_roles_label'           => removeCollon(trans('ceap.agreements.roles.label')),
            'agreements_actions_label'         => removeCollon(trans('ceap.agreements.actions.label')),
            'agreements_execution_cycle_label' => removeCollon(trans('ceap.agreements.execution_cycle.label')),
            'agreementsIndexRoute'             => route('frontend.ceap.agreements.index'),
            'agreementsShowRoute'              => route('frontend.ceap.agreements.show', 0),
        ]);

        return $dataTable->render('frontend.ceap.agreements.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $evaluation_cycles = EvaluationCycle::orderBy('id')->pluck('name', 'id');
        $agreement_roles = AgreementRole::orderBy('name', 'asc')->pluck('name', 'id');
        $topic_tags = TopicTag::orderBy('name', 'asc')->pluck('name', 'id');
        $monitoring_strategies = AgreementMonitoringStrategy::orderBy('title', 'asc')->pluck('title', 'id');
        $actions = Action::orderBy('name', 'asc')->pluck('name', 'id');

        return view('frontend.ceap.agreements.create',
            compact(['evaluation_cycles', 'agreement_roles', 'topic_tags', 'actions', 'monitoring_strategies']));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'title'                            => 'required|unique:agreements,title',
            'details'                          => 'required',
            'topic_tags'                       => 'required',
            'agreement_role'                   => 'required',
            'evaluation_cycle_id'              => 'required',
            'agreement_monitoring_strategy_id' => 'required',
        ]);

        $request->session()->forget('errors');

        $agreement = new Agreement();
        $agreement->title = $request->title;
        $agreement->details = $request->details;
        $agreement->evaluationCycle()->associate($request->evaluation_cycle_id);
        $agreement->agreementMonitoringStrategy()->associate($request->agreement_monitoring_strategy_id);
        $agreement->save();

        $agreement->agreementRoles()->sync($request->agreement_role);
        $agreement->topicTags()->sync($request->topic_tags);
        $agreement->actions()->sync($request->actions);

        session()->flash('flash_success',
            trans('ceap.createSuccessMessage', ['item' => 'combinado', 'name' => $agreement->title]));

        return redirect()->route('frontend.ceap.agreements.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show(Agreement $agreement)
    {
        $cycles = EvaluationCycle::orderBy('id')->pluck('name', 'id');
        $monitoring_strategies = AgreementMonitoringStrategy::pluck('title', 'id');

        $topic_tag = TopicTag::pluck('name', 'id');
        $topic_tag_selected = $agreement->topicTags->reduce(function ($carry, $tag) {
            return $carry . PHP_EOL . $tag["name"];
        });
        $topic_tag_selected = trim($topic_tag_selected, PHP_EOL);

        $roles = AgreementRole::pluck('name', 'id');
        $roles_selected = $agreement->agreementRoles->reduce(function ($carry, $tag) {
            return $carry . PHP_EOL . $tag["name"];
        });
        $roles_selected = trim($roles_selected, PHP_EOL);

        $actions = Action::pluck('name', 'id');
        $actions_selected = $agreement->actions->reduce(function ($carry, $tag) {
            return $carry . PHP_EOL . $tag["name"];
        });
        $actions_selected = trim($actions_selected, PHP_EOL);

	    if (request()->ajax() === true) {
		    try {
			    return $agreement->toJson();
		    } catch (JsonEncodingException $e) {
			    return json_encode([]);
		    }
	    }

        return view('frontend.ceap.agreements.show',
            compact('agreement', 'cycles', 'topic_tag', 'topic_tag_selected', 'roles', 'roles_selected',
                'actions', 'actions_selected', 'monitoring_strategies'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Agreement $agreement)
    {
        $cycles = EvaluationCycle::orderBy('id')->pluck('name', 'id');
        $monitoring_strategies = AgreementMonitoringStrategy::pluck('title', 'id');

        $topic_tag = TopicTag::orderBy('name', 'asc')->pluck('name', 'id');
        $topic_tag_selected = $agreement->topicTags->reduce(function ($carry, $tag) {
            return $carry->push($tag["id"]);
        }, collect([]));

        $roles = AgreementRole::orderBy('name', 'asc')->pluck('name', 'id');
        $roles_selected = $agreement->agreementRoles->reduce(function ($carry, $tag) {
            return $carry->push($tag["id"]);
        }, collect([]));

        $actions = Action::orderBy('name', 'asc')->pluck('name', 'id');
        $actions_selected = $agreement->actions->reduce(function ($carry, $tag) {
            return $carry->push($tag["id"]);
        }, collect([]));

        return view('frontend.ceap.agreements.edit',
            compact('agreement', 'cycles', 'topic_tag', 'topic_tag_selected', 'roles', 'roles_selected',
                'actions', 'actions_selected', 'monitoring_strategies'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  Agreement $agreement
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Agreement $agreement)
    {
        $this->validate($request, [
            'title'                            => 'required',
            'details'                          => 'required',
            'topic_tags'                       => 'required',
            'agreement_role'                   => 'required',
            'evaluation_cycle_id'              => 'required',
            'agreement_monitoring_strategy_id' => 'required',
        ]);

        $request->session()->forget('errors');

        $agreement->title = $request->title;
        $agreement->details = $request->details;
        $agreement->evaluationCycle()->associate($request->evaluation_cycle_id);
        $agreement->agreementMonitoringStrategy()->associate($request->agreement_monitoring_strategy_id);
        $agreement->save();

        $agreement->agreementRoles()->sync($request->agreement_role);
        $agreement->topicTags()->sync($request->topic_tags);
        $agreement->actions()->sync($request->actions);

	    if($request->ajax()){
		    return ['status'=>'Success'];
	    }

        session()->flash('flash_success',
            trans('ceap.editSuccessMessage', ['item' => 'combinado', 'name' => $agreement->title]));

        return redirect()->route('frontend.ceap.agreements.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $agreement
     * @return \Illuminate\Http\Response
     */
    public function destroy(Agreement $agreement)
    {
        $agreement->delete();

        session()->flash('flash_success', 'Combinado removido com sucesso');

	    if(request()->ajax() === true){
		    return collect([])->toJson();
	    }else{
		    return redirect()->route('frontend.ceap.agreements.index');
	    }

        return redirect()->route('frontend.ceap.agreements.index');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function restore(Request $request)
    {

        $id = $request->get('agreement_id');
        Agreement::withTrashed()
            ->where('id', $id)
            ->restore();

        session()->flash('flash_success', trans('ceap.goals.restore.text', ['item' => 'combinado']));

        return redirect()->route('frontend.ceap.agreements.index');
    }
}
