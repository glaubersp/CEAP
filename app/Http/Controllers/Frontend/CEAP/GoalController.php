<?php

namespace App\Http\Controllers\Frontend\CEAP;

use App\DataTables\ActionsInGoalDataTable;
use App\DataTables\GoalsDataTable;
use App\DataTables\Scopes\FilterByGoalScope;
use App\Http\Controllers\Controller;
use App\Models\CEAP\Goal;
use App\Models\CEAP\Indicator;
use Illuminate\Http\Request;

/**
 * Class GoalController
 * @package App\Http\Controllers\Frontend\CEAP
 */
class GoalController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param GoalsDataTable $dataTable
     * @return \Illuminate\Http\JsonResponse|\Illuminate\View\View
     */
    public function index(GoalsDataTable $dataTable)
    {
        return $dataTable->render('frontend.ceap.goals.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $indicators_select = Indicator::orderBy('name','asc')->pluck('name', 'id');
        $indicators_descriptions = Indicator::pluck('description', 'id');
        $indicator_id = $request->input('indicator_id');

        return view('frontend.ceap.goals.create',
            compact(['indicators_select', 'indicators_descriptions', 'indicator_id',]));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'indicator_id'  => 'required',
            'name'          => 'required|unique:goals,name',
            'justification' => 'required',
        ]);

        $request->session()->forget('errors');

        $indicator = Indicator::find($request->indicator_id);
        $goal = new Goal();
        $goal->name = $request->name;
        $goal->justification = $request->justification;
        $goal->indicator()->associate($indicator);
        $goal->save();

        session()->flash('flash_success',
            trans('ceap.createSuccessMessage', ['item' => 'objetivo', 'name' => $goal->name]));

        if ($request->input('save') != null) {
            return redirect()->route('frontend.ceap.goals.index');
        } else {
            $goal_id = $goal->id;
	        $indicator_id = $goal->indicator_id;
	        session()->flash('goal_id', $goal_id);
	        session()->flash('indicator_id', $indicator_id);
            return redirect()->route('frontend.ceap.actions.create');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param ActionsInGoalDataTable $dataTable
     * @param Goal $goal
     * @return \Illuminate\Http\Response
     * @internal param int $id
     */
    public function show(ActionsInGoalDataTable $dataTable, Goal $goal)
    {
        $indicators_select = Indicator::pluck('name', 'id');

        $goal->load([
            'indicator' => function ($query) {
                $query->select('id', 'name');
            },
        ]);

        return $dataTable->addScope(new FilterByGoalScope($goal))->render('frontend.ceap.goals.show', compact('goal', 'indicators_select'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Goal $goal
     * @return \Illuminate\Http\Response
     * @internal param int $id
     */
    public function edit(Goal $goal)
    {
        $indicators_select = Indicator::orderBy('name','asc')->pluck('name', 'id');
        return view('frontend.ceap.goals.edit', compact('goal', 'indicators_select'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param Goal $goal
     * @return \Illuminate\Http\Response
     * @internal param int $id
     */
    public function update(Request $request, Goal $goal)
    {
        $this->validate($request, [
            'name'          => 'required',
            'justification' => 'required',
            'indicator_id'  => 'required',
        ]);

        $request->session()->forget('errors');

        $goal->fill($request->input());
        $goal->save();

	    if($request->ajax()){
		    return ['status'=>'Success'];
	    }

        session()->flash('flash_success',
            trans('ceap.editSuccessMessage', ['item' => 'objetivo', 'name' => $goal->name]));

        return redirect()->route('frontend.ceap.goals.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Goal $goal)
    {
        session()->flash('flash_success', 'Objetivo removido com sucesso');

        if (request()->forceDelete === 'true') {
            $goal->forceDelete();
            session()->flash('flash_success', trans('ceap.goals.modal.delete.success', ['item' => 'objetivo']));
            return collect([])->toJson();
        }

        if (request()->forceDelete === 'false') {
            $goal->delete();
            session()->flash('flash_success', trans('ceap.goals.modal.trash.success', ['item' => 'objetivo']));
            return collect([])->toJson();
        }

    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function restore(Request $request)
    {

        $id = $request->get('goal_id');
        Goal::withTrashed()
              ->where('id', $id)
              ->restore();

        session()->flash('flash_success', trans('ceap.goals.restore.text', ['item' => 'objetivo']));

        return redirect()->route('frontend.ceap.goals.index');
    }
}
