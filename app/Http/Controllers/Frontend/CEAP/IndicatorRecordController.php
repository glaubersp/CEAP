<?php

namespace App\Http\Controllers\Frontend\CEAP;

use App\Http\Controllers\Controller;
use App\Models\CEAP\EvaluationCategory;
use App\Models\CEAP\EvaluationCycle;
use App\Models\CEAP\Indicator;
use App\Models\CEAP\IndicatorRecord;
use App\Models\CEAP\SchoolClass;
use Carbon\Carbon;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use function array_add;

class IndicatorRecordController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @param Indicator $indicator
     * @return JsonResponse|\Illuminate\Http\Response|\Illuminate\View\View
     */
    public function index(Request $request, Indicator $indicator)
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param Request $request
     * @param Indicator $indicator
     * @return JsonResponse|\Illuminate\Http\Response|\Illuminate\View\View
     */
    public function create(
        Request $request,
        Indicator $indicator
    ) {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'evaluationCategory' => 'required',
            'year'               => 'required_if:evaluationCategory,1',
            'indicatorValue'     => 'required_unless:evaluationCategory,1',
        ]);

        $evaluation_category = $request->evaluationCategory;
        $indicator = Indicator::find($request->input('indicator_id'));
        $indicatorRecord_obj = new IndicatorRecord();
        $indicatorRecord_obj->indicator()->associate($indicator);
        $indicatorRecord_obj->setDateAttribute(Carbon::now()->format('Y-m-d'));

        if (1 !== (int)$evaluation_category) {
            $indicatorRecord = clone $indicatorRecord_obj;
            $indicatorRecord->value = $request->indicatorValue;
            $indicatorRecord->save();
        } else {
            $year = substr($request->year, 1);
            $school_classes = SchoolClass::where('year', '=', $year)->get();

            foreach ($school_classes as $school_class) {
                $key = 'indicator_value_' . $school_class->grade . '_' . $school_class->room;
                $value = $request->input($key);
                if (!is_null($value)) {
                    $indicatorRecord = clone $indicatorRecord_obj;
                    $indicatorRecord->value = (float)$value;
                    $indicatorRecord->schoolClass()->associate($school_class);
                    $indicatorRecord->save();
                }
            }
        }

        session()->flash('flash_success',
            trans('ceap.registerSuccessMessage', ['item' => 'indicador', 'name' => $indicator->name]));

        return redirect()->route('frontend.ceap.indicators.indicatorRecords.index', $indicator->id);
    }

    /**
     * Display the specified resource.
     *
     * @param $id
     * @return JsonResponse|\Illuminate\Http\Response|\Illuminate\View\View
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Request $request
     * @param Indicator $indicator
     * @param SchoolClassIndicatorRecordsHistoryDataTable $school_class_indicator_records_history_data_table
     * @param IndicatorRecordsHistoryDataTable $indicator_records_history_data_table
     * @return JsonResponse|\Illuminate\Http\Response|\Illuminate\View\View
     * @internal param int $id
     */
    public function edit(
        Request $request,
        Indicator $indicator,
        SchoolClassIndicatorRecordsHistoryDataTable $school_class_indicator_records_history_data_table,
        IndicatorRecordsHistoryDataTable $indicator_records_history_data_table
    ) {
        $indicator->load([
            'evaluationCategory' => function ($query) {
                $query->select('id', 'name')->get();
            },
            'indicatorRecords'   => function ($query) {
                $query->select('value', 'indicator_id')->get();
            },
        ]);

        $cycles = EvaluationCycle::orderBy('id')->pluck('name');
        $categories = EvaluationCategory::all();

        $school_classes = SchoolClass::distinctYears()
            ->get()
            ->mapWithKeys(function ($item) {
                return ["Y" . "$item[year]" => $item['year']];
            });

        if ($indicator->evaluation_category_id === 1) {
            return $school_class_indicator_records_history_data_table
                ->render('frontend.ceap.indicatorRecords.edit_school_classes',
                    compact(['cycles', 'categories', 'school_classes', 'years_select', 'indicator']));
        } else {
            return $indicator_records_history_data_table
                ->render('frontend.ceap.indicatorRecords.edit',
                    compact(['cycles', 'categories', 'school_classes', 'years_select', 'indicator']));
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param Indicator $indicator
     * @return \Illuminate\Http\Response
     * @internal param int $id
     */
    public function update(Request $request, Indicator $indicator)
    {
        $validation_rules = [
            'evaluationCategory' => 'required',
            'year'               => 'required_if:evaluationCategory,1',
        ];
        foreach ((array)$request->input('indicator_record_id') as $id) {
            $validation_rules = array_add($validation_rules, 'indicator_value_' . $id,
                'required_unless:evaluationCategory,1');
        }
        $this->validate($request, $validation_rules);

        $evaluation_category = $request->evaluationCategory;

        if (1 !== (int)$evaluation_category) {
            foreach ((array)$request->input('indicator_record_id') as $id) {
                $validation_rules = array_add($validation_rules, 'indicator_value_' . $id,
                    'required_unless:evaluationCategory,1');
                $indicatorRecord = IndicatorRecord::find($id);
                $indicatorRecord->value = $request->input('indicator_value_' . $id);
                $indicatorRecord->save();
            }
        } else {
            $year = substr($request->year, 1);
            $school_classes = SchoolClass::where('year', '=', $year)->get();

            foreach ($school_classes as $school_class) {
                $key_value = 'indicator_value_' . $school_class->grade . '_' . $school_class->room;
                $key_record_id = 'indicator_record_id_' . $school_class->grade . '_' . $school_class->room;
                $value = $request->input($key_value);
                $record_id = $request->input($key_record_id);
                if (!is_null($value)) {
                    $indicatorRecord = IndicatorRecord::find($record_id);
                    $indicatorRecord->value = (float)$value;
                    $indicatorRecord->save();
                }
            }
        }

        session()->flash('flash_success',
            trans('ceap.editSuccessMessage', ['item' => 'indicador', 'name' => $indicator->name]));

        return redirect()->route('frontend.ceap.indicators.indicatorRecords.index', $indicator->id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
