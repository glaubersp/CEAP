<?php

namespace App\Http\Controllers\Frontend\CEAP;

use App\DataTables\MonitoringChartDataTable;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Config;

/**
 * Class CEAPController
 * @package App\Http\Controllers\Frontend
 */
class CEAPController extends Controller
{
    public function help()
    {
        $lang = Config::get('app.locale');
        return view('frontend.ceap.help-' . $lang);
    }
}