<?php

namespace App\Http\Controllers\Frontend\CEAP;

use App\DataTables\ActionEvaluationDataTable;
use App\DataTables\Scopes\FilterByActionScope;
use App\Http\Controllers\Controller;
use App\Models\CEAP\Action;
use App\Models\CEAP\ActionEvaluation;
use App\Models\CEAP\EvaluationTerm;
use Carbon\Carbon;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;

class ActionEvaluationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param ActionEvaluationDataTable $dataTable
     *
     * @return \Illuminate\Http\JsonResponse|\Illuminate\View\View
     */
    public function index(ActionEvaluationDataTable $dataTable, Action $action)
    {
        return $dataTable->addScope(new FilterByActionScope($action))->render(
            'frontend.ceap.actions.evaluations.index',
            ['action' => $action]
        );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // Done with Modal window.
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param Action $action
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Action $action)
    {
        $this->validate(
            $request,
            [
                'evaluation'    => 'required',
                'rating'        => 'required_unless:evaluation,Not implemented',
                'justification' => 'required',
            ]
        );

        $request->session()->forget('errors');

        $evaluation                = new ActionEvaluation;
        $evaluation->evaluation    = $request->evaluation;
        $evaluation->rating        = $request->rating;
        $evaluation->justification = $request->justification;
        $evaluation->action_id     = $action->id;
        $evaluation->user_id       = \Auth::user()->id;
        $evaluation->setEvaluationDateAttribute(Carbon::now()->format('Y-m-d'));
        $evaluation->save();

        $duplicate = $request->checked;
        if (!$duplicate) {
            $request->session()->flash('flash_success', trans('ceap.actions.evaluations.save_evaluation'));

            $action->delete();

            if ($request->ajax()) {
                return ['status' => 'Success'];
            }
            return redirect()->route('frontend.ceap.actions.index');
        }

        $actionTerm  = $action->evaluationTerm;
        $currentTerm = EvaluationTerm::current();

        $newAction = $action->replicate(['evidences', 'agreements', 'evaluations', 'deleted_at']);
        $newAction->parentAction()->associate($action);

        if ($actionTerm->year == $currentTerm->year && $actionTerm->sequence > $currentTerm->sequence) {

            $action->delete();

            if ($request->ajax()) {
                return $request->session()->flash('flash_danger', trans('ceap.actions.evaluations.save_evaluation_duplicate_error', ['name' => $action->name]));
            }

            return redirect()->route('frontend.ceap.actions.index');
        }

        if ($actionTerm->year < $currentTerm->year
            || ($actionTerm->year == $currentTerm->year && $actionTerm->sequence < $currentTerm->sequence)) {
            $newAction->evaluationTerm()->associate($currentTerm);
            try{
                $newAction->push();
            }catch (QueryException $e){
                $request->session()->flash('flash_danger', trans('ceap.actions.evaluations.save_evaluation_duplicate_error', ['name' => $action->name]));

                $action->delete();
                return redirect()->route('frontend.ceap.actions.index');
            }
        } elseif ($actionTerm->year == $currentTerm->year && $actionTerm->sequence == $currentTerm->sequence) {
            $nextTerm = EvaluationTerm::next();
            $newAction->evaluationTerm()->associate($nextTerm);
            try{
                $newAction->push();
            }catch (QueryException $e){
                $request->session()->flash('flash_danger', trans('ceap.actions.evaluations.save_evaluation_duplicate_error', ['name' => $action->name]));

                $action->delete();
                return redirect()->route('frontend.ceap.actions.index');
            }
        }

        $request->session()->flash('flash_success', trans('ceap.actions.evaluations.save_evaluation_duplicate_action', ['name' => $action->name]));

        $action->delete();

        if ($request->ajax()) {
            return ['status' => 'Success'];
        }

        return redirect()->route('frontend.ceap.actions.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param Action $action
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\Response
     * @internal param int $id
     */
    public function update(Request $request, Action $action)
    {
        $action->evaluation = $request->evaluation;
        $action->save();

        return back()->withInput([$action->name]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
