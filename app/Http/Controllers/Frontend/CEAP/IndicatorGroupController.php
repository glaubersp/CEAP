<?php

namespace App\Http\Controllers\Frontend\CEAP;

use App\Http\Controllers\Controller;
use App\Models\CEAP\Indicator;
use App\Models\CEAP\IndicatorGroup;
use App\Models\CEAP\IndicatorRecord;
use App\Models\CEAP\SchoolClass;
use DataTables;
use Illuminate\Database\Eloquent\JsonEncodingException;
use Illuminate\Http\Request;
use JavaScript;
use Validator;
use const null;
use function array_add;
use function collect;
use function session;

/**
 * Class IndicatorGroupController
 * @package App\Http\Controllers\Frontend\CEAP
 */
class IndicatorGroupController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {

        $indicatorGroupId = $request->input('indicatorGroupId');
        session()->flash('indicatorGroupId', $indicatorGroupId);

        return view('frontend.ceap.indicators.create');

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        session()->flash('error_route', 'frontend.ceap.indicatorGroups.store');
        $this->validate(
            $request,
            [
                'create_indicator_group_name'             => 'required|unique:indicator_groups,name',
                'create_indicator_group_description'      => 'required',
                'create_indicator_group_type'             => 'required',
                'create_indicator_group_evaluation_cycle' => 'required',
                'create_indicator_group_start_date'       => 'required|date',
            ]
        );

        $request->session()->forget('errors');
        session()->forget('error_route');

        $indicatorGroup              = new IndicatorGroup();
        $indicatorGroup->name        = $request->create_indicator_group_name;
        $indicatorGroup->description = $request->create_indicator_group_description;
        $indicatorGroup->type        = $request->create_indicator_group_type;
        $indicatorGroup->evaluationCycle()->associate($request->create_indicator_group_evaluation_cycle);
        $indicatorGroup->setNextEvaluationDateAttribute($request->create_indicator_group_start_date);
        $indicatorGroup->save();

        session()->flash(
            'flash_success',
            trans('ceap.createSuccessMessage', ['item' => 'indicador', 'name' => $indicatorGroup->name])
        );

        $indicatorGroupId = $indicatorGroup->id;
        session()->flash('indicatorGroupId', $indicatorGroup->id);
        JavaScript::put(['indicatorGroupId' => $indicatorGroup->id]);

        return redirect()->route('frontend.ceap.indicators.create');
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Models\CEAP\IndicatorGroup $indicatorGroup
     *
     * @return \Illuminate\Http\Response|string
     */
    public function show(IndicatorGroup $indicatorGroup)
    {
        if (request()->ajax() === true) {
            try {
                return $indicatorGroup->toJson();
            } catch (JsonEncodingException $e) {
                return json_encode([]);
            }
        }

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\CEAP\IndicatorGroup $indicatorGroup
     *
     * @return \Illuminate\Http\Response|string
     */
    public function edit(IndicatorGroup $indicatorGroup)
    {
        if (request()->ajax() === true) {
            try {
                return $indicatorGroup->toJson();
            } catch (JsonEncodingException $e) {
                return json_encode([]);
            }
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Models\CEAP\IndicatorGroup $indicatorGroup
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, IndicatorGroup $indicatorGroup)
    {
        session()->flash('error_route', 'frontend.ceap.indicatorGroups.update');
        if ($indicatorGroup->name === $request->input('edit_indicator_group_name')) {
            $this->validate(
                $request,
                [
                    'edit_indicator_group_description'      => 'required',
                    'edit_indicator_group_type'             => 'required',
                    'edit_indicator_group_evaluation_cycle' => 'required',
                    'edit_indicator_group_start_date'       => 'required|date',
                ]
            );
        } else {
            $this->validate(
                $request,
                [
                    'edit_indicator_group_name'             => 'required|unique:indicator_groups,name',
                    'edit_indicator_group_description'      => 'required',
                    'edit_indicator_group_type'             => 'required',
                    'edit_indicator_group_evaluation_cycle' => 'required',
                    'edit_indicator_group_start_date'       => 'required|date',
                ]
            );
        }

        $request->session()->forget('errors');
        session()->forget('error_route');

        $indicatorGroup->name        = $request->edit_indicator_group_name;
        $indicatorGroup->description = $request->edit_indicator_group_description;
        $indicatorGroup->type        = $request->edit_indicator_group_type;
        $indicatorGroup->evaluationCycle()->associate($request->edit_indicator_group_evaluation_cycle);
        $indicatorGroup->setNextEvaluationDateAttribute($request->edit_indicator_group_start_date);
        $indicatorGroup->save();

        session()->flash(
            'flash_success',
            trans('ceap.editSuccessMessage', ['item' => 'indicador', 'name' => $indicatorGroup->name])
        );

        $indicatorGroupId = $indicatorGroup->id;
        session()->flash('indicatorGroupId', $indicatorGroup->id);
        JavaScript::put(['indicatorGroupId' => $indicatorGroup->id]);

        if ($request->ajax()) {
            return collect([])->toJson();
        } else {
            return redirect()->route('frontend.ceap.indicators.create');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\CEAP\IndicatorGroup $indicatorGroup
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy(IndicatorGroup $indicatorGroup)
    {
        $indicatorGroup->delete();

        session()->flash('flash_success', 'Grupo e respectivos Indicadores removidos com sucesso');

        if (request()->ajax() === true) {
            return collect([])->toJson();
        } else {
            return redirect()->route('frontend.ceap.indicators.create');
        }
    }

    /*
    * IndicatorRecords CEAP
    */

    /**
     * @param Request $request
     * @param IndicatorGroup $indicatorGroup
     *
     * @return string
     */
    public function getCEAPModalData(Request $request, IndicatorGroup $indicatorGroup)
    {
        $indicators = IndicatorGroup::getIndicatorsFilteredByGroup($indicatorGroup->id)->get();

        $schoolGradeGroups = SchoolClass::getSchoolGradeGroups();
        $ceapReportDateMap = IndicatorGroup::getCEAPReportDates($indicatorGroup->id)->get()->groupBy('date');

        return collect(
            [
                'ceapReportDateMap' => $ceapReportDateMap,
                'schoolGradeGroups' => $schoolGradeGroups,
                'indicators'        => $indicators,
            ]
        )->toJson();
    }

    /**
     * @param Request $request
     * @param IndicatorGroup $indicatorGroup
     *
     * @return mixed
     */
    public function getCEAPIndicatorRecordsDataTable(Request $request, IndicatorGroup $indicatorGroup)
    {
        // Requires being called by queryBuilder() inside ajax()
        $indicatorGroupId           = $request->get('indicatorGroupId');
        $indicatorRecordsDateSelect = $request->get('indicatorRecordsDateSelect');
        $schoolGradeGroup           = $request->get('schoolGradeGroup');

        if ($indicatorRecordsDateSelect === null) {
            $query = IndicatorGroup::getIndicatorsFilteredByGroup($indicatorGroupId);
        } else {
            $query = IndicatorGroup::getIndicatorsFilteredByIndicatorGroupReportDateAndSchoolGradeGroup(
                $indicatorGroupId,
                $indicatorRecordsDateSelect,
                $schoolGradeGroup
            );
        }

        return DataTables::of($query)->make(true);
    }

    /**
     * @param  \Illuminate\Http\Request $request
     * @param IndicatorGroup $indicatorGroup
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function createGroupCEAP(Request $request, IndicatorGroup $indicatorGroup)
    {
        session()->flash('error_route', 'frontend.ceap.indicatorGroups.createGroupCEAP');

        //Create array with validations
        $rules = [
            'createCEAPIndicatorRecordsStartDate'              => 'required|date',
            'createCEAPIndicatorRecordsSchoolGradeGroupSelect' => 'required',
            'indicatorRecordCEAP.*'                            => 'required',
        ];

        $this->validateCreateCEAPIndicators($request, $rules);
        session()->forget('error_route');

        $date        = $request->createCEAPIndicatorRecordsStartDate;
        $grade_group = $request->createCEAPIndicatorRecordsSchoolGradeGroupSelect;

        $records = $request->indicatorRecordCEAP;
        foreach ($records as $k => $v) {
            $record = new IndicatorRecord;
            $record->indicator()->associate($k);
            $record->setDateAttribute($date);
            $record->school_grade_group = $grade_group;
            $record->value              = $v;
            $record->save();
        }

        session()->flash(
            'flash_success',
            trans('ceap.createSuccessMessage', ['item' => 'indicadores', 'name' => $indicatorGroup->name])
        );

        session()->flash('indicatorGroupId', $indicatorGroup->id);
        JavaScript::put(['indicatorGroupId' => $indicatorGroup->id]);

        if (request()->ajax() === true) {
            return collect([])->toJson();
        } else {
            return redirect()->route('frontend.ceap.indicators.index');
        }
    }

    /**
     * @param  \Illuminate\Http\Request $request
     * @param IndicatorGroup $indicatorGroup
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function updateGroupCEAP(Request $request, IndicatorGroup $indicatorGroup)
    {

        session()->flash('error_route', 'frontend.ceap.indicatorGroups.updateGroupCEAP');

        //Create array with validations
        $rules = [
            'updateCEAPIndicatorRecordsDateSelect'             => 'required|date',
            'updateCEAPIndicatorRecordsSchoolGradeGroupSelect' => 'required',
            'indicatorRecordCEAP.*'                            => 'required',
        ];

        $this->validateUpdateCEAPIndicators($request, $rules);
        session()->forget('error_route');

        $records = $request->input('indicatorRecordCEAP');
        foreach ($records as $k => $v) {
            $record        = IndicatorRecord::find($k);
            $record->value = $v;
            $record->save();
        }

        session()->flash(
            'flash_success',
            trans('ceap.editSuccessMessage', ['item' => 'indicadores', 'name' => $indicatorGroup->name])
        );

        session()->flash('indicatorGroupId', $indicatorGroup->id);
        JavaScript::put(['indicatorGroupId' => $indicatorGroup->id]);

        if (request()->ajax() === true) {
            return collect([])->toJson();
        } else {
            return redirect()->route('frontend.ceap.indicators.index');
        }
    }

    /**
     * @param Request $request
     * @param IndicatorGroup $indicatorGroup
     *
     * @return string
     */
    public function getRoomsModalData(Request $request, IndicatorGroup $indicatorGroup)
    {
        $indicators         = IndicatorGroup::getIndicatorsFilteredByGroup($indicatorGroup->id)->get();
        $roomsReportDateMap = IndicatorGroup::getRoomsReportDates($indicatorGroup->id)->get()->groupBy('year');

        return collect(
            [
                'roomsReportDateMap' => $roomsReportDateMap,
                'indicators'         => $indicators,
            ]
        )->toJson();
    }

    /**
     * @param Request $request
     * @param IndicatorGroup $indicatorGroup
     *
     * @return mixed
     */
    public function getRoomsIndicatorRecordsDataTable(Request $request, IndicatorGroup $indicatorGroup)
    {
        // Requires being called by queryBuilder() inside ajax()
        $indicatorGroupId = $request->get('indicatorGroupId');
        $schoolClassYear  = $request->get('schoolClassYear');
        $indicatorId      = $request->get('indicatorId');
        $recordDate       = $request->get('recordDate');

        if ($indicatorId) {
            $query = SchoolClass::getSchoolClassesRecords($indicatorId, $recordDate, $schoolClassYear);
        } else {
            $query = SchoolClass::getSchoolClassesForYear($schoolClassYear);
        }

        return DataTables::of($query)->make(true);
    }

    /*
    * IndicatorRecords Rooms
    */

    /**
     * @param  \Illuminate\Http\Request $request
     * @param IndicatorGroup $indicatorGroup
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function createGroupRooms(Request $request, IndicatorGroup $indicatorGroup)
    {
        session()->flash('error_route', 'frontend.ceap.indicatorGroups.createGroupRooms');

        //Create array with validations
        $rules = [
            'createRoomsIndicatorRecordsYearSelect' => 'required',
            'createRoomsIndicatorRecordsStartDate'  => 'required',
            'createRoomsIndicatorRecordsNameSelect' => 'required',
            'indicatorRecordRooms.*'                => 'required',
        ];

        $this->validateCreateGroupRooms($request, $rules);
        session()->forget('error_route');

        $records = $request->input('indicatorRecordRooms');
        foreach ($records as $k => $v) {
            $schoolClassId = SchoolClass::find($k);
            $indicatorId   = $request->createRoomsIndicatorRecordsNameSelect;
            $date          = $request->createRoomsIndicatorRecordsStartDate;

            $record = new IndicatorRecord;
            $record->indicator()->associate($indicatorId);
            $record->schoolClass()->associate($schoolClassId);
            $record->setDateAttribute($date);
            $record->value = $v;
            $record->save();
        }

        session()->flash(
            'flash_success',
            trans('ceap.createSuccessMessage', ['item' => 'indicadores', 'name' => $indicatorGroup->name])
        );

        session()->flash('indicatorGroupId', $indicatorGroup->id);
        JavaScript::put(['indicatorGroupId' => $indicatorGroup->id]);

        if (request()->ajax() === true) {
            return collect([])->toJson();
        } else {
            return redirect()->route('frontend.ceap.indicators.index');
        }
    }

    /**
     * @param  \Illuminate\Http\Request $request
     * @param IndicatorGroup $indicatorGroup
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function updateGroupRooms(Request $request, IndicatorGroup $indicatorGroup)
    {
        session()->flash('error_route', 'frontend.ceap.indicatorGroups.updateGroupRooms');

        //Create array with validations
        $rules = [
            'updateRoomsIndicatorRecordsYearSelect' => 'required',
            'updateRoomsIndicatorRecordsDateSelect' => 'required|date',
            'updateRoomsIndicatorRecordsNameSelect' => 'required',
            'indicatorRecordRooms.*'                => 'required',
        ];

        $this->validateUpdateGroupRooms($request, $rules);
        session()->forget('error_route');

        $records = $request->input('indicatorRecordRooms');
        foreach ($records as $k => $v) {
            $record        = IndicatorRecord::find($k);
            $record->value = $v;
            $record->save();
        }

        session()->flash(
            'flash_success',
            trans('ceap.editSuccessMessage', ['item' => 'indicadores', 'name' => $indicatorGroup->name])
        );

        session()->flash('indicatorGroupId', $indicatorGroup->id);
        JavaScript::put(['indicatorGroupId' => $indicatorGroup->id]);

        if (request()->ajax() === true) {
            return collect([])->toJson();
        } else {
            return redirect()->route('frontend.ceap.indicators.index');
        }
    }

    /**
     * @param Request $request
     * @param IndicatorGroup $indicatorGroup
     *
     * @return mixed
     */
    public function getSchoolModalData(Request $request, IndicatorGroup $indicatorGroup)
    {
        $recordDateList = IndicatorGroup::getSchoolReportDates($indicatorGroup->id)->get();

        return $recordDateList->toJson();
    }

    /**
     * @param Request $request
     * @param IndicatorGroup $indicatorGroup
     *
     * @return mixed
     */
    public function getSchoolIndicatorRecordsDataTable(Request $request, IndicatorGroup $indicatorGroup)
    {
        $indicatorGroupId           = $request->get('indicatorGroupId');
        $indicatorRecordsDateSelect = $request->get('indicatorRecordsDateSelect');

        if ($indicatorRecordsDateSelect === null) {
            $query = IndicatorGroup::getIndicatorsFilteredByGroup($indicatorGroupId);
        } else {
            $query = IndicatorGroup::getIndicatorRecordsForForGroupIdAndRecordDate(
                $indicatorGroupId,
                $indicatorRecordsDateSelect
            );
        }

        return DataTables::of($query)->make(true);
    }

    /**
     * @param  \Illuminate\Http\Request $request
     * @param IndicatorGroup $indicatorGroup
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function createGroupSchool(Request $request, IndicatorGroup $indicatorGroup)
    {

        session()->flash('error_route', 'frontend.ceap.indicatorGroups.createGroupSchool');

        //Create array with validations
        $rules = [
            'createSchoolIndicatorRecordsStartDate' => 'required|date',
            'indicatorRecordSchool.*'               => 'required',
        ];

        $this->validateCreateSchoolIndicators($request, $rules);
        session()->forget('error_route');

        $records = $request->input('indicatorRecordSchool');
        foreach ($records as $k => $v) {
            $date   = $request->createSchoolIndicatorRecordsStartDate;
            $record = new IndicatorRecord;
            $record->indicator()->associate($k);
            $record->setDateAttribute($date);
            $record->value = $v;
            $record->save();
        }

        session()->flash(
            'flash_success',
            trans('ceap.createSuccessMessage', ['item' => 'indicadores', 'name' => $indicatorGroup->name])
        );

        session()->flash('indicatorGroupId', $indicatorGroup->id);
        JavaScript::put(['indicatorGroupId' => $indicatorGroup->id]);

        if (request()->ajax() === true) {
            return collect([])->toJson();
        } else {
            return redirect()->route('frontend.ceap.indicators.index');
        }
    }

    /**
     * @param  \Illuminate\Http\Request $request
     * @param IndicatorGroup $indicatorGroup
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function updateGroupSchool(Request $request, IndicatorGroup $indicatorGroup)
    {
        session()->flash('error_route', 'frontend.ceap.indicatorGroups.updateGroupSchool');

        //Create array with validations
        $rules = [
            'updateSchoolIndicatorRecordsDateSelect' => 'required|date',
            'indicatorRecordSchool.*'                => 'required',
        ];

        $this->validateUpdateSchoolIndicators($request, $rules);
        session()->forget('error_route');

        $records = $request->input('indicatorRecordSchool');
        foreach ($records as $k => $v) {
            $record        = IndicatorRecord::find($k);
            $record->value = $v;
            $record->save();
        }

        session()->flash(
            'flash_success',
            trans('ceap.editSuccessMessage', ['item' => 'indicadores', 'name' => $indicatorGroup->name])
        );

        session()->flash('indicatorGroupId', $indicatorGroup->id);
        JavaScript::put(['indicatorGroupId' => $indicatorGroup->id]);

        if (request()->ajax() === true) {
            return collect([])->toJson();
        } else {
            return redirect()->route('frontend.ceap.indicators.index');
        }
    }


    /*
    * IndicatorRecords School
    */

    /**
     * Validates indicatorRecords array for CEAP
     *
     * @param Request $request
     * @param $rules
     */
    private function validateCreateCEAPIndicators(Request $request, $rules): void
    {
        $validator = Validator::make($request->all(), $rules);
        // Creates a set of AttributeNames to each indicator in the database
        $indicators     = Indicator::all(['id', 'name']);
        $attributeNames = [];
        foreach ($indicators as $indicator) {
            $attributeNames = array_add($attributeNames, 'indicatorRecordCEAP.'.$indicator->id, $indicator->name);
        }
        $validator->setAttributeNames(array_dot($attributeNames));
        $validator->validate();
    }

    /**
     * Validates indicatorRecords array for CEAP
     *
     * @param Request $request
     * @param $rules
     */
    private function validateUpdateCEAPIndicators(Request $request, $rules): void
    {
        $validator = Validator::make($request->all(), $rules);
        // Creates a set of AttributeNames to each indicatorRecord in the database
        $indicatorRecordIds = array_keys($indicatorIds = $request->input('indicatorRecordCEAP'));
        $indicatorRecords   = IndicatorRecord::with('indicator')->whereIn('id', $indicatorRecordIds)->get();
        $attributeNames     = [];
        foreach ($indicatorRecords as $indicatorRecord) {
            if ($indicatorRecord->indicator != null) {
                $attributeNames = array_add(
                    $attributeNames,
                    'indicatorRecordCEAP.'.$indicatorRecord->id,
                    $indicatorRecord->indicator->name
                );
            }
        }
        $validator->setAttributeNames(array_dot($attributeNames));
        $validator->validate();
    }

    /**
     * Validates indicatorRecords array for Rooms type
     *
     * @param Request $request
     * @param $rules
     */
    private function validateCreateGroupRooms(Request $request, $rules): void
    {
        $validator = Validator::make($request->all(), $rules);
        // Creates a set of AttributeNames to each schoolClass in the database
        $schoolClasses  = SchoolClass::all();
        $attributeNames = [];
        foreach ($schoolClasses as $schoolClass) {
            $attributeNames = array_add(
                $attributeNames,
                'indicatorRecordRooms.'.$schoolClass->id,
                $schoolClass->year.'-'.$schoolClass->grade.'-'.$schoolClass->room
            );
        }
        $validator->setAttributeNames(array_dot($attributeNames));
        $validator->validate();
    }

    /**
     * Validates indicatorRecords array for Rooms type
     *
     * @param Request $request
     * @param $rules
     */
    private function validateUpdateGroupRooms(Request $request, $rules): void
    {
        $validator = Validator::make($request->all(), $rules);
        // Creates a set of AttributeNames to each schoolClass in the database
        $indicatorRecordIds = array_keys($request->input('indicatorRecordRooms'));
        $indicatorRecords   = IndicatorRecord::with('schoolClass')->whereIn('id', $indicatorRecordIds)->get();
        $attributeNames     = [];
        foreach ($indicatorRecords as $indicatorRecord) {
            if ($indicatorRecord->schoolClass != null) {
                $attributeNames = array_add(
                    $attributeNames,
                    'indicatorRecordRooms.'.$indicatorRecord->id,
                    $indicatorRecord->schoolClass->year.'-'.$indicatorRecord->schoolClass->grade.'-'.$indicatorRecord->schoolClass->room
                );
            }
        }
        $validator->setAttributeNames(array_dot($attributeNames));
        $validator->validate();
    }

    /**
     * Validates indicatorRecords array for CEAP
     *
     * @param Request $request
     * @param $rules
     */
    private function validateCreateSchoolIndicators(Request $request, $rules): void
    {
        $validator = Validator::make($request->all(), $rules);
        // Creates a set of AttributeNames to each indicator in the database
        $indicators     = Indicator::all(['id', 'name']);
        $attributeNames = [];
        foreach ($indicators as $indicator) {
            $attributeNames = array_add($attributeNames, 'indicatorRecordCEAP.'.$indicator->id, $indicator->name);
        }
        $validator->setAttributeNames(array_dot($attributeNames));
        $validator->validate();
    }

    /**
     * Validates indicatorRecords array for CEAP and School types
     *
     * @param Request $request
     * @param $rules
     */
    private function validateUpdateSchoolIndicators(Request $request, $rules): void
    {
        $validator = Validator::make($request->all(), $rules);
        // Creates a set of AttributeNames to each indicatorRecord in the database
        $indicatorRecordIds = array_keys($indicatorIds = $request->input('indicatorRecordSchool'));
        $indicatorRecords   = IndicatorRecord::with('indicator')->whereIn('id', $indicatorRecordIds)->get();
        $attributeNames     = [];
        foreach ($indicatorRecords as $indicatorRecord) {
            if ($indicatorRecord->indicator != null) {
                $attributeNames = array_add(
                    $attributeNames,
                    'indicatorRecordSchool.'.$indicatorRecord->id,
                    $indicatorRecord->indicator->name
                );
            }
        }
        $validator->setAttributeNames(array_dot($attributeNames));
        $validator->validate();
    }
}
