<?php

namespace App\Http\Controllers\Frontend\CEAP;

use App\Http\Controllers\Controller;

class FileController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function getFile($foldername, $filename)
    {
        $fullpath="app/{$foldername}/{$filename}";
        return response()->download(storage_path($fullpath), null, [], null);
    }

}
