<?php

namespace App\Http\Controllers\Frontend\CEAP;

use App\DataTables\IndicatorsByGroupDataTable;
use App\Http\Controllers\Controller;
use App\Models\CEAP\EvaluationCategory;
use App\Models\CEAP\EvaluationCycle;
use App\Models\CEAP\Indicator;
use App\Models\CEAP\IndicatorGroup;
use App\Models\CEAP\IndicatorRecord;
use App\Models\CEAP\SchoolClass;
use Carbon\Carbon;
use DB;
use Illuminate\Database\Eloquent\JsonEncodingException;
use Illuminate\Http\Request;
use JavaScript;
use redirect;
use Yajra\DataTables\Facades\DataTables;
use function json_encode;

class IndicatorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\Response|\Illuminate\View\View|string
     */
    public function index(Request $request)
    {
        $indicatorGroupId = $request->input('indicatorGroupId');
        if ($request->ajax() === true) {

            $checked = $request->input('checked');

            if (!is_numeric($indicatorGroupId)) {
                $indicatorGroupId = '';
            }

            $query = DB::table('indicators')
                ->join('indicator_groups', 'indicator_groups.id', '=', 'indicators.indicator_group_id')
                ->select(
                    'indicators.id as indicator_id',
                    'indicators.name AS indicator_name',
                    'indicators.description AS indicator_description',
                    'indicators.updated_at as indicators_updated_at',
                    'indicators.deleted_at as indicators_deleted_at',
                    'indicators.indicator_group_id AS indicator_group_id',
                    'indicator_groups.type AS indicator_group_type')
                ->where('indicator_group_id', $indicatorGroupId);

            if ($checked === "0") {
                $query->where('indicators.deleted_at', null);
            }

            return DataTables::of($query)
                ->editColumn('indicators_updated_at', function ($item) {
                    return $item->indicators_updated_at ? with(new Carbon($item->indicators_updated_at)) : '';
                })->make(true);

        }

        $indicatorGroups = IndicatorGroup::all()->pluck('name', 'id');

        JavaScript::put([
            'cycles'                                                 => EvaluationCycle::orderBy('id')->pluck('name',
                'id')->toArray(),
            'indicatorGroupTypes'                                    => IndicatorGroup::getIndicatorGroupTypes(),
            'schoolGradeGroups'                                      => SchoolClass::getSchoolGradeGroups(),
            'permissions' => auth()->user()->getAllPermissions(),

            /*
             * Routes URL
             */
            'indicatorGroupsShowRoute'                               => route('frontend.ceap.indicatorGroups.show',
                0),
            'indicatorsCreateRoute'                                  => route('frontend.ceap.indicators.create'),
            'goalsCreateRoute'                                       => route('frontend.ceap.goals.create'),
            'indicatorRecordsIndexRoute'                             => route('frontend.ceap.indicators.indicatorRecords.index',
                0),
            'indicatorsIndexRoute'                                   => route('frontend.ceap.indicators.index'),
            'schoolClassesYearsRoute'                                => route('frontend.ceap.schoolClasses.years'),
            'schoolClassesDataTableRoute'                            => route('frontend.ceap.schoolClasses.dataTable'),
            'indicatorsRestoreRoute'                                 => route('frontend.ceap.indicators.restore'),

            /*
             *  IndicatorRecords CEAP
             */
            'indicatorGroupsGetCEAPModalDataRoute'                   => route('frontend.ceap.indicatorGroups.getCEAPModalData',
                0),
            'indicatorGroupsGetCEAPIndicatorRecordsDataTableRoute'   => route('frontend.ceap.indicatorGroups.getCEAPIndicatorRecordsDataTable',
                0),
            'indicatorGroupsCreateCEAPGroupRoute'                    => route('frontend.ceap.indicatorGroups.createGroupCEAP',
                0),
            'indicatorGroupsUpdateCEAPGroupRoute'                    => route('frontend.ceap.indicatorGroups.updateGroupCEAP',
                0),
            /*
             *  IndicatorRecords Rooms
             */
            'indicatorGroupsGetRoomsModalDataRoute'                  => route('frontend.ceap.indicatorGroups.getRoomsModalData',
                0),
            'indicatorGroupsGetRoomsIndicatorRecordsDataTableRoute'  => route('frontend.ceap.indicatorGroups.getRoomsIndicatorRecordsDataTable',
                0),
            'indicatorGroupsRoomsCreateGroupRoute'                   => route('frontend.ceap.indicatorGroups.createGroupRooms',
                0),
            'indicatorGroupsRoomsUpdateGroupRoute'                   => route('frontend.ceap.indicatorGroups.updateGroupRooms',
                0),
            /*
             *  IndicatorRecords School
             */
            'indicatorGroupsGetSchoolModalDataRoute'                 => route('frontend.ceap.indicatorGroups.getSchoolModalData',
                0),
            'indicatorGroupsGetSchoolIndicatorRecordsDataTableRoute' => route('frontend.ceap.indicatorGroups.getSchoolIndicatorRecordsDataTable',
                0),
            'indicatorGroupsSchoolCreateGroupRoute'                  => route('frontend.ceap.indicatorGroups.createGroupSchool',
                0),
            'indicatorGroupsSchoolUpdateGroupRoute'                  => route('frontend.ceap.indicatorGroups.updateGroupSchool',
                0),

            'indicatorsRecordsYearLabel'                     => removeCollon(trans('ceap.indicators.records.year.label')),
            'buttonCreateGraph'                              => removeCollon(trans('ceap.button.create_graph')),
            'buttonAddRegisterValue'                         => removeCollon(trans('ceap.button.add_register_value')),
            'buttonUpdateRegisterValue'                      => removeCollon(trans('ceap.button.update_register_value')),
            'buttonDefineGoals'                              => removeCollon(trans('ceap.button.define_goals')),
            'buttonRegisterHistory'                          => removeCollon(trans('ceap.button.register_history')),
            'indicatorsRecordsGradeLabel'                    => removeCollon(trans('ceap.indicators.records.grade.label')),
            'indicatorsRecordsRoomLabel'                     => removeCollon(trans('ceap.indicators.records.room.label')),
            'indicatorsRecordsValueLabel'                    => removeCollon(trans('ceap.indicators.records.value.label')),
            'actionIndicatorPlaceholder'                     => removeCollon(trans('ceap.actions.indicator.placeholder')),
            'indicatorsRecordsRecordDatePlaceholder'         => removeCollon(trans('ceap.indicators.records.recordDate.placeholder')),
            'indicatorsRecordsYearPlaceholder'               => removeCollon(trans('ceap.indicators.records.year.placeholder')),
            'actionsIndicatorPlaceholder'                    => removeCollon(trans('ceap.actions.indicator.placeholder')),
            'indicatorsModalCEAPSchoolGradeGroupPlaceholder' => removeCollon(trans('ceap.indicators.modal.ceap.school_grade_group.placeholder')),
            'indicatorRestoreLabel'                          => removeCollon(trans('ceap.button.restore')),
            '_token'                                         => csrf_token()
        ]);

        return view('frontend.ceap.indicators.index',
            compact([
                'indicatorGroups',
                'indicatorGroupId',
            ]));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param Request $request
     * @param IndicatorsByGroupDataTable $dataTable
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\Response|\Illuminate\View\View
     */
    public function create(Request $request, IndicatorsByGroupDataTable $dataTable)
    {

        $indicatorGroups = IndicatorGroup::orderBy('name')->pluck('name', 'id');
        $cycles = EvaluationCycle::orderBy('id')->pluck('name', 'id');
        $indicatorGroupId = session()->get('indicatorGroupId');
        if (!$indicatorGroupId) {
            $indicatorGroupId = $request->input('indicatorGroupId');
        }
        $indicatorGroupTypes = IndicatorGroup::getIndicatorGroupTypes();

        JavaScript::put([
            'cyclesMap'                       => EvaluationCycle::orderBy('id')->pluck('name', 'id')->toArray(),
            'indicatorGroupTypes'             => IndicatorGroup::getIndicatorGroupTypes(),
            'indicatorGroupTypesMap'          => IndicatorGroup::getIndicatorGroupTypes(),
            'schoolGradeGroups'               => SchoolClass::getSchoolGradeGroups(),
            'indicatorGroupType'              => SchoolClass::getSchoolGradeGroups(),
            'indicatorRecordSchoolGradeGroup' => IndicatorRecord::orderBy('id')->pluck('school_grade_group',
                'id')->unique(),
            /*
             * Routes URL
             */
            'indicatorGroupsCreateRoute'      => route('frontend.ceap.indicatorGroups.create'),
            'indicatorGroupsEditRoute'        => route('frontend.ceap.indicatorGroups.edit', 0),
            'indicatorGroupsDestroyRoute'     => route('frontend.ceap.indicatorGroups.destroy', 0),
            'indicatorGroupsShowRoute'        => route('frontend.ceap.indicatorGroups.show', 0),
            'indicatorGroupsUpdateRoute'      => route('frontend.ceap.indicatorGroups.update', 0),
            'indicatorGroupsStoreRoute'       => route('frontend.ceap.indicatorGroups.store', 0),
            'indicatorCreateRoute'            => route('frontend.ceap.indicators.create'),
            'indicatorEditRoute'              => route('frontend.ceap.indicators.edit', 0),
            'indicatorDestroyRoute'           => route('frontend.ceap.indicators.destroy', 0),
            'indicatorShowRoute'              => route('frontend.ceap.indicators.show', 0),
            'indicatorUpdateRoute'            => route('frontend.ceap.indicators.update', 0),
            'indicatorStoreRoute'             => route('frontend.ceap.indicators.store', 0),
            'indicatorGroupId'                => $indicatorGroupId,

        ]);
        session()->flash('indicatorGroupId', $indicatorGroupId);
        return $dataTable->render('frontend.ceap.indicators.create',
            compact([
                'indicatorGroups',
                'cycles',
                'indicatorGroupId',
                'indicatorGroupTypes',
            ]));
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        session()->flash('error_route', 'frontend.ceap.indicators.store');

        $this->validate($request, [
            'create_indicator_name'        => 'required|unique:indicators,name',
            'create_indicator_description' => 'required',
        ]);

        $request->session()->forget('errors');

        session()->forget('error_route');

        $indicatorGroupId = $request->indicatorGroupId;

        $indicator = new Indicator();
        $indicator->name = $request->create_indicator_name;
        $indicator->description = $request->create_indicator_description;
        $indicator->indicatorGroup()->associate($indicatorGroupId);
        $indicator->save();

        session()->flash('flash_success',
            trans('ceap.createSuccessMessage', ['item' => 'indicador', 'name' => $indicator->name]));
        session()->flash('indicatorGroupId', $indicatorGroupId);

        return redirect()->route('frontend.ceap.indicators.create');
    }

    /**
     * Display the specified resource.
     *
     * @param Indicator $indicator
     *
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\Response|\Illuminate\View\View|string
     */
    public function show(Request $request, Indicator $indicator)
    {
        if (request()->ajax() === true) {
            try {
                return $indicator->toJson();
            } catch (JsonEncodingException $e) {
                return json_encode([]);
            }
        }

        $indicator->load([
            'evaluationCategory' => function ($query) {
                $query->select('id', 'name')->get();
            },
            'indicatorRecords'   => function ($query) {
                $query->select('value', 'indicator_id')->get();
            },
        ]);

        $cycles = EvaluationCycle::orderBy('id')->pluck('name');
        $categories = EvaluationCategory::all();
        $indicatorGroupId = $indicator->indicatorGroup->id;

        $school_classes = SchoolClass::distinctYears()
            ->get()
            ->mapWithKeys(function ($item) {
                return ["Y" . "$item[year]" => $item['year']];
            });

        return view('frontend.ceap.indicators.show',
            compact(['cycles', 'categories', 'school_classes', 'years_select', 'indicator', 'indicatorGroupId']));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Request $request
     * @param Indicator $indicator
     *
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\Response|\Illuminate\View\View|string
     * @internal param int $id
     */
    public function edit(Request $request, Indicator $indicator)
    {

        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param Indicator $indicator
     * @return \Illuminate\Http\Response
     *
     */
    public function update(Request $request, Indicator $indicator)
    {
        session()->flash('error_route', 'frontend.ceap.indicators.update');
        if ($indicator->name === $request->edit_indicator_name) {
            $this->validate($request, [
                'edit_indicator_description' => 'required',
            ]);
        } else {
            $this->validate($request, [
                'edit_indicator_name'        => 'required|unique:indicators,name',
                'edit_indicator_description' => 'required',
            ]);
        }
        session()->forget('error_route');

        $indicatorGroupId = $request->indicatorGroupId;
        $indicator->name = $request->edit_indicator_name;
        $indicator->description = $request->edit_indicator_description;
        $indicator->save();

        session()->flash('flash_success',
            trans('ceap.editSuccessMessage', ['item' => 'indicador', 'name' => $indicator->name]));

	    if(request()->ajax() === true){
		    return collect([])->toJson();
	    }else{
		    return redirect()->route('frontend.ceap.indicators.create', compact('indicatorGroupId'));
	    }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Indicator $indicator
     *
     * @return \Illuminate\Http\Response
     * @internal param int $id
     */
    public function destroy(Indicator $indicator)
    {
        $indicator->delete();

        session()->flash('flash_success', 'Indicador removido com sucesso');

        $indicatorGroupId = $indicator->indicator_group_id;

	    if(request()->ajax() === true){
		    return collect([])->toJson();
	    }else{
		    return redirect()->route('frontend.ceap.indicators.create', compact('indicatorGroupId'));
	    }

    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function restore(Request $request)
    {
        $id = $request->get('indicator_id');
        $indicatorGroupId = $request->get('indicator_group_id');
        Indicator::withTrashed()
              ->where('id', $id)
              ->restore();

        session()->flash('flash_success', trans('ceap.actions.restore.text', ['item' => 'indicador']));

        return redirect()->route('frontend.ceap.indicators.index', compact('indicatorGroupId'));
    }

    public function showChart(Request $request)
    {

        $chartGenerator = null;

        $indicators = Indicator::with('indicatorRecords')->find($request->get('indicators', []));
        if (!$indicators) {
            redirect::back()->with('error', 'Indicadores não foram selecionados');
        }

        if ($request->indicatorGroupType === '1') {
            $chartGenerator = new CEAPChartGeneration();
        } elseif ($request->indicatorGroupType === '2') {
            $chartGenerator = new RoomsChartGeneration();
        } elseif ($request->indicatorGroupType === '3') {
            $chartGenerator = new SchoolChartGeneration();
        }
        return $chartGenerator->generateChartView($request);
    }
}
