<?php

namespace App\Http\Controllers\Frontend\CEAP;

use App\DataTables\ActionEvidencesDataTable;
use App\DataTables\Scopes\FilterByActionScope;
use App\Http\Controllers\Controller;
use App\Models\CEAP\Action;
use App\Models\CEAP\Evidence;
use Illuminate\Http\Request;
use Storage;
use function explode;

/**
 * Class ActionEvidenceController
 * @package App\Http\Controllers\Frontend\CEAP
 */
class ActionEvidenceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param ActionEvidencesDataTable $dataTable
     * @param Action $action
     * @return \Illuminate\Http\JsonResponse|\Illuminate\View\View
     */
    public function index(ActionEvidencesDataTable $dataTable, Action $action)
    {
        return $dataTable->addScope(new FilterByActionScope($action))->render('frontend.ceap.actions.evidences.index',
            ['action' => $action]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param Action $action
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Action $action)
    {
        $this->validate($request, [
            'title'     => 'required|unique:evidences,title',
            'details'   => 'required',
            'file_name' => 'max:20000',
        ]);

        $request->session()->forget('errors');

        $evidence = $action->evidences()->create(array_merge($request->input(), ['user_id' => \Auth::user()->id]));

        if ($request->file('file_name')) {
            $imageName = 'evidence_' . $evidence->id . '_' . $request->file('file_name')->getClientOriginalName();
            $evidence->file_name = $request->file('file_name')->storeAs('evidences', $imageName);
        }
        $evidence->save();
        return redirect()->route('frontend.ceap.actions.evidences.index', [$action->id, $evidence->id]);
    }

    /**
     * Display the specified resource.
     *
     * @param  Evidence $evidence
     * @return \Illuminate\Http\Response
     */
    public function show(Action $action, Evidence $evidence)
    {
        return view('frontend.ceap.actions.evidences.show', compact('action', 'evidence'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $evidence
     * @return \Illuminate\Http\Response
     */
    public function edit(Action $action, Evidence $evidence)
    {
        return view('frontend.ceap.actions.evidences.edit', compact('action', 'evidence'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Action $action, Evidence $evidence)
    {
        $this->validate($request, [
            'title'     => 'required',
            'details'   => 'required',
            'file_name' => 'max:20000',
        ]);

        $request->session()->forget('errors');

        if ($request->file('file_name')) {
            $newImageName = 'evidence_' . $evidence->id . '_' . $request->file('file_name')->getClientOriginalName();
            if ($evidence->file_name) {
                $existingImageName = explode('/', $evidence->file_name)[1];
                if ($existingImageName !== $newImageName) {
                    Storage::delete($evidence->file_name);
                    $evidence->file_name = $request->file('file_name')->storeAs('evidences', $newImageName);
                }
            }else{
                $evidence->file_name = $request->file('file_name')->storeAs('evidences', $newImageName);
            }
        }
        $evidence->update($request->input());
        $evidence->save();

        session()->flash('flash_success',
            trans('ceap.editSuccessMessage', ['item' => 'evidência', 'name' => $evidence->title]));

        return redirect()->route('frontend.ceap.actions.evidences.index', [$action->id, $evidence->id]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}