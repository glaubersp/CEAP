<?php

namespace App\Http\Controllers\Frontend\CEAP;

use App\Models\CEAP\Indicator;
use Charts;
use ConsoleTVs\Charts\Builder\Chart;
use ConsoleTVs\Charts\Builder\Multi;
use Illuminate\Http\Request;
use Illuminate\View\View;
use JavaScript;
use function collect;

/**
 * Interface ChartGenerationInterface
 */
interface ChartGenerationInterface
{
    /**
     * @param Request $request
     * @return View
     */
    public function generateChartView(Request $request): View;
}

/**
 * Class CEAPChartGeneration
 */
class CEAPChartGeneration implements ChartGenerationInterface
{
    /**
     * @param Request $request
     * @return View
     */
    public function generateChartView(Request $request): View
    {
        $indicators = Indicator::with('indicatorRecords')->find($request->get('indicators', []));
        $indicatorGroupId = $indicators[0]->indicator_group_id;

        // Find min and max values for YAxis
        $y_min_val = 100;
        $y_max_val = 0;
        foreach ($indicators as $indicator) {
            $period_name = 'elementary_school';
            $date_value_list = $indicator
                ->indicatorRecords()
                ->orderBy('value')
                ->where('school_grade_group', '=', $period_name)
                ->get();
            if ($date_value_list->isNotEmpty()) {
                $value = (float)$date_value_list->first()->value;
                if ($value < $y_min_val) {
                    $y_min_val = (float)$value;
                }
                $value = (float)$date_value_list->last()->value;
                if ($value > $y_max_val) {
                    $y_max_val = $value;
                }
            }

            $period_name = 'middle_school';
            $date_value_list = $indicator
                ->indicatorRecords()
                ->orderBy('value')
                ->where('school_grade_group', '=', $period_name)
                ->get();
            if ($date_value_list->isNotEmpty()) {
                $value = (float)$date_value_list->first()->value;
                if ($value < $y_min_val) {
                    $y_min_val = (float)$value;
                }
                $value = (float)$date_value_list->last()->value;
                if ($value > $y_max_val) {
                    $y_max_val = $value;
                }
            }

            $period_name = 'high_school';
            $date_value_list = $indicator
                ->indicatorRecords()
                ->orderBy('value')
                ->where('school_grade_group', '=', $period_name)
                ->get();
            if ($date_value_list->isNotEmpty()) {
                $value = (float)$date_value_list->first()->value;
                if ($value < $y_min_val) {
                    $y_min_val = (float)$value;
                }
                $value = (float)$date_value_list->last()->value;
                if ($value > $y_max_val) {
                    $y_max_val = $value;
                }
            }
        }

        // Create charts
        $charts = [];
        $charts_elementary = [];
        $charts_middle = [];
        $charts_high = [];
        foreach ($indicators as $indicator) {
            $chart = $this->createCEAPChart($indicator, 'elementary_school');
            if ($chart) {
                // Push chart to collection of charts to be shown
                $charts_elementary[] = $chart;
            }

            $chart = $this->createCEAPChart($indicator, 'middle_school');
            if ($chart) {
                // Push chart to collection of charts to be shown
                $charts_middle[] = $chart;
            }

            $chart = $this->createCEAPChart($indicator, 'high_school');
            if ($chart) {
                // Push chart to collection of charts to be shown
                $charts_high[] = $chart;
            }
        }

        $charts['elementary_school'] = $charts_elementary;
        $charts['middle_school'] = $charts_middle;
        $charts['high_school'] = $charts_high;

        JavaScript::put(['y_min_val' => $y_min_val, 'y_max_val' => $y_max_val]);

        return view('frontend.ceap.indicators.showChartCEAP', compact('charts', 'indicatorGroupId'));
    }

    /**
     * @param Indicator $indicator
     * @param $period_name
     * @return Chart|null
     */
    private function createCEAPChart(Indicator $indicator, $period_name): ?Chart
    {
        $chart = null;
        //Retira do indicator um array de [data , valor]
        $data_value_list = $indicator->indicatorRecords()->orderBy('date')
            ->where('school_grade_group', '=', $period_name)->get();

        if (!$data_value_list->isEmpty()) {

            // Don't create a chart when all elements are zeros
            $test_all_zeros = $data_value_list->reduce(function ($carry, $item) {
                return $carry && (0.0 === (float)$item->value);
            }, 1);
            if ($test_all_zeros) {
                return null;
            }

            //Prepares values
            $data_value_list = $data_value_list->map(function ($reg) {
                return [$reg->date, $reg->value];
            });

            //Creates chart
            $chart = Charts::create();

            //Add title to the chart
            $chart->title($indicator->name);

            // Add lables to the chart
            $chart->labels($data_value_list->map(function ($i, $k) {
                return $i[0];
            })->all());

            // Add values to the chart
            $chart->values($data_value_list->map(function ($i, $k) {
                return $i[1];
            })->all());

            // Add label to line
            $chart->elementLabel('Ciclo: ' . trans('ceap.indicators.records.school_grade_group.' . $period_name));
        }
        return $chart;
    }
}

/**
 * Class RoomsChartGeneration
 */
class RoomsChartGeneration implements ChartGenerationInterface
{
    /**
     * @param Request $request
     * @return View
     */
    public function generateChartView(Request $request): View
    {
        $indicators = Indicator::with('indicatorRecords')->find($request->get('indicators', []));
        $indicatorGroupId = $indicators[0]->indicator_group_id;

        $charts = [];
        foreach ($indicators as $indicator) {
            $chart = $this->createRoomsChart($indicator);
            if ($chart) {
                // Push chart to collection of charts to be shown
                $charts[] = $chart;
            }
        }

        return view('frontend.ceap.indicators.showChart', compact('charts', 'indicatorGroupId'));
    }

    /**
     * @param Indicator $indicator
     * @return Multi|null
     */
    private function createRoomsChart(Indicator $indicator): ?Multi
    {
        $chart = null;
        $data_value_list = $indicator->getRoomsIndicatorDataForGraphic()->get()->groupBy('year');

        if (!$data_value_list->isEmpty()) {

            //Prepares values
            $data = collect([]);
            foreach ($data_value_list as $year_key => $year_val) {
                $data->put($year_key, $year_val->mapWithKeys(function ($reg) {
                    return [$reg->grade . $reg->room => $reg->value];
                }));
            }

            //Creates chart
            $chart = Charts::multi();

            //Add title to the chart
            $chart->title($indicator->name);

            // Add lables to the chart
            $chart->labels($data->first()->keys());

            $chart->legend(true);
            $chart->elementLabel($indicator->name);

            // Add values to the chart
            foreach ($data as $k => $v) {
                $chart->dataset($k, $v->values());
            }
        }
        return $chart;
    }
}


/**
 * Class SchoolChartGeneration
 */
class SchoolChartGeneration implements ChartGenerationInterface
{
    /**
     * @param Request $request
     * @return View
     */
    public function generateChartView(Request $request): View
    {
        $indicators = Indicator::with('indicatorRecords')->find($request->get('indicators', []));
        $indicatorGroupId = $indicators[0]->indicator_group_id;

        // Find min and max values for YAxis
        $y_min_val = 1000;
        $y_max_val = 0;

        $charts = [];
        foreach ($indicators as $indicator) {
            $date_value_list = $indicator
                ->indicatorRecords()
                ->orderBy('value')
                ->get();
            if ($date_value_list->isNotEmpty()) {
                $value = (float)$date_value_list->first()->value;
                if ($value < $y_min_val) {
                    $y_min_val = (float)$value;
                }
                $value = (float)$date_value_list->last()->value;
                if ($value > $y_max_val) {
                    $y_max_val = $value;
                }
            }
            
            $chart = $this->createSchoolChart($indicator);
            if ($chart) {
                // Push chart to collection of charts to be shown
                $charts[] = $chart;
            }
        }

        JavaScript::put(['y_min_val' => $y_min_val, 'y_max_val' => $y_max_val]);

        return view('frontend.ceap.indicators.showChart', compact('charts', 'indicatorGroupId'));
    }

    /**
     * @param Indicator $indicator
     * @return Chart|null
     */
    private function createSchoolChart(Indicator $indicator): ?Chart
    {
        $chart = null;
        $data_value_list = $indicator->indicatorRecords()->orderBy('date')->get();

        if (!$data_value_list->isEmpty()) {

            //Prepares values
            $data = collect([]);
            foreach ($data_value_list as $record) {
                $data->put($record->date, $record->value);
            }

            //Creates chart
            $chart = Charts::create();

            //Add title to the chart
            $chart->title($indicator->name);

            // Add lables to the chart
            $chart->labels($data->keys());
            $chart->legend(true);
            $chart->elementLabel($indicator->name);

            // Add values to the chart
            $chart->values($data->values());
        }
        return $chart;
    }
}
