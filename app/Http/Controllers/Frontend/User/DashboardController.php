<?php

namespace App\Http\Controllers\Frontend\User;

use App\Http\Controllers\Controller;
use App\Models\CEAP\Action;
use App\Models\CEAP\Agreement;
use App\Models\CEAP\Indicator;
use App\Models\CEAP\Goal;

/**
 * Class DashboardController.
 */
class DashboardController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $indicators = Indicator::count();
        $goals = Goal::count();
        $actions = Action::count();
        $agreements = Agreement::count();

        return view('frontend.user.dashboard', compact("indicators", "goals", "actions", "agreements"));
    }
}
