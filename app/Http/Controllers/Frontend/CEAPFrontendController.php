<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;

/**
 * Class CEAPFrontendController
 * @package App\Http\Controllers\Frontend
 */
class CEAPFrontendController extends Controller
{

    /**
     * @return \Illuminate\Http\RedirectResponse
     */
    public function home()
    {
        if (auth()->guest()) {
            return redirect()->guest('login');
        }
        return redirect()->route('frontend.user.dashboard');
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function about()
    {
        return view('frontend.ceap.about');
    }
}
