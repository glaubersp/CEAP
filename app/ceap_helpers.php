<?php

if (!function_exists('removeCollon')) {
    /**
     * Remove ':' from label string
     * @param $string
     * @return mixed
     */
    function removeCollon($string)
    {
        return str_replace(':', '', $string);
    }
}
